﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace IhreEmailAlerts
{
	class Program
	{
		static bool ConsoleMode = false;
		static bool TestMode = false;
		static Guid IhreUserId = new Guid();

		static void Main(string[] args)
		{
			ConsoleMode = bool.Parse(ConfigurationManager.AppSettings["ConsoleMode"]);
			TestMode = bool.Parse(ConfigurationManager.AppSettings["TestMode"]);
			LogInfo("BEGIN IhreEmailAlerts");
			LogInfo("");

			SendDailyStatsEmail();

			LogInfo("END IhreEmailAlerts");
			if (ConsoleMode)
			{
				LogInfo("Press any key...");
				Console.ReadKey();
			}
		}


		static string GetIhreUserEmails(out ErrorObj Error)
		{
			List<string> UserEmails = new List<string>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_UsersSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							if ((bool)rdr["IsApproved"])
							{
								UserEmails.Add(rdr["Email"].ToString());
							}
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}

			return string.Join(",", UserEmails);
		}

		static void SendDailyStatsEmail()
		{
			LogInfo("BEGIN SendDailyStatsEmail");
			
			ErrorObj Error = null;
			string emailRecipients = GetIhreUserEmails(out Error);
			if (Error != null)
			{
				LogMessage(Error.ErrorMessage);
				return;
			}


			try
			{
				#region Generate HTML
				string TotalAffiliates = "";
				string ActiveAffiliates = "";


				string Breakdown_PendingAffiliate = "";
				string Breakdown_PendingOperator = "";
				string Breakdown_ConfirmedActive = "";
				string Breakdown_ConfirmedInactive = "";
				string Breakdown_ExistingActive = "";
				string Breakdown_ExistingInactive = "";
				string Breakdown_Existing = "";
				string Breakdown_Rejected = "";
				string Breakdown_AwaitingResponse = "";

				SqlCommand breakdown_cmd = new SqlCommand("GetDealsBreakdown", new SqlConnection(ConfigurationManager.ConnectionStrings["IhreDbConnectionString"].ConnectionString));
				breakdown_cmd.CommandType = System.Data.CommandType.StoredProcedure;
				breakdown_cmd.Connection.Open();
				SqlDataReader breakdown_rdr = breakdown_cmd.ExecuteReader();

				breakdown_rdr.Read();
				TotalAffiliates = breakdown_rdr["total_affiliates"].ToString();

				breakdown_rdr.NextResult();
				breakdown_rdr.Read();
				ActiveAffiliates = breakdown_rdr["active_affiliates"].ToString();

				breakdown_rdr.NextResult();

				while (breakdown_rdr.Read())
				{
					switch (breakdown_rdr["status"].ToString())
					{
						case "8": Breakdown_PendingAffiliate = breakdown_rdr["count"].ToString(); break;
						case "7": Breakdown_PendingOperator = breakdown_rdr["count"].ToString(); break;
						case "3": Breakdown_ConfirmedActive = breakdown_rdr["count"].ToString(); break;
						case "6": Breakdown_ConfirmedInactive = breakdown_rdr["count"].ToString(); break;
						case "10": Breakdown_ExistingActive = breakdown_rdr["count"].ToString(); break;
						case "11": Breakdown_ExistingInactive = breakdown_rdr["count"].ToString(); break;
						case "5": Breakdown_Existing = breakdown_rdr["count"].ToString(); break;
						case "4": Breakdown_Rejected = breakdown_rdr["count"].ToString(); break;
						case "2": Breakdown_AwaitingResponse = breakdown_rdr["count"].ToString(); break;
					}
				}
				breakdown_cmd.Connection.Close();
				breakdown_cmd.Dispose();




				DateTime from = DateTime.Now.AddDays(-1);
				DateTime to = DateTime.Now.AddDays(1);
				string fromStr = from.ToShortDateString();
				string Deals_PendingAffiliate = "";
				string Deals_PendingOperator = "";
				string Deals_ConfirmedActive = "";
				string Deals_ConfirmedInactive = "";
				string Deals_ExistingActive = "";
				string Deals_ExistingDealsInactive = "";
				SqlCommand cmd = new SqlCommand("UserDealsReport", new SqlConnection(ConfigurationManager.ConnectionStrings["IhreDbConnectionString"].ConnectionString));
				cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("UserId", null);
				cmd.Parameters.AddWithValue("from", from);
				cmd.Parameters.AddWithValue("to", to);
				cmd.Connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					Deals_PendingAffiliate += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_name"].ToString(),
						rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}

				rdr.NextResult();
				while (rdr.Read())
				{
					Deals_PendingOperator += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_name"].ToString(),
						rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}

				rdr.NextResult();
				while (rdr.Read())
				{
					Deals_ConfirmedActive += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_name"].ToString(),
						rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}

				rdr.NextResult();
				while (rdr.Read())
				{
					Deals_ConfirmedInactive += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_name"].ToString(),
						rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}

				rdr.NextResult();
				while (rdr.Read())
				{
					Deals_ExistingActive += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_name"].ToString(),
						rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}

				rdr.NextResult();
				while (rdr.Read())
				{
					Deals_ExistingDealsInactive += string.Format(
						"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
						rdr["client_id"].ToString() + "'>" + rdr["client_name"].ToString(),
						rdr["affiliate_id"].ToString() + "'>" + rdr["affiliate_name"].ToString(),
						DateTime.Parse(rdr["created"].ToString()).ToShortDateString(),
						rdr["UserName"].ToString()
					);
				}


				rdr.Dispose();
				cmd.Connection.Close();
				cmd.Dispose();



				string StatsBody = string.Format(@"
<h1>Daily Stats</h1>

<h2>Database Info</h2>
<p>Total Affiliates: <strong>{0}</strong></p>
<p>Active Affiliates: <strong>{1}</strong></p>

<h2>Deals Breakdown</h2>
<p>Pending - Affiliate: <strong>{2}</strong></p>
<p>Pending - Operator: <strong>{3}</strong></p>
<p>Confirmed - Active: <strong>{4}</strong></p>
<p>Confirmed - Inactive: <strong>{5}</strong></p>
<p>Existing - Active: <strong>{6}</strong></p>
<p>Existing - Inactive: <strong>{7}</strong></p>
<p>Existing: <strong>{8}</strong></p>
<p>Rejected: <strong>{9}</strong></p>
<p>Awaiting Response: <strong>{10}</strong></p>

<h2>Affiliate Deals From {11}</h2>
<h3>Pending - Affiliate</h3>
<table>{12}{13}</table>
<h3>Pending - Operator</h3>
<table>{12}{14}</table>
<h3>Confirmed - Active</h3>
<table>{12}{15}</table>
<h3>Confirmed - Inactive</h3>
<table>{12}{16}</table>
<h3>Existing - Active</h3>
<table>{12}{17}</table>
<h3>Existing - Inactive</h3>
<table>{12}{18}</table>
				",
					/* 0 */	TotalAffiliates,
					/* 1 */	ActiveAffiliates,

					/* 2 */	Breakdown_PendingAffiliate,
					/* 3 */	Breakdown_PendingOperator,
					/* 4 */	Breakdown_ConfirmedActive,
					/* 5 */	Breakdown_ConfirmedInactive,
					/* 6 */	Breakdown_ExistingActive,
					/* 7 */	Breakdown_ExistingInactive,
					/* 8 */	Breakdown_Existing,
					/* 9 */	Breakdown_Rejected,
					/* 10 */	Breakdown_AwaitingResponse,

					/* 11 */	fromStr,
					/* 12 */	"<tr><th>Client</th><th>Affiliate</th><th>Created</th><th>Account Manager</th></tr>",

					/* 13 */	Deals_PendingAffiliate,
					/* 14 */	Deals_PendingOperator,
					/* 15 */	Deals_ConfirmedActive,
					/* 16 */	Deals_ConfirmedInactive,
					/* 17 */	Deals_ExistingActive,
					/* 18 */	Deals_ExistingDealsInactive
				);
				#endregion Generate HTML


				LogInfo(string.Format("Sending stats email to: {0}", emailRecipients));

				SmtpClient smtp;
				smtp = new SmtpClient("smtp.mse2010.com");
				smtp.UseDefaultCredentials = false;
				smtp.Credentials = new NetworkCredential("partners@ihreconsulting.com", "ihre2012");
	
				smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
				MailMessage mm = new MailMessage("partners@ihreconsulting.com", emailRecipients);
				mm.Subject = string.Format("Daily Report");
				mm.IsBodyHtml = true;
				mm.Body = StatsBody;
				smtp.Send(mm);

				mm.Dispose();
				LogInfo("Daily Stats Email sent");
			}
			catch (Exception ex)
			{
				LogError(ex);
			}

			LogInfo("END SendDailyStatsEmail");
		}

	


		private static void Dot(int c)
		{
			if (ConsoleMode)
			{
				if (c % 100 == 0)
				{
					Console.WriteLine(c);
				}
				Console.Write(".");
			}
		}
		private static void LogInfo(string message)
		{
			if (ConsoleMode)
			{
				Console.WriteLine(message);
			}
		}
		private static void LogError(Exception ex)
		{
			LogMessage(ex.ToString());
		}
		private static void LogMessage(string message)
		{
			try
			{
				DateTime now = DateTime.Now;
				string fileName = string.Format(
					@"C:\Logs\ihrealerts_{0}{1}{2}{3}{4}{5}{6}.log",
					now.Year.ToString("d4"),
					now.Month.ToString("d2"),
					now.Day.ToString("d2"),
					now.Hour.ToString("d2"),
					now.Minute.ToString("d2"),
					now.Second.ToString("d2"),
					now.Millisecond.ToString()
				);

				string[] messages = { message };
				LogMessage(messages);
			}
			catch { }
		}
		private static void LogMessage(string[] messages)
		{
			try
			{
				DateTime now = DateTime.Now;
				string fileName = string.Format(
					@"C:\Logs\ihrealerts_{0}{1}{2}{3}{4}{5}{6}.log",
					now.Year.ToString("d4"),
					now.Month.ToString("d2"),
					now.Day.ToString("d2"),
					now.Hour.ToString("d2"),
					now.Minute.ToString("d2"),
					now.Second.ToString("d2"),
					now.Millisecond.ToString()
				);
				File.WriteAllLines(fileName, messages);
				if (ConsoleMode)
				{
					foreach (string message in messages)
					{
						Console.WriteLine(message);
					}
				}

			}
			catch { }

		}

	}

	public class OperatorDetail
	{
		public string OperatorName { get; set; }
		public string SignupUrl { get; set; }
		public OperatorDetail(string OperatorName, string SignupUrl)
		{
			this.OperatorName = OperatorName;
			this.SignupUrl = SignupUrl;
		}
	}
	public class DealDetail
	{
		public int OperatorId { get; set; }
		public OperatorDetail OperatorDetail { get; set; }
		public string DealInfo { get; set; }
		public DealDetail(int OperatorId, OperatorDetail OperatorDetail)
		{
			this.OperatorId = OperatorId;
			this.OperatorDetail = OperatorDetail;
			this.DealInfo = null;
		}
		public DealDetail(int OperatorId, OperatorDetail OperatorDetail, string DealInfo)
		{
			this.OperatorId = OperatorId;
			this.OperatorDetail = OperatorDetail;
			this.DealInfo = DealInfo;
		}
	}
	public class AffiliateEmailDetail
	{
		public int affiliateId { get; set; }
		public string affiliateName { get; set; }
		public string ContactName { get; set; }
		public string ManagerUserId { get; set; }
		public string ManagerEmail { get; set; }
		public string affiliateEmail { get; set; }
		public List<DealDetail> ConfirmedActiveDeals { get; set; }
		public List<DealDetail> PendingAffiliateDeals { get; set; }
		public List<DealDetail> SuggestedDeals { get; set; }
		public AffiliateEmailDetail(int affiliateId, string affiliateName, string ContactName, string ManagerUserId, string ManagerEmail, string affiliateEmail)
		{
			this.affiliateId = affiliateId;
			this.affiliateName = affiliateName;
			this.ContactName = ContactName;
			this.ManagerUserId = ManagerUserId;
			this.ManagerEmail = ManagerEmail;
			this.affiliateEmail = affiliateEmail;
			this.ConfirmedActiveDeals = new List<DealDetail>();
			this.PendingAffiliateDeals = new List<DealDetail>();
			this.SuggestedDeals = new List<DealDetail>();
		}
	}


}