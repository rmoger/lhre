﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IhreEmailAlerts
{
	public class DbCommand : IDisposable
	{
		private ErrorObj _Error = new ErrorObj();
		public ErrorObj CheckErrors()
		{
			if (this._Error.ErrorCode == 0)
			{
				this._Error.ErrorCode = int.Parse(this.ReturnParam.Value.ToString());
				string ErrorInfo = this.ErrorInfoParam.Value.ToString();
				switch (this._Error.ErrorCode)
				{
					case 401: this._Error.ErrorMessage = "Unauthorised"; break;
					case 404: this._Error.ErrorMessage = "Not found"; break;
					case 7: this._Error.ErrorMessage = "Email address is taken"; break;
					case 69: this._Error.ErrorMessage = "A record with this name already exists"; break;
					case 96: this._Error.ErrorMessage = ErrorInfo; break;
					case 9999999: this._Error.ErrorMessage = string.Format("This report contains {0} records.", ErrorInfo); break;
					case 0:
					default: this._Error = null; break;
				}
			}
			return this._Error;
		}

		private SqlCommand cmd;
		public SqlCommand Command { get { return this.cmd; } }
		private SqlParameter ResultCountParam;
		private SqlParameter ErrorInfoParam;
		private SqlParameter ReturnParam;
		private SqlParameter NewIdParam;
		public int ResultCount
		{
			get
			{
				try
				{
					return int.Parse(this.ResultCountParam.Value.ToString());
				}
				catch
				{
					return 0;
				}
			}
		}

		public int NewId
		{
			get
			{
				try
				{
					return int.Parse(this.NewIdParam.Value.ToString());
				}
				catch
				{
					return 0;
				}
			}
		}


		public DbCommand(Guid ManagerUserId, string CommandText)
		{
			this.Init(ManagerUserId, CommandText, false, false);
		}
		public DbCommand(Guid ManagerUserId, string CommandText, bool IncludeResultCount)
		{
			this.Init(ManagerUserId, CommandText, IncludeResultCount, false);
		}
		public DbCommand(Guid ManagerUserId, string CommandText, bool IncludeResultCount, bool IncludeNewIdOutput)
		{
			this.Init(ManagerUserId, CommandText, IncludeResultCount, IncludeNewIdOutput);
		}

		private void Init(Guid ManagerUserId, string CommandText, bool IncludeResultCount, bool IncludeNewIdOutput)
		{
			this.cmd = new SqlCommand();
			this.cmd.CommandText = CommandText;
			cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["IhreDbConnectionString"].ConnectionString);
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("CurrentUserId", ManagerUserId);

			this.ErrorInfoParam = new SqlParameter("error_info", System.Data.SqlDbType.VarChar);
			this.ErrorInfoParam.Size = 256;
			this.ErrorInfoParam.Direction = System.Data.ParameterDirection.Output;
			cmd.Parameters.Add(this.ErrorInfoParam);

			if (IncludeResultCount)
			{
				this.ResultCountParam = new SqlParameter("result_count", 0);
				this.ResultCountParam.Direction = System.Data.ParameterDirection.Output;
				cmd.Parameters.Add(this.ResultCountParam);
			}
			if (IncludeNewIdOutput)
			{
				this.NewIdParam = new SqlParameter("new_id", 0);
				this.NewIdParam.Direction = System.Data.ParameterDirection.Output;
				cmd.Parameters.Add(this.NewIdParam);
			}
			this.ReturnParam = new SqlParameter("retval", null);
			this.ReturnParam.Direction = System.Data.ParameterDirection.ReturnValue;
			cmd.Parameters.Add(this.ReturnParam);
		}

		public void AddParamWithValue(string ParamName, object ParamValue)
		{
			this.cmd.Parameters.AddWithValue(ParamName, ParamValue);
		}

		public string ParamsToString()
		{
			string RetVal = "";
			foreach (SqlParameter param in cmd.Parameters)
			{
				if (param.Value != null)
				{
					RetVal += param.ParameterName + ": " + param.Value.ToString() + "\r\n";
				}
			}
			return RetVal;
		}

		public void OpenConnection()
		{
			this.cmd.Connection.Open();
		}
		public int ExecuteNonQuery()
		{
			int RetVal = 0;
			try
			{
				RetVal = this.cmd.ExecuteNonQuery();
				//				this.CheckErrors();
			}
			catch (SqlException ex)
			{
				this._Error.ErrorCode = ex.ErrorCode;
				this._Error = new ErrorObj(ex.ToString());
				RetVal = -1;
			}
			return RetVal;
		}
		public object ExecuteScalar()
		{
			object RetVal = null;
			try
			{
				RetVal = this.cmd.ExecuteScalar();
				//		this.CheckErrors();
			}
			catch (SqlException ex)
			{
				this._Error.ErrorCode = ex.ErrorCode;
				this._Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}
		public SqlDataReader ExecuteReader()
		{
			SqlDataReader RetVal = null;
			try
			{
				RetVal = this.cmd.ExecuteReader();
			}
			catch (SqlException ex)
			{
				this._Error.ErrorCode = ex.ErrorCode;
				this._Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}
		public void Dispose()
		{
			this.cmd.Connection.Dispose();
			this.cmd.Dispose();
		}
	}


	public class ErrorObj
	{
		public string ErrorMessage { get; set; }
		public int ErrorCode { get; set; }
		public ErrorObj()
		{
			this.ErrorMessage = null;
			this.ErrorCode = 0;
		}
		public ErrorObj(string ErrorMessage)
		{
			this.ErrorMessage = ErrorMessage;
		}
		public ErrorObj(string ErrorMessage, int ErrorCode)
		{
			this.ErrorMessage = ErrorMessage;
			this.ErrorCode = ErrorCode;
		}
	}
}
