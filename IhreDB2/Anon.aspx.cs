﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IhreDB2
{
	public partial class Anon : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{
		
		}
		protected void ForgottenPasswordButton_Click(object sender, EventArgs e)
		{
			Login1.Visible = false;
			PasswordRecovery1.Visible = true;
		}
		protected void CancelForgottenPasswordButton_Click(object sender, EventArgs e)
		{
			Login1.Visible = true;
			PasswordRecovery1.Visible = false;
		}

		protected void ForgottenPasswordContinueButton_Click(object sender, EventArgs e)
		{
			Login1.Visible = true;
			PasswordRecovery1.Visible = false;
		}
		protected void PasswordRecovery1_SendingMail(object sender, MailMessageEventArgs e)
		{
			e.Message.Subject = "db.ihreconsulting.com Password Recovery";
			SmtpClient sc = new SmtpClient();
			//sc.EnableSsl = true;
			sc.Send(e.Message);
			e.Cancel = true;
		}

		protected void Login1_LoggedIn(object sender, EventArgs e)
		{
			if (Request["ReturnUrl"] != null)
			{
				string ReturnUrl = "/";
				string[] returnUrlArr = Request["ReturnUrl"].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
				for (int c=0; c < returnUrlArr.Length; c++)
				{
					if (c < 3)
					{
						ReturnUrl += returnUrlArr[c] + "/";
					}
				}
				Response.Redirect(ReturnUrl);
			}
			else
			{
				Response.Redirect("/");
			}
		}
	}
}