﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IhreDB2
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				string InitSection = "'dashboard'";
				string InitParam = "null";
				if (Request["section"] != null && Request["section"] != "")
				{
					InitSection = "'" + Request["section"] + "'";
				}
				if (Request["param"] != null && Request["param"] != "")
				{
					InitParam = "'" + Request["param"] + "'";
				}
				serverJsVars.Text = string.Format(
					"<script type=\"text/javascript\">{0}{1}{2}</script>",
					"var debugmode = " + (Boolean.Parse(ConfigurationManager.AppSettings["debugmodeon"]) ? "true" : "false") + ";",
					"var initialsection = " + InitSection + ";",
					"var initialparam = " + InitParam + ";"
				);
			}

		}
	}
}