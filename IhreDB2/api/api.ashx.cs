﻿using IhreDB2.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace IhreDB2.api
{
	public class api : IHttpHandler
	{
		private HttpContext _context;
		JsonRequest RequestData;
		JsonResponse ResponseData;
		MembershipUser CurrentUser = null;
		Guid CurrentUserId;

		public void ProcessRequest(HttpContext context)
		{
			this._context = context;
			this.RequestData = new JsonRequest(context.Request["data"]);
			this.ResponseData = new JsonResponse();
			this.CurrentUser = Membership.GetUser();
			if (this.CurrentUser == null)
			{
				this.ResponseData.ReturnError("Not logged in", 1);
			}
			else
			{
				this.CurrentUserId = Guid.Parse(this.CurrentUser.ProviderUserKey.ToString());
				foreach (string action in this.RequestData.GetValueString("action").Split('_'))
				{
					switch (action)
					{
						case "getdbsummary": GetDbSummary(); break;
						case "getdashboardpendingdeals": GetDashboardPendingDeals(); break;
						case "getdashboardconfirmedactivedeals": GetDashboardConfirmedActiveDeals(); break;
						case "getdashboardconfirmedinactivedeals": GetDashboardConfirmedInactiveDeals(); break;
						case "getdashboardawaitingresponsedeals": GetDashboardAwaitingResponseDeals(); break;
						case "getfavouriteoperators": GetFavouriteOperators(); break;
						case "getlatestoperators": GetLatestOperators(); break;
						case "getuserstats": GetUserStats(); break;

						case "getusers": GetIhreUsers(false, false); break;
						case "getuserslite": GetIhreUsers(true, false); break;
						case "getusersliteselcur": GetIhreUsers(true, true); break;
						case "activateuser": ActivateUser(); break;
						case "deactivateuser": DeactivateUser(); break;
						case "unlockuser": UnlockUser(); break;
						case "getcurrentuserroles": GetCurrentUserRoles(); break;
						case "getuserroles": GetIhreUserRoles(); break;
						case "updateuserroles": UpdateUserRoles(); break;
						case "getroles": GetIhreRoles(); break;
						case "newuser": NewUser(); break;
						case "changepassword": ChangePassword(); break;

						case "getaffiliates": GetAffiliates(); break;
						case "reassignaffiliates": ReassignAffiliates(); break;

						case "getaffiliate": GetAffiliate(); break;
						case "newaffiliate": AddAffiliate(); break;
						case "removeaffiliate": RemoveAffiliate(); break;
						case "getaffiliateoperatorsuggestions": GetAffiliateOperatorSuggestions(); break;

						case "updateaffiliateprofile": UpdateAffiliateProfile(); break;
						case "updateaffiliatecontactdetails": UpdateAffiliateContactDetails(false); break;
						case "addaffiliatecontactdetail": UpdateAffiliateContactDetails(true); break;
						case "removeaffiliatecontactdetail": RemoveAffiliateContactDetail(); break;
						case "updateaffiliateurls": UpdateAffiliateURLs(false); break;
						case "addaffiliateurl": UpdateAffiliateURLs(true); break;
						case "removeaffiliateurl": RemoveAffiliateURL(); break;
						case "updateaffiliatecountries": UpdateAffiliateCountries(); break;
						case "updateaffiliateaffiliatetypes": UpdateAffiliateAffiliateTypes(); break;
						case "updateaffiliatedealtypes": UpdateAffiliateDealTypes(); break;
						case "updateaffiliatemarketingtypes": UpdateAffiliateMarketingTypes(); break;

						case "getaffiliatenotes": GetAffiliateNotes(); break;
						case "addaffiliatenote": AddAffiliateNote(); break;
						case "updateaffiliatenote": UpdateAffiliateNote(); break;
						case "removeaffiliatenote": RemoveAffiliateNote(); break;

						case "getaffiliatedeals": GetAffiliateDeals(); break;
						case "getoperatordeals": GetOperatorDeals(); break;
						case "updatedeal": UpdateDeal(); break;
						case "adddeal": AddDeal(); break;


						case "getoperators": GetOperators(false); break;
						case "getoperatorslite": GetOperators(true); break;
						case "getoperator": GetOperator(); break;
						case "newoperator": AddOperator(); break;

						case "updateoperatorfavourite": UpdateOperatorFavourite(); break;
						case "updateoperatorprofile": UpdateOperatorProfile(); break;
						case "updateoperatorcontactdetails": UpdateOperatorContactDetails(false); break;
						case "removeoperatorcontactdetail": RemoveOperatorContactDetail(); break;
						case "addoperatorcontactdetail": UpdateOperatorContactDetails(true); break;
						case "updateoperatorlanguages": UpdateOperatorLanguages(); break;
						case "updateoperatoraffiliatetypes": UpdateOperatorAffiliateTypes(); break;
						case "updateoperatordealtypes": UpdateOperatorDealTypes(); break;
						case "updateoperatorsoftwaretypes": UpdateOperatorSoftwareTypes(); break;
						case "updateoperatorlicencetypes": UpdateOperatorLicenceTypes(); break;
						case "updateoperatorpaymenttypes": UpdateOperatorPaymentTypes(); break;
						case "updateoperatorbrands": UpdateOperatorBrands(false); break;
						case "addoperatorbrand": UpdateOperatorBrands(true); break;
						case "updateoperatorcountries": UpdateOperatorCountries(); break;



						case "getoperatornotes": GetOperatorNotes(); break;
						case "addoperatornote": AddOperatorNote(); break;
						case "updateoperatornote": UpdateOperatorNote(); break;
						case "removeoperatornote": RemoveOperatorNote(); break;




						case "getaffiliatetypes": GetAffiliateTypes(); break;
						case "addaffiliatetype": AddAffiliateType(); break;
						case "updateaffiliatetype": UpdateAffiliateType(); break;

						case "getcontactdetailtypes": GetContactDetailTypes(); break;
						case "addcontactdetailtype": AddContactDetailType(); break;
						case "updatecontactdetailtype": UpdateContactDetailType(); break;

						case "getcountries": GetCountries(); break;
						case "addcountry": AddCountry(); break;
						case "updatecountry": UpdateCountry(); break;

						case "getdealtypes": GetDealTypes(); break;
						case "adddealtype": AddDealType(); break;
						case "updatedealtype": UpdateDealType(); break;

						case "getdealinfos": GetDealInfos(); break;
						case "adddealinfo": AddDealInfo(); break;
						case "updatedealinfo": UpdateDealInfo(); break;

						case "getmarketingtypes": GetMarketingTypes(); break;
						case "addmarketingtype": AddMarketingType(); break;
						case "updatemarketingtype": UpdateMarketingType(); break;

						case "getsoftwaretypes": GetSoftwareTypes(); break;
						case "addsoftwaretype": AddSoftwareType(); break;
						case "updatesoftwaretype": UpdateSoftwareType(); break;

						case "getlicencetypes": GetLicenceTypes(); break;
						case "addlicencetype": AddLicenceType(); break;
						case "updatelicencetype": UpdateLicenceType(); break;

						case "getlanguages": GetLanguages(); break;
						case "addlanguage": AddLanguage(); break;
						case "updatelanguage": UpdateLanguage(); break;

						case "getpaymenttypes": GetPaymentTypes(); break;
						case "addpaymenttype": AddPaymentType(); break;
						case "updatepaymenttype": UpdatePaymentType(); break;

						case "getdealstatuses": GetDealStatuses(); break;

						case "getcustomreports": GetCustomReports(); break;
						case "removecustomreport":RemoveCustomReport();break;
						case "getcustomreportinfo": GetCustomReportInfo(); break;
						case "newcustomreport": AddCustomReport(); break;

						case "getreportaffiliates": GetReportAffiliates(); break;
						case "getreportoperators": GetReportOperators(); break;
						case "getreportdeals": GetReportDeals(); break;


						case "getoperatordealssummary": GetOperatorDealsSummary(); break;
						case "getuserdealssummary": GetUserDealsSummary(); break;
					}
				}
			}
			context.Response.ContentType = "application/json";
			context.Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
			context.Response.Write(this.ResponseData.WebResponse());
		}

		#region Dashboard
		private List<Operator> GetFavouriteOperators()
		{
			List<Operator> RetVal = new List<Operator>();
			try
			{
				ErrorObj Error = new ErrorObj();
				RetVal = OperatorHelper.GetFavouriteOperators(
					this.CurrentUserId,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("FavouriteOperators", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<Operator> GetLatestOperators()
		{
			List<Operator> RetVal = new List<Operator>();
			try
			{
				ErrorObj Error = new ErrorObj();
				RetVal = OperatorHelper.GetLatestOperators(
					this.CurrentUserId,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("LatestOperators", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private DbSummary GetDbSummary()
		{
			DbSummary RetVal = new DbSummary();
			try
			{
				ErrorObj Error = null;
				RetVal = ReportHelper.GetDbSummary(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("DbSummary", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private ChartData GetUserStats()
		{
			ChartData RetVal = new ChartData();
			try
			{
				ErrorObj Error = null;
				RetVal = IhreUserHelper.GetUserStats(this.CurrentUserId, this.RequestData.GetValueDateTime("dashboarduserstatsfrom"), this.RequestData.GetValueDateTime("dashboarduserstatsto"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("UserStats", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void GetDashboardPendingDeals()
		{
			try
			{
				List<Dictionary<string, object>> PendingDeals = new List<Dictionary<string, object>>();
				ErrorObj Error = null;
				int ResultCount = 0;
				switch (this.RequestData.GetValueString("dashboardpendingdealsgroupby"))
				{
					case "affiliate": PendingDeals = ReportHelper.GetAffiliateDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardpendingdealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "7,8", null, this.RequestData.GetValueDateTime("dashboardpendingdealsfrom"), this.RequestData.GetValueDateTime("dashboardpendingdealsto"), this.RequestData.GetValueInt("dashboardpendingdealsoffset"), this.RequestData.GetValueInt("dashboardpendingdealscount"), out ResultCount, out Error); break;
					case "operator": PendingDeals = ReportHelper.GetOperatorDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardpendingdealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "7,8", null, this.RequestData.GetValueDateTime("dashboardpendingdealsfrom"), this.RequestData.GetValueDateTime("dashboardpendingdealsto"), this.RequestData.GetValueInt("dashboardpendingdealsoffset"), this.RequestData.GetValueInt("dashboardpendingdealscount"), out ResultCount, out Error); break;
					case "none": PendingDeals = ReportHelper.GetDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardpendingdealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "7,8", null, this.RequestData.GetValueDateTime("dashboardpendingdealsfrom"), this.RequestData.GetValueDateTime("dashboardpendingdealsto"), this.RequestData.GetValueInt("dashboardpendingdealsoffset"), this.RequestData.GetValueInt("dashboardpendingdealscount"), out ResultCount, out Error); break;
				}
				this.ResponseData.AddData("PendingDeals", PendingDeals);
				this.ResponseData.AddData("PendingDealsCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void GetDashboardConfirmedActiveDeals()
		{
			try
			{
				List<Dictionary<string, object>> ConfirmedActiveDeals = new List<Dictionary<string, object>>();
				ErrorObj Error = null;
				int ResultCount = 0;
				switch (this.RequestData.GetValueString("dashboardconfirmedactivedealsgroupby"))
				{
					case "affiliate": ConfirmedActiveDeals = ReportHelper.GetAffiliateDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "3", null, this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedactivedealscount"), out ResultCount, out Error); break;
					case "operator": ConfirmedActiveDeals = ReportHelper.GetOperatorDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "3", null, this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedactivedealscount"), out ResultCount, out Error); break;
					case "none": ConfirmedActiveDeals = ReportHelper.GetDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "3", null, this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedactivedealscount"), out ResultCount, out Error); break;
				}
				this.ResponseData.AddData("ConfirmedActiveDeals", ConfirmedActiveDeals);
				this.ResponseData.AddData("ConfirmedActiveDealsCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void GetDashboardConfirmedInactiveDeals()
		{
			try
			{
				List<Dictionary<string, object>> ConfirmedInactiveDeals = new List<Dictionary<string, object>>();
				ErrorObj Error = null;
				int ResultCount = 0;
				switch (this.RequestData.GetValueString("dashboardconfirmedinactivedealsgroupby"))
				{
					case "affiliate": ConfirmedInactiveDeals = ReportHelper.GetAffiliateDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedinactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "6", null, this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealscount"), out ResultCount, out Error); break;
					case "operator": ConfirmedInactiveDeals = ReportHelper.GetOperatorDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedinactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "6", null, this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealscount"), out ResultCount, out Error); break;
					case "none": ConfirmedInactiveDeals = ReportHelper.GetDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardconfirmedinactivedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "6", null, this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsfrom"), this.RequestData.GetValueDateTime("dashboardconfirmedinactivedealsto"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealsoffset"), this.RequestData.GetValueInt("dashboardconfirmedinactivedealscount"), out ResultCount, out Error); break;
				}
				this.ResponseData.AddData("ConfirmedInactiveDeals", ConfirmedInactiveDeals);
				this.ResponseData.AddData("ConfirmedInactiveDealsCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void GetDashboardAwaitingResponseDeals()
		{
			try
			{
				List<Dictionary<string, object>> AwaitingResponseDeals = new List<Dictionary<string, object>>();
				ErrorObj Error = null;
				int ResultCount = 0;
				switch (this.RequestData.GetValueString("dashboardawaitingresponsedealsgroupby"))
				{
					case "affiliate": AwaitingResponseDeals = ReportHelper.GetAffiliateDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardawaitingresponsedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "2", null, this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsfrom"), this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsto"), this.RequestData.GetValueInt("dashboardawaitingresponsedealsoffset"), this.RequestData.GetValueInt("dashboardawaitingresponsedealscount"), out ResultCount, out Error); break;
					case "operator": AwaitingResponseDeals = ReportHelper.GetOperatorDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardawaitingresponsedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "2", null, this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsfrom"), this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsto"), this.RequestData.GetValueInt("dashboardawaitingresponsedealsoffset"), this.RequestData.GetValueInt("dashboardawaitingresponsedealscount"), out ResultCount, out Error); break;
					case "none": AwaitingResponseDeals = ReportHelper.GetDealsReport(this.CurrentUserId, RequestData.GetValueBool("dashboardawaitingresponsedealsallusers") ? (Guid?)null : this.CurrentUserId, null, null, true, "2", null, this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsfrom"), this.RequestData.GetValueDateTime("dashboardawaitingresponsedealsto"), this.RequestData.GetValueInt("dashboardawaitingresponsedealsoffset"), this.RequestData.GetValueInt("dashboardawaitingresponsedealscount"), out ResultCount, out Error); break;
				}
				this.ResponseData.AddData("AwaitingResponseDeals", AwaitingResponseDeals);
				this.ResponseData.AddData("AwaitingResponseDealsCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Dashboard


		#region Users
		private List<IhreUser> GetIhreUsers(bool Lite, bool SelectCurrent)
		{
			List<IhreUser> RetVal = new List<IhreUser>();
			try
			{
				ErrorObj Error = null;
				RetVal = IhreUserHelper.GetIhreUsers(this.CurrentUserId, Lite, SelectCurrent, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Users", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<IhreUser> ActivateUser()
		{
			List<IhreUser> RetVal = new List<IhreUser>();
			try
			{
				ErrorObj Error = null;
				Guid? userId = this.RequestData.GetValueGuid("userid");
				if (IhreUserHelper.ActivateUser(CurrentUserId, (Guid)userId, out Error))
				{
					if (Error != null)
					{
						this.ResponseData.ReturnError(Error);
					}
					else
					{
						RetVal = GetIhreUsers(false, false);
					}
				}
			}
			catch (Exception ex)
			{
				ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<IhreUser> DeactivateUser()
		{
			List<IhreUser> RetVal = new List<IhreUser>();
			try
			{
				ErrorObj Error = null;
				Guid? userId = this.RequestData.GetValueGuid("userid");
				if (IhreUserHelper.DeactivateUser(CurrentUserId, (Guid)userId, out Error))
				{
					if (Error != null)
					{
						this.ResponseData.ReturnError(Error);
					}
					else
					{
						RetVal = GetIhreUsers(false, false);
					}
				}
			}
			catch (Exception ex)
			{
				ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<IhreUser> UnlockUser()
		{
			List<IhreUser> RetVal = new List<IhreUser>();
			try
			{
				ErrorObj Error = null;
				Guid? userId = this.RequestData.GetValueGuid("userid");
				if (IhreUserHelper.UnlockUser(CurrentUserId, (Guid)userId, out Error))
				{
					if (Error != null)
					{
						this.ResponseData.ReturnError(Error);
					}
					else
					{
						RetVal = GetIhreUsers(false, false);
					}
				}
			}
			catch (Exception ex)
			{
				ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private Dictionary<string, bool> GetCurrentUserRoles()
		{
			Dictionary<string, bool> RetVal = new Dictionary<string, bool>();
			try
			{
				ErrorObj Error = null;
				RetVal = IhreUserHelper.GetCurrentUserRolesDictionary(CurrentUserId, this.CurrentUser.UserName, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("UserRoles", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<IhreRole> GetIhreUserRoles()
		{
			List<IhreRole> RetVal = new List<IhreRole>();
			try
			{
				ErrorObj Error = null;
				Guid? userId = this.RequestData.GetValueGuid("userid");
				string userName = Membership.GetUser(userId).UserName;
				RetVal = IhreUserHelper.GetIhreUserRoles(CurrentUserId, userName, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					Dictionary<string, object> ResponseObject = new Dictionary<string, object>();
					ResponseObject.Add("Roles", RetVal);
					ResponseObject.Add("UserId", this.RequestData.GetValueString("userid"));
					ResponseObject.Add("UserName", userName);
					this.ResponseData.AddData("UserRoles", ResponseObject);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<IhreRole> GetIhreRoles()
		{
			List<IhreRole> RetVal = new List<IhreRole>();
			try
			{
				ErrorObj Error = null;
				RetVal = IhreUserHelper.GetIhreUserRoles(CurrentUserId, null, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewUserRoles", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<IhreUser> NewUser()
		{
			List<IhreUser> RetVal = new List<IhreUser>();
			try
			{
				ErrorObj Error = null;
				if (IhreUserHelper.NewIhreUser(CurrentUserId, this.RequestData.GetValueString("username"), this.RequestData.GetValueString("password"), this.RequestData.GetValueString("email"), this.RequestData.GetValueString("roles"), out Error))
				{
					if (Error != null)
					{
						this.ResponseData.ReturnError(Error);
					}
					else
					{
						RetVal = GetIhreUsers(false, false);
					}
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateUserRoles()
		{
			try
			{
				ErrorObj Error = null;
				Guid? userId = this.RequestData.GetValueGuid("userid");
				string userName = Membership.GetUser(userId).UserName;
				string roles = this.RequestData.GetValueString("roles");
				if (IhreUserHelper.UpdateUserRoles(CurrentUserId, userName, roles, out Error))
				{
					if (Error != null)
					{
						this.ResponseData.ReturnError(Error);
					}
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void ChangePassword()
		{
			try
			{
				ErrorObj Error = null;
				string NewPassword = this.RequestData.GetValueString("password");
				IhreUserHelper.UpdatePassword(this.CurrentUserId, NewPassword, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Users


		#region Affiliates

		#region Affiliates Browse
		private List<Affiliate> GetAffiliates()
		{
			List<Affiliate> RetVal = new List<Affiliate>();
			try
			{
				ErrorObj Error = null;
				int ResultCount = 0;
				RetVal = AffiliateHelper.GetAffiliates(
					this.CurrentUserId,
					this.RequestData.GetValueString("searchstr"),
					this.RequestData.GetValueString("affmanagerids"),
					this.RequestData.GetValueBool("includeunassigned"),
					this.RequestData.GetValueShort("affsizetype"),
					this.RequestData.GetValueShort("contactabletype"),
					this.RequestData.GetValueShort("maxmailtype"),
					this.RequestData.GetValueShort("affstatustype"),
					this.RequestData.GetValueShort("operatortypeoption"),
					this.RequestData.GetValueString("operatorids"),
					this.RequestData.GetValueString("countrytypeoption"),
					this.RequestData.GetValueString("countryids"),
					this.RequestData.GetValueString("affiliatetypeoption"),
					this.RequestData.GetValueString("affiliatetypeids"),
					this.RequestData.GetValueString("dealtypeoption"),
					this.RequestData.GetValueString("dealtypeids"),
					this.RequestData.GetValueString("marketingtypeoption"),
					this.RequestData.GetValueString("marketingtypeids"),
					null,
					null,
					this.RequestData.GetValueBool("incaffiliatetypes"),
					this.RequestData.GetValueBool("incdealtypes"),
					this.RequestData.GetValueBool("incmarketingtypes"),
					this.RequestData.GetValueBool("inccountries"),
					this.RequestData.GetValueBool("incoperators"),

					this.RequestData.GetValueBool("checkqueryspeed"),
					this.RequestData.GetValueInt("offset"),
					this.RequestData.GetValueInt("count"),
					out ResultCount,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Affiliates", RetVal);
				this.ResponseData.AddData("AffiliatesCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void ReassignAffiliates()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.ReassignAffiliates(CurrentUserId, (Guid)this.RequestData.GetValueGuid("affmanageruserid"), this.RequestData.GetValueString("affiliateids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Affiliates Browse

		#region Affiliates - Add
		private int AddAffiliate()
		{
			int RetVal = -1;
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.AddAffiliate(
					CurrentUserId,
					this.RequestData.GetValueString("affiliatename"),
					this.RequestData.GetValueString("contactname"),
					this.RequestData.GetValueGuid("userid"),
					this.RequestData.GetValueShort("affiliatesize"),
					this.RequestData.GetValueBool("contactable"),
					this.RequestData.GetValueBool("maxmail"),
					this.RequestData.GetValueString("comment"),
					this.RequestData.GetValueInt("primarycontactdetailtype"),
					this.RequestData.GetValueString("primarycontactdetailtext"),
					this.RequestData.GetValueString("primaryurltext"), 
					this.RequestData.GetValueString("countryids"),
					this.RequestData.GetValueString("affiliatetypeids"),
					this.RequestData.GetValueString("dealtypeids"),
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewAffiliateID", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		#endregion Affiliates - Add

		#region Affiliates - Remove
		private bool RemoveAffiliate()
		{
			bool RetVal = false;
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.RemoveAffiliate(
					CurrentUserId,
					this.RequestData.GetValueInt("affiliateid"),
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		#endregion Affiliates - Remove

		#region Affiliate View
		private Affiliate GetAffiliate()
		{
			Affiliate RetVal = new Affiliate();
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.GetAffiliate(this.CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Affiliate", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<Operator> GetAffiliateOperatorSuggestions()
		{
			List<Operator> retVal = new List<Operator>();
			try
			{
				ErrorObj Error = null;
				retVal = AffiliateHelper.GetAffiliateOperatorSuggestions(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("AffiliateOperatorSuggestions", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		#endregion Affiliate View

		#region Affiliate Update
		private void UpdateAffiliateProfile()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateProfile(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("affiliatename"), this.RequestData.GetValueString("contactname"), this.RequestData.GetValueGuid("affmanageruserid"), this.RequestData.GetValueShort("affsize"), this.RequestData.GetValueBool("allowcontact"), this.RequestData.GetValueBool("maxmail"), this.RequestData.GetValueBool("isactive"), this.RequestData.GetValueString("comment"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private List<ContactDetail> UpdateAffiliateContactDetails(bool ReturnContactDetails)
		{
			List<ContactDetail> retVal = new List<ContactDetail>();
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateContactDetails(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("contactdetailinfo"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else if (ReturnContactDetails)
				{
					retVal = AffiliateHelper.GetAffiliateContactDetails(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
					this.ResponseData.AddData("AffiliateContactDetails", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		private List<ContactDetail> RemoveAffiliateContactDetail()
		{
			List<ContactDetail> retVal = new List<ContactDetail>();
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.RemoveAffiliateContactDetail(CurrentUserId, this.RequestData.GetValueInt("contactdetailid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					retVal = AffiliateHelper.GetAffiliateContactDetails(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
					this.ResponseData.AddData("AffiliateContactDetails", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}

		private List<AffiliateURL> UpdateAffiliateURLs(bool ReturnUrls)
		{
			List<AffiliateURL> retVal = new List<AffiliateURL>();
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateURLs(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("urlinfo"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else if (ReturnUrls)
				{
					retVal = AffiliateHelper.GetAffiliateURLs(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
					this.ResponseData.AddData("AffiliateUrls", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		private List<AffiliateURL> RemoveAffiliateURL()
		{
			List<AffiliateURL> retVal = new List<AffiliateURL>();
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.RemoveAffiliateURL(CurrentUserId, this.RequestData.GetValueInt("urlid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					retVal = AffiliateHelper.GetAffiliateURLs(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
					this.ResponseData.AddData("AffiliateUrls", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		private void UpdateAffiliateCountries()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateCountries(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("countryids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateAffiliateAffiliateTypes()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateAffiliateTypes(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("affiliatetypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateAffiliateDealTypes()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateDealTypes(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("dealtypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateAffiliateMarketingTypes()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateMarketingTypes(CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("marketingtypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Affiliate Update

		#region Affiliate Notes
		public List<AffiliateNote> GetAffiliateNotes()
		{
			List<AffiliateNote> RetVal = new List<AffiliateNote>();
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.GetAffiliateNotes(this.CurrentUserId, this.RequestData.GetValueInt("affiliateid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("AffiliateNotes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		public void UpdateAffiliateNote()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateNote(this.CurrentUserId, this.RequestData.GetValueInt("noteid"), this.RequestData.GetValueString("notetext"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		public AffiliateNote AddAffiliateNote()
		{
			AffiliateNote affiliateNote = null;
			try
			{
				ErrorObj Error = null;
				affiliateNote = AffiliateHelper.AddAffiliateNote(this.CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("notetext"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewAffiliateNote", affiliateNote);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return affiliateNote;
		}
		public void RemoveAffiliateNote()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.RemoveAffiliateNote(this.CurrentUserId, this.RequestData.GetValueInt("noteid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Affiliate Notes

		#endregion Affiliates


		#region Deals
		public List<Deal> GetAffiliateDeals()
		{
			List<Deal> RetVal = new List<Deal>();
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.GetAffiliateDeals(this.CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueBool("dealsincludeinactive"), this.RequestData.GetValueGuid("dealsassigneduser"), this.RequestData.GetValueInt("dealsdealinfo"), this.RequestData.GetValueInt("dealsdealstatus"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("AffiliateDeals", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		public List<Deal> GetOperatorDeals()
		{
			List<Deal> RetVal = new List<Deal>();
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.GetOperatorDeals(
					this.CurrentUserId,
					this.RequestData.GetValueInt("operatorid"),
					this.RequestData.GetValueBool("dealsincludeinactive"),
					this.RequestData.GetValueGuid("dealsassigneduser"),
					this.RequestData.GetValueInt("dealsdealinfo"),
					this.RequestData.GetValueInt("dealsdealstatus"),
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("OperatorDeals", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		public Deal UpdateDeal()
		{
			Deal RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.UpdateDeal(this.CurrentUserId, this.RequestData.GetValueInt("dealid"), (Guid)this.RequestData.GetValueGuid("assigneduserid"), this.RequestData.GetValueInt("dealstatusid"), this.RequestData.GetValueString("dealtext"), this.RequestData.GetValueInt("dealinfoid"), this.RequestData.GetValueBool("isactive"), this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueInt("affiliateid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("UpdatedDeal", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		public Deal AddDeal()
		{
			Deal deal = null;
			try
			{
				ErrorObj Error = null;
				deal = DealHelper.AddDeal(this.CurrentUserId, this.RequestData.GetValueInt("dealstatusid"), this.RequestData.GetValueString("dealtext"), this.RequestData.GetValueInt("dealinfoid"), this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueInt("affiliateid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewDeal", deal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return deal;
		}
		#endregion Deals


		#region Operators

		#region Operators - Browse
		private List<Operator> GetOperators(bool Lite)
		{
			List<Operator> RetVal = new List<Operator>();
			try
			{
				ErrorObj Error = null;
				int ResultCount = 0;
				if (Lite)
				{
					RetVal = OperatorHelper.GetOperatorsLite(this.CurrentUserId, out Error);
				}
				else
				{
					RetVal = OperatorHelper.GetOperators(
						this.CurrentUserId,
						this.RequestData.GetValueString("searchstr"),
						this.RequestData.GetValueShort("operatorstatustype"),
						this.RequestData.GetValueString("countrytypeoption"),
						this.RequestData.GetValueString("countryids"),
						this.RequestData.GetValueString("languagetypeoption"),
						this.RequestData.GetValueString("languageids"),
						this.RequestData.GetValueString("affiliatetypeoption"),
						this.RequestData.GetValueString("affiliatetypeids"),
						this.RequestData.GetValueString("dealtypeoption"),
						this.RequestData.GetValueString("dealtypeids"),
						this.RequestData.GetValueString("softwaretypeoption"),
						this.RequestData.GetValueString("softwaretypeids"),
						this.RequestData.GetValueString("licencetypeoption"),
						this.RequestData.GetValueString("licencetypeids"),
						this.RequestData.GetValueString("paymenttypeoption"),
						this.RequestData.GetValueString("paymenttypeids"),
						this.RequestData.GetValueInt("offset"),
						this.RequestData.GetValueInt("count"),
						out ResultCount,
						out Error
					);

				}
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Operators", RetVal);
				this.ResponseData.AddData("OperatorsCount", ResultCount);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		#endregion Operators - Browse

		#region Operators - Add
		private int AddOperator()
		{
			int RetVal = -1;
			try
			{
				ErrorObj Error = null;
				RetVal = OperatorHelper.AddOperator(CurrentUserId, this.RequestData.GetValueString("operatorname"), this.RequestData.GetValueString("affiliatesignupurl"), this.RequestData.GetValueString("websitereviewurl"), this.RequestData.GetValueInt("primarycontactdetailtype"), this.RequestData.GetValueString("primarycontactdetailtext"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewOperatorID", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		#endregion Operators - Add

		#region Operator - View
		private Operator GetOperator()
		{
			Operator RetVal = new Operator();
			try
			{
				ErrorObj Error = null;
				RetVal = OperatorHelper.GetOperator(this.CurrentUserId, this.RequestData.GetValueInt("operatorid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Operator", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		#endregion Operator - View

		#region Operator Update
		private void UpdateOperatorFavourite()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorFavourite(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueBool("isfavourite"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorProfile()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorProfile(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("operatorname"), this.RequestData.GetValueString("affiliatesignupurl"), this.RequestData.GetValueString("websitereviewurl"), this.RequestData.GetValueBool("isactive"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private List<ContactDetail> UpdateOperatorContactDetails(bool ReturnContactDetails)
		{
			List<ContactDetail> retVal = new List<ContactDetail>();
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorContactDetails(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("contactdetailinfo"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else if (ReturnContactDetails)
				{
					retVal = OperatorHelper.GetOperatorContactDetails(CurrentUserId, this.RequestData.GetValueInt("operatorid"), out Error);
					this.ResponseData.AddData("OperatorContactDetails", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		private List<ContactDetail> RemoveOperatorContactDetail()
		{
			List<ContactDetail> retVal = new List<ContactDetail>();
			try
			{
				ErrorObj Error = null;
				OperatorHelper.RemoveOperatorContactDetail(CurrentUserId, this.RequestData.GetValueInt("contactdetailid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					retVal = OperatorHelper.GetOperatorContactDetails(CurrentUserId, this.RequestData.GetValueInt("operatorid"), out Error);
					this.ResponseData.AddData("OperatorContactDetails", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}

		private void UpdateOperatorLanguages()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorLanguages(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("languageids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorAffiliateTypes()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorAffiliateTypes(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("affiliatetypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorDealTypes()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorDealTypes(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("dealtypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorSoftwareTypes()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorSoftwareTypes(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("softwaretypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorLicenceTypes()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorLicenceTypes(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("licencetypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private void UpdateOperatorPaymentTypes()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorPaymentTypes(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("paymenttypeids"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private List<Brand> UpdateOperatorBrands(bool ReturnBrandDetails)
		{
			List<Brand> retVal = new List<Brand>();
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorBrands(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("brandinfo"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else if (ReturnBrandDetails)
				{
					retVal = OperatorHelper.GetOperatorBrands(CurrentUserId, this.RequestData.GetValueInt("operatorid"), out Error);
					this.ResponseData.AddData("OperatorBrands", retVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return retVal;
		}
		private void UpdateOperatorCountries()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorCountries(CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("countryinfo"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Operator Update

		#region Operator Notes
		public List<OperatorNote> GetOperatorNotes()
		{
			List<OperatorNote> RetVal = new List<OperatorNote>();
			try
			{
				ErrorObj Error = null;
				RetVal = OperatorHelper.GetOperatorNotes(this.CurrentUserId, this.RequestData.GetValueInt("operatorid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("OperatorNotes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		public void UpdateOperatorNote()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.UpdateOperatorNote(this.CurrentUserId, this.RequestData.GetValueInt("noteid"), this.RequestData.GetValueString("notetext"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		public OperatorNote AddOperatorNote()
		{
			OperatorNote operatorNote = null;
			try
			{
				ErrorObj Error = null;
				operatorNote = OperatorHelper.AddOperatorNote(this.CurrentUserId, this.RequestData.GetValueInt("operatorid"), this.RequestData.GetValueString("notetext"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewOperatorNote", operatorNote);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return operatorNote;
		}
		public void RemoveOperatorNote()
		{
			try
			{
				ErrorObj Error = null;
				OperatorHelper.RemoveOperatorNote(this.CurrentUserId, this.RequestData.GetValueInt("noteid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		#endregion Operator Notes


		#endregion Operators


		#region Filters

		private List<AffiliateType> GetAffiliateTypes()
		{
			List<AffiliateType> RetVal = new List<AffiliateType>();
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.GetAffiliateTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("AffiliateTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateAffiliateType()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateAffiliateType(this.CurrentUserId, this.RequestData.GetValueInt("affiliateid"), this.RequestData.GetValueString("affiliatetypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private AffiliateType AddAffiliateType()
		{
			AffiliateType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.AddAffiliateType(this.CurrentUserId, this.RequestData.GetValueString("affiliatetypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewAffiliateType", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<ContactDetailType> GetContactDetailTypes()
		{
			List<ContactDetailType> RetVal = new List<ContactDetailType>();
			try
			{
				ErrorObj Error = null;
				RetVal = ContactDetailHelper.GetContactDetailTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("ContactDetailTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateContactDetailType()
		{
			try
			{
				ErrorObj Error = null;
				ContactDetailHelper.UpdateContactDetailType(this.CurrentUserId, this.RequestData.GetValueInt("contactdetailid"), this.RequestData.GetValueString("contactdetailtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private ContactDetailType AddContactDetailType()
		{
			ContactDetailType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = ContactDetailHelper.AddContactDetailType(this.CurrentUserId, this.RequestData.GetValueString("contactdetailtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				else
				{
					this.ResponseData.AddData("NewContactDetailType", RetVal);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<Country> GetCountries()
		{
			List<Country> RetVal = new List<Country>();
			try
			{
				ErrorObj Error = null;
				RetVal = CountryHelper.GetCountries(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Countries", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateCountry()
		{
			try
			{
				ErrorObj Error = null;
				CountryHelper.UpdateCountry(this.CurrentUserId, this.RequestData.GetValueInt("countryid"), this.RequestData.GetValueString("countryname"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private Country AddCountry()
		{
			Country RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = CountryHelper.AddCountry(this.CurrentUserId, this.RequestData.GetValueString("countryname"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewCountry", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<DealType> GetDealTypes()
		{
			List<DealType> RetVal = new List<DealType>();
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.GetDealTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("DealTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateDealType()
		{
			try
			{
				ErrorObj Error = null;
				DealHelper.UpdateDealType(this.CurrentUserId, this.RequestData.GetValueInt("dealtypeid"), this.RequestData.GetValueString("dealtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private DealType AddDealType()
		{
			DealType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.AddDealType(this.CurrentUserId, this.RequestData.GetValueString("dealtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewDealType", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<MarketingType> GetMarketingTypes()
		{
			List<MarketingType> RetVal = new List<MarketingType>();
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.GetMarketingTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("MarketingTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateMarketingType()
		{
			try
			{
				ErrorObj Error = null;
				AffiliateHelper.UpdateMarketingType(this.CurrentUserId, this.RequestData.GetValueInt("marketingtypeid"), this.RequestData.GetValueString("marketingtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private MarketingType AddMarketingType()
		{
			MarketingType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = AffiliateHelper.AddMarketingType(this.CurrentUserId, this.RequestData.GetValueString("marketingtypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewMarketingType", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<SoftwareType> GetSoftwareTypes()
		{
			List<SoftwareType> RetVal = new List<SoftwareType>();
			try
			{
				ErrorObj Error = null;
				RetVal = SoftwareHelper.GetSoftwareTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("SoftwareTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateSoftwareType()
		{
			try
			{
				ErrorObj Error = null;
				SoftwareHelper.UpdateSoftwareType(this.CurrentUserId, this.RequestData.GetValueInt("softwaretypeid"), this.RequestData.GetValueString("softwaretypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private SoftwareType AddSoftwareType()
		{
			SoftwareType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = SoftwareHelper.AddSoftwareType(this.CurrentUserId, this.RequestData.GetValueString("softwaretypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewSoftwareType", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<LicenceType> GetLicenceTypes()
		{
			List<LicenceType> RetVal = new List<LicenceType>();
			try
			{
				ErrorObj Error = null;
				RetVal = LicenceHelper.GetLicenceTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("LicenceTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateLicenceType()
		{
			try
			{
				ErrorObj Error = null;
				LicenceHelper.UpdateLicenceType(this.CurrentUserId, this.RequestData.GetValueInt("licencetypeid"), this.RequestData.GetValueString("licencetypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private LicenceType AddLicenceType()
		{
			LicenceType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = LicenceHelper.AddLicenceType(this.CurrentUserId, this.RequestData.GetValueString("licencetypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewLicenceType", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<Language> GetLanguages()
		{
			List<Language> RetVal = new List<Language>();
			try
			{
				ErrorObj Error = null;
				RetVal = LanguageHelper.GetLanguages(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("Languages", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdateLanguage()
		{
			try
			{
				ErrorObj Error = null;
				LanguageHelper.UpdateLanguage(this.CurrentUserId, this.RequestData.GetValueInt("languageid"), this.RequestData.GetValueString("languagename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private Language AddLanguage()
		{
			Language RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = LanguageHelper.AddLanguage(this.CurrentUserId, this.RequestData.GetValueString("languagename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewLanguage", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<PaymentType> GetPaymentTypes()
		{
			List<PaymentType> RetVal = new List<PaymentType>();
			try
			{
				ErrorObj Error = null;
				RetVal = PaymentTypeHelper.GetPaymentTypes(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("PaymentTypes", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private void UpdatePaymentType()
		{
			try
			{
				ErrorObj Error = null;
				PaymentTypeHelper.UpdatePaymentType(this.CurrentUserId, this.RequestData.GetValueInt("paymenttypeid"), this.RequestData.GetValueString("paymenttypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
		}
		private PaymentType AddPaymentType()
		{
			PaymentType RetVal = null;
			try
			{
				ErrorObj Error = null;
				RetVal = PaymentTypeHelper.AddPaymentType(this.CurrentUserId, this.RequestData.GetValueString("paymenttypename"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewPaymentType", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private List<DealInfo> GetDealInfos()
		{
			List<DealInfo> RetVal = new List<DealInfo>();
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.GetDealInfos(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("DealInfos", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<DealInfo> UpdateDealInfo()
		{
			try
			{
				ErrorObj Error = null;
				DealHelper.UpdateDealInfo(this.CurrentUserId, this.RequestData.GetValueInt("dealinfoid"), this.RequestData.GetValueString("dealinfoname"), this.RequestData.GetValueInt("dealinfovaluerank"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return GetDealInfos();
		}
		private List<DealInfo> AddDealInfo()
		{
			List<DealInfo> RetVal = null;
			try
			{
				ErrorObj Error = null;
				DealHelper.AddDealInfo(this.CurrentUserId, this.RequestData.GetValueString("dealinfoname"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewDealInfo", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return GetDealInfos();
		}

		private List<DealStatus> GetDealStatuses()
		{
			List<DealStatus> RetVal = new List<DealStatus>();
			try
			{
				ErrorObj Error = null;
				RetVal = DealHelper.GetDealStatuses(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("DealStatuses", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		#endregion Filters


		#region Reports

		private List<CustomReportInfo> GetCustomReports()
		{
			List<CustomReportInfo> RetVal = new List<CustomReportInfo>();
			try
			{
				ErrorObj Error = null;
				RetVal = ReportHelper.GetCustomReports(this.CurrentUserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("CustomReports", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private CustomReportInfo GetCustomReportInfo()
		{
			CustomReportInfo RetVal = new CustomReportInfo();
			try
			{
				ErrorObj Error = null;
				RetVal = ReportHelper.GetCustomReportInfo(this.CurrentUserId, this.RequestData.GetValueInt("customreportid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("CustomReportInfo", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private int AddCustomReport()
		{
			int RetVal = -1;
			try
			{
				ErrorObj Error = null;
				Guid? UserId = this.RequestData.GetValueBool("currentuseronly") ? this.CurrentUserId : (Guid?)null;
				RetVal = ReportHelper.AddCustomReport(this.CurrentUserId, this.RequestData.GetValueString("customreportname"), this.RequestData.GetValueString("customreporttype"), this.RequestData.GetValueString("customreportparams"), UserId, out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("NewCustomReportId", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private List<CustomReportInfo> RemoveCustomReport()
		{
			try
			{
				ErrorObj Error = null;
				ReportHelper.RemoveCustomReport(this.CurrentUserId, this.RequestData.GetValueInt("customreportid"), out Error);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return GetCustomReports();
		}

		private ReportData GetReportAffiliates()
		{
			ReportData RetVal = new ReportData();
			try
			{
				ErrorObj Error = null;
				int ResultCount = 0;
				RetVal = ReportHelper.GetAffiliateReport(
					this.CurrentUserId,
					this.RequestData.GetValueString("searchstr"),
					Roles.IsUserInRole("UnrestrictedReports") || Roles.IsUserInRole("SuperUser") ? this.RequestData.GetValueString("affmanagerids") : (this.RequestData.GetValueBool("allusers") ? "" : this.CurrentUserId.ToString()),
					this.RequestData.GetValueBool("includeunassigned"),
					this.RequestData.GetValueShort("affsizetype"),
					this.RequestData.GetValueShort("contactabletype"),
					this.RequestData.GetValueShort("maxmailtype"),
					this.RequestData.GetValueShort("affstatustype"),
					this.RequestData.GetValueShort("operatortypeoption"),
					this.RequestData.GetValueString("operatorids"),
					this.RequestData.GetValueString("countrytypeoption"),
					this.RequestData.GetValueString("countryids"),
					this.RequestData.GetValueString("affiliatetypeoption"),
					this.RequestData.GetValueString("affiliatetypeids"),
					this.RequestData.GetValueString("dealtypeoption"),
					this.RequestData.GetValueString("dealtypeids"),
					this.RequestData.GetValueString("marketingtypeoption"),
					this.RequestData.GetValueString("marketingtypeids"),
					this.RequestData.GetValueDateTime("createdfrom"),
					this.RequestData.GetValueDateTime("createdto"),
					this.RequestData.GetValueBool("incaffiliatetypes"),
					this.RequestData.GetValueBool("incdealtypes"),
					this.RequestData.GetValueBool("incmarketingtypes"),
					this.RequestData.GetValueBool("inccountries"),
					this.RequestData.GetValueBool("incoperators"),

					this.RequestData.GetValueBool("checkqueryspeed"),
					out ResultCount,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("AffiliatesReportData", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private ReportData GetReportOperators()
		{
			ReportData RetVal = new ReportData();
			try
			{
				ErrorObj Error = null;
				int ResultCount = 0;
				RetVal = ReportHelper.GetOperatorReport(
					this.CurrentUserId,
					this.RequestData.GetValueShort("statustype"),
					this.RequestData.GetValueDateTime("createdfrom"),
					this.RequestData.GetValueDateTime("createdto"),
					this.RequestData.GetValueString("countrytypeoption"),
					this.RequestData.GetValueString("countryids"),
					this.RequestData.GetValueString("languagetypeoption"),
					this.RequestData.GetValueString("languageids"),
					this.RequestData.GetValueString("affiliatetypeoption"),
					this.RequestData.GetValueString("affiliatetypeids"),
					this.RequestData.GetValueString("dealtypeoption"),
					this.RequestData.GetValueString("dealtypeids"),
					this.RequestData.GetValueString("softwaretypeoption"),
					this.RequestData.GetValueString("softwaretypeids"),
					this.RequestData.GetValueString("licencetypeoption"),
					this.RequestData.GetValueString("licencetypeids"),
					this.RequestData.GetValueString("paymenttypeoption"),
					this.RequestData.GetValueString("paymenttypeids"),
					this.RequestData.GetValueBool("incbrands"),
					this.RequestData.GetValueBool("incsignupurls"),
					this.RequestData.GetValueBool("inccountries"),
					this.RequestData.GetValueBool("inclanguages"),
					this.RequestData.GetValueBool("incaffiliatetypes"),
					this.RequestData.GetValueBool("incdealtypes"),
					this.RequestData.GetValueBool("incsoftwaretypes"),
					this.RequestData.GetValueBool("inclicencetypes"),
					this.RequestData.GetValueBool("incpaymenttypes"),
					this.RequestData.GetValueBool("incsecondarycontactdetails"),

					this.RequestData.GetValueBool("checkqueryspeed"),
					out ResultCount,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("OperatorsReportData", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}

		private ReportData GetReportDeals()
		{
			ReportData RetVal = new ReportData();
			try
			{
				ErrorObj Error = null;
				int ResultCount = 0;
				RetVal = ReportHelper.GetDealReport(
					this.CurrentUserId,
					this.RequestData.GetValueString("affmanageroption"),
					Roles.IsUserInRole("UnrestrictedReports") || Roles.IsUserInRole("SuperUser") ? this.RequestData.GetValueString("affmanagerids") : (this.RequestData.GetValueBool("allusers") ? "" : this.CurrentUserId.ToString()),
					this.RequestData.GetValueBool("includeunassigned"),
					this.RequestData.GetValueShort("dealactivestatustype"),
					this.RequestData.GetValueDateTime("createdfrom"),
					this.RequestData.GetValueDateTime("createdto"),
					this.RequestData.GetValueDateTime("datesetfrom"),
					this.RequestData.GetValueDateTime("datesetto"),
					this.RequestData.GetValueString("dealstatusids"),
					this.RequestData.GetValueString("dealinfoids"),
					this.RequestData.GetValueBool("checkqueryspeed"),
					out ResultCount,
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("DealsReportData", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}


		private DealSummary GetUserDealsSummary()
		{
			DealSummary RetVal = new DealSummary();
			try
			{
				ErrorObj Error = null;
				RetVal = ReportHelper.GetUserDealsSummaryReport(
					this.CurrentUserId,
					this.RequestData.GetValueShort("dealactivestatustype"),
					this.RequestData.GetValueDateTime("datesetfrom"),
					this.RequestData.GetValueDateTime("datesetto"),
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("UserDealsSummary", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}
		private DealSummary GetOperatorDealsSummary(){
			DealSummary RetVal = new DealSummary();
			try
			{
				ErrorObj Error = null;
				RetVal = ReportHelper.GetOperatorDealsSummaryReport(
					this.CurrentUserId,
					this.RequestData.GetValueShort("dealactivestatustype"),
					this.RequestData.GetValueDateTime("datesetfrom"),
					this.RequestData.GetValueDateTime("datesetto"),
					out Error
				);
				if (Error != null)
				{
					this.ResponseData.ReturnError(Error);
				}
				this.ResponseData.AddData("OperatorDealsSummary", RetVal);
			}
			catch (Exception ex)
			{
				this.ResponseData.ReturnError(ex.ToString());
			}
			return RetVal;
		}


		#endregion Reports


		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}