﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IhreDB2.Default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="ihreapp">
<head id="Head1" runat="server">
	<title>Ihre Consulting Database</title>
	<link type="text/css" rel="stylesheet" href="/styles/main.css" />
	<link type="text/css" rel="stylesheet" href="/styles/jquery-ui-1.11.2.custom/jquery-ui.css" />
</head>
<body ng-controller="MainController" id="DocBody" ng-class="FontSize.Class">
	<form id="form1" runat="server">
		
		<div class="initialising" ng-show="isInitializing"><div>Initialising</div></div>
		<div class="loading" ng-show="isProcessing"><div>Loading</div></div>

		<div class="header">
			<div class="logo" ng-href="{{GetLink('dashboard')}}" ng-click="RenderSection('dashboard')"></div>
			<ul class="menu">
				<li>
					<a class="btn" ng-href="{{GetLink('dashboard')}}" ng-click="RenderSection('dashboard')" ng-class="CurrentSection == 'dashboard' ? 'active' : ''"><span></span><span>Dashboard</span></a>
				</li>
				<li>
					<a class="btn" ng-href="{{GetLink('affiliates')}}" ng-click="RenderSection('affiliates')" ng-class="CurrentSection == 'affiliates' ? 'active' : ''"><span></span><span>Affiliates</span></a>
					<ul class="submenu">
						<li>
							<a class="btn" ng-href="{{GetLink('affiliates')}}" ng-click="RenderSection('affiliates')" ng-class="CurrentSection == 'affiliates' ? 'active' : ''"><span></span><span>Browse Affiliates</span></a>
						</li>
						<li>
							<a class="btn" ng-href="{{GetLink('addaffiliate')}}" ng-click="RenderSection('addaffiliate')" ng-class="CurrentSection == 'addaffiliate' ? 'active' : ''"><span></span><span>New Affiliate</span></a>
						</li>
					</ul>
				</li>
				<li>
					<a class="btn" ng-href="{{GetLink('operators')}}" ng-click="RenderSection('operators')" ng-class="CurrentSection == 'operators' ? 'active' : ''"><span></span><span>Operators</span></a>
					<ul class="submenu">
						<li>
							<a class="btn" ng-href="{{GetLink('operators')}}" ng-click="RenderSection('operators')" ng-class="CurrentSection == 'operators' && FilterOperators.ViewAffiliateSignupUrls == false ? 'active' : ''"><span></span><span>Browse Operators</span></a>
						</li>
						<li>
							<a class="btn" ng-href="{{GetLink('addoperator')}}" ng-click="RenderSection('addoperator')" ng-class="CurrentSection == 'addoperator' ? 'active' : ''"><span></span><span>New Operator</span></a>
						</li>
						<li>
							<a class="btn" ng-href="{{GetLink('operators', 'signuplinks')}}" ng-click="RenderSection('operators', 'signuplinks')" ng-class="CurrentSection == 'operators' && FilterOperators.ViewAffiliateSignupUrls == true ? 'active' : ''"><span></span><span>Operator Signup Links</span></a>
						</li>
					</ul>
				</li>
				<li>
					<a class="btn" ng-href="{{GetLink('report-affiliates')}}" ng-click="RenderSection('report-affiliates')" ng-class="CurrentSection.indexOf('report-') != -1 ? 'active' : ''"><span></span><span>Reports</span></a>
					<ul class="submenu">
						<li><a class="btn" ng-href="{{GetLink('report-affiliates')}}" ng-click="RenderSection('report-affiliates')" ng-class="CurrentSection == 'report-affiliates' ? 'active' : ''"><span></span><span>Affiliates Report</span></a></li>
						<li><a class="btn" ng-href="{{GetLink('report-operators')}}" ng-click="RenderSection('report-operators')" ng-class="CurrentSection == 'report-operators' ? 'active' : ''"><span></span><span>Operators Report</span></a></li>
						<li><a class="btn" ng-href="{{GetLink('report-deals')}}" ng-click="RenderSection('report-deals')" ng-class="CurrentSection == 'report-deals' ? 'active' : ''"><span></span><span>Deals Report</span></a></li>
						<li><a ng-show="IsUserInRole('UnrestrictedReports')" class="btn" ng-href="{{GetLink('report-user-deals-summary')}}" ng-click="RenderSection('report-user-deals-summary')" ng-class="CurrentSection == 'report-user-deals-summary' ? 'active' : ''"><span></span><span>User Deals Summary</span></a></li>
						<li><a ng-show="IsUserInRole('UnrestrictedReports')"class="btn" ng-href="{{GetLink('report-operator-deals-summary')}}" ng-click="RenderSection('report-operator-deals-summary')" ng-class="CurrentSection == 'report-operator-deals-summary' ? 'active' : ''"><span></span><span>Operator Deals Summary</span></a></li>
						<li><a class="btn" ng-href="{{GetLink('report-custom')}}" ng-click="RenderSection('report-custom')" ng-class="CurrentSection == 'report-custom' ? 'active' : ''"><span></span><span>Custom Reports</span></a></li>
					</ul>
				</li>
				<li class="has-sub-menu" ng-show="IsUserInRole('ManageUsers')">
					<a class="btn" ng-href="{{GetLink('users')}}" ng-click="RenderSection('users')" ng-class="CurrentSection == 'users' ? 'active' : ''"><span></span><span>Users</span></a>
					<ul class="submenu">
						<li>
							<a class="btn" ng-href="{{GetLink('adduser')}}" ng-click="RenderSection('adduser')" ng-class="CurrentSection == 'adduser' ? 'active' : ''"><span></span><span>New User</span></a>
						</li>
						<li>
							<a class="btn" ng-href="{{GetLink('users')}}" ng-click="RenderSection('users')" ng-class="CurrentSection == 'users' ? 'active' : ''"><span></span><span>Manage Users</span></a>
						</li>
					</ul>
				</li>
				<li class="has-sub-menu" ng-show="IsUserInRole('ManageFilters')">
					<a class="btn" ng-href="{{GetLink('settings')}}" ng-click="RenderSection('settings')" ng-class="CurrentSection == 'settings' ? 'active' : ''"><span></span><span>Settings</span></a>
					<ul class="submenu">
						<li><a class="btn" ng-class="Settings.ActiveTab == 'affiliatetypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'affiliatetypes')}}" ng-click="RenderSection('settings', 'affiliatetypes')"><span>Affiliate/Operator Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'contactdetailtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'contactdetailtypes')}}" ng-click="RenderSection('settings', 'contactdetailtypes')"><span>Contact Detail Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'countries' ? 'active' : ''" ng-href="{{GetLink('settings', 'countries')}}" ng-click="RenderSection('settings', 'countries')"><span>Countries</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'dealtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'dealtypes')}}" ng-click="RenderSection('settings', 'dealtypes')"><span>Deal Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'dealinfos' ? 'active' : ''" ng-href="{{GetLink('settings', 'dealinfos')}}" ng-click="RenderSection('settings', 'dealinfos')"><span>Deal Infos</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'marketingtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'marketingtypes')}}" ng-click="RenderSection('settings', 'marketingtypes')"><span>Marketing Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'softwaretypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'softwaretypes')}}" ng-click="RenderSection('settings', 'softwaretypes')"><span>Software Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'licencetypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'licencetypes')}}" ng-click="RenderSection('settings', 'licencetypes')"><span>Licence Types</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'languages' ? 'active' : ''" ng-href="{{GetLink('settings', 'languages')}}" ng-click="RenderSection('settings', 'languages')"><span>Languages</span></a></li>
						<li><a class="btn" ng-class="Settings.ActiveTab == 'paymenttypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'paymenttypes')}}" ng-click="RenderSection('settings', 'paymenttypes')"><span></span><span>Payment Types</span></a></li>
					</ul>
				</li>
			</ul>
			<div class="account-bar">
				<asp:LoginName ID="LoginName1" runat="server" />
				<span class="text-btn" ng-click="ChangePassword()">[Change Password]</span>
				<br />
				<select ng-model="FontSize" ng-options="fontSize.Label for fontSize in FontSizeOptions"></select>
				<asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="btn logout-btn" />
			</div>
		</div>
		<div class="header-fade"></div>

		<div class="main" id="main-scroller">

			<div ng-show-message="Message != null" id="message">
				<ul ng-class="Message.cssclass">
					<li ng-bind="Message.text"></li>
				</ul>
			</div>
			
			<div id="dashboard" class="layoutcols" ng-show="CurrentSection == 'dashboard'">
				<div class="layoutcol layoutcol_1_2">
					<div>

						<div class="component">
							<h3>Affiliate Quick Search</h3>
							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<input type="text" placeholder="ID or Search Text" class="short-textinput" ng-model="FilterAffiliates.QuickSearch" ng-keypress="AffiliateQuickSearchEnter($event, FilterAffiliates.QuickSearch)" />
											<button type="button" class="btn" ng-click="AffiliateQuickSearch(FilterAffiliates.QuickSearch);">Go</button>
										</dd>
									</dl>
								</div>
							</div>
						</div>
						
						<div class="component">
							<h3>Favourite Operators</h3>
							<div>
								<div class="form small-form">
									<dl ng-show="Dashboard.FavouriteOperators.length == 0">
										<dd>
											<span>No favourites set</span>
										</dd>
									</dl>
									<dl ng-repeat="favoperator in Dashboard.FavouriteOperators">
										<dd>
											<a class="text-btn" ng-href="{{GetLink('operator', favoperator.OperatorID)}}" ng-click="RenderSection('operator', favoperator.OperatorID)">
												<span ng-bind="favoperator.OperatorName"></span>
											</a>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component">
							<h3>Latest Operators</h3>
							<div>
								<div class="form small-form">
									<dl ng-repeat="operator in Dashboard.LatestOperators">
										<dd>
											<a class="text-btn" ng-href="{{GetLink('operator', operator.OperatorID)}}" ng-click="RenderSection('operator', operator.OperatorID)">
												<span ng-bind="operator.OperatorName"></span>
											</a>
										</dd>
									</dl>
								</div>
							</div>
						</div>


						<div class="component">
							<h3>Database Info</h3>
							<div>
								<div class="form tiny-form">
									<dl>
										<dt><span>Total Affiliates:</span></dt>
										<dd><span ng-bind="Dashboard.DbSummary.TotalAffiliates"></span></dd>
									</dl>
									<dl>
										<dt><span>Active Affiliates:</span></dt>
										<dd><span ng-bind="Dashboard.DbSummary.ActiveAffiliates"></span></dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component">
							<h3>Deals Breakdown</h3>
							<div>
								<div class="form tiny-form">
									<dl ng-repeat="dealBreakdown in Dashboard.DbSummary.DealsBreakdown">
										<dt><span ng-bind="dealBreakdown[0]"></span>:</dt>
										<dd><span ng-bind="dealBreakdown[1]"></span></dd>
									</dl>
								</div>
							</div>
						</div>



					</div>
				</div>
				<div class="layoutcol layoutcol_2_2">
					<div>

						<div class="component">
							<h3>
								<span>Acc. Manager Stats</span>
								<select ng-model="DashboardFilter.UserStats.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="DashboardUpdateUserStats();"></select>
								<date-range-picker style="display:inline-block;" ng-show="DashboardFilter.UserStats.DateType.Label == 'Custom'" dpid="dashboarduserstatsFilterDateRange" dpfrom="DashboardFilter.UserStats.From" dpto="DashboardFilter.UserStats.To" dponchange="DashboardUpdateUserStats();"></date-range-picker>
							</h3>
							<div>
								<div id="acc-manager-stats-chart-container"></div>
							</div>
						</div>


						<div class="component">
							<h3 class="has-pager">
								<span>
									<span>Affiliate Deals Pending</span>
									<select ng-model="DashboardFilter.PendingDeals.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="DashboardUpdatePendingDeals();"></select>
									<date-range-picker style="display:inline-block;" ng-show="DashboardFilter.PendingDeals.DateType.Label == 'Custom'" dpid="dashboardPendingDealsFilterDateRange" dpfrom="DashboardFilter.PendingDeals.From" dpto="DashboardFilter.PendingDeals.To" dponchange="DashboardUpdatePendingDeals();"></date-range-picker>

									<select ng-model="DashboardFilter.PendingDeals.GroupByOption" ng-options="groupByOption.Label for groupByOption in GroupByOptions track by groupByOption.Value" ng-change="DashboardUpdatePendingDeals();"></select>

									<label>
										<input type="checkbox" ng-model="DashboardFilter.PendingDeals.AllUsers" ng-checked="DashboardFilter.PendingDeals.AllUsers" ng-change="DashboardUpdatePendingDeals();" />
										<span>All Users</span>
									</label>
								</span>
								<pager-header pagerobj="DashboardFilter.PendingDeals.Pager" extendedheader="false"></pager-header>
							</h3>
							<div ng-show="Dashboard.PendingDeals.length == 0">
								<span>None</span>
							</div>
							<div ng-show="Dashboard.PendingDeals.length > 0">
								<table class="table-list" ng-show="DashboardFilter.PendingDeals.GroupByOption.Value == 'affiliate'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Operators</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.PendingDeals">
										<td><a class="text-btn" ng-bind="deal.affiliate_id" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.affiliate_name"></span></td>
										<td>
											<a ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.affiliate_primary_contact_detail.substr(7)}}?body={{deal.affiliate_name}}" ng-bind="deal.affiliate_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.affiliate_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="client in deal.clients.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="client.split('|')[1]" ng-href="{{GetLink('operator', client.split('|')[0])}}" ng-click="RenderSection('operator', client.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>
								<table class="table-list" ng-show="DashboardFilter.PendingDeals.GroupByOption.Value == 'operator'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Affiliates</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.PendingDeals">
										<td><a class="text-btn" ng-bind="deal.client_id" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><span ng-bind="deal.client_name"></span></td>
										<td>
											<a ng-show="deal.client_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.client_primary_contact_detail.substr(7)}}?body={{deal.client_name}}" ng-bind="deal.client_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.client_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.client_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="affiliate in deal.affiliates.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="affiliate.split('|')[1]" ng-href="{{GetLink('affiliate', affiliate.split('|')[0])}}" ng-click="RenderSection('affiliate', affiliate.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>

								<table class="table-list" ng-show="DashboardFilter.PendingDeals.GroupByOption.Value == 'none'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Status</span></th>
										<th><span>Deal Set</span></th>
										<th><span>Info</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.PendingDeals">
										<td><span ng-bind="deal.deal_id"></span></td>
										<td><a class="text-btn" ng-bind="deal.client_name" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><a class="text-btn" ng-bind="deal.affiliate_name" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.deal_status_name"></span></td>
										<td><span ng-bind="FormatTimestampString(deal.date_set, 'd M, y')"></span></td>
										<td><span ng-bind="deal.deal_info_name"></span></td>
										<td><span ng-bind="deal.AssignedUserName"></span></td>
									</tr>
								</table>

							</div>
							<h3 class="has-pager">
								<pager-footer pagerobj="DashboardFilter.PendingDeals.Pager"></pager-footer>
							</h3>
						</div>
								
						<div class="component">
							<h3 class="has-pager">
								<span>
									<span>Affiliate Deals Confirmed Active</span>
									<select ng-model="DashboardFilter.ConfirmedActiveDeals.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="DashboardUpdateConfirmedActiveDeals();"></select>
									<date-range-picker style="display:inline-block;" ng-show="DashboardFilter.ConfirmedActiveDeals.DateType.Label == 'Custom'" dpid="dashboardConfirmedActiveDealsFilterDateRange" dpfrom="DashboardFilter.ConfirmedActiveDeals.From" dpto="DashboardFilter.ConfirmedActiveDeals.To" dponchange="DashboardUpdateConfirmedActiveDeals();"></date-range-picker>
									<select ng-model="DashboardFilter.ConfirmedActiveDeals.GroupByOption" ng-options="groupByOption.Label for groupByOption in GroupByOptions track by groupByOption.Value" ng-change="DashboardUpdateConfirmedActiveDeals();"></select>
									<label>
										<input type="checkbox" ng-model="DashboardFilter.ConfirmedActiveDeals.AllUsers" ng-checked="DashboardFilter.ConfirmedActiveDeals.AllUsers" ng-change="DashboardUpdateConfirmedActiveDeals();" />
										<span>All Users</span>
									</label>
								</span>
								<pager-header pagerobj="DashboardFilter.ConfirmedActiveDeals.Pager" extendedheader="false"></pager-header>
							</h3>
							<div ng-show="Dashboard.ConfirmedActiveDeals.length == 0">
								<span>None</span>
							</div>
							<div ng-show="Dashboard.ConfirmedActiveDeals.length > 0">
								<table class="table-list" ng-show="DashboardFilter.ConfirmedActiveDeals.GroupByOption.Value == 'affiliate'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Operators</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedActiveDeals">
										<td><a class="text-btn" ng-bind="deal.affiliate_id" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.affiliate_name"></span></td>
										<td>
											<a ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.affiliate_primary_contact_detail.substr(7)}}?body={{deal.affiliate_name}}" ng-bind="deal.affiliate_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.affiliate_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="client in deal.clients.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="client.split('|')[1]" ng-href="{{GetLink('operator', client.split('|')[0])}}" ng-click="RenderSection('operator', client.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>
								<table class="table-list" ng-show="DashboardFilter.ConfirmedActiveDeals.GroupByOption.Value == 'operator'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Affiliates</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedActiveDeals">
										<td><a class="text-btn" ng-bind="deal.client_id" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><span ng-bind="deal.client_name"></span></td>
										<td>
											<a ng-show="deal.client_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.client_primary_contact_detail.substr(7)}}?body={{deal.client_name}}" ng-bind="deal.client_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.client_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.client_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="affiliate in deal.affiliates.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="affiliate.split('|')[1]" ng-href="{{GetLink('affiliate', affiliate.split('|')[0])}}" ng-click="RenderSection('affiliate', affiliate.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>

								<table class="table-list" ng-show="DashboardFilter.ConfirmedActiveDeals.GroupByOption.Value == 'none'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Status</span></th>
										<th><span>Deal Set</span></th>
										<th><span>Info</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedActiveDeals">
										<td><span ng-bind="deal.deal_id"></span></td>
										<td><a class="text-btn" ng-bind="deal.client_name" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><a class="text-btn" ng-bind="deal.affiliate_name" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.deal_status_name"></span></td>
										<td><span ng-bind="FormatTimestampString(deal.date_set, 'd M, y')"></span></td>
										<td><span ng-bind="deal.deal_info_name"></span></td>
										<td><span ng-bind="deal.AssignedUserName"></span></td>
									</tr>
								</table>
							</div>
							
							<h3 class="has-pager">
								<pager-footer pagerobj="DashboardFilter.ConfirmedActiveDeals.Pager"></pager-footer>
							</h3>
						</div>

						<div class="component">
							<h3 class="has-pager">
								<span>
									<span>Affiliate Deals Confirmed Inactive</span>
									<select ng-model="DashboardFilter.ConfirmedInactiveDeals.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="DashboardUpdateConfirmedInactiveDeals();"></select>
									<date-range-picker style="display:inline-block;" ng-show="DashboardFilter.ConfirmedInactiveDeals.DateType.Label == 'Custom'" dpid="dashboardConfirmedInactiveDealsFilterDateRange" dpfrom="DashboardFilter.ConfirmedInactiveDeals.From" dpto="DashboardFilter.ConfirmedInactiveDeals.To" dponchange="DashboardUpdateConfirmedInactiveDeals();"></date-range-picker>
									<select ng-model="DashboardFilter.ConfirmedInactiveDeals.GroupByOption" ng-options="groupByOption.Label for groupByOption in GroupByOptions track by groupByOption.Value" ng-change="DashboardUpdateConfirmedInactiveDeals();"></select>
									<label>
										<input type="checkbox" ng-model="DashboardFilter.ConfirmedInactiveDeals.AllUsers" ng-checked="DashboardFilter.ConfirmedInactiveDeals.AllUsers" ng-change="DashboardUpdateConfirmedInactiveDeals();" />
										<span>All Users</span>
									</label>
								</span>
								<pager-header pagerobj="DashboardFilter.ConfirmedInactiveDeals.Pager" extendedheader="false"></pager-header>
							</h3>
							<div ng-show="Dashboard.ConfirmedInactiveDeals.length == 0">
								<span>None</span>
							</div>
							<div ng-show="Dashboard.ConfirmedInactiveDeals.length > 0">
								<table class="table-list" ng-show="DashboardFilter.ConfirmedInactiveDeals.GroupByOption.Value == 'affiliate'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Operators</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedInactiveDeals">
										<td><a class="text-btn" ng-bind="deal.affiliate_id" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.affiliate_name"></span></td>
										<td>
											<a ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.affiliate_primary_contact_detail.substr(7)}}?body={{deal.affiliate_name}}" ng-bind="deal.affiliate_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.affiliate_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="client in deal.clients.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="client.split('|')[1]" ng-href="{{GetLink('operator', client.split('|')[0])}}" ng-click="RenderSection('operator', client.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>

								<table class="table-list" ng-show="DashboardFilter.ConfirmedInactiveDeals.GroupByOption.Value == 'operator'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Affiliates</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedInactiveDeals">
										<td><a class="text-btn" ng-bind="deal.client_id" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><span ng-bind="deal.client_name"></span></td>
										<td>
											<a ng-show="deal.client_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.client_primary_contact_detail.substr(7)}}?body={{deal.client_name}}" ng-bind="deal.client_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.client_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.client_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="affiliate in deal.affiliates.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="affiliate.split('|')[1]" ng-href="{{GetLink('affiliate', affiliate.split('|')[0])}}" ng-click="RenderSection('affiliate', affiliate.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>

								<table class="table-list" ng-show="DashboardFilter.ConfirmedInactiveDeals.GroupByOption.Value == 'none'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Status</span></th>
										<th><span>Deal Set</span></th>
										<th><span>Info</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.ConfirmedInactiveDeals">
										<td><span ng-bind="deal.deal_id"></span></td>
										<td><a class="text-btn" ng-bind="deal.client_name" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><a class="text-btn" ng-bind="deal.affiliate_name" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.deal_status_name"></span></td>
										<td><span ng-bind="FormatTimestampString(deal.date_set, 'd M, y')"></span></td>
										<td><span ng-bind="deal.deal_info_name"></span></td>
										<td><span ng-bind="deal.AssignedUserName"></span></td>
									</tr>
								</table>

							</div>
							<h3 class="has-pager">
								<pager-footer pagerobj="DashboardFilter.ConfirmedInactiveDeals.Pager"></pager-footer>
							</h3>
						</div>

						<div class="component">
							<h3 class="has-pager">
								<span>
									<span>Affiliate Deals Awaiting Response</span>
									<select ng-model="DashboardFilter.AwaitingResponseDeals.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="DashboardUpdateAwaitingResponseDeals();"></select>
									<date-range-picker style="display:inline-block;" ng-show="DashboardFilter.AwaitingResponseDeals.DateType.Label == 'Custom'" dpid="dashboardAwaitingResponseDealsFilterDateRange" dpfrom="DashboardFilter.AwaitingResponseDeals.From" dpto="DashboardFilter.AwaitingResponseDeals.To" dponchange="DashboardUpdateAwaitingResponseDeals();"></date-range-picker>
									<select ng-model="DashboardFilter.AwaitingResponseDeals.GroupByOption" ng-options="groupByOption.Label for groupByOption in GroupByOptions track by groupByOption.Value" ng-change="DashboardUpdateAwaitingResponseDeals();"></select>
									<label>
										<input type="checkbox" ng-model="DashboardFilter.AwaitingResponseDeals.AllUsers" ng-checked="DashboardFilter.AwaitingResponseDeals.AllUsers" ng-change="DashboardUpdateAwaitingResponseDeals();" />
										<span>All Users</span>
									</label>
								</span>
								<pager-header pagerobj="DashboardFilter.AwaitingResponseDeals.Pager" extendedheader="false"></pager-header>
							</h3>
							<div ng-show="Dashboard.AwaitingResponseDeals.length == 0">
								<span>None</span>
							</div>
							<div ng-show="Dashboard.AwaitingResponseDeals.length > 0">
								<table class="table-list" ng-show="DashboardFilter.AwaitingResponseDeals.GroupByOption.Value == 'affiliate'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Operators</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.AwaitingResponseDeals">
										<td><a class="text-btn" ng-bind="deal.affiliate_id" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.affiliate_name"></span></td>
										<td>
											<a ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.affiliate_primary_contact_detail.substr(7)}}?body={{deal.affiliate_name}}" ng-bind="deal.affiliate_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.affiliate_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.affiliate_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="client in deal.clients.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="client.split('|')[1]" ng-href="{{GetLink('operator', client.split('|')[0])}}" ng-click="RenderSection('operator', client.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>
								<table class="table-list" ng-show="DashboardFilter.AwaitingResponseDeals.GroupByOption.Value == 'operator'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Primary Contact</span></th>
										<th><span>Affiliates</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.AwaitingResponseDeals">
										<td><a class="text-btn" ng-bind="deal.client_id" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><span ng-bind="deal.client_name"></span></td>
										<td>
											<a ng-show="deal.client_primary_contact_detail.indexOf('Email: ') != -1" href="mailto:{{deal.client_primary_contact_detail.substr(7)}}?body={{deal.client_name}}" ng-bind="deal.client_primary_contact_detail.substr(7)"></a>
											<span ng-show="deal.client_primary_contact_detail.indexOf('Email: ') == -1" ng-bind="deal.client_primary_contact_detail"></span>
										</td>
										<td class="breakable">
											<span ng-repeat="affiliate in deal.affiliates.split(',')">
												<span ng-show="$index != 0">/</span>
												<a class="text-btn" ng-bind="affiliate.split('|')[1]" ng-href="{{GetLink('affiliate', affiliate.split('|')[0])}}" ng-click="RenderSection('affiliate', affiliate.split('|')[0]);"></a>
											</span>
										</td>
										<td><span ng-bind="deal.UserName"></span></td>
									</tr>
								</table>

								<table class="table-list" ng-show="DashboardFilter.AwaitingResponseDeals.GroupByOption.Value == 'none'">
									<tr>
										<th><span>ID</span></th>
										<th><span>Operator</span></th>
										<th><span>Affiliate</span></th>
										<th><span>Status</span></th>
										<th><span>Deal Set</span></th>
										<th><span>Info</span></th>
										<th><span>Acc. Manager</span></th>
									</tr>
									<tr ng-repeat="deal in Dashboard.AwaitingResponseDeals">
										<td><span ng-bind="deal.deal_id"></span></td>
										<td><a class="text-btn" ng-bind="deal.client_name" ng-href="{{GetLink('operator', deal.client_id)}}" ng-click="RenderSection('operator', deal.client_id);"></a></td>
										<td><a class="text-btn" ng-bind="deal.affiliate_name" ng-href="{{GetLink('affiliate', deal.affiliate_id)}}" ng-click="RenderSection('affiliate', deal.affiliate_id);"></a></td>
										<td><span ng-bind="deal.deal_status_name"></span></td>
										<td><span ng-bind="FormatTimestampString(deal.date_set, 'd M, y')"></span></td>
										<td><span ng-bind="deal.deal_info_name"></span></td>
										<td><span ng-bind="deal.AssignedUserName"></span></td>
									</tr>
								</table>
							</div>
							<h3 class="has-pager">
								<pager-footer pagerobj="DashboardFilter.AwaitingResponseDeals.Pager"></pager-footer>
							</h3>
						</div>

					</div>
				</div>
			</div>

			<div id="affiliates" class="layoutcols" ng-show="CurrentSection == 'affiliates'">
				<div class="layoutcol layoutcol_1_2">
					<div>
						<div class="component">
							<h3>Affiliate Quick Search</h3>
							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<input type="text" placeholder="ID or Search Text" class="short-textinput" ng-model="FilterAffiliates.QuickSearch" ng-keypress="AffiliateQuickSearchEnter($event, FilterAffiliates.QuickSearch)" />
											<button type="button" class="btn" ng-click="AffiliateQuickSearch(FilterAffiliates.QuickSearch);">Go</button>
										</dd>
									</dl>
								</div>
							</div>
						</div>


						<div class="component">
							<h3>Filter Results</h3>
							<div>
								<div class="form small-form" ng-keypress="AffiliatesFilterSearchEnter($event)">
									<dl>
										<dt>
											<span>Search:</span>
										</dt>
										<dd>
											<input type="text" placeholder="Search Text" ng-model="FilterAffiliates.SearchStr" />
										</dd>
									</dl>

									<!--AFFMANAGER-->
									<dl class="expander" ng-class="FilterAffiliates.AffiliateManagerFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.AffiliateManagerFilterOn = !FilterAffiliates.AffiliateManagerFilterOn" title="Toggle">
											<span>Affiliate Managers</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<span class="checkbox-list">
												<label ng-repeat="user in FilterAffiliates.AffiliateManagerList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="user.IsSelected" ng-model="user.IsSelected" />
													<span ng-bind="user.UserName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--INCLUDE UNASSIGNED-->
									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="FilterAffiliates.IncludeUnassigned" ng-checked="FilterAffiliates.IncludeUnassigned" />
												<span>Include Unassigned</span>
											</label>
										</dd>
									</dl>

									<!--SIZE-->
									<dl>
										<dt>
											<span>Size</span>
										</dt>
										<dd>
											<select ng-options="size.Label for size in FilterAffiliates.SizeList track by size.ID" ng-model="FilterAffiliates.SizeSelected"></select>
										</dd>
									</dl>
									
									<!--CONTACTABLE-->
									<dl>
										<dt>
											<span>Contactable</span>
										</dt>
										<dd>
											<select ng-options="contactable.Label for contactable in FilterAffiliates.ContactableList track by contactable.ID" ng-model="FilterAffiliates.ContactableSelected"></select>
										</dd>
									</dl>
									
									<!--MAXMAIL-->
									<dl>
										<dt>
											<span>MaxMail</span>
										</dt>
										<dd>
											<select ng-options="maxmail.Label for maxmail in FilterAffiliates.MaxMailList track by maxmail.ID" ng-model="FilterAffiliates.MaxMailSelected"></select>
										</dd>
									</dl>

									<!--ACTIVE-->
									<dl>
										<dt>
											<span>Affiliate Status:</span>
										</dt>
										<dd>
											<select ng-options="affstatus.Label for affstatus in FilterAffiliates.AffStatusList track by affstatus.ID" ng-model="FilterAffiliates.AffStatusSelected"></select>
										</dd>
									</dl>

									<!--OPERATORS-->
									<dl class="expander" ng-class="FilterAffiliates.OperatorFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.OperatorFilterOn = !FilterAffiliates.OperatorFilterOn" title="Toggle">
											<span>Operators</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="operatoroption.DealStatusName for operatoroption in FilterAffiliates.OperatorOptions track by operatoroption.DealStatusID" ng-model="FilterAffiliates.OperatorOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="operator in FilterAffiliates.OperatorList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="operator.IsSelected" ng-model="operator.IsSelected" />
													<span ng-bind="operator.OperatorName"></span>
												</label>
											</span>
										</dd>
									</dl>
									
									<!--COUNTRIES-->
									<dl class="expander" ng-class="FilterAffiliates.CountryFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.CountryFilterOn = !FilterAffiliates.CountryFilterOn" title="Toggle">
											<span>Countries</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="countryoption.Label for countryoption in FilterAffiliates.CountryOptions track by countryoption.ID" ng-model="FilterAffiliates.CountryOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="country in FilterAffiliates.CountryList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="country.IsSelected" ng-model="country.IsSelected" />
													<span ng-bind="country.CountryName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--AFFILIATETYPES-->
									<dl class="expander" ng-class="FilterAffiliates.AffiliateTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.AffiliateTypeFilterOn = !FilterAffiliates.AffiliateTypeFilterOn" title="Toggle">
											<span>Affiliate Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="affiliatetypeoption.Label for affiliatetypeoption in FilterAffiliates.AffiliateTypeOptions track by affiliatetypeoption.ID" ng-model="FilterAffiliates.AffiliateTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="affiliatetype in FilterAffiliates.AffiliateTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="affiliatetype.IsSelected" ng-model="affiliatetype.IsSelected" />
													<span ng-bind="affiliatetype.AffiliateTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--DEALTYPES-->
									<dl class="expander" ng-class="FilterAffiliates.DealTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.DealTypeFilterOn = !FilterAffiliates.DealTypeFilterOn" title="Toggle">
											<span>Deal Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="dealtypeoption.Label for dealtypeoption in FilterAffiliates.DealTypeOptions track by dealtypeoption.ID" ng-model="FilterAffiliates.DealTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="dealtype in FilterAffiliates.DealTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="dealtype.IsSelected" ng-model="dealtype.IsSelected" />
													<span ng-bind="dealtype.DealTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--MARKETINGTYPES-->
									<dl class="expander" ng-class="FilterAffiliates.MarketingTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterAffiliates.MarketingTypeFilterOn = !FilterAffiliates.MarketingTypeFilterOn" title="Toggle">
											<span>Marketing Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="marketingtypeoption.Label for marketingtypeoption in FilterAffiliates.MarketingTypeOptions track by marketingtypeoption.ID" ng-model="FilterAffiliates.MarketingTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="marketingtype in FilterAffiliates.MarketingTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="marketingtype.IsSelected" ng-model="marketingtype.IsSelected" />
													<span ng-bind="marketingtype.MarketingTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<dl>
										<dt></dt>
										<dd>
											<button type="button" ng-click="AffiliatesSearchSubmit()" class="btn"><span>Search</span></button>
											<button type="button" ng-click="AffiliatesSearchReport()" class="btn"><span>Save As Report</span></button>
										</dd>
									</dl>

								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="layoutcol layoutcol_2_2">
					<div>
						<div class="component">
							<h3>Affiliates</h3>
							<div>
								<pager-header pagerobj="FilterAffiliates.Pager" extendedheader="false"></pager-header>
								<table class="table-list">
									<tr>
										<th ng-show="FilterAffiliates.ShowBatchOperations">
											<span>
												<input type="checkbox" ng-click="AffiliatesBatchSelectAll()" ng-checked="AffiliatesBatchCheckAllSelected()" />
										    </span>
										</th>
										<th><span>ID</span></th>
										<th><span>Acc. Manager</span></th>
										<th><span>Name</span></th>
										<th><span>Primary URL</span></th>
										<th><span>Primary Contact</span></th>
										<th></th>
									</tr>
									<tr ng-repeat="affiliate in Affiliates" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
										<td ng-show="FilterAffiliates.ShowBatchOperations">
											<span>
												<input type="checkbox" ng-checked="affiliate.IsSelected" ng-model="affiliate.IsSelected" />
										    </span>
										</td>
										<td><a class="text-btn" ng-bind="affiliate.AffiliateID" ng-href="{{GetLink('affiliate', affiliate.AffiliateID)}}" ng-click="RenderSection('affiliate', affiliate.AffiliateID);"></a></td>
										<td><span ng-bind="affiliate.AffManagerUser.UserName"></span></td>
										<td><a class="text-btn" ng-bind="affiliate.AffiliateName" ng-href="{{GetLink('affiliate', affiliate.AffiliateID)}}" ng-click="RenderSection('affiliate', affiliate.AffiliateID);"></a></td>
										<td class="breakable"><a href="{{ValidifyUrl(affiliate.PrimaryURL.Url)}}" ng-bind="affiliate.PrimaryURL.Url" target="_blank"></a></td>
										<td class="breakable">
											<a href="mailto:{{affiliate.PrimaryContactDetail.ContactDetailText}}?body={{affiliate.AffiliateName}}" ng-bind="affiliate.PrimaryContactDetail.ContactDetailText" ng-show="affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeID == 1"></a>
											<span ng-show="affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeID != 1">
												<span ng-bind="affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeName"></span>
												<span>: </span>
												<span ng-bind="affiliate.PrimaryContactDetail.ContactDetailText"></span>
											</span>
										</td>
										<td>
											<button type="button" class="btn" ng-href="{{GetLink('affiliate', affiliate.AffiliateID)}}" ng-click="RenderSection('affiliate', affiliate.AffiliateID);">View</button>
										</td>
									</tr>
								</table>
								<pager-footer pagerobj="FilterAffiliates.Pager"></pager-footer>
							</div>
						</div>
					
					
						<div class="component expander" ng-class="FilterAffiliates.ShowBatchOperations ? 'expander-expanded' : 'expander-collapsed'" ng-show="IsUserInRole('BatchOperations')">
							<h3 ng-click="FilterAffiliates.ShowBatchOperations = !FilterAffiliates.ShowBatchOperations" title="Toggle">
								<span>Batch Operations</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form">
									<dl>
										<dt><span>Re-assign Selected:</span></dt>
										<dd>
											<select ng-options="user.UserName for user in FilterAffiliates.AffiliateManagerList track by user.UserId" ng-model="FilterAffiliates.BatchOperationSelectedAffiliateManager"></select>
											<button type="button" class="btn" ng-click="AffiliatesReassign();"><span>Go</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

			<div id="addaffiliate" class="layoutcols" ng-show="CurrentSection == 'addaffiliate'">
				<div class="layoutcol layoutcol_1_1">
					<div>
						<div class="component">
							<h3>New Affiliate</h3>
							<div>
								<div class="form">
									<dl>
										<dt><span>Full Name:</span></dt>
										<dd><input type="text" placeholder="Full Name" ng-model="NewAffiliate.AffiliateName" ng-blur="ValidateRequired(NewAffiliate.AffiliateName, NewAffiliate.Errors, 'AffiliateName');" form-error="NewAffiliate.Errors.Get('AffiliateName')" /></dd>
									</dl>
									<dl>
										<dt><span>First Name:</span></dt>
										<dd><input type="text" placeholder="First Name" ng-model="NewAffiliate.ContactName" ng-blur="ValidateRequired(NewAffiliate.ContactName, NewAffiliate.Errors, 'ContactName');" form-error="NewAffiliate.Errors.Get('ContactName')" /></dd>
									</dl>

									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="NewAffiliate.AllowContact" ng-checked="NewAffiliate.AllowContact" />
												<span>Is Contactable</span>
											</label>
										</dd>
									</dl>
									<dl>
										<dt><span>Account Manager:</span></dt>
										<dd>
											<select ng-options="affmanager.UserName for affmanager in NewAffiliate.AffiliateManagerList track by affmanager.UserId" ng-model="NewAffiliate.AffManagerUser"></select>
										</dd>
									</dl>
									<dl>
										<dt><span>Affiliate Size:</span></dt>
										<dd>
											<select ng-options="affsize.Label for affsize in AffiliateSizes track by affsize.Value" ng-model="NewAffiliate.AffiliateSize"></select>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="NewAffiliate.MaxMail" ng-checked="NewAffiliate.MaxMail" />
												<span>MaxMail:</span>
											</label>
										</dd>
									</dl>
									<dl>
										<dt><span>Special Notes:</span></dt>
										<dd>
											<textarea ng-model="NewAffiliate.Comment" placeholder="Special Notes"></textarea>
										</dd>
									</dl>
									<dl>
										<dt><span>Primary Contact Detail:</span></dt>
										<dd>
											<span class="block">
												<select ng-options="ocdt.ContactDetailTypeName for ocdt in NewAffiliate.ContactDetailTypes track by ocdt.ContactDetailTypeID" ng-model="NewAffiliate.PrimaryContactDetailType"></select>
											</span>
											<span class="block">
												<input type="text" ng-model="NewAffiliate.PrimaryContactDetailText" placeholder="Contact Detail" ng-blur="ValidateRequired(NewAffiliate.PrimaryContactDetailText, NewAffiliate.Errors, 'PrimaryContactDetailText');" form-error="NewAffiliate.Errors.Get('PrimaryContactDetailText')" />
											</span>
										</dd>
									</dl>
									<dl>
										<dt><span>Primary URL:</span></dt>
										<dd>
											<input type="text" placeholder="Primary URL" ng-model="NewAffiliate.PrimaryUrlText" ng-blur="ValidateRequired(NewAffiliate.PrimaryUrlText, NewAffiliate.Errors, 'PrimaryUrlText');" form-error="NewAffiliate.Errors.Get('PrimaryUrlText')" />
										</dd>
									</dl>

									<dl>
										<dt><span>Countries:</span></dt>
										<dd>
											<span class="checkbox-list" form-error="NewAffiliate.Errors.Get('Countries')">
												<label ng-repeat="country in NewAffiliate.Countries" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="country.IsSelected" ng-model="country.IsSelected">
													<span ng-bind="country.CountryName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<dl>
										<dt><span>Affiliate Type(s):</span></dt>
										<dd>
											<span class="checkbox-list" form-error="NewAffiliate.Errors.Get('AffiliateTypes')">
												<label ng-repeat="affiliateType in NewAffiliate.AffiliateTypes">
													<input type="checkbox" ng-model="affiliateType.IsSelected" ng-checked="affiliateType.IsSelected" />
													<span ng-bind="affiliateType.AffiliateTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<dl>
										<dt><span>Deal Type(s):</span></dt>
										<dd>
											<span class="checkbox-list" form-error="NewAffiliate.Errors.Get('DealTypes')">
												<label ng-repeat="dealtype in NewAffiliate.DealTypes" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="dealtype.IsSelected" ng-model="dealtype.IsSelected" />
													<span ng-bind="dealtype.DealTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-href="/area/dashboard/" ng-click="RenderSection('dashboard')"><span></span><span>Cancel</span></button>
											<button type="button" class="btn" ng-click="AddAffiliateSubmit()" form-error="NewAffiliate.Errors.Get('Main')"><span>Create Affiliate</span></button>
										</dd>
									</dl>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="affiliate" class="layoutcols layoutcol_fixedleft_400" ng-show="CurrentSection == 'affiliate'">
				<div class="layoutcol layoutcol_1_2">

					<div>
						<div class="component">
							<div>
								<a class="btn" ng-href="{{GetLink('affiliates')}}" ng-click="RenderSection('affiliates')"><span>&lt;&lt; Back to Search Results</span></a>
							</div>
						</div>

						<div class="component">
							<h3>Affiliate Quick Search</h3>
							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<input type="text" placeholder="ID or Search Text" class="short-textinput" ng-model="FilterAffiliates.QuickSearch" ng-keypress="AffiliateQuickSearchEnter($event, FilterAffiliates.QuickSearch)" />
											<button type="button" class="btn" ng-click="AffiliateQuickSearch(FilterAffiliates.QuickSearch);">Go</button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component" ng-show="Affiliate != null">
							<h3>Affiliate Details</h3>
							<div>
								<div class="form medium-form">
									<dl>
										<dt><span>Affiliate ID:</span></dt>
										<dd><span ng-bind="Affiliate.AffiliateID"></span></dd>
									</dl>
									<dl>
										<dt><span>Full Name:</span></dt>
										<dd><span ng-bind="Affiliate.AffiliateName"></span></dd>
									</dl>
									<dl>
										<dt><span>First Name:</span></dt>
										<dd><span ng-bind="Affiliate.ContactName"></span></dd>
									</dl>
									<dl>
										<dt><span>Contactable:</span></dt>
										<dd><span ng-bind="Affiliate.AllowContact ? 'Yes' : 'No'"></span></dd>
									</dl>
									<dl>
										<dt><span>Acc. Manager:</span></dt>
										<dd><span ng-bind="Affiliate.AffManagerUser.UserName"></span></dd>
									</dl>
									<dl>
										<dt><span>Affiliate Size:</span></dt>
										<dd><span ng-bind="AffiliateSizes[Affiliate.AffiliateSize].Label"></span></dd>
									</dl>
									<dl>
										<dt><span>MaxMail:</span></dt>
										<dd><span ng-bind="Affiliate.MaxMail ? 'Yes' : 'No'"></span></dd>
									</dl>
									<dl>
										<dt><span>Active:</span></dt>
										<dd><span ng-bind="Affiliate.IsActive ? 'Yes' : 'No'"></span></dd>
									</dl>
									<dl>
										<dt><span>Special Notes:</span></dt>
										<dd><span ng-bind="Affiliate.Comment"></span></dd>
									</dl>
									<dl ng-class="Affiliate.ContactDetails.length == 1 ? '' : 'expander ' + (AffiliateFilter.ViewContactDetailsModeOn ? 'expander-expanded' : 'expander-collapsed')">
										<dt>
											<span>Primary Contact:</span>
										</dt>
										<dd>
											<a ng-bind="Affiliate.PrimaryContactDetail.ContactDetailText" href="mailto:{{Affiliate.PrimaryContactDetail.ContactDetailText}}?body={{Affiliate.AffiliateName}}" ng-show="Affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeID == 1" class="breakable"></a>
											<span ng-show="Affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeID != 1" class="breakable">
												<span ng-bind="Affiliate.PrimaryContactDetail.ContactType.ContactDetailTypeName"></span>
												<span>: </span>
												<span ng-bind="Affiliate.PrimaryContactDetail.ContactDetailText"></span>
											</span>
											<span class="icon" ng-click="AffiliateFilter.ViewContactDetailsModeOn = !AffiliateFilter.ViewContactDetailsModeOn" title="Toggle"></span>
										</dd>
									</dl>
									<dl ng-repeat="contactDetail in Affiliate.ContactDetails" ng-show="AffiliateFilter.ViewContactDetailsModeOn && !contactDetail.IsPrimary">
										<dt><span ng-bind="contactDetail.ContactType.ContactDetailTypeName" ng-class="!contactDetail.IsActive ? 'inactive' : ''"></span></dt>
										<dd>
											<a ng-bind="contactDetail.ContactDetailText" ng-show="contactDetail.ContactType.ContactDetailTypeID == '1'" href="mailto:{{contactDetail.ContactDetailText}}?body={{Affiliate.AffiliateName}}" ng-class="!contactDetail.IsActive ? 'inactive' : ''" class="breakable"></a>
											<span ng-bind="contactDetail.ContactDetailText" ng-show="contactDetail.ContactType.ContactDetailTypeID != '1'" ng-class="!contactDetail.IsActive ? 'inactive' : ''" class="breakable"></span>
										</dd>
									</dl>
									<dl ng-class="Affiliate.URLs.length == 1 ? '' : 'expander ' + (AffiliateFilter.ViewUrlsModeOn ? 'expander-expanded' : 'expander-collapsed')">
										<dt>
											<span>Primary URL:</span>
										</dt>
										<dd>
											<a ng-bind="Affiliate.PrimaryURL.Url" href="{{ValidifyUrl(Affiliate.PrimaryURL.Url)}}" target="_blank" class="breakable"></a>
											<span class="icon" ng-click="AffiliateFilter.ViewUrlsModeOn = !AffiliateFilter.ViewUrlsModeOn" title="Toggle"></span>
										</dd>
									</dl>
									<dl ng-repeat="url in Affiliate.URLs" ng-show="AffiliateFilter.ViewUrlsModeOn && !url.IsPrimary">
										<dt>
											<span ng-class="!url.IsActive ? 'inactive' : ''">URL:</span>
										</dt>
										<dd>
											<a ng-bind="url.Url" href="{{ValidifyUrl(url.Url)}}" target="_blank" ng-class="!url.IsActive ? 'inactive' : ''" class="breakable"></a>
										</dd>
									</dl>
									<dl>
										<dt><span>Countries:</span></dt>
										<dd><span class="block" ng-repeat="country in Affiliate.Countries | filter:{IsSelected:true}" ng-bind="country.CountryName"></span></dd>
									</dl>
									<dl>
										<dt><span>Affiliate Types:</span></dt>
										<dd><span class="block" ng-repeat="affiliateType in Affiliate.AffiliateTypes | filter:{IsSelected:true}" ng-bind="affiliateType.AffiliateTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Deal Types:</span></dt>
										<dd><span class="block" ng-repeat="dealType in Affiliate.DealTypes | filter:{IsSelected:true}" ng-bind="dealType.DealTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Marketing Types:</span></dt>
										<dd><span class="block" ng-repeat="marketingType in Affiliate.MarketingTypes | filter:{IsSelected:true}" ng-bind="marketingType.MarketingTypeName"></span></dd>
									</dl>
								</div>

							</div>
						</div>
			
					</div>
				</div>

				<div class="layoutcol layoutcol_2_2">
					<div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'profile' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Affiliate Profile</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl>
										<dt><span>Full Name:</span></dt>
										<dd><input type="text" ng-model="Affiliate.AffiliateName" placeholder="Full Name" /></dd>
									</dl>
									<dl>
										<dt><span>First Name:</span></dt>
										<dd><input type="text" ng-model="Affiliate.ContactName" placeholder="First Name" /></dd>
									</dl>
									<dl>
										<dt><span>Acc. Manager:</span></dt>
										<dd>
											<select ng-options="affmanager.UserName for affmanager in Affiliate.AffiliateManagerList track by affmanager.UserId" ng-model="Affiliate.AffManagerUser"></select>
										</dd>
									</dl>
									<dl>
										<dt><span>Affiliate Size:</span></dt>
										<dd>
											<select ng-options="affsize.Value as affsize.Label for affsize in AffiliateSizes" ng-model="Affiliate.AffiliateSize"></select>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="Affiliate.AllowContact" ng-checked="Affiliate.AllowContact" />
												<span>Is Contactable</span>
											</label>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="Affiliate.MaxMail" ng-checked="Affiliate.MaxMail" />
												<span>MaxMail:</span>
											</label>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<label>
												<input type="checkbox" ng-model="Affiliate.IsActive" ng-checked="Affiliate.IsActive" />
												<span>Is Active</span>
											</label>
										</dd>
									</dl>
									<dl>
										<dt><span>Special Notes:</span></dt>
										<dd>
											<textarea ng-model="Affiliate.Comment" placeholder="Special Notes"></textarea>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
											<button type="button" class="btn" ng-click="RemoveAffiliateSubmit()"><span>Remove</span></button>
											<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'contactdetails' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Contact Details</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="contactDetail in Affiliate.ContactDetails">
										<dd>
											<span class="block">
												<select ng-options="acdt.ContactDetailTypeName for acdt in AffiliateFilter.ContactDetailTypes track by acdt.ContactDetailTypeID" ng-model="contactDetail.ContactType"></select>
												<input type="text" ng-model="contactDetail.ContactDetailText" placeholder="Contact Detail" />
												<label>
													<input type="checkbox" ng-model="contactDetail.IsActive" ng-true-value="false" ng-false-value="true" />
													<span>Do Not Contact</span>
												</label>
												<label>
													<input type="radio" name="editAffiliateContactPrimary" ng-checked="contactDetail.IsPrimary" ng-click="AffiliateEditSetPrimaryContactDetail(contactDetail)" />
													<span>Is Primary</span>
												</label>
												<button type="button" class="btn" ng-click="AffiliateEditRemoveContactDetail(contactDetail)"><span>Remove</span></button>
											</span>
										</dd>
									</dl>
									<dl>
										<dd>
											<select ng-options="acdt.ContactDetailTypeName for acdt in AffiliateFilter.ContactDetailTypes track by acdt.ContactDetailTypeID" ng-model="AffiliateFilter.ContactDetailNewType">
												<option value="">[ Select Contact Type ]</option>
											</select>
											<input type="text" ng-model="AffiliateFilter.ContactDetailNewText" placeholder="New Contact Detail" />
											<button type="button" class="btn" ng-click="AffiliateEditAddContactDetail()"><span>Add</span></button>
										</dd>
									</dl>
									<dl>
										<dd>
											<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
											<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'urls' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Affiliate URLs</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="url in Affiliate.URLs">
										<dd>
											<input type="text" ng-model="url.Url" placeholder="URL" />
											<label>
												<input type="checkbox" ng-model="url.IsActive" ng-checked="url.IsActive" />
												<span>Active</span>
											</label>
											<label>
												<input type="radio" name="editAffiliateUrlPrimary" ng-checked="url.IsPrimary" ng-click="AffiliateEditSetPrimaryUrl(url)" />
												<span>Is Primary</span>
											</label>
											<button type="button" class="btn" ng-click="AffiliateEditRemoveUrl(url)"><span>Remove</span></button>
										</dd>
									</dl>
									<dl>
										<dd>
											<input type="text" ng-model="AffiliateFilter.UrlNewText" placeholder="New URL" />
											<button type="button" class="btn" ng-click="AffiliateEditAddUrl()"><span>Add</span></button>
										</dd>
									</dl>
									<dl>
										<dd>
											<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
											<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'countries' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Affiliate Countries</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="country in Affiliate.Countries">
										<dd>
											<label>
												<input type="checkbox" ng-model="country.IsSelected" ng-checked="country.IsSelected" />
												<span ng-bind="country.CountryName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'affiliatetypes' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Affiliate Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="affiliateType in Affiliate.AffiliateTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="affiliateType.IsSelected" ng-checked="affiliateType.IsSelected" />
												<span ng-bind="affiliateType.AffiliateTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'dealtypes' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Deal Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="dealType in Affiliate.DealTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="dealType.IsSelected" ng-checked="dealType.IsSelected" />
												<span ng-bind="dealType.DealTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
						<div class="component expander expander-expanded" ng-show="AffiliateFilter.EditMode == 'marketingtypes' || AffiliateFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Marketing Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="marketingType in Affiliate.MarketingTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="marketingType.IsSelected" ng-checked="marketingType.IsSelected" />
												<span ng-bind="marketingType.MarketingTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="AffiliateEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="AffiliateEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>

						<div class="tabs-container" ng-show="Affiliate != null && AffiliateFilter.EditMode == 'none'">
							<ul class="tabs-menu">
								<li ng-class="AffiliateFilter.ActiveTab == 'overview' ? 'active-tab' : ''"><a ng-click="AffiliateSelectTab('overview');">Overview</a></li>
								<li ng-class="AffiliateFilter.ActiveTab == 'deals' ? 'active-tab' : ''"><a ng-click="AffiliateSelectTab('deals');">Deals</a></li>
								<li ng-class="AffiliateFilter.ActiveTab == 'notes' ? 'active-tab' : ''"><a ng-click="AffiliateSelectTab('notes');">Notes</a></li>
							</ul>
						
							<div class="tab" ng-show="AffiliateFilter.ActiveTab == 'overview'">
								<div class="form medium-form">
									<dl>
										<dt></dt>
										<dd>
											<button type="button" ng-click="AffiliateEdit('edit')"><span>Edit Details</span></button>
										</dd>
									</dl>
									<!--dl>
										<dt>Edit Details:</dt>
										<dd>
											<select ng-change="AffiliateEdit(null)" ng-model="AffiliateFilter.EditMode">
												<option value="none">[ Select ]</option>
												<option value="profile">Profile</option>
												<option value="contactdetails">Contact Details</option>
												<option value="urls">URLs</option>
												<option value="countries">Countries</option>
												<option value="affiliatetypes">Affiliate Types</option>
												<option value="dealtypes">Deal Types</option>
												<option value="marketingtypes">Marketing Types</option>
											</select>
										</dd>
									</dl-->
								</div>


								<div class="component">
									<h3><span>Confirmed Active vs Confirmed Inactive</span></h3>
									<div>
										<div id="affiliate-confirmed-active-vs-confirmed-inactive-chart-container" style="width: auto; height: 150px; max-width: 100%; margin: 0 auto;"></div>
									</div>
								</div>

								<div class="component">
									<h3>Suggested Operators</h3>
									<div>
										<div class="form small-form">
											<dl ng-repeat="operatorSuggestion in Affiliate.OperatorSuggestions">
												<dd>
													<a class="text-btn" ng-href="{{GetLink('operator', operatorSuggestion.OperatorID)}}" ng-click="RenderSection('operator', operatorSuggestion.OperatorID)">
														<span ng-bind="operatorSuggestion.OperatorName"></span>
													</a>
												</dd>
											</dl>
										</div>
									</div>
								</div>

							</div>

							<div class="tab" ng-show="AffiliateFilter.ActiveTab == 'deals'">

								<button type="button" class="btn margin-bottom-10" ng-click="AffiliateDealToggleNew();" ng-show="!AffiliateFilter.DealsNewDealModeOn"><span>New Deal</span></button>
								<div class="deal" ng-show="AffiliateFilter.DealsNewDealModeOn">
									<div class="deal-header">
										<span><span>New Deal</span></span>
										<span></span>
									</div>
									<div class="deal-body">
										
										<div class="form medium-form">
											<dl>
												<dt><span>Deal Notes:</span></dt>
												<dd>
													<textarea ng-model="AffiliateFilter.DealsNewDealText" placeholder="Deal notes..."></textarea>
												</dd>
											</dl>
											<dl>
												<dt><span>Operator:</span></dt>
												<dd>
													<select ng-options="operator.OperatorName for operator in AffiliateFilter.DealsOperatorList track by operator.OperatorID" ng-model="AffiliateFilter.DealsNewOperator">
														<option value="">[ Select ]</option>
													</select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Type:</span></dt>
												<dd>
													<select ng-options="dealinfooption.DealInfoName for dealinfooption in AffiliateFilter.DealsDealInfoList track by dealinfooption.DealInfoID" ng-model="AffiliateFilter.DealsNewDealInfo">
														<option value="">[ Select ]</option>
													</select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Status:</span></dt>
												<dd>
													<select ng-options="dealstatusoption.DealStatusName for dealstatusoption in AffiliateFilter.DealsDealStatusList track by dealstatusoption.DealStatusID" ng-model="AffiliateFilter.DealsNewDealStatus">
														<option value="">[ Select ]</option>
													</select>
												</dd>
											</dl>
											<dl>
												<dt></dt>
												<dd>
													<button type="button" class="btn" ng-click="AffiliateDealToggleNew();"><span>Cancel</span></button>
													<button type="button" class="btn" ng-click="AffiliateDealNew();"><span>Add Deal</span></button>
												</dd>
											</dl>
										</div>

									</div>
								</div>
								
								<div class="component expander" ng-class="AffiliateFilter.DealsShowFilter ? 'expander-expanded' : 'expander-collapsed'">
									<h3 ng-click="AffiliateFilter.DealsShowFilter = !AffiliateFilter.DealsShowFilter" title="Toggle">
										<span>Filter Deals</span>
										<span class="icon"></span>
									</h3>
									<div class="form">
										<dl>
											<dt><span>Assigned To:</span></dt>
											<dd>
												<select ng-options="assigneduseroption.UserName for assigneduseroption in AffiliateFilter.DealsAssignedUserList track by assigneduseroption.UserId" ng-model="AffiliateFilter.DealsAssignedUserSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt><span>Deal Type:</span></dt>
											<dd>
												<select ng-options="dealinfooption.DealInfoName for dealinfooption in AffiliateFilter.DealsDealInfoList track by dealinfooption.DealInfoID" ng-model="AffiliateFilter.DealsDealInfoSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt><span>Deal Status:</span></dt>
											<dd>
												<select ng-options="dealstatusoption.DealStatusName for dealstatusoption in AffiliateFilter.DealsDealStatusList track by dealstatusoption.DealStatusID" ng-model="AffiliateFilter.DealsDealStatusSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt></dt>
											<dd>
												<label>
													<input type="checkbox" ng-model="AffiliateFilter.DealsIncludeInactive" ng-checked="AffilaiteFilter.DealsIncludeInactive" />
													<span>Include Inactive Deals</span>
												</label>
											</dd>
										</dl>
										<dl>
											<dt></dt>
											<dd>
												<button type="button" class="btn" ng-click="AffiliateRefreshDeals()"><span>Filter Deals</span></button>
											</dd>
										</dl>
									</div>
								</div>

								<p ng-show="Affiliate.Deals.length == 0">No deals found</p>

								<div class="deal" ng-class="affDeal.IsSelected ? 'deal-expanded' : 'deal-collapsed'" ng-repeat="affDeal in Affiliate.Deals">
									
									<div class="deal-header" ng-click="affDeal.IsSelected = !affDeal.IsSelected">
										<span><span ng-bind="affDeal.Operator.OperatorName"></span></span>
										<span>
											<span ng-bind="affDeal.DealInfo.DealInfoName + ' / ' + affDeal.DealStatus.DealStatusName + ' / ' + FormatTimestampString(affDeal.DateSet, 'd M, y hh:mm')"></span>
										</span>
										<span class="icon"></span>
									</div>
									
									<div class="deal-body" ng-show="!affDeal.EditModeOn">

										<div class="deal-body-text" ng-bind="affDeal.DealText == '' ? 'No deal notes' : affDeal.DealText"></div>
										<p>
											<span>Deal Set: </span>
											<span ng-bind="FormatTimestampString(affDeal.DateSet, 'd M, y hh:mm')"></span>
										</p>
										<p>
											<span>Assigned To: </span>
											<span ng-bind="affDeal.AssignedUser.UserName"></span>
										</p>
										<p>
											<span>Created: </span>
											<span ng-bind="FormatTimestampString(affDeal.Created, 'd M, y hh:mm')"></span>
										</p>
										<p>
											<span>Created By: </span>
											<span ng-bind="affDeal.CreatedUser.UserName"></span>
										</p>

										<p ng-show="!affDeal.EditModeOn">
											<button type="button" class="btn" ng-click="AffiliateDealEditToggle(affDeal)"><span>Edit</span></button>
										</p>

									</div>


									<div class="deal-body" ng-show="affDeal.EditModeOn">
										<div class="form medium-form">
											<dl>
												<dt><span>Deal Notes:</span></dt>
												<dd>
													<textarea ng-model="affDeal.DealText" placeholder="Deal notes..."></textarea>
												</dd>
											</dl>
											<dl>
												<dt><span>Assigned To:</span></dt>
												<dd>
													<select ng-options="assigneduseroption.UserName for assigneduseroption in AffiliateFilter.DealsAssignedUserList track by assigneduseroption.UserId" ng-model="affDeal.AssignedUser"></select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Info:</span></dt>
												<dd>
													<select ng-options="dealinfooption.DealInfoName for dealinfooption in AffiliateFilter.DealsDealInfoList track by dealinfooption.DealInfoID" ng-model="affDeal.DealInfo"></select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Status:</span></dt>
												<dd>
													<select ng-options="dealstatusoption.DealStatusName for dealstatusoption in AffiliateFilter.DealsDealStatusList track by dealstatusoption.DealStatusID" ng-model="affDeal.DealStatus"></select>
												</dd>
											</dl>
											<dl>
												<dt></dt>
												<dd>
													<label>
														<input type="checkbox" ng-model="affDeal.IsActive" ng-checked="affDeal.IsActive" />
														<span>Active</span>
													</label>
												</dd>
											</dl>
											<dl>
												<dt></dt>
												<dd>
													<button type="button" class="btn" ng-click="AffiliateDealEditUpdate(affDeal, $index)"><span>Update</span></button>
													<button type="button" class="btn" ng-click="AffiliateDealEditToggle(affDeal)"><span>Cancel</span></button>
												</dd>
											</dl>
										</div>
									</div>

								</div>

							</div>



							<div class="tab" ng-show="AffiliateFilter.ActiveTab == 'notes'">

								<button type="button" class="btn margin-bottom-10" ng-click="AffiliateNoteToggleNew();" ng-show="!AffiliateFilter.NotesNewNoteModeOn"><span>New Note</span></button>
								<div class="note" ng-show="AffiliateFilter.NotesNewNoteModeOn">
									<div class="note-header">
										<span><span>New Note</span></span>
										<span></span>
									</div>
									<div class="note-body">
										<textarea ng-model="AffiliateFilter.NotesNewNoteText" placeholder="New Note..."></textarea>
										<button type="button" class="btn" ng-click="AffiliateNoteToggleNew();"><span>Cancel</span></button>
										<button type="button" class="btn" ng-click="AffiliateNoteNew();"><span>Add Note</span></button>
									</div>
								</div>

								<p ng-show="Affiliate.Notes.length == 0">No notes found</p>
								<div class="note" ng-repeat="affNote in Affiliate.Notes" ng-show="AffiliateDoShowNote(affNote);">
									<div class="note-header span-3cols">
										<span>
											<span>User: </span>
											<span ng-bind="affNote.AccManagerUserName"></span>
										</span>
										<span>
											<span>Last Updated: </span>
											<span ng-bind="FormatTimestampString(affNote.Modified, 'd M, y hh:mm')"></span>
										</span>
										<span>
											<span>Created: </span>
											<span ng-bind="FormatTimestampString(affNote.Created, 'd M, y hh:mm')"></span>
										</span>
									</div>
									<div class="note-body" style="white-space:pre-wrap;" ng-bind="affNote.NoteText == '' ? 'No note text' : affNote.NoteText" ng-show="!affNote.IsSelected"></div>
									<div class="note-body" ng-show="affNote.IsSelected">
										<textarea ng-model="affNote.NoteText" placeholder="Note Text..."></textarea>
										<button type="button" class="btn" ng-click="AffiliateNoteEditToggle(affNote);"><span>Cancel</span></button>
										<button type="button" class="btn" ng-click="AffiliateNoteEditUpdate(affNote);"><span>Update</span></button>
									</div>
									<div class="note-footer">
										<span ng-show="!affNote.IsSelected">
											<span class="text-btn" ng-click="AffiliateNoteRemove(affNote);">[Remove]</span>
											<span class="text-btn" ng-click="AffiliateNoteEditToggle(affNote)">[Edit]</span>
										</span>
									</div>
								</div>

								<button type="button" class="btn" ng-click="AffiliateFilter.NotesShowOlderNotes = true" ng-show="!AffiliateFilter.NotesShowOlderNotes"><span>Show Older Notes</span></button>
								<button type="button" class="btn" ng-click="AffiliateFilter.NotesShowOlderNotes = false" ng-show="AffiliateFilter.NotesShowOlderNotes"><span>Hide Older Notes</span></button>

							</div>
		
							
						</div>
					</div>
				</div>

			</div>


			<div id="operators" class="layoutcols" ng-show="CurrentSection == 'operators'">
				<div class="layoutcol layoutcol_1_2">
					<div>
						<div class="component">
							<h3>Operator Quick Search</h3>
							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<input type="text" placeholder="ID or Search Text" class="short-textinput" ng-model="FilterOperators.QuickSearch" ng-keypress="OperatorQuickSearchEnter($event, FilterOperators.QuickSearch)" />
											<button type="button" class="btn" ng-click="OperatorQuickSearch(FilterOperators.QuickSearch);">Go</button>
										</dd>
									</dl>
								</div>
							</div>
						</div>
						
						<div class="component">
							<h3>Filter</h3>
							<div>
							
								<div class="form small-form" ng-keypress="OperatorsFilterSearchEnter($event)">
									<dl>
										<dt>
											<span>Search:</span>
										</dt>
										<dd>
											<input type="text" placeholder="Search" ng-model="FilterOperators.SearchStr" />
										</dd>
									</dl>

									<!--ACTIVE-->
									<dl>
										<dt>
											<span>Operator Status:</span>
										</dt>
										<dd>
											<select ng-options="opstatus.Label for opstatus in FilterOperators.OperatorStatusList track by opstatus.ID" ng-model="FilterOperators.OperatorStatusSelected"></select>
										</dd>
									</dl>

									<!--COUNTRIES-->
									<dl class="expander" ng-class="FilterOperators.CountryFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.CountryFilterOn = !FilterOperators.CountryFilterOn" title="Toggle">
											<span>Countries</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="countryoption.Label for countryoption in FilterOperators.CountryOptions track by countryoption.ID" ng-model="FilterOperators.CountryOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="country in FilterOperators.CountryList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="country.IsSelected" ng-model="country.IsSelected" />
													<span ng-bind="country.CountryName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--LANGUAGES-->
									<dl class="expander" ng-class="FilterOperators.LanguageFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.LanguageFilterOn = !FilterOperators.LanguageFilterOn" title="Toggle">
											<span>Languages</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="languageoption.Label for languageoption in FilterOperators.LanguageOptions track by languageoption.ID" ng-model="FilterOperators.LanguageOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="language in FilterOperators.LanguageList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="language.IsSelected" ng-model="language.IsSelected" />
													<span ng-bind="language.LanguageName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--AFFILIATETYPES-->
									<dl class="expander" ng-class="FilterOperators.AffiliateTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.AffiliateTypeFilterOn = !FilterOperators.AffiliateTypeFilterOn" title="Toggle">
											<span>Operator Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="affiliatetypeoption.Label for affiliatetypeoption in FilterOperators.AffiliateTypeOptions track by affiliatetypeoption.ID" ng-model="FilterOperators.AffiliateTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="affiliatetype in FilterOperators.AffiliateTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="affiliatetype.IsSelected" ng-model="affiliatetype.IsSelected" />
													<span ng-bind="affiliatetype.AffiliateTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--DEALTYPES-->
									<dl class="expander" ng-class="FilterOperators.DealTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.DealTypeFilterOn = !FilterOperators.DealTypeFilterOn" title="Toggle">
											<span>Deal Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="dealtypeoption.Label for dealtypeoption in FilterOperators.DealTypeOptions track by dealtypeoption.ID" ng-model="FilterOperators.DealTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="dealtype in FilterOperators.DealTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="dealtype.IsSelected" ng-model="dealtype.IsSelected" />
													<span ng-bind="dealtype.DealTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--SOFTWARETYPES-->
									<dl class="expander" ng-class="FilterOperators.SoftwareTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.SoftwareTypeFilterOn = !FilterOperators.SoftwareTypeFilterOn" title="Toggle">
											<span>Software Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="softwaretypeoption.Label for softwaretypeoption in FilterOperators.SoftwareTypeOptions track by softwaretypeoption.ID" ng-model="FilterOperators.SoftwareTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="softwaretype in FilterOperators.SoftwareTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="softwaretype.IsSelected" ng-model="softwaretype.IsSelected" />
													<span ng-bind="softwaretype.SoftwareTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--LICENCETYPES-->
									<dl class="expander" ng-class="FilterOperators.LicenceTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.LicenceTypeFilterOn = !FilterOperators.LicenceTypeFilterOn" title="Toggle">
											<span>Licence Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="licencetypeoption.Label for licencetypeoption in FilterOperators.LicenceTypeOptions track by licencetypeoption.ID" ng-model="FilterOperators.LicenceTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="licencetype in FilterOperators.LicenceTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="licencetype.IsSelected" ng-model="licencetype.IsSelected" />
													<span ng-bind="licencetype.LicenceTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<!--PAYMENTTYPES-->
									<dl class="expander" ng-class="FilterOperators.PaymentTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
										<dt ng-click="FilterOperators.PaymentTypeFilterOn = !FilterOperators.PaymentTypeFilterOn" title="Toggle">
											<span>Payment Type</span>
											<span class="icon"></span>
										</dt>
										<dd>
											<select ng-options="paymenttypeoption.Label for paymenttypeoption in FilterOperators.PaymentTypeOptions track by paymenttypeoption.ID" ng-model="FilterOperators.PaymentTypeOptionSelected"></select>
											<br />
											<span class="checkbox-list">
												<label ng-repeat="paymenttype in FilterOperators.PaymentTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="paymenttype.IsSelected" ng-model="paymenttype.IsSelected" />
													<span ng-bind="paymenttype.PaymentTypeName"></span>
												</label>
											</span>
										</dd>
									</dl>

									<dl>
										<dt></dt>
										<dd>
											<button type="button" ng-click="OperatorsSearchSubmit()" class="btn"><span>Search</span></button>
										</dd>
									</dl>

								</div>

							</div>
						</div>
					</div>

				</div>

				<div class="layoutcol layoutcol_2_2">
					<div>
						<div class="component">
							<h3>Operators</h3>
							<div>
								<pager-header pagerobj="FilterOperators.Pager" extendedheader="false"></pager-header>
								<table class="table-list">
									<tr>
										<th><span>Name</span></th>
										<th ng-show="!FilterOperators.ViewAffiliateSignupUrls"><span>Primary Contact</span></th>
										<th ng-show="!FilterOperators.ViewAffiliateSignupUrls"><span>Brands(s)</span></th>
										<th ng-show="!FilterOperators.ViewAffiliateSignupUrls"><span>Deal Type(s)</span></th>
										<th ng-show="!FilterOperators.ViewAffiliateSignupUrls"></th>
										<th ng-show="FilterOperators.ViewAffiliateSignupUrls"><span>Affiliate Signup URL</span></th>
									</tr>
									<tr ng-repeat="operator in Operators" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
										<td><a class="text-btn" ng-bind="operator.OperatorName" ng-href="{{GetLink('operator', operator.OperatorID)}}" ng-click="RenderSection('operator', operator.OperatorID);"></a></td>
										<td ng-show="!FilterOperators.ViewAffiliateSignupUrls">
											<a href="mailto:{{operator.PrimaryContactDetail.ContactDetailText}}?body={{operator.OperatorName}}" ng-bind="operator.PrimaryContactDetail.ContactDetailText" ng-show="operator.PrimaryContactDetail.ContactType.ContactDetailTypeID == 1"></a>
											<span ng-show="operator.PrimaryContactDetail.ContactType.ContactDetailTypeID != 1">
												<span ng-bind="operator.PrimaryContactDetail.ContactType.ContactDetailTypeName"></span>
												<span>: </span>
												<span ng-bind="operator.PrimaryContactDetail.ContactDetailText"></span>
											</span>
										</td>
										<td ng-show="!FilterOperators.ViewAffiliateSignupUrls" class="breakable"><span ng-bind="operator.BrandsStr"></span></td>
										<td ng-show="!FilterOperators.ViewAffiliateSignupUrls" class="breakable"><span ng-bind="operator.DealTypesStr"></span></td>
										<td ng-show="!FilterOperators.ViewAffiliateSignupUrls">
											<button type="button" class="btn" ng-href="{{GetLink('operator', operator.OperatorID)}}" ng-click="RenderSection('operator', operator.OperatorID);">View</button>
										</td>
										<td ng-show="FilterOperators.ViewAffiliateSignupUrls" class="breakable">
											<a href="{{ValidifyUrl(operator.AffiliateSignupUrl)}}" ng-bind="operator.AffiliateSignupUrl" target="_blank"></a>
										</td>
									</tr>
								</table>
								<pager-footer pagerobj="FilterOperators.Pager"></pager-footer>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div id="addoperator" class="layoutcols" ng-show="CurrentSection == 'addoperator'">
				<div class="layoutcol layoutcol_1_1">
					<div>
						<div class="component">
							<h3>New Operator</h3>
							<div>
								<div class="form">
									<dl>
										<dt><span>Operator Name:</span></dt>
										<dd>
											<input type="text" placeholder="Operator Name" ng-model="NewOperator.OperatorName" ng-blur="ValidateRequired(NewOperator.OperatorName, NewOperator.Errors, 'OperatorName');" form-error="NewOperator.Errors.Get('OperatorName')" />
										</dd>
									</dl>
									<dl>
										<dt><span>Affiliate Signup URL:</span></dt>
										<dd>
											<input type="text" placeholder="Affiliate Signup URL" ng-model="NewOperator.AffiliateSignupUrl" />
										</dd>
									</dl>
									<dl>
										<dt><span>Website Review URL:</span></dt>
										<dd>
											<input type="text" placeholder="Website Review URL" ng-model="NewOperator.WebsiteReviewUrl" />
										</dd>
									</dl>
									<dl>
										<dt><span>Primary Contact Detail:</span></dt>
										<dd>
											<span class="block">
												<select ng-options="ocdt.ContactDetailTypeName for ocdt in NewOperator.ContactDetailTypes track by ocdt.ContactDetailTypeID" ng-model="NewOperator.PrimaryContactDetailType"></select>
											</span>
											<span class="block">
												<input type="text" ng-model="NewOperator.PrimaryContactDetailText" placeholder="Contact Detail" ng-blur="ValidateRequired(NewOperator.PrimaryContactDetailText, NewOperator.Errors, 'PrimaryContactDetailText');" form-error="NewOperator.Errors.Get('PrimaryContactDetailText')" />
											</span>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-href="/area/dashboard/" ng-click="RenderSection('dashboard')"><span></span><span>Cancel</span></button>
											<button type="button" class="btn" ng-click="AddOperatorSubmit()" form-error="NewOperator.Errors.Get('Main')"><span>Create Operator</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div id="operator" class="layoutcols layoutcol_fixedleft_400" ng-show="CurrentSection == 'operator'">
				<div class="layoutcol layoutcol_1_2">
					<div>
						<div class="component">
							<div>
								<a class="btn" ng-href="{{GetLink('operators')}}" ng-click="RenderSection('operators')"><span>&lt;&lt; Back to Search Results</span></a>
							</div>
						</div>

						<div class="component">
							<h3>Operator Quick Search</h3>
							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<input type="text" placeholder="ID or Search Text" class="short-textinput" ng-model="FilterOperators.QuickSearch" ng-keypress="OperatorQuickSearchEnter($event, FilterOperators.QuickSearch)" />
											<button type="button" class="btn" ng-click="OperatorQuickSearch(FilterOperators.QuickSearch);">Go</button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component" ng-show="Operator != null">
							<h3>Operator Details</h3>
							<div>
								<div class="form medium-form">
									<dl>
										<dt><span>Operator ID:</span></dt>
										<dd><span ng-bind="Operator.OperatorID"></span></dd>
									</dl>
									<dl>
										<dt><span>Operator Name:</span></dt>
										<dd><span ng-bind="Operator.OperatorName"></span></dd>
									</dl>
									<dl>
										<dt><span>Affiliate Signup URL:</span></dt>
										<dd><a href="{{ValidifyUrl(Operator.AffiliateSignupUrl)}}" ng-bind="Operator.AffiliateSignupUrl" target="_blank" class="breakable"></a></dd>
									</dl>
									<dl>
										<dt><span>Website Review URL:</span></dt>
										<dd><a href="{{ValidifyUrl(Operator.WebsiteReviewUrl)}}" ng-bind="Operator.WebsiteReviewUrl" target="_blank" class="breakable"></a></dd>
									</dl>
									<dl>
										<dt><span>Active:</span></dt>
										<dd><span ng-bind="Operator.IsActive ? 'Yes' : 'No'"></span></dd>
									</dl>

									<dl ng-class="Operator.ContactDetails.length == 1 ? '' : 'expander ' + (OperatorFilter.ViewContactDetailsModeOn ? 'expander-expanded' : 'expander-collapsed')">
										<dt>
											<span>Primary Contact:</span>
										</dt>
										<dd>
											<a ng-bind="Operator.PrimaryContactDetail.ContactDetailText" href="mailto:{{Operator.PrimaryContactDetail.ContactDetailText}}?body={{Operator.OperatorName}}" ng-show="Operator.PrimaryContactDetail.ContactType.ContactDetailTypeID == 1" class="breakable"></a>
											<span ng-show="Operator.PrimaryContactDetail.ContactType.ContactDetailTypeID != 1" class="breakable">
												<span ng-bind="Operator.PrimaryContactDetail.ContactType.ContactDetailTypeName"></span>
												<span>: </span>
												<span ng-bind="Operator.PrimaryContactDetail.ContactDetailText"></span>
											</span>
											<span class="icon" ng-click="OperatorFilter.ViewContactDetailsModeOn = !OperatorFilter.ViewContactDetailsModeOn" title="Toggle"></span>
										</dd>
									</dl>
									<dl ng-repeat="contactDetail in Operator.ContactDetails" ng-show="OperatorFilter.ViewContactDetailsModeOn && !contactDetail.IsPrimary">
										<dt><span ng-bind="contactDetail.ContactType.ContactDetailTypeName" ng-class="!contactDetail.IsActive ? 'inactive' : ''"></span></dt>
										<dd>
											<a ng-bind="contactDetail.ContactDetailText" ng-show="contactDetail.ContactType.ContactDetailTypeID == '1'" href="mailto:{{contactDetail.ContactDetailText}}?body={{Operator.OperatorName}}" ng-class="!contactDetail.IsActive ? 'inactive' : ''" class="breakable"></a>
											<span ng-bind="contactDetail.ContactDetailText" ng-show="contactDetail.ContactType.ContactDetailTypeID != '1'" ng-class="!contactDetail.IsActive ? 'inactive' : ''" class="breakable"></span>
										</dd>
									</dl>
									<dl>
										<dt><span>Target Countries:</span></dt>
										<dd><span class="block" ng-repeat="country in Operator.Countries | filter:{Status:1, IsSelected:true}" ng-bind="country.CountryName"></span></dd>
									</dl>
									<dl>
										<dt><span>Banned Countries:</span></dt>
										<dd>
											<span class="block" ng-repeat="country in Operator.Countries | filter:{Status:0}" ng-bind="country.CountryName"></span>
										</dd>
									</dl>
									<dl>
										<dt><span>Languages:</span></dt>
										<dd><span class="block" ng-repeat="language in Operator.Languages | filter:{IsSelected:true}" ng-bind="language.LanguageName"></span></dd>
									</dl>
									<dl>
										<dt><span>Operator Types:</span></dt>
										<dd><span class="block" ng-repeat="affiliateType in Operator.AffiliateTypes | filter:{IsSelected:true}" ng-bind="affiliateType.AffiliateTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Deal Types:</span></dt>
										<dd><span class="block" ng-repeat="dealType in Operator.DealTypes | filter:{IsSelected:true}" ng-bind="dealType.DealTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Software Types:</span></dt>
										<dd><span class="block" ng-repeat="softwareType in Operator.SoftwareTypes | filter:{IsSelected:true}" ng-bind="softwareType.SoftwareTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Licence Types:</span></dt>
										<dd><span class="block" ng-repeat="licenceType in Operator.LicenceTypes | filter:{IsSelected:true}" ng-bind="licenceType.LicenceTypeName"></span></dd>
									</dl>
									<dl>
										<dt><span>Brands:</span></dt>
										<dd><span ng-bind="Operator.BrandsStr"></span></dd>
									</dl>
									<dl>
										<dt><span>Payment Types:</span></dt>
										<dd>
											<span class="block" ng-repeat="paymentType in Operator.paymentTypes | filter:{IsSelected:true}" ng-bind="paymentType.PaymentTypeName"></span>
										</dd>
									</dl>



								</div>
							</div>
						</div>
			
					</div>
				</div>

				<div class="layoutcol layoutcol_2_2">
					<div>



						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'profile' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Operator Details</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl>
										<dt><span>Operator Name:</span></dt>
										<dd><input type="text" ng-model="Operator.OperatorName" placeholder="Operator Name" /></dd>
									</dl>
									<dl>
										<dt><span>Affiliate Signup URL:</span></dt>
										<dd><input type="text" ng-model="Operator.AffiliateSignupUrl" placeholder="Affiliate Signup URL" /></dd>
									</dl>
									<dl>
										<dt><span>Website Review URL:</span></dt>
										<dd><input type="text" ng-model="Operator.WebsiteReviewUrl" placeholder="Website Review URL" /></dd>
									</dl>
									<dl>
										<dt>
											<label>
												<input type="checkbox" ng-model="Operator.IsActive" ng-checked="Operator.IsActive" />
												<span>Is Active</span>
											</label>
										</dt>
										<dd></dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
											<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'contactdetails' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Contact Details</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="contactDetail in Operator.ContactDetails">
										<dd>
											<span class="block">
												<select ng-options="ocdt.ContactDetailTypeName for ocdt in OperatorFilter.ContactDetailTypes track by ocdt.ContactDetailTypeID" ng-model="contactDetail.ContactType"></select>
												<input type="text" ng-model="contactDetail.ContactDetailText" placeholder="Contact Detail" />
												<label>
													<input type="checkbox" ng-model="contactDetail.IsActive" ng-true-value="false" ng-false-value="true" />
													<span>Do Not Contact</span>
												</label>
												<label>
													<input type="radio" name="editOperatorContactPrimary" ng-checked="contactDetail.IsPrimary" ng-click="OperatorEditSetPrimaryContactDetail(contactDetail)" />
													<span>Is Primary</span>
												</label>
												<button type="button" class="btn" ng-click="OperatorEditRemoveContactDetail(contactDetail)"><span>Remove</span></button>
											</span>
										</dd>
									</dl>
									<dl>
										<dd>
											<span class="block">
												<select ng-options="ocdt.ContactDetailTypeName for ocdt in OperatorFilter.ContactDetailTypes track by ocdt.ContactDetailTypeID" ng-model="OperatorFilter.ContactDetailNewType">
													<option value="">[ Select Contact Type ]</option>
												</select>
												<input type="text" ng-model="OperatorFilter.ContactDetailNewText" placeholder="New Contact Detail" />
												<button type="button" class="btn" ng-click="OperatorEditAddContactDetail()"><span>Add</span></button>
											</span>
										</dd>
									</dl>
									<dl>
										<dd>
											<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
											<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'countries' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Targetted Countries</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="country in Operator.Countries  | filter: { Status: 1}:true">
										<dt>
											<span ng-bind="country.CountryName" style="color:#090;"></span>
										</dt>
										<dd>
											<label ng-repeat="countryStatus in CountryStatuses">
												<input type="radio" ng-model="country.Status" ng-value="countryStatus.Value" name="countryStatusRadio_{{country.CountryID}}" />
												<span ng-bind="countryStatus.Label" />
											</label>
										</dd>
									</dl>
								</div>
							</div>
						</div>
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'countries' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Banned Countries</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="country in Operator.Countries  | filter: { Status: 0}:true">
										<dt>
											<span ng-bind="country.CountryName" style="color:#900;"></span>
										</dt>
										<dd>
											<label ng-repeat="countryStatus in CountryStatuses">
												<input type="radio" ng-model="country.Status" ng-value="countryStatus.Value" name="countryStatusRadio_{{country.CountryID}}" />
												<span ng-bind="countryStatus.Label" />
											</label>
										</dd>
									</dl>
								</div>
							</div>
						</div>
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'countries' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Other Countries</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form">
									<dl ng-repeat="country in Operator.Countries  | filter: { Status: -1}:true">
										<dt>
											<span ng-bind="country.CountryName"></span>
										</dt>
										<dd>
											<label ng-repeat="countryStatus in CountryStatuses">
												<input type="radio" ng-model="country.Status" ng-value="countryStatus.Value" name="countryStatusRadio_{{country.CountryID}}" />
												<span ng-bind="countryStatus.Label" />
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'languages' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Operator Languages</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="language in Operator.Languages">
										<dd>
											<label>
												<input type="checkbox" ng-model="language.IsSelected" ng-checked="language.IsSelected" />
												<span ng-bind="language.LanguageName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'affiliatetypes' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Operator Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="affiliateType in Operator.AffiliateTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="affiliateType.IsSelected" ng-checked="affiliateType.IsSelected" />
												<span ng-bind="affiliateType.AffiliateTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>

						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'dealtypes' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Deal Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="dealType in Operator.DealTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="dealType.IsSelected" ng-checked="dealType.IsSelected" />
												<span ng-bind="dealType.DealTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'softwaretypes' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Software Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="softwareType in Operator.SoftwareTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="softwareType.IsSelected" ng-checked="softwareType.IsSelected" />
												<span ng-bind="softwareType.SoftwareTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'licencetypes' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Licence Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="licenceType in Operator.LicenceTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="licenceType.IsSelected" ng-checked="licenceType.IsSelected" />
												<span ng-bind="licenceType.LicenceTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'brands' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Brands</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="brand in Operator.Brands">
										<dd>
											<span class="block">
												<input type="text" ng-model="brand.BrandName" placeholder="Brand Name" />
											</span>
										</dd>
									</dl>
								</div>
								<div class="form large-form">
									<dl>
										<dd>
											<input type="text" ng-model="OperatorFilter.BrandNewText" placeholder="New Brand Name" />
											<button type="button" class="btn" ng-click="OperatorEditAddBrand()"><span>Add</span></button>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
						<div class="component expander expander-expanded" ng-show="OperatorFilter.EditMode == 'paymenttypes' || OperatorFilter.EditMode == 'edit'">
							<h3 onclick="this.parentElement.className = this.parentElement.className.indexOf('expander-expanded') != -1 ? this.parentElement.className.replace('expander-expanded', 'expander-collapsed') : this.parentElement.className.replace('expander-collapsed', 'expander-expanded')" title="Toggle">
								<span>Edit Payment Types</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="form large-form inline-200">
									<dl ng-repeat="paymentType in Operator.PaymentTypes">
										<dd>
											<label>
												<input type="checkbox" ng-model="paymentType.IsSelected" ng-checked="paymentType.IsSelected" />
												<span ng-bind="paymentType.PaymentTypeName"></span>
											</label>
										</dd>
									</dl>
								</div>
								<p>
									<button type="button" class="btn" ng-click="OperatorEditUpdate()"><span>Update</span></button>
									<button type="button" class="btn" ng-click="OperatorEditCancel()"><span>Cancel</span></button>
								</p>
							</div>
						</div>
						 
			

						<div class="tabs-container" ng-show="Operator != null && OperatorFilter.EditMode == 'none'">
							<ul class="tabs-menu">
								<li ng-class="OperatorFilter.ActiveTab == 'overview' ? 'active-tab' : ''"><a ng-click="OperatorSelectTab('overview');">Overview</a></li>
								<li ng-class="OperatorFilter.ActiveTab == 'deals' ? 'active-tab' : ''"><a ng-click="OperatorSelectTab('deals');">Deals</a></li>
								<li ng-class="OperatorFilter.ActiveTab == 'notes' ? 'active-tab' : ''"><a ng-click="OperatorSelectTab('notes');">Notes</a></li>
							</ul>
						
							<div class="tab" ng-show="OperatorFilter.ActiveTab == 'overview'">


								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-click="OperatorEdit('edit')"><span>Edit Operator</span></button>
										</dd>
									</dl>
									<!--dl>
										<dt>Edit Details:</dt>
										<dd>
											<select ng-change="OperatorEdit(null)" ng-model="OperatorFilter.EditMode">
												<option value="none">[ Select ]</option>
												<option value="profile">Profile</option>
												<option value="contactdetails">Contact Details</option>
												<option value="countries">Countries</option>
												<option value="languages">Languages</option>
												<option value="affiliatetypes">Operator Types</option>
												<option value="dealtypes">Deal Types</option>
												<option value="softwaretypes">Software Types</option>
												<option value="licencetypes">Licence Types</option>
												<option value="brands">Brands</option>
												<option value="paymenttypes">Payment Types</option>
											</select>
										</dd>
									</dl-->
								</div>

								<p style="text-align:right;">

									<span style="float:left;display:inline-block;">
										<span>Best Deal: </span>
										<strong ng-bind="Operator.BestDealInfoName"></strong>
									</span>

									<span class="text-btn" ng-show="!IsUserInRole('ManageOperatorFavs')">&nbsp;</span>
									<span class="text-btn" ng-click="OperatorToggleFavourite();" ng-show="IsUserInRole('ManageOperatorFavs')">
										<span class="fav" ng-class="Operator.IsFavourite ? 'fav-isfav' : 'fav-notfav'" title="{{ Operator.IsFavourite ? 'Remove from favourites' : 'Add to favourites' }}"></span>
										<span style="float:right;display:inline-block"><strong>Toggle Favourite</strong></span>
									</span>
								</p>
								

								<div class="split-50-50">
									<div>
										<div class="component">
											<h3>Summary - Deal Status</h3>
											<div id="operator-deal-statuses-chart-container"></div>
										</div>
									</div>
									<div>
										<div class="component">
											<h3>Summary - Deal Info</h3>
											<div id="operator-deal-infos-chart-container"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab" ng-show="OperatorFilter.ActiveTab == 'deals'">

								<div class="component expander" ng-class="OperatorFilter.DealsShowFilter ? 'expander-expanded' : 'expander-collapsed'">
									<h3 ng-click="OperatorFilter.DealsShowFilter = !OperatorFilter.DealsShowFilter" title="Toggle">
										<span>Filter Deals</span>
										<span class="icon"></span>
									</h3>
									<div class="form">
										<dl>
											<dt><span>Assigned To:</span></dt>
											<dd>
												<select ng-options="assigneduseroption.UserName for assigneduseroption in OperatorFilter.DealsAssignedUserList track by assigneduseroption.UserId" ng-model="OperatorFilter.DealsAssignedUserSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt><span>Deal Type:</span></dt>
											<dd>
												<select ng-options="dealinfooption.DealInfoName for dealinfooption in OperatorFilter.DealsDealInfoList track by dealinfooption.DealInfoID" ng-model="OperatorFilter.DealsDealInfoSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt><span>Deal Status:</span></dt>
											<dd>
												<select ng-options="dealstatusoption.DealStatusName for dealstatusoption in OperatorFilter.DealsDealStatusList track by dealstatusoption.DealStatusID" ng-model="OperatorFilter.DealsDealStatusSelected">
													<option value="">[ All ]</option>
												</select>
											</dd>
										</dl>
										<dl>
											<dt></dt>
											<dd>
												<label>
													<input type="checkbox" ng-model="OperatorFilter.DealsIncludeInactive" ng-checked="OperatorFilter.DealsIncludeInactive" />
													<span>Include Inactive Deals</span>
												</label>
											</dd>
										</dl>
										<dl>
											<dt></dt>
											<dd>
												<button type="button" class="btn" ng-click="OperatorRefreshDeals()"><span>Filter Deals</span></button>
											</dd>
										</dl>
									</div>
								</div>

								<p ng-show="Operator.Deals.length == 0">No deals found</p>

								<div class="deal" ng-class="opDeal.IsSelected ? 'deal-expanded' : 'deal-collapsed'" ng-repeat="opDeal in Operator.Deals">
									
									<div class="deal-header" ng-click="opDeal.IsSelected = !opDeal.IsSelected">
										<span><span ng-bind="opDeal.Affiliate.AffiliateName"></span></span>
										<span>
											<span ng-bind="opDeal.DealInfo.DealInfoName + ' / ' + opDeal.DealInfo.DealInfoName + ' / ' + opDeal.DealInfo.DealInfoName + ' / ' + opDeal.DealStatus.DealStatusName + ' / ' + FormatTimestampString(opDeal.DateSet, 'd M, y hh:mm')"></span>
										</span>
										<span class="icon"></span>
									</div>
									
									<div class="deal-body" ng-show="!opDeal.EditModeOn">

			
										<div class="deal-body-text" ng-bind="opDeal.DealText == '' ? 'No deal notes' : opDeal.DealText"></div>
										<p>
											<span>Deal Set: </span>
											<span ng-bind="FormatTimestampString(opDeal.DateSet, 'd M, y hh:mm')"></span>
										</p>
										<p>
											<span>Assigned To: </span>
											<span ng-bind="opDeal.AssignedUser.UserName"></span>
										</p>
										<p>
											<span>Created: </span>
											<span ng-bind="FormatTimestampString(opDeal.Created, 'd M, y hh:mm')"></span>
										</p>
										<p>
											<span>Created By: </span>
											<span ng-bind="opDeal.CreatedUser.UserName"></span>
										</p>
										<p ng-show="!opDeal.EditModeOn">
											<button type="button" class="btn" ng-click="OperatorDealEditToggle(opDeal)"><span>Edit</span></button>
										</p>
									</div>


									<div class="deal-body" ng-show="opDeal.EditModeOn">
										<div class="form medium-form">
											<dl>
												<dt><span>Deal Notes:</span></dt>
												<dd>
													<textarea ng-model="opDeal.DealText" placeholder="Deal notes..."></textarea>
												</dd>
											</dl>
											<dl>
												<dt><span>Assigned To:</span></dt>
												<dd>
													<select ng-options="assigneduseroption.UserName for assigneduseroption in OperatorFilter.DealsAssignedUserList track by assigneduseroption.UserId" ng-model="opDeal.AssignedUser"></select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Info:</span></dt>
												<dd>
													<select ng-options="dealinfooption.DealInfoName for dealinfooption in OperatorFilter.DealsDealInfoList track by dealinfooption.DealInfoID" ng-model="opDeal.DealInfo"></select>
												</dd>
											</dl>
											<dl>
												<dt><span>Deal Status:</span></dt>
												<dd>
													<select ng-options="dealstatusoption.DealStatusName for dealstatusoption in OperatorFilter.DealsDealStatusList track by dealstatusoption.DealStatusID" ng-model="opDeal.DealStatus"></select>
												</dd>
											</dl>
											<dl>
												<dt></dt>
												<dd>
													<label>
														<input type="checkbox" ng-model="opDeal.IsActive" ng-checked="opDeal.IsActive" />
														<span>Active</span>
													</label>
												</dd>
											</dl>
											<dl>
												<dt></dt>
												<dd>
													<button type="button" class="btn" ng-click="OperatorDealEditUpdate(opDeal, $index)"><span>Update</span></button>
													<button type="button" class="btn" ng-click="OperatorDealEditToggle(opDeal)"><span>Cancel</span></button>
												</dd>
											</dl>
										</div>
									</div>


								</div>

							</div>


							<div class="tab" ng-show="OperatorFilter.ActiveTab == 'notes'">

								<button type="button" class="btn margin-bottom-10" ng-click="OperatorNoteToggleNew();" ng-show="!OperatorFilter.NotesNewNoteModeOn"><span>New Note</span></button>
								<div class="note" ng-show="OperatorFilter.NotesNewNoteModeOn">
									<div class="note-header">
										<span><span>New Note</span></span>
										<span></span>
									</div>
									<div class="note-body">
										<textarea ng-model="OperatorFilter.NotesNewNoteText" placeholder="New Note..."></textarea>
										<button type="button" class="btn" ng-click="OperatorNoteToggleNew();"><span>Cancel</span></button>
										<button type="button" class="btn" ng-click="OperatorNoteNew();"><span>Add Note</span></button>
									</div>
								</div>

								<p ng-show="Operator.Notes.length == 0">No notes found</p>
								<div class="note" ng-repeat="opNote in Operator.Notes" ng-show="OperatorDoShowNote(opNote);">
									<div class="note-header span-3cols">
										<span>
											<span>User: </span>
											<span ng-bind="opNote.AccManagerUserName"></span>
										</span>
										<span>
											<span>Last Updated: </span>
											<span ng-bind="FormatTimestampString(opNote.Modified, 'd M, y hh:mm')"></span>
										</span>
										<span>
											<span>Created: </span>
											<span ng-bind="FormatTimestampString(opNote.Created, 'd M, y hh:mm')"></span>
										</span>
									</div>
									<div class="note-body" style="white-space:pre-wrap;" ng-bind="opNote.NoteText == '' ? 'No note text' : opNote.NoteText" ng-show="!opNote.IsSelected"></div>
									<div class="note-body" ng-show="opNote.IsSelected">
										<textarea ng-model="opNote.NoteText" placeholder="Note Text..."></textarea>
										<button type="button" class="btn" ng-click="OperatorNoteEditToggle(opNote);"><span>Cancel</span></button>
										<button type="button" class="btn" ng-click="OperatorNoteEditUpdate(opNote);"><span>Update</span></button>
									</div>
									<div class="note-footer">
										<span ng-show="!opNote.IsSelected">
											<span class="text-btn" ng-click="OperatorNoteRemove(opNote);">[Remove]</span>
											<span class="text-btn" ng-click="OperatorNoteEditToggle(opNote)">[Edit]</span>
										</span>
									</div>
								</div>

								<button type="button" class="btn" ng-click="OperatorFilter.NotesShowOlderNotes = true" ng-show="!OperatorFilter.NotesShowOlderNotes"><span>Show Older Notes</span></button>
								<button type="button" class="btn" ng-click="OperatorFilter.NotesShowOlderNotes = false" ng-show="OperatorFilter.NotesShowOlderNotes"><span>Hide Older Notes</span></button>

							</div>
		
						</div>
					</div>
				</div>

			</div>



			<div id="report-custom" class="layoutcols" ng-show="CurrentSection == 'report-custom'">
				<div class="layoutcol">
					<div>
						<div class="component">
							<h3>Custom Reports</h3>
							<div>
								<p ng-show="CustomReports.length == 0">No Custom Reports Available</p>

								<table ng-show="CustomReports.length > 0">
									<tr ng-repeat="customreportinfo in CustomReports">
										<td>
											<a ng-href="{{GetLink(customreportinfo.CustomReportType, customreportinfo.CustomReportID)}}" ng-click="RenderSection(customreportinfo.CustomReportType, customreportinfo.CustomReportID)" ng-bind="customreportinfo.CustomReportName"></a>
										</td>
										<td>
											<button type="button" class="btn" ng-show="customreportinfo.CreatedByCurrentUser" ng-click="ReportCustomRemove(customreportinfo);"><span>Delete</span></button>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="report-affiliates" class="layoutcols" ng-show="CurrentSection == 'report-affiliates'">
				<div class="layoutcol">
					<div>

						<div id="report-affiliate-filter" class="component expander" ng-class="FilterReportAffiliates.ExpandFilter ? 'expander-expanded' : 'expander-collapsed'">
							<h3 ng-click="FilterReportAffiliates.ExpandFilter = !FilterReportAffiliates.ExpandFilter">
								<span>Affiliate Report - Filter</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="split-50-50">

									<div>
										<div class="component">
											<h3>Affiliate Status</h3>
											<div class="form medium-form">

												<!--SEARCH-->
												<dl>
													<dt><span>Search</span></dt>
													<dd>
														<input type="text" placeholder="Search..." ng-model="FilterReportAffiliates.SearchStr" />
													</dd>
												</dl>

												<!--ALL USERS-->
												<dl ng-show="!IsUserInRole('UnrestrictedReports')">
													<dt><span>Assigned To</span></dt>
													<dd>
														<label>
															<input type="checkbox" ng-model="FilterReportAffiliates.AllUsers" ng-checked="FilterReportAffiliates.AllUsers" />
															<span>All Users</span>
														</label>
													</dd>
												</dl>

												<!--SIZE-->
												<dl>
													<dt><span>Size</span></dt>
													<dd><select ng-options="size.Label for size in FilterReportAffiliates.SizeList track by size.ID" ng-model="FilterReportAffiliates.SizeSelected"></select></dd>
												</dl>
									
												<!--CONTACTABLE-->
												<dl>
													<dt><span>Contactable</span></dt>
													<dd><select ng-options="contactable.Label for contactable in FilterReportAffiliates.ContactableList track by contactable.ID" ng-model="FilterReportAffiliates.ContactableSelected"></select></dd>
												</dl>
									
												<!--MAXMAIL-->
												<dl>
													<dt><span>MaxMail</span></dt>
													<dd><select ng-options="maxmail.Label for maxmail in FilterReportAffiliates.MaxMailList track by maxmail.ID" ng-model="FilterReportAffiliates.MaxMailSelected"></select></dd>
												</dl>

												<!--ACTIVE-->
												<dl>
													<dt><span>Affiliate Status:</span></dt>
													<dd><select ng-options="affstatus.Label for affstatus in FilterReportAffiliates.AffStatusList track by affstatus.ID" ng-model="FilterReportAffiliates.AffStatusSelected"></select></dd>
												</dl>

												<!--INCLUDE UNASSIGNED-->
												<dl>
													<dt></dt>
													<dd>
														<label>
															<input type="checkbox" ng-model="FilterReportAffiliates.IncludeUnassigned" ng-checked="FilterReportAffiliates.IncludeUnassigned" />
															<span>Include Unassigned</span>
														</label>
													</dd>
												</dl>

												<!--CREATED DATERANGE-->
												<dl>
													<dt><span>Created:</span></dt>
													<dd>
														<select ng-model="FilterReportAffiliates.CreatedDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportAffiliates.CreatedDateRange.DateType.Label == 'Custom'" dpid="affiliatereportcreatedFilterDateRange" dpfrom="FilterReportAffiliates.CreatedDateRange.From" dpto="FilterReportAffiliates.CreatedDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>
											</div>

										</div>

										<div class="component">
											<h3>Include:</h3>
											<div class="serial-checkboxes">
												<label>
													<input type="checkbox" ng-model="FilterReportAffiliates.IncAffiliateTypes" ng-checked="FilterReportAffiliates.IncAffiliateTypes" />
													<span>Affiliate Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportAffiliates.IncDealTypes" ng-checked="FilterReportAffiliates.IncDealTypes" />
													<span>Deal Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportAffiliates.IncMarketingTypes" ng-checked="FilterReportAffiliates.IncMarketingTypes" />
													<span>Marketing Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportAffiliates.IncCountries" ng-checked="FilterReportAffiliates.IncCountries" />
													<span>Countries</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportAffiliates.IncOperators" ng-checked="FilterReportAffiliates.IncOperators" />
													<span>Operators</span>
												</label>
											</div>
										</div>


									</div>
									<div>

										<!--AFFMANAGER-->
										<div class="component expander" ng-class="FilterReportAffiliates.AffiliateManagerFilterOn ? 'expander-expanded' : 'expander-collapsed'" ng-show="IsUserInRole('UnrestrictedReports')">
											<h3 ng-click="FilterReportAffiliates.AffiliateManagerFilterOn = !FilterReportAffiliates.AffiliateManagerFilterOn" title="Toggle">
												<span>Account Managers</span>
												<span class="icon"></span>
											</h3>
											<div>
												<span class="checkbox-list">
													<label ng-repeat="user in FilterReportAffiliates.AffiliateManagerList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="user.IsSelected" ng-model="user.IsSelected" />
														<span ng-bind="user.UserName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--OPERATORS-->
										<div class="component expander" ng-class="FilterReportAffiliates.OperatorFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportAffiliates.OperatorFilterOn = !FilterReportAffiliates.OperatorFilterOn" title="Toggle">
												<span>Operators</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="operatoroption.DealStatusName for operatoroption in FilterReportAffiliates.OperatorOptions track by operatoroption.DealStatusID" ng-model="FilterReportAffiliates.OperatorOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="operator in FilterReportAffiliates.OperatorList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="operator.IsSelected" ng-model="operator.IsSelected" />
														<span ng-bind="operator.OperatorName"></span>
													</label>
												</span>
											</div>
										</div>
									
										<!--COUNTRIES-->
										<div class="component expander" ng-class="FilterReportAffiliates.CountryFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportAffiliates.CountryFilterOn = !FilterReportAffiliates.CountryFilterOn" title="Toggle">
												<span>Countries</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="countryoption.Label for countryoption in FilterReportAffiliates.CountryOptions track by countryoption.ID" ng-model="FilterReportAffiliates.CountryOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="country in FilterReportAffiliates.CountryList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="country.IsSelected" ng-model="country.IsSelected" />
														<span ng-bind="country.CountryName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--AFFILIATETYPES-->
										<div class="component expander" ng-class="FilterReportAffiliates.AffiliateTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportAffiliates.AffiliateTypeFilterOn = !FilterReportAffiliates.AffiliateTypeFilterOn" title="Toggle">
												<span>Affiliate Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="affiliatetypeoption.Label for affiliatetypeoption in FilterReportAffiliates.AffiliateTypeOptions track by affiliatetypeoption.ID" ng-model="FilterReportAffiliates.AffiliateTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="affiliatetype in FilterReportAffiliates.AffiliateTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="affiliatetype.IsSelected" ng-model="affiliatetype.IsSelected" />
														<span ng-bind="affiliatetype.AffiliateTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--DEALTYPES-->
										<div class="component expander" ng-class="FilterReportAffiliates.DealTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportAffiliates.DealTypeFilterOn = !FilterReportAffiliates.DealTypeFilterOn" title="Toggle">
												<span>Deal Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="dealtypeoption.Label for dealtypeoption in FilterReportAffiliates.DealTypeOptions track by dealtypeoption.ID" ng-model="FilterReportAffiliates.DealTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="dealtype in FilterReportAffiliates.DealTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealtype.IsSelected" ng-model="dealtype.IsSelected" />
														<span ng-bind="dealtype.DealTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--MARKETINGTYPES-->
										<div class="component expander" ng-class="FilterReportAffiliates.MarketingTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportAffiliates.MarketingTypeFilterOn = !FilterReportAffiliates.MarketingTypeFilterOn" title="Toggle">
												<span>Marketing Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="marketingtypeoption.Label for marketingtypeoption in FilterReportAffiliates.MarketingTypeOptions track by marketingtypeoption.ID" ng-model="FilterReportAffiliates.MarketingTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="marketingtype in FilterReportAffiliates.MarketingTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="marketingtype.IsSelected" ng-model="marketingtype.IsSelected" />
														<span ng-bind="marketingtype.MarketingTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										
									</div>
								</div>
							</div>

							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd style="text-align:center;">
											<button type="button" ng-click="ReportAffiliatesSubmit()" class="btn"><span>Create Report</span></button>
											<button type="button" ng-click="ReportAffiliatesSave()" class="btn"><span>Save Custom Report Settings</span></button>
										</dd>
									</dl>

								</div>
							</div>

						</div>


						<div class="component" ng-show="FilterReportAffiliates.ReportHelper.ReportLoaded">
							<h3>Affiliates Report</h3>
							<div>
								<report-table pagerobj="FilterReportAffiliates.Pager" reporthelper="FilterReportAffiliates.ReportHelper" showtotals="true"></report-table>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div id="report-operators" class="layoutcols" ng-show="CurrentSection == 'report-operators'">
				<div class="layoutcol">
					<div>

						<div class="component expander" ng-class="FilterReportOperators.ExpandFilter ? 'expander-expanded' : 'expander-collapsed'">
							<h3 ng-click="FilterReportOperators.ExpandFilter = !FilterReportOperators.ExpandFilter">
								<span>Operators Report - Filter</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="split-50-50">

									<div>
										<div class="component">
											<h3>Operator Status</h3>
											<div class="form medium-form">

												<!--ACTIVE-->
												<dl>
													<dt><span>Operator Status:</span></dt>
													<dd><select ng-options="status.Label for status in FilterReportOperators.StatusList track by status.ID" ng-model="FilterReportOperators.StatusSelected"></select></dd>
												</dl>

												<!--CREATED DATERANGE-->
												<dl>
													<dt><span>Created:</span></dt>
													<dd>
														<select ng-model="FilterReportOperators.CreatedDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportOperators.CreatedDateRange.DateType.Label == 'Custom'" dpid="operatorreportcreatedFilterDateRange" dpfrom="FilterReportOperators.CreatedDateRange.From" dpto="FilterReportOperators.CreatedDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>

											</div>
										</div>

										<div class="component">
											<h3>Include:</h3>
											<div class="serial-checkboxes">
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncBrands" ng-checked="FilterReportOperators.IncBrands" />
													<span>Brands</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncSignupUrls" ng-checked="FilterReportOperators.IncSignupUrls" />
													<span>Signup URLs</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncCountries" ng-checked="FilterReportOperators.IncCountries" />
													<span>Countries</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncLanguages" ng-checked="FilterReportOperators.IncLanguages" />
													<span>Languages</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncAffiliateTypes" ng-checked="FilterReportOperators.IncAffiliateTypes" />
													<span>Operator Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncDealTypes" ng-checked="FilterReportOperators.IncDealTypes" />
													<span>Deal Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncSoftwareTypes" ng-checked="FilterReportOperators.IncSoftwareTypes" />
													<span>Software Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncLicenceTypes" ng-checked="FilterReportOperators.IncLicenceTypes" />
													<span>Licence Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncPaymentTypes" ng-checked="FilterReportOperators.IncPaymentTypes" />
													<span>Payment Types</span>
												</label>
												<label>
													<input type="checkbox" ng-model="FilterReportOperators.IncSecondaryContactDetails" ng-checked="FilterReportOperators.IncSecondaryContactDetails" />
													<span>Secondary Contact Details</span>
												</label>
											</div>
										</div>


									</div>
									<div>

										<!--COUNTRIES-->
										<div class="component expander" ng-class="FilterReportOperators.CountryFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.CountryFilterOn = !FilterReportOperators.CountryFilterOn" title="Toggle">
												<span>Countries</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="countryoption.Label for countryoption in FilterReportOperators.CountryOptions track by countryoption.ID" ng-model="FilterReportOperators.CountryOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="country in FilterReportOperators.CountryList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="country.IsSelected" ng-model="country.IsSelected" />
														<span ng-bind="country.CountryName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--LANGUAGES-->
										<div class="component expander" ng-class="FilterReportOperators.LanguageFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.LanguageFilterOn = !FilterReportOperators.LanguageFilterOn" title="Toggle">
												<span>Languages</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="languageoption.Label for languageoption in FilterReportOperators.LanguageOptions track by languageoption.ID" ng-model="FilterReportOperators.LanguageOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="language in FilterReportOperators.LanguageList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="language.IsSelected" ng-model="language.IsSelected" />
														<span ng-bind="language.LanguageName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--AFFILIATETYPES-->
										<div class="component expander" ng-class="FilterReportOperators.AffiliateTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.AffiliateTypeFilterOn = !FilterReportOperators.AffiliateTypeFilterOn" title="Toggle">
												<span>Operator Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="affiliatetypeoption.Label for affiliatetypeoption in FilterReportOperators.AffiliateTypeOptions track by affiliatetypeoption.ID" ng-model="FilterReportOperators.AffiliateTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="affiliatetype in FilterReportOperators.AffiliateTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="affiliatetype.IsSelected" ng-model="affiliatetype.IsSelected" />
														<span ng-bind="affiliatetype.AffiliateTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--DEALTYPES-->
										<div class="component expander" ng-class="FilterReportOperators.DealTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.DealTypeFilterOn = !FilterReportOperators.DealTypeFilterOn" title="Toggle">
												<span>Deal Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="dealtypeoption.Label for dealtypeoption in FilterReportOperators.DealTypeOptions track by dealtypeoption.ID" ng-model="FilterReportOperators.DealTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="dealtype in FilterReportOperators.DealTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealtype.IsSelected" ng-model="dealtype.IsSelected" />
														<span ng-bind="dealtype.DealTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--SOFTWARETYPES-->
										<div class="component expander" ng-class="FilterReportOperators.SoftwareTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.SoftwareTypeFilterOn = !FilterReportOperators.SoftwareTypeFilterOn" title="Toggle">
												<span>Software Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="softwaretypeoption.Label for softwaretypeoption in FilterReportOperators.SoftwareTypeOptions track by softwaretypeoption.ID" ng-model="FilterReportOperators.SoftwareTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="softwaretype in FilterReportOperators.SoftwareTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="softwaretype.IsSelected" ng-model="softwaretype.IsSelected" />
														<span ng-bind="softwaretype.SoftwareTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--LICENCETYPES-->
										<div class="component expander" ng-class="FilterReportOperators.LicenceTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.LicenceTypeFilterOn = !FilterReportOperators.LicenceTypeFilterOn" title="Toggle">
												<span>Licence Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="licencetypeoption.Label for licencetypeoption in FilterReportOperators.LicenceTypeOptions track by licencetypeoption.ID" ng-model="FilterReportOperators.LicenceTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="licencetype in FilterReportOperators.LicenceTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="licencetype.IsSelected" ng-model="licencetype.IsSelected" />
														<span ng-bind="licencetype.LicenceTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--PAYMENTTYPES-->
										<div class="component expander" ng-class="FilterReportOperators.PaymentTypeFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportOperators.PaymentTypeFilterOn = !FilterReportOperators.PaymentTypeFilterOn" title="Toggle">
												<span>Payment Type</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="paymenttypeoption.Label for paymenttypeoption in FilterReportOperators.PaymentTypeOptions track by paymenttypeoption.ID" ng-model="FilterReportOperators.PaymentTypeOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="paymenttype in FilterReportOperators.PaymentTypeList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="paymenttype.IsSelected" ng-model="paymenttype.IsSelected" />
														<span ng-bind="paymenttype.PaymentTypeName"></span>
													</label>
												</span>
											</div>
										</div>

										
									</div>
								</div>
							</div>


							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd style="text-align:center;">
											<button type="button" ng-click="ReportOperatorsSubmit()" class="btn"><span>Create Report</span></button>
											<button type="button" ng-click="ReportOperatorsSave()" class="btn"><span>Save Custom Report Settings</span></button>
										</dd>
									</dl>

								</div>
							</div>

						</div>


						<div class="component" ng-show="FilterReportOperators.ReportHelper.ReportLoaded">
							<h3>Operators Report</h3>
							<div>
								<report-table pagerobj="FilterReportOperators.Pager" reporthelper="FilterReportOperators.ReportHelper" showtotals="true"></report-table>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div id="report-deals" class="layoutcols" ng-show="CurrentSection == 'report-deals'">
				<div class="layoutcol">
					<div>

						<div id="report-deals-filter" class="component expander" ng-class="FilterReportDeals.ExpandFilter ? 'expander-expanded' : 'expander-collapsed'">
							<h3 ng-click="FilterReportDeals.ExpandFilter = !FilterReportDeals.ExpandFilter">
								<span>Deal Report - Filter</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="split-50-50">

									<div>
										<div class="component">
											<h3>Deal Status</h3>
											<div class="form medium-form">

												<!--ALL USERS-->
												<dl ng-show="!IsUserInRole('UnrestrictedReports')">
													<dt><span>Assigned To</span></dt>
													<dd>
														<label>
															<input type="checkbox" ng-model="FilterReportDeals.AllUsers" ng-checked="FilterReportDeals.AllUsers" />
															<span>All Users</span>
														</label>
													</dd>
												</dl>

												<!--ACTIVE-->
												<dl>
													<dt><span>Deal Status:</span></dt>
													<dd><select ng-options="dealactivestatus.Label for dealactivestatus in FilterReportDeals.DealActiveStatusList track by dealactivestatus.ID" ng-model="FilterReportDeals.DealActiveStatusSelected"></select></dd>
												</dl>

												<!--INCLUDE UNASSIGNED-->
												<dl>
													<dt></dt>
													<dd>
														<label>
															<input type="checkbox" ng-model="FilterReportDeals.IncludeUnassigned" ng-checked="FilterReportDeals.IncludeUnassigned" />
															<span>Include Unassigned</span>
														</label>
													</dd>
												</dl>

												<!--CREATED DATERANGE-->
												<dl>
													<dt><span>Created:</span></dt>
													<dd>
														<select ng-model="FilterReportDeals.CreatedDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportDeals.CreatedDateRange.DateType.Label == 'Custom'" dpid="dealreportcreatedFilterDateRange" dpfrom="FilterReportDeals.CreatedDateRange.From" dpto="FilterReportDeals.CreatedDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>

												<!--DATESET DATERANGE-->
												<dl>
													<dt><span>Deal Set:</span></dt>
													<dd>
														<select ng-model="FilterReportDeals.DateSetDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportDeals.DateSetDateRange.DateType.Label == 'Custom'" dpid="dealreportdatesetFilterDateRange" dpfrom="FilterReportDeals.DateSetDateRange.From" dpto="FilterReportDeals.DateSetDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>
											</div>

										</div>
									

									</div>
									<div>

										<!--AFFMANAGER-->
										<div class="component expander" ng-class="FilterReportDeals.AffiliateManagerFilterOn ? 'expander-expanded' : 'expander-collapsed'" ng-show="IsUserInRole('UnrestrictedReports')">
											<h3 ng-click="FilterReportDeals.AffiliateManagerFilterOn = !FilterReportDeals.AffiliateManagerFilterOn" title="Toggle">
												<span>Account Managers</span>
												<span class="icon"></span>
											</h3>
											<div>
												<select ng-options="affmanoption.Label for affmanoption in FilterReportDeals.AffiliateManagerOptions track by affmanoption.ID" ng-model="FilterReportDeals.AffiliateManagerOptionSelected"></select>
												<br />
												<span class="checkbox-list">
													<label ng-repeat="affman in FilterReportDeals.AffiliateManagerList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="affman.IsSelected" ng-model="affman.IsSelected" />
														<span ng-bind="affman.UserName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--DEALSTATUS-->
										<div class="component expander" ng-class="FilterReportDeals.DealStatusFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportDeals.DealStatusFilterOn = !FilterReportDeals.DealStatusFilterOn" title="Toggle">
												<span>Deal Status</span>
												<span class="icon"></span>
											</h3>
											<div>
												<span class="checkbox-list">
													<label ng-repeat="dealstatus in FilterReportDeals.DealStatusList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealstatus.IsSelected" ng-model="dealstatus.IsSelected" />
														<span ng-bind="dealstatus.DealStatusName"></span>
													</label>
												</span>
											</div>
										</div>

										<!--DEALINFO-->
										<div class="component expander" ng-class="FilterReportDeals.DealInfoFilterOn ? 'expander-expanded' : 'expander-collapsed'">
											<h3 ng-click="FilterReportDeals.DealInfoFilterOn = !FilterReportDeals.DealInfoFilterOn" title="Toggle">
												<span>Deal Info</span>
												<span class="icon"></span>
											</h3>
											<div>
												<span class="checkbox-list">
													<label ng-repeat="dealinfo in FilterReportDeals.DealInfoList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealinfo.IsSelected" ng-model="dealinfo.IsSelected" />
														<span ng-bind="dealinfo.DealInfoName"></span>
													</label>
												</span>
											</div>
										</div>
										
									</div>
								</div>
							</div>

							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd style="text-align:center;">
											<button type="button" ng-click="ReportDealsSubmit()" class="btn"><span>Create Report</span></button>
											<button type="button" ng-click="ReportDealsSave()" class="btn"><span>Save Custom Report Settings</span></button>
										</dd>
									</dl>

								</div>
							</div>

						</div>


						<div class="component" ng-show="FilterReportDeals.ReportHelper.ReportLoaded">
							<h3>Deals Report</h3>
							<div>
								<report-table pagerobj="FilterReportDeals.Pager" reporthelper="FilterReportDeals.ReportHelper" showtotals="true"></report-table>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div id="report-user-deals-summary" class="layoutcols" ng-show="CurrentSection == 'report-user-deals-summary' && IsUserInRole('UnrestrictedReports')">
				<div class="layoutcol">
					<div>

						<div id="report-user-deals-summary-filter" class="component expander" ng-class="FilterReportUserDealsSummary.ExpandFilter ? 'expander-expanded' : 'expander-collapsed'">
							<h3 ng-click="FilterReportUserDealsSummary.ExpandFilter = !FilterReportUserDealsSummary.ExpandFilter">
								<span>User Deals Summary - Filter</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="split-50-50">

									<div>
										<div class="component">
											<h3>Deal Status</h3>
											<div class="form medium-form">

												<!--ACTIVE-->
												<dl>
													<dt><span>Deal Status:</span></dt>
													<dd><select ng-options="dealactivestatus.Label for dealactivestatus in FilterReportUserDealsSummary.DealActiveStatusList track by dealactivestatus.ID" ng-model="FilterReportUserDealsSummary.DealActiveStatusSelected"></select></dd>
												</dl>

												<!--DATESET DATERANGE-->
												<dl>
													<dt><span>Deal Set:</span></dt>
													<dd>
														<select ng-model="FilterReportUserDealsSummary.DateSetDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportUserDealsSummary.DateSetDateRange.DateType.Label == 'Custom'" dpid="userdealsreportsummarydatesetFilterDateRange" dpfrom="FilterReportUserDealsSummary.DateSetDateRange.From" dpto="FilterReportUserDealsSummary.DateSetDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>
											</div>
										</div>
									

									</div>
									<div>

										<!--SHOW DEAL STATUSES-->
										<div class="component">
											<h3><span>Show</span></h3>
											<div>
												<span class="checkbox-list">
													<label ng-repeat="dealstatus in FilterReportUserDealsSummary.DealStatusList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealstatus.IsSelected" ng-model="dealstatus.IsSelected" />
														<span ng-bind="dealstatus.DealStatusName"></span>
													</label>
												</span>
											</div>
										</div>

										
										
									</div>
								</div>
							</div>

							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd style="text-align:center;">
											<button type="button" ng-click="ReportUserDealsSummarySubmit()" class="btn"><span>Create Report</span></button>
										</dd>
									</dl>

								</div>
							</div>

						</div>


						<div class="component" ng-show="FilterReportUserDealsSummary.IsLoaded">
							<h3>User Deals Summary</h3>
							<div>
								<table class="table-list">
									<tr>
										<th><span>User</span></th>
										<th ng-repeat="dealstatus in FilterReportUserDealsSummary.DealStatusList" ng-show="dealstatus.IsSelected"><span ng-bind="dealstatus.DealStatusName"></span></th>
									</tr>
									<tr ng-repeat="row in ReportUserDealsSummary.Rows" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
										<td>
											<span class="link text-btn" ng-click="ReportUserDealsDrilldown(row.ID, null)" ng-bind="row.Label"></span>
										</td>
										<td ng-repeat="dealstatus in FilterReportUserDealsSummary.DealStatusList" ng-show="dealstatus.IsSelected">
											<span class="link text-btn" ng-click="ReportUserDealsDrilldown(row.ID, dealstatus.DealStatusID)" ng-bind="row.Cols[dealstatus.DealStatusID]"></span>
										</td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div id="report-operator-deals-summary" class="layoutcols" ng-show="CurrentSection == 'report-operator-deals-summary' && IsUserInRole('UnrestrictedReports')">
				<div class="layoutcol">
					<div>

						<div id="report-operator-deals-summary-filter" class="component expander" ng-class="FilterReportOperatorDealsSummary.ExpandFilter ? 'expander-expanded' : 'expander-collapsed'">
							<h3 ng-click="FilterReportOperatorDealsSummary.ExpandFilter = !FilterReportOperatorDealsSummary.ExpandFilter">
								<span>Operator Deals Summary - Filter</span>
								<span class="icon"></span>
							</h3>
							<div>
								<div class="split-50-50">

									<div>
										<div class="component">
											<h3>Deal Status</h3>
											<div class="form medium-form">

												<!--ACTIVE-->
												<dl>
													<dt><span>Deal Status:</span></dt>
													<dd><select ng-options="dealactivestatus.Label for dealactivestatus in FilterReportOperatorDealsSummary.DealActiveStatusList track by dealactivestatus.ID" ng-model="FilterReportOperatorDealsSummary.DealActiveStatusSelected"></select></dd>
												</dl>

												<!--DATESET DATERANGE-->
												<dl>
													<dt><span>Deal Set:</span></dt>
													<dd>
														<select ng-model="FilterReportOperatorDealsSummary.DateSetDateRange.DateType" ng-options="datefilter.Label for datefilter in DateFilters track by datefilter.ID" ng-change="null"></select>
														<date-range-picker style="display:inline-block;" ng-show="FilterReportOperatorDealsSummary.DateSetDateRange.DateType.Label == 'Custom'" dpid="operatordealsreportsummarydatesetFilterDateRange" dpfrom="FilterReportOperatorDealsSummary.DateSetDateRange.From" dpto="FilterReportOperatorDealsSummary.DateSetDateRange.To" dponchange="null"></date-range-picker>
													</dd>
												</dl>
											</div>
										</div>
									

									</div>
									<div>

										<!--SHOW DEAL STATUSES-->
										<div class="component">
											<h3><span>Show</span></h3>
											<div>
												<span class="checkbox-list">
													<label ng-repeat="dealstatus in FilterReportOperatorDealsSummary.DealStatusList" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
														<input type="checkbox" ng-checked="dealstatus.IsSelected" ng-model="dealstatus.IsSelected" />
														<span ng-bind="dealstatus.DealStatusName"></span>
													</label>
												</span>
											</div>
										</div>

										
										
									</div>
								</div>
							</div>

							<div>
								<div class="form small-form">
									<dl>
										<dt></dt>
										<dd style="text-align:center;">
											<button type="button" ng-click="ReportOperatorDealsSummarySubmit()" class="btn"><span>Create Report</span></button>
										</dd>
									</dl>

								</div>
							</div>

						</div>


						<div class="component" ng-show="FilterReportOperatorDealsSummary.IsLoaded">
							<h3>Operator Deals Summary</h3>
							<div>
								<table class="table-list">
									<tr>
										<th><span>Operator</span></th>
										<th ng-repeat="dealstatus in FilterReportOperatorDealsSummary.DealStatusList" ng-show="dealstatus.IsSelected"><span ng-bind="dealstatus.DealStatusName"></span></th>
									</tr>
									<tr ng-repeat="row in ReportOperatorDealsSummary.Rows" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
										<td>
											<span ng-bind="row.Label"></span>
										</td>
										<td ng-repeat="dealstatus in FilterReportOperatorDealsSummary.DealStatusList" ng-show="dealstatus.IsSelected">
											<span ng-bind="row.Cols[dealstatus.DealStatusID]"></span>
										</td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>









			<div id="users" class="layoutcols" ng-show="CurrentSection == 'users' && IsUserInRole('ManageUsers')">
				<div class="layoutcol layoutcol_1_1">
					<div>
						<div class="component">
							<h3>Users</h3>
							<div>
								<table class="table-list">
									<tr>
										<th><span>Username</span></th>
										<th><span>Email</span></th>
										<th><span>Last Login</span></th>
										<th><span>Status</span></th>
										<th><span>Actions</span></th>
									</tr>
									<tr ng-repeat="user in Users" ng-class="!user.IsApproved ? 'disabled-row' : ($index % 2 == 0 ? 'alt-row' : '')">
										<td><span ng-bind="user.UserName"></span></td>
										<td><a href="mailto:{{user.Email}}?body={{user.UserName}}" ng-bind="user.Email"></a></td>
										<td class="align-center">
											<span ng-show="!user.IsOnline" ng-bind="FormatTimestampString(user.LastLoginDate, 'dd/mm/yyyy hh:mm')"></span>
											<span ng-show="user.IsOnline" class="hl-green">Online</span>
										</td>
										<td class="align-center">
											<span ng-show="user.IsApproved" class="hl-green">Active</span>
											<span ng-show="!user.IsApproved" class="hl-red">Disabled</span>
											<span ng-show="user.IsLockedOut" class="hl-yellow">Locked</span>
										</td>
										<td class="align-center">
											<button type="button" class="btn" ng-click="UserShowRoles(user.UserId)">
												<span>Roles</span>
											</button>
											<button ng-show="user.IsApproved" type="button" class="btn" ng-click="UserDeactivate(user.UserId)">
												<span>Disable</span>
											</button>
											<button ng-show="!user.IsApproved" type="button" class="btn" ng-click="UserActivate(user.UserId)">
												<span>Enable</span>
											</button>
											<button ng-show="user.IsLockedOut" type="button" class="btn" ng-click="UserUnlock(user.UserId)">
												<span>Unlock</span>
											</button>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="adduser" class="layoutcols" ng-show="CurrentSection == 'adduser' && IsUserInRole('ManageUsers')">
				<div class="layoutcol layoutcol_1_1">
					<div>
						<div class="component">
							<h3>New User</h3>
							<div>
								<div class="form">
									<dl>
										<dt><span>User Name:</span></dt>
										<dd>
											<input type="text" placeholder="User Name" ng-model="NewUser.UserName" ng-blur="ValidateUsername(NewUser.UserName, NewUser.Errors, 'UserName');" form-error="NewUser.Errors.Get('UserName')" />
										</dd>
									</dl>
									<dl>
										<dt><span>Password:</span></dt>
										<dd>
											<input type="password" placeholder="Password" ng-model="NewUser.Password" ng-blur="ValidatePassword(NewUser.Password, NewUser.Errors, 'Password');" form-error="NewUser.Errors.Get('Password')" />
										</dd>
									</dl>
									<dl>
										<dt><span>Confirm Password:</span></dt>
										<dd>
											<input type="password" placeholder="Confirm Password" ng-model="NewUser.Password2" ng-blur="ValidateCompare(NewUser.Password2, NewUser.Password, NewUser.Errors, 'Password2');" form-error="NewUser.Errors.Get('Password2')" />
										</dd>
									</dl>
									<dl>
										<dt><span>Email:</span></dt>
										<dd>
											<input type="email" placeholder="Email" ng-model="NewUser.Email" ng-blur="ValidateEmail(NewUser.Email, NewUser.Errors, 'Email');" form-error="NewUser.Errors.Get('Email')" />
										</dd>
									</dl>
									<dl>
										<dt><span>Roles:</span></dt>
										<dd>
											<span class="checkbox-list" style="width:250px;">
												<label ng-repeat="newUserRole in NewUserRoles" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
													<input type="checkbox" ng-checked="newUserRole.UserHasRole" ng-model="newUserRole.UserHasRole" />
													<span ng-bind="newUserRole.RoleName"></span>
												</label>
											</span>
										</dd>
									</dl>
									<dl>
										<dt></dt>
										<dd>
											<button type="button" class="btn" ng-click="AddUserSubmit()" form-error="NewUser.Errors.Get('Main')"><span>Create User</span></button>
										</dd>
									</dl>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="settings" class="layoutcols" ng-show="CurrentSection == 'settings' && IsUserInRole('ManageFilters')">
				
				<div class="layoutcol layoutcol_1_2">
					<div>
						<div class="component">
							<h3>Settings</h3>
							<div>
								<ul class="side-menu">
									<li><a class="btn" ng-class="Settings.ActiveTab == 'affiliatetypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'affiliatetypes')}}" ng-click="RenderSection('settings', 'affiliatetypes')"><span>Affiliate/Operator Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'contactdetailtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'contactdetailtypes')}}" ng-click="RenderSection('settings', 'contactdetailtypes')"><span>Contact Detail Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'countries' ? 'active' : ''" ng-href="{{GetLink('settings', 'countries')}}" ng-click="RenderSection('settings', 'countries')"><span>Countries</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'dealtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'dealtypes')}}" ng-click="RenderSection('settings', 'dealtypes')"><span>Deal Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'dealinfos' ? 'active' : ''" ng-href="{{GetLink('settings', 'dealinfos')}}" ng-click="RenderSection('settings', 'dealinfos')"><span>Deal Infos</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'marketingtypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'marketingtypes')}}" ng-click="RenderSection('settings', 'marketingtypes')"><span>Marketing Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'softwaretypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'softwaretypes')}}" ng-click="RenderSection('settings', 'softwaretypes')"><span>Software Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'licencetypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'licencetypes')}}" ng-click="RenderSection('settings', 'licencetypes')"><span>Licence Types</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'languages' ? 'active' : ''" ng-href="{{GetLink('settings', 'languages')}}" ng-click="RenderSection('settings', 'languages')"><span>Languages</span></a></li>
									<li><a class="btn" ng-class="Settings.ActiveTab == 'paymenttypes' ? 'active' : ''" ng-href="{{GetLink('settings', 'paymenttypes')}}" ng-click="RenderSection('settings', 'paymenttypes')"><span></span><span>Payment Types</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="layoutcol layoutcol_2_2">
					<div>
						<div class="component" ng-show="Settings.ActiveTab == 'affiliatetypes'">
							<h3>Affiliate/Operator Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="affType in Settings.AffiliateTypes">
									<td>
										<span ng-bind="affType.AffiliateTypeName" ng-show="!affType.IsSelected"></span>
										<input type="text" ng-model="affType.AffiliateTypeName" placeholder="Affiliate/Operator Type" ng-show="affType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsAffiliateTypeEdit(affType)" ng-show="!affType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsAffiliateTypeEditCancel(affType)" ng-show="affType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsAffiliateTypeEditUpdate(affType)" ng-show="affType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewAffiliateType" placeholder="New Affiliate/Operator Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsAffiliateTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'contactdetailtypes'">
							<h3>Contact Detail Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="contactDetailType in Settings.ContactDetailTypes">
									<td>
										<span ng-bind="contactDetailType.ContactDetailTypeName" ng-show="!contactDetailType.IsSelected"></span>
										<input type="text" ng-model="contactDetailType.ContactDetailTypeName" placeholder="Contact Detail Type" ng-show="contactDetailType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsContactDetailTypeEdit(contactDetailType)" ng-show="!contactDetailType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsContactDetailTypeEditCancel(contactDetailType)" ng-show="contactDetailType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsContactDetailTypeEditUpdate(contactDetailType)" ng-show="contactDetailType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewContactDetailType" placeholder="New Contact Detail Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsContactDetailTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'countries'">
							<h3>Countries</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="country in Settings.Countries">
									<td>
										<span ng-bind="country.CountryName" ng-show="!country.IsSelected"></span>
										<input type="text" ng-model="country.CountryName" placeholder="Country" ng-show="country.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsCountryEdit(country)" ng-show="!country.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsCountryEditCancel(country)" ng-show="country.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsCountryEditUpdate(country)" ng-show="country.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewCountry" placeholder="New Country" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsCountryAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'dealtypes'">
							<h3>Deal Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="dealType in Settings.DealTypes">
									<td>
										<span ng-bind="dealType.DealTypeName" ng-show="!dealType.IsSelected"></span>
										<input type="text" ng-model="dealType.DealTypeName" placeholder="Deal Type" ng-show="dealType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsDealTypeEdit(dealType)" ng-show="!dealType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsDealTypeEditCancel(dealType)" ng-show="dealType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsDealTypeEditUpdate(dealType)" ng-show="dealType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewDealType" placeholder="New Deal Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsDealTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'dealinfos'">
							<h3>Deal Infos</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="dealInfo in Settings.DealInfos">
									<td>
										<span ng-bind="dealInfo.DealInfoName" ng-show="!dealInfo.IsSelected"></span>
										<input type="text" ng-model="dealInfo.DealInfoName" placeholder="Deal Info" ng-show="dealInfo.IsSelected" />
									</td>
									<td>
										<span ng-show="!dealInfo.IsSelected">
											<span>Rank: </span>
											<span ng-bind="dealInfo.DealInfoValueRank == -1 ? 'N/A' : dealInfo.DealInfoValueRank"></span>
										</span>
										<span ng-show="dealInfo.IsSelected">
											<span>Rank: </span>
											<input type="text" ng-model="dealInfo.DealInfoValueRank" placeholder="Rank" />
										</span>
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsDealInfoEdit(dealInfo)" ng-show="!dealInfo.IsSelected && dealInfo.DealInfoValueRank != -1">Edit</button>
										<button type="button" class="btn" ng-click="SettingsDealInfoEditCancel(dealInfo)" ng-show="dealInfo.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsDealInfoEditUpdate(dealInfo)" ng-show="dealInfo.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewDealInfo" placeholder="New Deal Info" />
									</td>
									<td></td>
									<td>
										<button type="button" class="btn" ng-click="SettingsDealInfoAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'marketingtypes'">
							<h3>Marketing Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="marketingType in Settings.MarketingTypes">
									<td>
										<span ng-bind="marketingType.MarketingTypeName" ng-show="!marketingType.IsSelected"></span>
										<input type="text" ng-model="marketingType.MarketingTypeName" placeholder="Marketing Type" ng-show="marketingType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsMarketingTypeEdit(marketingType)" ng-show="!marketingType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsMarketingTypeEditCancel(marketingType)" ng-show="marketingType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsMarketingTypeEditUpdate(marketingType)" ng-show="marketingType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewMarketingType" placeholder="New Marketing Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsMarketingTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'softwaretypes'">
							<h3>Software Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="softwareType in Settings.SoftwareTypes">
									<td>
										<span ng-bind="softwareType.SoftwareTypeName" ng-show="!softwareType.IsSelected"></span>
										<input type="text" ng-model="softwareType.SoftwareTypeName" placeholder="Software Type" ng-show="softwareType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsSoftwareTypeEdit(softwareType)" ng-show="!softwareType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsSoftwareTypeEditCancel(softwareType)" ng-show="softwareType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsSoftwareTypeEditUpdate(softwareType)" ng-show="softwareType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewSoftwareType" placeholder="New Software Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsSoftwareTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'licencetypes'">
							<h3>Licence Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="licenceType in Settings.LicenceTypes">
									<td>
										<span ng-bind="licenceType.LicenceTypeName" ng-show="!licenceType.IsSelected"></span>
										<input type="text" ng-model="licenceType.LicenceTypeName" placeholder="Licence Type" ng-show="licenceType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsLicenceTypeEdit(licenceType)" ng-show="!licenceType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsLicenceTypeEditCancel(licenceType)" ng-show="licenceType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsLicenceTypeEditUpdate(licenceType)" ng-show="licenceType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewLicenceType" placeholder="New Licence Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsLicenceTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
						<div class="component" ng-show="Settings.ActiveTab == 'languages'">
							<h3>Languages</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="language in Settings.Languages">
									<td>
										<span ng-bind="language.LanguageName" ng-show="!language.IsSelected"></span>
										<input type="text" ng-model="language.LanguageName" placeholder="Language" ng-show="language.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsLanguageEdit(language)" ng-show="!language.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsLanguageEditCancel(language)" ng-show="language.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsLanguageEditUpdate(language)" ng-show="language.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewLanguage" placeholder="New Language" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsLanguageAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>
					
						<div class="component" ng-show="Settings.ActiveTab == 'paymenttypes'">
							<h3>Payment Types</h3>
							<table class="table-list">
								<tr ng-class="$index % 2 == 0 ? 'alt-row' : ''" ng-repeat="paymentType in Settings.PaymentTypes">
									<td>
										<span ng-bind="paymentType.PaymentTypeName" ng-show="!paymentType.IsSelected"></span>
										<input type="text" ng-model="paymentType.PaymentTypeName" placeholder="Payment Type" ng-show="paymentType.IsSelected" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsPaymentTypeEdit(paymentType)" ng-show="!paymentType.IsSelected">Edit</button>
										<button type="button" class="btn" ng-click="SettingsPaymentTypeEditCancel(paymentType)" ng-show="paymentType.IsSelected">Cancel</button>
										<button type="button" class="btn" ng-click="SettingsPaymentTypeEditUpdate(paymentType)" ng-show="paymentType.IsSelected">Update</button>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" ng-model="Settings.NewPaymentType" placeholder="New Payment Type" />
									</td>
									<td>
										<button type="button" class="btn" ng-click="SettingsPaymentTypeAdd()">Add</button>
									</td>
								</tr>
							</table>
						</div>					
					</div>
				</div>

			</div>



		</div>
		<div class="footer-fade"></div>
		<div class="footer"><p>&copy; <%= DateTime.Now.Year.ToString() %> - Ihre Consulting AB</p></div>


		<div ng-show="PopupMessage" class="popup-message" ng-class="PopupMessage.cssclass">
			<div>
				<span class="model-div-close-btn" ng-click="ClosePopupMessage()" title="Close"></span>
				<h3 class="model-div-header">
					<span>Alert</span>
				</h3>
				<div class="model-div-content">
					<p ng-bind="PopupMessage.text"></p>
					<p>
						<button type="button" class="btn" ng-click="ClosePopupMessage()"><span>OK</span></button>
					</p>
				</div>
			</div>
		</div>

		<div ng-show="CustomReportParamsModeOn" class="model-div">
			<span class="model-div-close-btn" ng-click="CustomReportParamsModeOn = false" title="Close"></span>
			<h3 class="model-div-header">
				<span>Custom Report Options</span>
			</h3>
			<div class="model-div-content">
				<div class="form">
					<dl>
						<dt><span>Custom Report Name:</span></dt>
						<dd>
							<input type="text" placeholder="Custom Report Name" ng-model="CustomReport.CustomReportName" ng-blur="ValidateRequired(CustomReport.CustomReportName, CustomReport.Errors, 'CustomReportName');" form-error="CustomReport.Errors.Get('CustomReportName')" />
						</dd>
					</dl>
					<dl>
						<dt><span>Users:</span></dt>
						<dd>
							<label>
								<input type="checkbox" ng-model="CustomReport.CurrentUserOnly" />
								<span>Current User Only</span>
							</label>
						</dd>
					</dl>
					<dl>
						<dt></dt>
						<dd>
							<button type="button" class="btn" ng-click="CustomReportParamsModeOn = false"><span>Cancel</span></button>
							<button type="button" class="btn" ng-click="CustomReportParamsSave()" form-error="CustomReport.Errors.Get('Main')"><span>Save</span></button>
						</dd>
					</dl>
				</div>
			</div>
		</div>

		<div ng-show="ChangePasswordModeOn" class="model-div">
			<span class="model-div-close-btn" ng-click="ChangePasswordCancel()" title="Close"></span>
			<h3 class="model-div-header">
				<span>Change Password</span>
			</h3>
			<div class="model-div-content">
				<div class="form">
					<dl>
						<dt><span>New Password:</span></dt>
						<dd>
							<input type="password" placeholder="New Password" ng-model="ChangePassword.Password" ng-blur="ValidatePassword(ChangePassword.Password, ChangePassword.Errors, 'Password');" form-error="ChangePassword.Errors.Get('Password')" />
						</dd>
					</dl>
					<dl>
						<dt><span>Confirm New Password:</span></dt>
						<dd>
							<input type="password" placeholder="Confirm New Password" ng-model="ChangePassword.Password2" ng-blur="ValidateCompare(ChangePassword.Password2, ChangePassword.Password, ChangePassword.Errors, 'Password2');" form-error="ChangePassword.Errors.Get('Password2')" />
						</dd>
					</dl>
					<dl>
						<dt></dt>
						<dd>
							<button type="button" class="btn" ng-click="ChangePasswordCancel()"><span>Cancel</span></button>
							<button type="button" class="btn" ng-click="ChangePasswordUpdate()" form-error="ChangePassword.Errors.Get('Main')"><span>Update</span></button>
						</dd>
					</dl>
				</div>
			</div>
		</div>

		<div ng-show="UserRolesModeOn" class="model-div">
			<span class="model-div-close-btn" ng-click="UserShowRolesCancel()" title="Close"></span>
			<h3 class="model-div-header">
				<span ng-bind="UserRoles.UserName"></span>
				<span> - Edit Roles</span>
			</h3>
			<div class="model-div-content">
				<table class="table-list">
					<tr ng-repeat="userRole in UserRoles.Roles" ng-class="$index % 2 == 0 ? 'alt-row' : ''">
						<td>
							<label>
								<input type="checkbox" ng-model="userRole.UserHasRole" ng-checked="userRole.UserHasRole" />
								<span ng-bind="userRole.RoleName"></span>
							</label>
						</td>
					</tr>
				</table>
				<div>
					<button type="button" class="btn" ng-click="UserShowRolesCancel()"><span>Cancel</span></button>
					<button type="button" class="btn" ng-click="UserShowRolesUpdate()"><span>Update</span></button>
				</div>
			</div>
		</div>

	</form>
	<asp:Literal ID="serverJsVars" runat="server" />
	<!--[if lt IE 9]><script src="/client/scripts/css3-mediaqueries.js"></script><![endif]-->
	<script src="/scripts/jquery-2.2.3.min.js"></script>
	<script src="/scripts/jquery-ui-1.11.4.min.js"></script>
	<script src="/scripts/angular.min.js" type="text/javascript"></script>
	<script src="/scripts/angular-animate.min.js" type="text/javascript"></script>
	<script src="/scripts/angular-cookies.min.js" type="text/javascript"></script>
	<script src="/scripts/angular-touch.min.js" type="text/javascript"></script>
	<script src="/app/draganddrop.js" type="text/javascript"></script>
	<script src="/scripts/base64.js" type="text/javascript"></script>
	<script src="/scripts/spectrum.js" type="text/javascript"></script>
	<script src="/scripts/xlsx.core.min.js" type="text/javascript"></script>
	<script src="/scripts/Blob.js" type="text/javascript"></script>
	<script src="/scripts/FileSaver.js" type="text/javascript"></script>
	<script src="/scripts/jsapi.js" type="text/javascript"></script>
	
	<script src="/scripts/highcharts-425/highcharts.js" type="text/javascript"></script>
	<script src="/scripts/highcharts-425/modules/exporting.js"></script>

	<script src="/app/app.js" type="text/javascript"></script>
	<script src="/app/store.js" type="text/javascript"></script>
	<script src="/app/maincontroller.js?v1" type="text/javascript"></script>
</body>
</html>
