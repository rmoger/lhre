﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class LicenceType
	{
		public int LicenceTypeID { get; set; }
		public string LicenceTypeName { get; set; }
		public bool IsSelected { get; set; }
		public LicenceType(int LicenceTypeID, string LicenceTypeName)
		{
			this.LicenceTypeID = LicenceTypeID;
			this.LicenceTypeName = LicenceTypeName;
			this.IsSelected = false;
		}
	}
	public class LicenceHelper
	{
		public static List<LicenceType> GetLicenceTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<LicenceType> licenceTypes = new List<LicenceType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LicencesSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							LicenceType licenceType = new LicenceType(int.Parse(rdr["licence_id"].ToString()), rdr["licence_name"].ToString());
							licenceTypes.Add(licenceType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return licenceTypes;
		}
		public static void UpdateLicenceType(Guid IhreUserId, int LicenceTypeID, string LicenceTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LicenceUpdate"))
				{
					cmd.AddParamWithValue("licence_id", LicenceTypeID);
					cmd.AddParamWithValue("licence_name", LicenceTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static LicenceType AddLicenceType(Guid IhreUserId, string LicenceTypeName, out ErrorObj Error)
		{
			LicenceType licenceType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LicenceInsert", false, true))
				{
					cmd.AddParamWithValue("licence_name", LicenceTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					licenceType = new LicenceType(cmd.NewId, LicenceTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return licenceType;
		}
	}
}