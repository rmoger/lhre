﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace IhreDB2.App_Code
{
	public class ErrorObj
	{
		public string ErrorMessage { get; set; }
		public int ErrorCode { get; set; }
		public ErrorObj()
		{
			this.ErrorMessage = null;
			this.ErrorCode = 0;
		}
		public ErrorObj(string ErrorMessage)
		{
			this.ErrorMessage = ErrorMessage;
		}
		public ErrorObj(string ErrorMessage, int ErrorCode)
		{
			this.ErrorMessage = ErrorMessage;
			this.ErrorCode = ErrorCode;
		}
	}
	public class JsonResponse
	{
		public bool Success { get; set; }
		public string Error { get; set; }
		public int ErrorCode { get; set; }
		public Dictionary<string, object> ResponseData { get; set; }

		public JsonResponse()
		{
			this.Success = true;
			this.Error = null;
			this.ErrorCode = 0;
			this.ResponseData = new Dictionary<string, object>();
		}
		public void AddData(string key, object value)
		{
			this.ResponseData.Add(key, value);
		}
		public void ReturnError(ErrorObj Error)
		{
			this.Success = false;
			this.Error = Error.ErrorMessage;
			this.ErrorCode = Error.ErrorCode;
		}
		public void ReturnError(string ErrorMessage)
		{
			this.Success = false;
			this.Error = ErrorMessage;
		}
		public void ReturnError(string ErrorMessage, int ErrorCode)
		{
			this.Success = false;
			this.Error = ErrorMessage;
			this.ErrorCode = ErrorCode;
		}
		public string WebResponse()
		{
			bool DebugModeOn = Boolean.Parse(ConfigurationManager.AppSettings["debugmodeon"]);
			if (!DebugModeOn)
			{
				string xs = JsonConvert.SerializeObject(this);
				byte[] y = Encoding.ASCII.GetBytes(xs);
				return Convert.ToBase64String(y);
			}
			else
			{
				return JsonConvert.SerializeObject(this);
			}
		}

	}
}