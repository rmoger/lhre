﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class Operator
	{
		public int OperatorID { get; set; }
		public string OperatorName { get; set; }
		public bool IsSelected { get; set; }
		public bool IsActive { get; set; }
		public bool IsFavourite { get; set; }
		public string AffiliateSignupUrl { get; set; }
		public string WebsiteReviewUrl { get; set; }
		public List<Brand> Brands { get; set; }
		public string BrandsStr { get; set; }
		public ContactDetail PrimaryContactDetail { get; set; }
		public List<ContactDetail> ContactDetails { get; set; }
		public List<Country> Countries { get; set; }
		public List<Language> Languages { get; set; }
		public List<AffiliateType> AffiliateTypes { get; set; }
		public List<DealType> DealTypes { get; set; }
		public string DealTypesStr { get; set; }
		public List<SoftwareType> SoftwareTypes { get; set; }
		public List<LicenceType> LicenceTypes { get; set; }
		public List<PaymentType> PaymentTypes { get; set; }
		public Operator()
		{
			this.IsSelected = false;
			this.BrandsStr = "";
		}
		public Operator(int OperatorID, string OperatorName)
		{
			this.OperatorID = OperatorID;
			this.OperatorName = OperatorName;
			this.IsSelected = false;
			this.BrandsStr = "";
		}
	}
	public class Brand
	{
		public int BrandID { get; set; }
		public string BrandName { get; set; }
		public Brand(int BrandID, string BrandName)
		{
			this.BrandID = BrandID;
			this.BrandName = BrandName;
		}
	}
	public class OperatorNote
	{
		public int NoteID { get; set; }
		public string NoteText { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public string AccManagerUserName { get; set; }
		public bool IsSelected { get; set; }
		public OperatorNote()
		{
			this.IsSelected = false;
		}
	}

	public static class OperatorHelper
	{
		public static List<Operator> GetOperators(Guid IhreUserId, string SearchStr, Int16 OperatorStatusType, string CountryOption, string CountryIDs, string LanguageOption, string LanguageIDs, string AffiliateTypeOption, string AffiliateTypeIDs, string DealTypeOption, string DealTypeIDs, string SoftwareTypeOption, string SoftwareTypeIDs, string LicenceTypeOption, string LicenceTypeIDs, string PaymentTypeOption, string PaymentTypeIDs, int Offset, int Count, out int ResultCount, out ErrorObj Error)
		{
			List<Operator> operators = new List<Operator>();
			ResultCount = 0;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorSearch", true))
				{
					cmd.AddParamWithValue("search_str", SearchStr);
					cmd.AddParamWithValue("active", OperatorStatusType);
					if (CountryIDs != null && CountryIDs != "")
					{
						cmd.AddParamWithValue("countrysearch", CountryOption);
						cmd.AddParamWithValue("country_id_list", CountryIDs);
					}
					if (LanguageIDs != null && LanguageIDs != "")
					{
						cmd.AddParamWithValue("languagesearch", LanguageOption);
						cmd.AddParamWithValue("language_id_list", LanguageIDs);
					}
					if (AffiliateTypeIDs != null && AffiliateTypeIDs != "")
					{
						cmd.AddParamWithValue("affiliate_typesearch", AffiliateTypeOption);
						cmd.AddParamWithValue("affiliate_type_id_list", AffiliateTypeIDs);
					}
					if (DealTypeIDs != null && DealTypeIDs != "")
					{
						cmd.AddParamWithValue("deal_typesearch", DealTypeOption);
						cmd.AddParamWithValue("deal_type_id_list", DealTypeIDs);
					}
					if (SoftwareTypeIDs != null && SoftwareTypeIDs != "")
					{
						cmd.AddParamWithValue("software_typesearch", SoftwareTypeOption);
						cmd.AddParamWithValue("software_type_id_list", SoftwareTypeIDs);
					}
					if (LicenceTypeIDs != null && LicenceTypeIDs != "")
					{
						cmd.AddParamWithValue("licence_typesearch", LicenceTypeOption);
						cmd.AddParamWithValue("licence_type_id_list", LicenceTypeIDs);
					}
					if (PaymentTypeIDs != null && PaymentTypeIDs != "")
					{
						cmd.AddParamWithValue("payment_typesearch", PaymentTypeOption);
						cmd.AddParamWithValue("payment_type_id_list", PaymentTypeIDs);
					}
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Operator op = new Operator();
							op.OperatorID = int.Parse(rdr["client_id"].ToString());
							op.OperatorName = rdr["client_name"].ToString();
							op.IsActive = bool.Parse(rdr["active"].ToString());
							op.BrandsStr = rdr["brands_str"].ToString();
							op.PrimaryContactDetail = new ContactDetail();
							op.AffiliateSignupUrl = rdr["affiliate_signup_url"].ToString();
							op.WebsiteReviewUrl = rdr["website_review_url"].ToString();
							if (rdr["contact_type_id"].ToString() != "")
							{
								op.PrimaryContactDetail.ContactDetailText = rdr["contact_detail"].ToString();
								op.PrimaryContactDetail.ContactType = new ContactDetailType();
								op.PrimaryContactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
								op.PrimaryContactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							}
							op.DealTypesStr = rdr["deal_types"].ToString();

							operators.Add(op);
						}
					}
					ResultCount = cmd.ResultCount;
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operators;
		}

		public static List<Operator> GetOperatorsLite(Guid IhreUserId, out ErrorObj Error)
		{
			List<Operator> operators = new List<Operator>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorsSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Operator op = new Operator(
								int.Parse(rdr["client_id"].ToString()),
								rdr["client_name"].ToString()
							);
							operators.Add(op);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operators;
		}
		public static List<Operator> GetFavouriteOperators(Guid IhreUserId, out ErrorObj Error)
		{
			List<Operator> operators = new List<Operator>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorFavouritesSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Operator op = new Operator(
								int.Parse(rdr["client_id"].ToString()),
								rdr["client_name"].ToString()
							);
							operators.Add(op);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operators;
		}

		public static List<Operator> GetLatestOperators(Guid IhreUserId, out ErrorObj Error)
		{
			List<Operator> operators = new List<Operator>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorsLatestSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Operator op = new Operator(
								int.Parse(rdr["client_id"].ToString()),
								rdr["client_name"].ToString()
							);
							operators.Add(op);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operators;
		}

		public static Operator GetOperator(Guid IhreUserId, int OperatorID, out ErrorObj Error)
		{
			Operator op = new Operator();
			op.Brands = new List<Brand>();
			op.ContactDetails = new List<ContactDetail>();
			op.Countries = new List<Country>();
			op.Languages = new List<Language>();
			op.AffiliateTypes = new List<AffiliateType>();
			op.DealTypes = new List<DealType>();
			op.SoftwareTypes = new List<SoftwareType>();
			op.LicenceTypes = new List<LicenceType>();
			op.PaymentTypes = new List<PaymentType>();

			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorSelect", false))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							op.OperatorID = int.Parse(rdr["client_id"].ToString());
							op.OperatorName = rdr["client_name"].ToString();
							op.IsActive = bool.Parse(rdr["active"].ToString());
							op.AffiliateSignupUrl = rdr["affiliate_signup_url"].ToString();
							op.WebsiteReviewUrl = rdr["website_review_url"].ToString();
							op.IsFavourite = bool.Parse(rdr["is_favourite"].ToString());
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							ContactDetail contactDetail = new ContactDetail();
							contactDetail.ContactDetailID = int.Parse(rdr["client_contact_detail_id"].ToString());
							contactDetail.OperatorID = OperatorID;
							contactDetail.ContactType = new ContactDetailType();
							contactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
							contactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							contactDetail.ContactDetailText = rdr["contact_detail"].ToString();
							contactDetail.IsActive = bool.Parse(rdr["active"].ToString());
							contactDetail.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());
							op.ContactDetails.Add(contactDetail);
							if (contactDetail.IsPrimary)
							{
								op.PrimaryContactDetail = contactDetail;
							}
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							Brand brand = new Brand(int.Parse(rdr["brand_id"].ToString()), rdr["brand_name"].ToString());
							op.BrandsStr += (op.BrandsStr != "" ? ", " : "") + brand.BrandName;
							op.Brands.Add(brand);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							Country country = new Country(int.Parse(rdr["country_id"].ToString()), rdr["country_name"].ToString());
							country.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							country.Status = country.IsSelected ? Int16.Parse(rdr["status"].ToString()) : (short)-1;
							op.Countries.Add(country);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							Language language = new Language(int.Parse(rdr["language_id"].ToString()), rdr["language_name"].ToString());
							language.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.Languages.Add(language);
						}

						rdr.NextResult();
						while (rdr.Read())
						{
							AffiliateType affiliateType = new AffiliateType(int.Parse(rdr["affiliate_type_id"].ToString()), rdr["affiliate_type_name"].ToString());
							affiliateType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.AffiliateTypes.Add(affiliateType);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							DealType dealType = new DealType(int.Parse(rdr["deal_type_id"].ToString()), rdr["deal_type_name"].ToString());
							dealType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.DealTypes.Add(dealType);
						}

						rdr.NextResult();
						while (rdr.Read())
						{
							SoftwareType softwareType = new SoftwareType(int.Parse(rdr["software_id"].ToString()), rdr["software_name"].ToString());
							softwareType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.SoftwareTypes.Add(softwareType);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							LicenceType licenceType = new LicenceType(int.Parse(rdr["licence_id"].ToString()), rdr["licence_name"].ToString());
							licenceType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.LicenceTypes.Add(licenceType);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							PaymentType paymentType = new PaymentType(int.Parse(rdr["payment_type_id"].ToString()), rdr["payment_type_name"].ToString());
							paymentType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							op.PaymentTypes.Add(paymentType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return op;
		}

		public static int AddOperator(Guid IhreUserId, string OperatorName, string AffiliateSignupURL, string WebsiteReviewURL, int PrimaryContactDetailType, string PrimaryContactDetailText, out ErrorObj Error)
		{
			int NewOperatorID = -1;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorInsert", false, true))
				{
					cmd.AddParamWithValue("client_name", OperatorName);
					cmd.AddParamWithValue("affiliate_signup_url", AffiliateSignupURL);
					cmd.AddParamWithValue("website_review_url", WebsiteReviewURL);
					cmd.AddParamWithValue("contact_type_id", PrimaryContactDetailType);
					cmd.AddParamWithValue("contact_detail", PrimaryContactDetailText);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
					NewOperatorID = cmd.NewId;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return NewOperatorID;
		}
		
		public static void UpdateOperatorFavourite(Guid IhreUserId, int OperatorID, bool IsFavourite, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorSetFavourite"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("is_favourite", IsFavourite);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		public static void UpdateOperatorProfile(Guid IhreUserId, int OperatorID, string OperatorName, string AffiliateSignupUrl, string WebsiteReviewUrl, bool IsActive, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorProfileUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("client_name", OperatorName);
					cmd.AddParamWithValue("affiliate_signup_url", AffiliateSignupUrl);
					cmd.AddParamWithValue("website_review_url", WebsiteReviewUrl);
					cmd.AddParamWithValue("active", IsActive);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static List<ContactDetail> GetOperatorContactDetails(Guid IhreUserId, int OperatorID, out ErrorObj Error)
		{
			List<ContactDetail> contactDetails = new List<ContactDetail>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorContactDetailsSelect", false))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							ContactDetail contactDetail = new ContactDetail();
							contactDetail.ContactDetailID = int.Parse(rdr["client_contact_detail_id"].ToString());
							contactDetail.OperatorID = OperatorID;
							contactDetail.ContactType = new ContactDetailType();
							contactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
							contactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							contactDetail.ContactDetailText = rdr["contact_detail"].ToString();
							contactDetail.IsActive = bool.Parse(rdr["active"].ToString());
							contactDetail.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());
							contactDetails.Add(contactDetail);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return contactDetails;
		}
		public static void UpdateOperatorContactDetails(Guid IhreUserId, int OperatorID, string ContactDetailInfo, out ErrorObj Error)
		{
			Error = null;
			try
			{
				//	contact_detail_id,contact_type_id,contact_detail,active,is_primary
				DataTable updatesTable = new DataTable();
				updatesTable.Columns.Add("client_contact_detail_id", typeof(int));
				updatesTable.Columns.Add("contact_type_id", typeof(int));
				updatesTable.Columns.Add("contact_detail", typeof(string));
				updatesTable.Columns.Add("active", typeof(bool));
				updatesTable.Columns.Add("is_primary", typeof(bool));
				foreach (string contactDetailToUpdate in ContactDetailInfo.Split('*'))
				{
					string[] contactDetailDataCols = contactDetailToUpdate.Split(',');
					int? contactDetailId = contactDetailDataCols[0] != "null" ? int.Parse(contactDetailDataCols[0]) : (int?)null;
					int? contactDetailTypeId = contactDetailDataCols[1] != "null" ? int.Parse(contactDetailDataCols[1]) : (int?)null;
					string contactDetailText = contactDetailDataCols[2];
					bool contactDetailIsActive = bool.Parse(contactDetailDataCols[3]);
					bool contactDetailIsPrimary = bool.Parse(contactDetailDataCols[4]);
					updatesTable.Rows.Add(contactDetailId, contactDetailTypeId, contactDetailText, contactDetailIsActive, contactDetailIsPrimary);
				}
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorContactDetailsUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("client_contact_details_table", updatesTable);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
				updatesTable.Dispose();
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void RemoveOperatorContactDetail(Guid IhreUserId, int ClientContactDetailID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorContactDetailRemove"))
				{
					cmd.AddParamWithValue("client_contact_detail_id", ClientContactDetailID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		public static void UpdateOperatorLanguages(Guid IhreUserId, int OperatorID, string LanguageIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorLanguagesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("language_ids_str", LanguageIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorAffiliateTypes(Guid IhreUserId, int OperatorID, string AffiliateTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorAffiliateTypesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("affiliate_type_ids_str", AffiliateTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorDealTypes(Guid IhreUserId, int OperatorID, string DealTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorDealTypesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("deal_type_ids_str", DealTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorSoftwareTypes(Guid IhreUserId, int OperatorID, string SoftwareTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorSoftwareUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("software_ids_str", SoftwareTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorLicenceTypes(Guid IhreUserId, int OperatorID, string LicenceTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorLicencesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("licence_ids_str", LicenceTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorPaymentTypes(Guid IhreUserId, int OperatorID, string PaymentTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorPaymentTypesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("payment_type_ids_str", PaymentTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static List<Brand> GetOperatorBrands(Guid IhreUserId, int OperatorID, out ErrorObj Error)
		{
			List<Brand> brands = new List<Brand>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorBrandsSelect", false))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							brands.Add(new Brand(int.Parse(rdr["brand_id"].ToString()), rdr["brand_name"].ToString()));
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return brands;
		}
		public static void UpdateOperatorBrands(Guid IhreUserId, int OperatorID, string BrandInfo, out ErrorObj Error)
		{
			Error = null;
			try
			{
				//	brand_id,brand_name
				DataTable updatesTable = new DataTable();
				updatesTable.Columns.Add("brand_id", typeof(int));
				updatesTable.Columns.Add("client_id", typeof(int));
				updatesTable.Columns.Add("brand_name", typeof(string));
				if (BrandInfo != null && BrandInfo != "")
				{
					foreach (string brandToUpdate in BrandInfo.Split('*'))
					{
						string[] brandDataCols = brandToUpdate.Split(',');
						int? brandId = brandDataCols[0] != "null" ? int.Parse(brandDataCols[0]) : (int?)null;
						string brandName = brandDataCols[1];
						updatesTable.Rows.Add(brandId, null, brandName);
					}
				}
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorBrandsUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("brands_table", updatesTable);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
				updatesTable.Dispose();
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateOperatorCountries(Guid IhreUserId, int OperatorID, string CountryInfo, out ErrorObj Error)
		{
			Error = null;
			try
			{
				DataTable updatesTable = new DataTable();
				updatesTable.Columns.Add("country_id", typeof(int));
				updatesTable.Columns.Add("status", typeof(string));
				if (CountryInfo != null && CountryInfo != "")
				{
					foreach (string countryToUpdate in CountryInfo.Split('*'))
					{
						string[] countryDataCols = countryToUpdate.Split(',');
						int countryId = int.Parse(countryDataCols[0]);
						short countryStatus = short.Parse(countryDataCols[1]);
						if (countryStatus != -1)
						{
							updatesTable.Rows.Add(countryId, countryStatus);
						}
					}
				}
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorCountriesUpdate"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("client_countries_table", updatesTable);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}





		public static List<OperatorNote> GetOperatorNotes(Guid IhreUserId, int OperatorID, out ErrorObj Error)
		{
			List<OperatorNote> operatorNotes = new List<OperatorNote>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorNotesSelect"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							OperatorNote operatorNote = new OperatorNote();
							operatorNote.NoteID = int.Parse(rdr["note_id"].ToString());
							operatorNote.NoteText = rdr["note_text"].ToString();
							operatorNote.Created = DateTime.Parse(rdr["created"].ToString());
							operatorNote.Modified = DateTime.Parse(rdr["modified"].ToString());
							operatorNote.AccManagerUserName = rdr["AffManagerUserName"].ToString();
							operatorNotes.Add(operatorNote);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operatorNotes;
		}
		public static void UpdateOperatorNote(Guid IhreUserId, int NoteID, string NoteText, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorNoteUpdate"))
				{
					cmd.AddParamWithValue("note_id", NoteID);
					cmd.AddParamWithValue("note_text", NoteText);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static OperatorNote AddOperatorNote(Guid IhreUserId, int OperatorID, string NoteText, out ErrorObj Error)
		{
			OperatorNote operatorNote = new OperatorNote();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorNoteInsert", false, true))
				{
					cmd.AddParamWithValue("note_text", NoteText);
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							operatorNote.NoteID = int.Parse(rdr["note_id"].ToString());
							operatorNote.NoteText = rdr["note_text"].ToString();
							operatorNote.Created = DateTime.Parse(rdr["created"].ToString());
							operatorNote.Modified = DateTime.Parse(rdr["modified"].ToString());
							operatorNote.AccManagerUserName = rdr["AffManagerUserName"].ToString();
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operatorNote;
		}
		public static void RemoveOperatorNote(Guid IhreUserId, int NoteID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorNoteRemove"))
				{
					cmd.AddParamWithValue("note_id", NoteID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
	}



}