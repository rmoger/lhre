﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class DealStatus
	{
		public int DealStatusID { get; set; }
		public string DealStatusName { get; set; }
		public bool IsSelected { get; set; }
		public DealStatus(int DealStatusID, string DealStatusName)
		{
			this.DealStatusID = DealStatusID;
			this.DealStatusName = DealStatusName;
			this.IsSelected = false;
		}
	}
	public class DealType
	{
		public int DealTypeID { get; set; }
		public string DealTypeName { get; set; }
		public bool IsSelected { get; set; }
		public DealType(int DealTypeID, string DealTypeName)
		{
			this.DealTypeID = DealTypeID;
			this.DealTypeName = DealTypeName;
			this.IsSelected = false;
		}
	}
	public class DealInfo
	{
		public int DealInfoID { get; set; }
		public string DealInfoName { get; set; }
		public int DealInfoValueRank { get; set; }
		public bool IsSelected { get; set; }
		public DealInfo(int DealInfoID, string DealInfoName)
		{
			this.DealInfoID = DealInfoID;
			this.DealInfoName = DealInfoName;
			this.IsSelected = false;
		}
	}

	public class Deal
	{
		public int DealID { get; set; }
		public string DealText { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public DateTime DateSet { get; set; }
		public DealStatus DealStatus { get; set; }
		public DealInfo DealInfo { get; set; }
		public Affiliate Affiliate { get; set; }
		public Operator Operator { get; set; }
		public IhreUser AssignedUser { get; set; }
		public IhreUser CreatedUser { get; set; }
		public bool IsActive { get; set; }
		public bool IsSelected { get; set; }
		public bool EditModeOn { get; set; }
		public Deal()
		{
			this.IsSelected = false;
			this.EditModeOn = false;
		}
	}


	public class DealHelper
	{
		public static List<DealStatus> GetDealStatuses(Guid IhreUserId, out ErrorObj Error)
		{
			List<DealStatus> dealStatuses = new List<DealStatus>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealStatusesSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							DealStatus dealStatus = new DealStatus(
								int.Parse(rdr["deal_status_id"].ToString()),
								rdr["deal_status_name"].ToString()
							);
							dealStatuses.Add(dealStatus);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealStatuses;
		}
		
		public static List<DealType> GetDealTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<DealType> dealTypes = new List<DealType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealTypesSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							DealType dealType = new DealType(
								int.Parse(rdr["deal_type_id"].ToString()),
								rdr["deal_type_name"].ToString()
							);
							dealTypes.Add(dealType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealTypes;
		}
		public static void UpdateDealType(Guid IhreUserId, int DealTypeID, string DealTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealTypeUpdate"))
				{
					cmd.AddParamWithValue("deal_type_id", DealTypeID);
					cmd.AddParamWithValue("deal_type_name", DealTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static DealType AddDealType(Guid IhreUserId, string DealTypeName, out ErrorObj Error)
		{
			DealType dealType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealTypeInsert", false, true))
				{
					cmd.AddParamWithValue("deal_type_name", DealTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					dealType = new DealType(cmd.NewId, DealTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealType;
		}

		public static List<DealInfo> GetDealInfos(Guid IhreUserId, out ErrorObj Error)
		{
			List<DealInfo> dealInfos = new List<DealInfo>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealInfosSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							DealInfo dealInfo = new DealInfo(int.Parse(rdr["deal_info_id"].ToString()), rdr["deal_info_name"].ToString());
							dealInfo.DealInfoValueRank = int.Parse(rdr["deal_info_value_rank"].ToString());
							dealInfos.Add(dealInfo);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealInfos;
		}
		public static void UpdateDealInfo(Guid IhreUserId, int DealInfoID, string DealInfoName,int DealInfoValueRank, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealInfoUpdate"))
				{
					cmd.AddParamWithValue("deal_info_id", DealInfoID);
					cmd.AddParamWithValue("deal_info_name", DealInfoName);
					cmd.AddParamWithValue("deal_info_value_rank", DealInfoValueRank);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static DealInfo AddDealInfo(Guid IhreUserId, string DealInfoName, out ErrorObj Error)
		{
			DealInfo dealInfo = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealInfoInsert", false, true))
				{
					cmd.AddParamWithValue("deal_info_name", DealInfoName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					dealInfo = new DealInfo(cmd.NewId, DealInfoName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealInfo;
		}


		public static List<Deal> GetAffiliateDeals(Guid IhreUserId, int AffiliateID, bool IncludeInactive, Guid? AssignedUserId, int DealInfoID, int DealStatusID, out ErrorObj Error)
		{
			List<Deal> deals = new List<Deal>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateDealsSelect"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("include_inactive", IncludeInactive);
					cmd.AddParamWithValue("AssignedUserId", AssignedUserId);
					cmd.AddParamWithValue("deal_info_id", DealInfoID);
					cmd.AddParamWithValue("deal_status_id", DealStatusID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Deal deal = new Deal();
							deal.DealID = int.Parse(rdr["deal_id"].ToString());
							deal.DealText = rdr["deal_text"].ToString();
							deal.Created = DateTime.Parse(rdr["created"].ToString());
							deal.Modified = DateTime.Parse(rdr["modified"].ToString());
							deal.DateSet = DateTime.Parse(rdr["date_set"].ToString());
							deal.IsActive = bool.Parse(rdr["active"].ToString());
							deal.DealStatus = new DealStatus(int.Parse(rdr["deal_status_id"].ToString()), rdr["deal_status_name"].ToString());
							deal.DealInfo = new DealInfo(int.Parse(rdr["deal_info_id"].ToString()), rdr["deal_info_name"].ToString());
							deal.DealInfo.DealInfoValueRank = int.Parse(rdr["deal_info_value_rank"].ToString());
							deal.Operator = new Operator(int.Parse(rdr["client_id"].ToString()), rdr["client_name"].ToString());
							deal.AssignedUser = new IhreUser();
							if (rdr["assignedUserId"].ToString() != "")
							{
								deal.AssignedUser.UserId = Guid.Parse(rdr["assignedUserId"].ToString());
								deal.AssignedUser.UserName = rdr["assignedUserName"].ToString();
							}
							deal.CreatedUser = new IhreUser();
							if (rdr["createdUserId"].ToString() != "")
							{
								deal.CreatedUser.UserId = Guid.Parse(rdr["createdUserId"].ToString());
								deal.CreatedUser.UserName = rdr["createdUserName"].ToString();
							}
							deals.Add(deal);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return deals;
		}
		public static List<Deal> GetOperatorDeals(Guid IhreUserId, int OperatorID, bool IncludeInactive, Guid? AssignedUserId, int DealInfoID, int DealStatusID, out ErrorObj Error)
		{
			List<Deal> deals = new List<Deal>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorDealsSelect"))
				{
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("include_inactive", IncludeInactive);
					cmd.AddParamWithValue("AssignedUserId", AssignedUserId);
					cmd.AddParamWithValue("deal_info_id", DealInfoID);
					cmd.AddParamWithValue("deal_status_id", DealStatusID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Deal deal = new Deal();
							deal.DealID = int.Parse(rdr["deal_id"].ToString());
							deal.DealText = rdr["deal_text"].ToString();
							deal.Created = DateTime.Parse(rdr["created"].ToString());
							deal.Modified = DateTime.Parse(rdr["modified"].ToString());
							deal.DateSet = DateTime.Parse(rdr["date_set"].ToString());
							deal.IsActive = bool.Parse(rdr["active"].ToString());
							deal.DealStatus = new DealStatus(int.Parse(rdr["deal_status_id"].ToString()), rdr["deal_status_name"].ToString());
							deal.DealInfo = new DealInfo(int.Parse(rdr["deal_info_id"].ToString()), rdr["deal_info_name"].ToString());
							deal.DealInfo.DealInfoValueRank = int.Parse(rdr["deal_info_value_rank"].ToString());
							deal.Affiliate = new Affiliate(int.Parse(rdr["affiliate_id"].ToString()), rdr["affiliate_name"].ToString());
							deal.AssignedUser = new IhreUser();
							if (rdr["assignedUserId"].ToString() != "")
							{
								deal.AssignedUser.UserId = Guid.Parse(rdr["assignedUserId"].ToString());
								deal.AssignedUser.UserName = rdr["assignedUserName"].ToString();
							}
							deal.CreatedUser = new IhreUser();
							if (rdr["createdUserId"].ToString() != "")
							{
								deal.CreatedUser.UserId = Guid.Parse(rdr["createdUserId"].ToString());
								deal.CreatedUser.UserName = rdr["createdUserName"].ToString();
							}
							deals.Add(deal);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return deals;
		}

		public static Deal UpdateDeal(Guid IhreUserId, int DealID, Guid AssignedUserId, int DealStatusID, string DealText, int DealInfoID, bool IsActive, int OperatorID, int AffiliateID, out ErrorObj Error)
		{
			Deal deal = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealUpdate"))
				{
					cmd.AddParamWithValue("deal_id", DealID);
					cmd.AddParamWithValue("AssignedUserId", AssignedUserId);
					cmd.AddParamWithValue("deal_status_id", DealStatusID);
					cmd.AddParamWithValue("deal_text", DealText);
					cmd.AddParamWithValue("deal_info_id", DealInfoID);
					cmd.AddParamWithValue("active", IsActive);
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							deal = new Deal();
							deal.DealID = int.Parse(rdr["deal_id"].ToString());
							deal.DealText = rdr["deal_text"].ToString();
							deal.Created = DateTime.Parse(rdr["created"].ToString());
							deal.Modified = DateTime.Parse(rdr["modified"].ToString());
							deal.DateSet = DateTime.Parse(rdr["date_set"].ToString());
							deal.IsActive = bool.Parse(rdr["active"].ToString());
							deal.DealStatus = new DealStatus(int.Parse(rdr["deal_status_id"].ToString()), rdr["deal_status_name"].ToString());
							deal.DealInfo = new DealInfo(int.Parse(rdr["deal_info_id"].ToString()), rdr["deal_info_name"].ToString());
							deal.Operator = new Operator(int.Parse(rdr["client_id"].ToString()), rdr["client_name"].ToString());
							deal.Affiliate = new Affiliate(int.Parse(rdr["affiliate_id"].ToString()), rdr["affiliate_name"].ToString());
							if (rdr["assignedUserId"].ToString() != "")
							{
								deal.AssignedUser = new IhreUser(Guid.Parse(rdr["assignedUserId"].ToString()), rdr["assignedUserName"].ToString());
							}
							if (rdr["createdUserId"].ToString() != "")
							{
								deal.CreatedUser = new IhreUser(Guid.Parse(rdr["createdUserId"].ToString()), rdr["createdUserName"].ToString());
							}
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return deal;
		}
		public static Deal AddDeal(Guid IhreUserId, int DealStatusId, string DealText, int DealInfoID, int OperatorID, int AffiliateID, out ErrorObj Error)
		{
			Deal deal = new Deal();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateDealInsert", false, true))
				{
					cmd.AddParamWithValue("deal_status_id", DealStatusId);
					cmd.AddParamWithValue("deal_text", DealText);
					cmd.AddParamWithValue("deal_info_id", DealInfoID);
					cmd.AddParamWithValue("client_id", OperatorID);
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							deal.DealID = int.Parse(rdr["deal_id"].ToString());
							deal.DealText = rdr["deal_text"].ToString();
							deal.Created = DateTime.Parse(rdr["created"].ToString());
							deal.Modified = DateTime.Parse(rdr["modified"].ToString());
							deal.DateSet = DateTime.Parse(rdr["date_set"].ToString());
							deal.IsActive = bool.Parse(rdr["active"].ToString());
							deal.DealStatus = new DealStatus(int.Parse(rdr["deal_status_id"].ToString()), rdr["deal_status_name"].ToString());
							deal.DealInfo = new DealInfo(int.Parse(rdr["deal_info_id"].ToString()), rdr["deal_info_name"].ToString());
							deal.Operator = new Operator(int.Parse(rdr["client_id"].ToString()), rdr["client_name"].ToString());
							deal.Affiliate = new Affiliate(int.Parse(rdr["affiliate_id"].ToString()), rdr["affiliate_name"].ToString());
							if (rdr["assignedUserId"].ToString() != "")
							{
								deal.AssignedUser = new IhreUser(Guid.Parse(rdr["assignedUserId"].ToString()), rdr["assignedUserName"].ToString());
							}
							if (rdr["createdUserId"].ToString() != "")
							{
								deal.CreatedUser = new IhreUser(Guid.Parse(rdr["createdUserId"].ToString()), rdr["createdUserName"].ToString());
							}
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return deal;
		}



	}
}