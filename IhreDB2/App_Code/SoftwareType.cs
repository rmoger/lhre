﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class SoftwareType
	{
		public int SoftwareTypeID { get; set; }
		public string SoftwareTypeName { get; set; }
		public bool IsSelected { get; set; }
		public SoftwareType(int SoftwareTypeID, string SoftwareTypeName)
		{
			this.SoftwareTypeID = SoftwareTypeID;
			this.SoftwareTypeName = SoftwareTypeName;
			this.IsSelected = false;
		}
	}
	public class SoftwareHelper
	{
		public static List<SoftwareType> GetSoftwareTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<SoftwareType> softwareTypes = new List<SoftwareType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_SoftwareSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							SoftwareType softwareType = new SoftwareType(int.Parse(rdr["software_id"].ToString()), rdr["software_name"].ToString());
							softwareTypes.Add(softwareType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error =new ErrorObj(ex.ToString());
			}
			return softwareTypes;
		}
		public static void UpdateSoftwareType(Guid IhreUserId, int SoftwareTypeID, string SoftwareTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_SoftwareUpdate"))
				{
					cmd.AddParamWithValue("software_id", SoftwareTypeID);
					cmd.AddParamWithValue("software_name", SoftwareTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static SoftwareType AddSoftwareType(Guid IhreUserId, string SoftwareTypeName, out ErrorObj Error)
		{
			SoftwareType softwareType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_SoftwareInsert", false, true))
				{
					cmd.AddParamWithValue("software_name", SoftwareTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					softwareType = new SoftwareType(cmd.NewId, SoftwareTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return softwareType;
		}

	}
}