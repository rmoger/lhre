﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class ChartData
	{
		public List<ChartSeriesData> SeriesData {get;set;}
		public ChartData()
		{
			this.SeriesData = new List<ChartSeriesData>();
		}
	}

	public class ChartSeriesData
	{
		public string SeriesName { get; set; }
		public List<object> SeriesValues { get; set; }
		public void AddData(string Name, int Value)
		{
			List<object> newData = new List<object>();
			newData.Add(Name);
			newData.Add(Value);
			this.SeriesValues.Add(newData);
		}
		public ChartSeriesData(string SeriesName)
		{
			this.SeriesName = SeriesName;
			this.SeriesValues = new List<object>();
		}
	}


}