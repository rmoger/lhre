﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class Country
	{
		public int CountryID { get; set; }
		public string CountryName { get; set; }
		public bool IsSelected { get; set; }
		public Int16 Status { get; set; }//used for operator banned or targetted
		public Country(int CountryID, string CountryName)
		{
			this.CountryID = CountryID;
			this.CountryName = CountryName;
			this.IsSelected = false;
		}
	}

	public static class CountryHelper
	{
		public static List<Country> GetCountries(Guid IhreUserId, out ErrorObj Error)
		{
			List<Country> countries = new List<Country>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CountriesSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Country country = new Country(
								int.Parse(rdr["country_id"].ToString()),
								rdr["country_name"].ToString()
							);
							countries.Add(country);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return countries;
		}
		public static void UpdateCountry(Guid IhreUserId, int CountryID, string CountryName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CountryUpdate"))
				{
					cmd.AddParamWithValue("country_id", CountryID);
					cmd.AddParamWithValue("country_name", CountryName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static Country AddCountry(Guid IhreUserId, string CountryName, out ErrorObj Error)
		{
			Country country = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CountryInsert", false, true))
				{
					cmd.AddParamWithValue("country_name", CountryName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					country = new Country(cmd.NewId, CountryName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return country;
		}


	}
}