﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace IhreDB2.App_Code
{
	public class IhreRole
	{
		public string RoleName { get; set; }
		public bool UserHasRole { get; set; }
		public IhreRole(string RoleName)
		{
			this.RoleName = RoleName;
		}
	}
	public class IhreUser
	{
		public Guid? UserId { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Email { get; set; }
		public bool IsApproved { get; set; }
		public bool IsLockedOut { get; set; }
		public bool IsOnline { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime LastLoginDate { get; set; }
		public string Comment { get; set; }
		public string Status { get; set; }
		public bool IsSelected { get; set; }
		//public List<Role
		public IhreUser()
		{
			this.IsSelected = false;
		}
		public IhreUser(Guid UserId, string UserName)
		{
			this.UserId = UserId;
			this.UserName = UserName;
			this.IsSelected = false;
		}
	}

	public static class IhreUserHelper
	{
		public static List<IhreUser> GetIhreUsers(Guid CurrentUserId, bool Lite, out ErrorObj Error)
		{
			return GetIhreUsers(CurrentUserId, Lite, false, out Error);
		}
		public static List<IhreUser> GetIhreUsers(Guid CurrentUserId, bool Lite, bool CurrentUserSelected, out ErrorObj Error)
		{
			List<IhreUser> ihreUsers = new List<IhreUser>();
			Error = null;
			try
			{
				bool ShowUnapprovedUsers = Roles.IsUserInRole("ShowInactiveUsersInFilters") || Roles.IsUserInRole("SuperUser");
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_UsersSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							if (bool.Parse(rdr["IsApproved"].ToString()) || ShowUnapprovedUsers)
							{

								IhreUser ihreUser = new IhreUser();
								ihreUser.UserId = Guid.Parse(rdr["UserId"].ToString());
								ihreUser.UserName = rdr["UserName"].ToString();
								if (CurrentUserSelected)
								{
									ihreUser.IsSelected = ihreUser.UserId == CurrentUserId;
								}
								if (!Lite)
								{
									ihreUser.Email = rdr["Email"].ToString();
									ihreUser.IsApproved = bool.Parse(rdr["IsApproved"].ToString());
									ihreUser.IsLockedOut = bool.Parse(rdr["IsLockedOut"].ToString());
									ihreUser.IsOnline = bool.Parse(rdr["IsOnline"].ToString());
									ihreUser.CreateDate = DateTime.Parse(rdr["CreateDate"].ToString());
									ihreUser.LastLoginDate = DateTime.Parse(rdr["LastLoginDate"].ToString());
									ihreUser.Comment = rdr["Comment"].ToString();
									if (ihreUser.IsLockedOut)
									{
										ihreUser.Status = "LOCKED";
									}
									else if (!ihreUser.IsApproved)
									{
										ihreUser.Status = "DISABLED";
									}
									else
									{
										ihreUser.Status = "ACTIVE";
									}
								}
								ihreUsers.Add(ihreUser);
							}
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return ihreUsers;
		}
		public static bool ActivateUser(Guid CurrentUserId, Guid IhreUserId, out ErrorObj Error)
		{
			Error = null;
			try
			{
				MembershipUser mu = Membership.GetUser(IhreUserId);
				mu.IsApproved = true;
				Membership.UpdateUser(mu);
				return true;
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
				return false;
			}
		}
		public static bool DeactivateUser(Guid CurrentUserId, Guid IhreUserId, out ErrorObj Error)
		{
			Error = null;
			try
			{
				MembershipUser mu = Membership.GetUser(IhreUserId);
				mu.IsApproved = false;
				Membership.UpdateUser(mu);
				return true;
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
				return false;
			}
		}
		public static bool UnlockUser(Guid CurrentUserId, Guid IhreUserId, out ErrorObj Error)
		{
			Error = null;
			try
			{
				MembershipUser mu = Membership.GetUser(IhreUserId);
				mu.UnlockUser();
				return true;
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
				return false;
			}
		}

		public static Dictionary<string, bool> GetCurrentUserRolesDictionary(Guid CurrentUserId, string UserName, out ErrorObj Error)
		{
			Error = null;
			Dictionary<string, bool> RetVal = new Dictionary<string, bool>();
			try
			{
				foreach (string role in Roles.GetAllRoles())
				{
					RetVal.Add(role, Roles.IsUserInRole(UserName, role));
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}

		public static List<IhreRole> GetIhreUserRoles(Guid CurrentUserId, string UserName, out ErrorObj Error)
		{
			Error = null;
			List<IhreRole> RetVal = new List<IhreRole>();
			try
			{
				foreach (string role in Roles.GetAllRoles())
				{
					IhreRole ihreRole = new IhreRole(role);
					ihreRole.UserHasRole = UserName == null ? false : Roles.IsUserInRole(UserName, role);
					RetVal.Add(ihreRole);
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}
		public static bool UpdateUserRoles(Guid CurrentUserId, string userName, string roles, out ErrorObj Error)
		{
			Error = null;
			try
			{
				string[] currentRoles = Roles.GetRolesForUser(userName);
				if (currentRoles.Length > 0)
				{
					Roles.RemoveUserFromRoles(userName, currentRoles);
				}
				Roles.AddUserToRoles(userName, roles.Split('_'));
				return true;
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return false;
		}
		public static bool NewIhreUser(Guid CurrentUserId, string UserName, string Password, string Email, string roles, out ErrorObj Error)
		{
			Error = null;
			try
			{
				MembershipUser mu = null;
				MembershipCreateStatus createdStatus;
				mu = Membership.CreateUser(
					UserName,
					Password,
					Email,
					null,
					null,
					true,
					out createdStatus
				);
				if (mu == null)
				{
					switch (createdStatus)
					{
						case MembershipCreateStatus.DuplicateEmail:
							Error.ErrorMessage = "Duplicate Email";
							break;
						case MembershipCreateStatus.DuplicateUserName:
							Error.ErrorMessage = "Duplicate Username";
							break;
						case MembershipCreateStatus.InvalidEmail:
							Error.ErrorMessage = "Invalid Email";
							break;
						case MembershipCreateStatus.InvalidPassword:
							Error.ErrorMessage = "Invalid Password";
							break;
						case MembershipCreateStatus.InvalidUserName:
							Error.ErrorMessage = "Invalid Username";
							break;
						default:
							Error.ErrorMessage = "Unknown error";
							break;
					}
					return false;
				}
				else
				{
					if (roles != null)
					{
						Roles.AddUserToRoles(UserName, roles.Split('_'));
					}
				}
			}
			catch (Exception ex)
			{
				Membership.DeleteUser(UserName, true);
				Error = new ErrorObj(ex.ToString());
				return false;
			}
			return true;
		}
		public static bool UpdatePassword(Guid CurrentUserId, string NewPassword, out ErrorObj Error)
		{
			Error = null;
			try
			{
				MembershipUser mu = Membership.GetUser(CurrentUserId);
				string tempPw = mu.ResetPassword();
				mu.ChangePassword(tempPw, NewPassword);
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
				return false;
			}
			return true;
		}
		public static ChartData GetUserStats(Guid IhreUserId, DateTime? From, DateTime? To, out ErrorObj Error)
		{
			ChartData chartData = new ChartData();
			Error = null;
			try
			{
				ChartSeriesData userCountSeries = new ChartSeriesData("User Deals");
				ChartSeriesData otherUsersCountSeries = new ChartSeriesData("Other Users");
				ChartSeriesData totalCountSeries = new ChartSeriesData("Total");

				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_UserDealsStats"))
				{
					cmd.AddParamWithValue("from", From);
					cmd.AddParamWithValue("to", To);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							if (rdr["deal_status_name"].ToString() != "Existing")
							{
								userCountSeries.AddData(rdr["deal_status_name"].ToString(), int.Parse(rdr["user_count"].ToString()));
								otherUsersCountSeries.AddData(rdr["deal_status_name"].ToString(), int.Parse(rdr["total_count"].ToString()) - int.Parse(rdr["user_count"].ToString()));
								totalCountSeries.AddData(rdr["deal_status_name"].ToString(), int.Parse(rdr["total_count"].ToString()));
							}
						}
					}
					Error = cmd.CheckErrors();
				}
				chartData.SeriesData.Add(userCountSeries);
				chartData.SeriesData.Add(otherUsersCountSeries);
				chartData.SeriesData.Add(totalCountSeries);
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return chartData;
		}

	}

}