﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace IhreDB2.App_Code
{
	public class JsonRequest
	{
		public Dictionary<string, string> Values;
		public JsonRequest(string RequestDataStr)
		{
			bool DebugModeOn = Boolean.Parse(ConfigurationManager.AppSettings["debugmodeon"]);
			this.Values = new Dictionary<string, string>();
			if (!DebugModeOn)
			{
				RequestDataStr = Encoding.UTF8.GetString(Convert.FromBase64String(RequestDataStr));
			}
			foreach (string RequestPart in RequestDataStr.Split(new string[] { "***" }, StringSplitOptions.None))
			{
				string[] RequestPartSplit = RequestPart.Split('|');
				if (RequestPartSplit[1] == "null")
				{
					RequestPartSplit[1] = null;
				}
				this.Values.Add(RequestPartSplit[0], RequestPartSplit[1]);
			}
		}
		public short GetValueShort(string Key)
		{
			short RetVal = 0;
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					if (!short.TryParse(this.Values[Key], out RetVal))
					{
						RetVal = 0;
					}
				}
			}
			catch { }
			return RetVal;
		}
		public int GetValueInt(string Key)
		{
			int RetVal = 0;
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					if (!int.TryParse(this.Values[Key], out RetVal))
					{
						RetVal = 0;
					}
				}
			}
			catch { }
			return RetVal;
		}
		public bool GetValueBool(string Key)
		{
			bool RetVal = false;
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					RetVal = this.Values[Key] == "1" || this.Values[Key].ToLower() == "true";
				}
			}
			catch { }
			return RetVal;
		}
		public decimal GetValueDecimal(string Key)
		{
			decimal RetVal = 0;
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					if (!decimal.TryParse(this.Values[Key], out RetVal))
					{
						RetVal = 0;
					}
				}
			}
			catch { }
			return RetVal;
		}
		public string GetValueString(string Key)
		{
			return GetValueString(Key, null);
		}
		public string GetValueString(string Key, string ValueIfNull)
		{
			string RetVal = ValueIfNull;
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					RetVal = this.Values[Key].Trim();
				}
			}
			catch { }
			return RetVal;
		}
		public DateTime? GetValueDateTime(string Key)
		{
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					DateTime AttemptedParse;
					if (!DateTime.TryParse(this.Values[Key], out AttemptedParse))
					{
						return null;
					}
					else
					{
						return AttemptedParse;
					}
				}
			}
			catch { }
			return null;
		}

		public Guid? GetValueGuid(string Key)
		{
			try
			{
				if (this.Values[Key] != null && this.Values[Key] != "")
				{
					Guid AttemptedParse;
					if (!Guid.TryParse(this.Values[Key], out AttemptedParse))
					{
						return null;
					}
					else
					{
						return AttemptedParse;
					}
				}
			}
			catch { }
			return null;

		}


	}
}