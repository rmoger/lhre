﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class PaymentType
	{
		public int PaymentTypeID { get; set; }
		public string PaymentTypeName { get; set; }
		public bool IsSelected { get; set; }
		public PaymentType(int PaymentTypeID, string PaymentTypeName)
		{
			this.PaymentTypeID = PaymentTypeID;
			this.PaymentTypeName = PaymentTypeName;
			this.IsSelected = false;
		}
	}
	public static class PaymentTypeHelper
	{
		public static List<PaymentType> GetPaymentTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<PaymentType> paymentTypes = new List<PaymentType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_PaymentTypesSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							PaymentType paymentType = new PaymentType(int.Parse(rdr["payment_type_id"].ToString()), rdr["payment_type_name"].ToString());
							paymentTypes.Add(paymentType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return paymentTypes;
		}
		public static void UpdatePaymentType(Guid IhreUserId, int PaymentTypeID, string PaymentTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_PaymentTypeUpdate"))
				{
					cmd.AddParamWithValue("payment_type_id", PaymentTypeID);
					cmd.AddParamWithValue("payment_type_name", PaymentTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static PaymentType AddPaymentType(Guid IhreUserId, string PaymentTypeName, out ErrorObj Error)
		{
			PaymentType paymentType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_PaymentTypeInsert", false, true))
				{
					cmd.AddParamWithValue("payment_type_name", PaymentTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					paymentType = new PaymentType(cmd.NewId, PaymentTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return paymentType;
		}
	}
}