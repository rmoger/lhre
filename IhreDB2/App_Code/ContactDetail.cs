﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class ContactDetail
	{
		public int ContactDetailID { get; set; }
		public int AffiliateID { get; set; }
		public int OperatorID { get; set; }
		public ContactDetailType ContactType { get; set; }
		public string ContactDetailText { get; set; }
		public bool IsActive { get; set; }
		public bool IsPrimary { get; set; }
		public ContactDetail() { }
	}
	public class ContactDetailType
	{
		public int ContactDetailTypeID { get; set; }
		public string ContactDetailTypeName { get; set; }
		public ContactDetailType() { }
		public ContactDetailType(int ContactDetailTypeID, string ContactDetailTypeName)
		{
			this.ContactDetailTypeID = ContactDetailTypeID;
			this.ContactDetailTypeName = ContactDetailTypeName;
		}
	}


	public class ContactDetailHelper
	{
		public static List<ContactDetailType> GetContactDetailTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<ContactDetailType> contactDetailTypes = new List<ContactDetailType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_ContactDetailTypesSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							ContactDetailType contactDetailType = new ContactDetailType();
							contactDetailType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
							contactDetailType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							contactDetailTypes.Add(contactDetailType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return contactDetailTypes;
		}
		public static void UpdateContactDetailType(Guid IhreUserId, int ContactDetailTypeID, string ContactDetailTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_ContactDetailTypeUpdate"))
				{
					cmd.AddParamWithValue("contact_type_id", ContactDetailTypeID);
					cmd.AddParamWithValue("contact_type_name", ContactDetailTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static ContactDetailType AddContactDetailType(Guid IhreUserId, string ContactDetailTypeName, out ErrorObj Error)
		{
			ContactDetailType contactDetailType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_ContactDetailTypeInsert", false, true))
				{
					cmd.AddParamWithValue("contact_type_name", ContactDetailTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					contactDetailType = new ContactDetailType(cmd.NewId, ContactDetailTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return contactDetailType;
		}
	}
}