﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace IhreDB2.App_Code
{
	public class wordpress : IHttpHandler
	{
		private HttpContext _context = null;

		public void ProcessRequest(HttpContext context)
		{
			this._context = context;
			switch (context.Request["action"])
			{
				case "markets": GetMarkets(); break;
				case "types": GetTypes(); break;
				case "search": Search(); break;
				case "register": Register(); break;
				case "markettypes": GetMarketTypes(); break;
				case "dealtypes": GetDealTypes(); break;
				case "operators": GetOperators(); break;
			}
		}
		private void GetMarkets()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loadmarkets({content:\"");
			try
			{
				ErrorObj Error = null;
				List<Country> countries = CountryHelper.GetCountries(new Guid(), out Error);
				foreach (Country country in countries)
				{
					this._context.Response.Write(string.Format(
						"<div class='filter-container'><input type='checkbox' class='filter-market' value='{0}' id='market-cb-{0}' /><label for='market-cb-{0}'>{1}</label></div>",
						country.CountryID.ToString(),
						country.CountryName
					));
				}
			}
			catch { }
			this._context.Response.Write("\"});");
		}
		private void GetTypes()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loadtypes({content:\"");
			try
			{
				ErrorObj Error = null;
				List<AffiliateType> affiliateTypes = AffiliateHelper.GetAffiliateTypes(new Guid(), out Error);
				foreach (AffiliateType affiliateType in affiliateTypes)
				{
					if (affiliateType.AffiliateTypeName != "Generic" && affiliateType.AffiliateTypeName != "Non Gaming")
					{
						this._context.Response.Write(string.Format(
							"<div class='filter-container'><input type='checkbox' class='filter-type' value='{0}' id='type-cb-{0}' /><label for='type-cb-{0}'>{1}</label></div>",
							affiliateType.AffiliateTypeID.ToString(),
							affiliateType.AffiliateTypeName
						));
					}
				}
			}
			catch { }
			this._context.Response.Write("\"});");
		}
		private void Search()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loadsearchresults({content:\"");
			SqlCommand cmd = new SqlCommand("db2_wpClientSearch", new SqlConnection(ConfigurationManager.ConnectionStrings["AffiliatesDbConnectionString"].ConnectionString));
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			string FilterSearch = this._context.Request["search"];
			string Markets = this._context.Request["markets"];
			string Types = this._context.Request["types"];

			cmd.Parameters.AddWithValue("search_str", FilterSearch == "" ? "%" : FilterSearch);
			cmd.Parameters.AddWithValue("countries", Markets != null ? Markets : "");
			cmd.Parameters.AddWithValue("affiliate_types", Types != null ? Types : "");

			cmd.Connection.Open();
			SqlDataReader rdr = cmd.ExecuteReader();
			if (!rdr.HasRows)
			{
				this._context.Response.Write("<p>No results</p>");
			}
			else
			{
				this._context.Response.Write("<table class='wp-table-reloaded wp-table-reloaded-id-44; id='wp-table-reloaded-id-44-no-1'><tbody>");

				int rowNum = 0;
				while (rdr.Read())
				{
					rowNum++;
					string oddEve = rowNum % 2 == 0 ? "even" : "odd";
					bool hasReviewUrl = rdr["website_review_url"].ToString() != "";
					bool hasSignupUrl = rdr["affiliate_signup_url"].ToString() != "";

					this._context.Response.Write(
						string.Format(
							"<tr class='{0}'><td class='column-1'>{1}</td><td class='column-2'>{2}{3}<a href='{4}' target='{5}'>Sign up</a></td></tr>",
							"row-" + rowNum.ToString() + " " + oddEve,
							rdr["client_name"].ToString(),
							hasReviewUrl ? "<a href='" + rdr["website_review_url"].ToString() + "'>Full Operator Review</a>" : "",
							hasReviewUrl ? " | " : "",
							hasSignupUrl ? rdr["affiliate_signup_url"].ToString() : ConfigurationManager.AppSettings["wordpress-website-register-url"] + "?clientid=" + rdr["client_id"].ToString() + "&clientname=" + HttpUtility.UrlEncode(rdr["client_name"].ToString()),
							hasSignupUrl ? "_blank" : "_self"
						)
					);
				}
				this._context.Response.Write("</tbody></table>");
			}
			rdr.Dispose();
			cmd.Connection.Close();
			cmd.Dispose();
			this._context.Response.Write("\"});");

		}

		private void Register()
		{
			try
			{
				string RegisterEmailBody = string.Format(@"
					<p><strong>Name: </strong>{0}</p>
					<p><strong>Website: </strong><a href='{1}' target='_blank'>{1}</a></p>
					<p><strong>Email: </strong><a href='mailto:{2}'>{2}</a></p>
					<p><strong>Other: </strong>{3}</p>
					<p><strong>Marketer Types: </strong>{4}</p>
					<p><strong>Deal Types: </strong>{5}</p>
					<p><strong>Operators: </strong>{6}</p>
					<p><strong>Comments: </strong>{7}</p>
					",
					this._context.Request["contactName"],
					this._context.Request["website"],
					this._context.Request["email"],
					this._context.Request["other"],
					this._context.Request["marketertypes"],
					this._context.Request["dealtypes"],
					this._context.Request["operators"],
					this._context.Request["commentsText"]
				);

				MailMessage mm = new MailMessage(
					this._context.Request["email"].ToString(),
					ConfigurationManager.AppSettings["wordpress-website-register-mail-to"],
					"Website Request",
					RegisterEmailBody
				);
				mm.IsBodyHtml = true;
				SmtpClient sc = new SmtpClient();
				//sc.EnableSsl = true;
				sc.Send(mm);
				this._context.Response.ContentType = "application/json";
				this._context.Response.Write("ihre_registercallback({content:\"Thank you. Your request was successfully sent.\"});");
			}
			catch (Exception ex)
			{
				this._context.Response.ContentType = "application/json";
				this._context.Response.Write("ihre_registercallback({content:\"There was an error submitting your request. Please try again or email " + ConfigurationManager.AppSettings["wordpress-website-register-mail-to"] + ".\"});");
			}

		}

		private void GetMarketTypes()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loadmarkettypes({content:\"");
			this._context.Response.Write("<div class='filter-container'><input type='checkbox' title='Select all' class='filter-markettype' value='0' id='markettype-cb-0' /><label for='markettype-cb-0'>Select all</label></div>");
			try
			{
				ErrorObj Error = null;
				List<MarketingType> marketingTypes = AffiliateHelper.GetMarketingTypes(new Guid(), out Error);
				foreach (MarketingType marketingType in marketingTypes)
				{
					this._context.Response.Write(string.Format(
						"<div class='filter-container'><input type='checkbox' title='{1}' class='filter-markettype' value='{0}' id='markettype-cb-{0}' /><label for='markettype-cb-{0}'>{1}</label></div>",
						marketingType.MarketingTypeID.ToString(),
						marketingType.MarketingTypeName
					));
				}
			}
			catch { }
			this._context.Response.Write("\"});");
		}
		private void GetDealTypes()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loaddealtypes({content:\"");
			this._context.Response.Write("<div class='filter-container'><input type='checkbox' title='Select all' class='filter-dealtype' value='0' id='dealtype-cb-0' /><label for='dealtype-cb-0'>Select all</label></div>");
			try
			{
				ErrorObj Error = null;
				List<DealType> dealTypes = DealHelper.GetDealTypes(new Guid(), out Error);
				foreach (DealType dealType in dealTypes)
				{
					this._context.Response.Write(string.Format(
						"<div class='filter-container'><input type='checkbox' title='{1}' class='filter-dealtype' value='{0}' id='dealtype-cb-{0}' /><label for='dealtype-cb-{0}'>{1}</label></div>",
						dealType.DealTypeID.ToString(),
						dealType.DealTypeName
					));
				}
			}
			catch { }
			this._context.Response.Write("\"});");
		}
		private void GetOperators()
		{
			this._context.Response.ContentType = "application/json";
			this._context.Response.Write("ihre_loadoperators({content:\"");
			this._context.Response.Write("<div class='filter-container'><input type='checkbox' title='Select all' class='filter-operator' value='0' id='operator-cb-0' /><label for='operator-cb-0'>Select all</label></div>");
			try
			{
				ErrorObj Error = null;
				List<Operator> operators = OperatorHelper.GetOperatorsLite(new Guid(), out Error);
				foreach (Operator op in operators)
				{
					this._context.Response.Write(
						string.Format(
							"<div class='filter-container'><input type='checkbox' title='{1}' class='filter-operator' value='{0}' id='operator-cb-{0}' /><label for='operator-cb-{0}'>{1}</label></div>",
							op.OperatorID.ToString(),
							op.OperatorName
						)
					);
				}
			}
			catch { }
			this._context.Response.Write("\"});");
		}

		public bool IsReusable
		{
			// Return false in case your Managed Handler cannot be reused for another request.
			// Usually this would be false in case you have some state information preserved per request.
			get { return true; }
		}
	}
}