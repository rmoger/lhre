﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace IhreDB2.App_Code
{
	public class ReportData
	{
		public List<ReportOptionsField> Headers { get; set; }
		public List<List<string>> Rows { get; set; }
		public SortInfo SortInfo { get; set; }
		public int Count { get; set; }
		public ReportData()
		{
			this.Headers = new List<ReportOptionsField>();
			this.Rows = new List<List<string>>();
			this.SortInfo = new SortInfo();
			this.Count = 0;
		}
	}
	public class ReportOptions
	{
		public SortInfo SortInfo { get; set; }
		public ReportOptionsField SortField { get; set; }
		public List<ReportOptionsField> Fields { get; set; }

		public ReportOptions()
		{
			this.Fields = new List<ReportOptionsField>();
			this.SortInfo = new SortInfo();
		}
		public void SetSortOrder(ReportOptionsField Field, bool IsAsc)
		{
			this.SortInfo.Field = Field;
			this.SortInfo.Dir = IsAsc ? "asc" : "desc";
			Field.Sort = this.SortInfo.Dir;
		}
		public void AddField(string Label, string DBID, string DBType)
		{
			this.Fields.Add(new ReportOptionsField(Label, DBID, DBType, false, true, false, false, false));
		}
		public void AddField(string Label, string DBID, string DBType, bool IsOptional, bool IsChecked, bool CanFilter, bool IsGroupBy, bool CanTotal)
		{
			this.Fields.Add(new ReportOptionsField(Label, DBID, DBType, IsOptional, IsChecked, CanFilter, IsGroupBy, CanTotal));
		}

		public void AddFieldPrime(string Label, string DBID, string DBType)
		{
			ReportOptionsField field = new ReportOptionsField(Label, DBID, DBType, false, true, false, true, false);
			field.Sort = "asc";
			this.Fields.Add(field);
			this.SetSortOrder(field, true);
		}

	}
	public class ReportOptionsField
	{
		public int Index { get; set; }
		public string Label { get; set; }
		public string ID { get; set; }
		public bool IsOptional { get; set; }
		public bool IsChecked { get; set; }
		public bool CanFilter { get; set; }
		public bool IsGroupBy { get; set; }
		public string Type { get; set; }
		public string Sort { get; set; }
		public bool CanTotal { get; set; }
		public ReportOptionsField(int Index, string Label, string ID, string Type, string Sort)
		{
			this.Index = Index;
			this.Label = Label;
			this.ID = ID;
			this.Type = Type;
			this.Sort = Sort;
		}
		public ReportOptionsField(string Label, string ID, string Type, bool IsOptional, bool IsChecked, bool CanFilter, bool IsGroupBy, bool CanTotal)
		{
			this.Label = Label;
			this.ID = ID;
			this.Type = Type;
			this.IsOptional = IsOptional;
			this.IsChecked = IsChecked;
			this.CanFilter = CanFilter;
			this.IsGroupBy = IsGroupBy;
			this.CanTotal = CanTotal;
			this.Sort = null;
		}
	}

	public class SortInfo
	{
		public ReportOptionsField Field { get; set; }
		public string Dir { get; set; }
		public SortInfo() { }
	}
	public class DbSummary
	{
		public int TotalAffiliates { get; set; }
		public int ActiveAffiliates { get; set; }
		public List<object[]> DealsBreakdown { get; set; }
		public DbSummary()
		{
			this.TotalAffiliates = 0;
			this.ActiveAffiliates = 0;
			this.DealsBreakdown = new List<object[]>();
		}
	}

	public class CustomReportInfo
	{
		public int CustomReportID { get; set; }
		public string CustomReportName { get; set; }
		public string CustomReportType { get; set; }
		public string CustomReportParamsStr { get; set; }
		public bool CreatedByCurrentUser { get; set; }
		public CustomReportInfo() { }
		public CustomReportInfo(int CustomReportID, string CustomReportName, string CustomReportType, string CustomReportParamsStr)
		{
			this.CustomReportID = CustomReportID;
			this.CustomReportName = CustomReportName;
			this.CustomReportType = CustomReportType;
			this.CustomReportParamsStr = CustomReportParamsStr;
			this.CreatedByCurrentUser = false;
		}
	}

	public class DealSummary
	{
		public string SummaryType { get; set; }
		public List<DealSummaryRow> Rows { get; set; }
		public DealSummary()
		{
			this.Rows = new List<DealSummaryRow>();
		}
	}
	public class DealSummaryRow
	{
		public object ID { get; set; }
		public string Label { get; set; }
		public Dictionary<int, int> Cols { get; set; }
		public DealSummaryRow()
		{
			this.Cols = new Dictionary<int, int>();
		}
	}


	public static class ReportHelper
	{
		public static List<CustomReportInfo> GetCustomReports(Guid IhreUserId, out ErrorObj Error)
		{
			List<CustomReportInfo> RetVal = new List<CustomReportInfo>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CustomReportsSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							CustomReportInfo customReportInfo = new CustomReportInfo(
								int.Parse(rdr["custom_report_id"].ToString()),
								rdr["custom_report_name"].ToString(),
								rdr["custom_report_type"].ToString(),
								rdr["custom_report_params"].ToString()
							);
							if (Guid.Parse(rdr["created_UserId"].ToString()) == IhreUserId)
							{
								customReportInfo.CreatedByCurrentUser = true;
							}
							RetVal.Add(customReportInfo);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}
		public static void RemoveCustomReport(Guid IhreUserId, int CustomReportID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CustomReportRemove"))
				{
					cmd.AddParamWithValue("custom_report_id", CustomReportID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		public static CustomReportInfo GetCustomReportInfo(Guid IhreUserId, int CustomReportID, out ErrorObj Error)
		{
			CustomReportInfo RetVal = new CustomReportInfo();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CustomReportSelect"))
				{
					cmd.AddParamWithValue("custom_report_id", CustomReportID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						if (rdr.HasRows)
						{
							rdr.Read();
							RetVal = new CustomReportInfo(
								int.Parse(rdr["custom_report_id"].ToString()),
								rdr["custom_report_name"].ToString(),
								rdr["custom_report_type"].ToString(),
								rdr["custom_report_params"].ToString()
							);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}

		public static int AddCustomReport(Guid IhreUserId, string CustomReportName, string CustomReportType, string CustomReportParams, Guid? UserId, out ErrorObj Error)
		{
			int NewCustomReportID = -1;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_CustomReportInsert", false, true))
				{
					cmd.AddParamWithValue("custom_report_name", CustomReportName);
					cmd.AddParamWithValue("custom_report_type", CustomReportType);
					cmd.AddParamWithValue("custom_report_params", CustomReportParams);
					cmd.AddParamWithValue("UserId", UserId);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
					NewCustomReportID = cmd.NewId;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return NewCustomReportID;
		}



		public static object[] CreateReportOption(string Label, string DBID, string DBType, bool IsOptional, bool IsChecked, bool CanFilter, bool IsGroupBy, bool IsDefaultSortField, bool CanTotal)
		{
			return new object[] { Label, DBID, DBType, IsOptional, IsChecked, CanFilter, IsGroupBy, IsDefaultSortField, CanTotal };
		}
		public static ReportOptions GetReportOptions(object[] reportOptionsArr, string Fields, out string ErrorMessage)
		{
			ErrorMessage = null;
			string[] FieldsArr = new string[0];
			if (Fields != null)
			{
				FieldsArr = Fields.Split('$');
			}
			ReportOptions reportOptions = new ReportOptions();
			foreach (object[] obj in reportOptionsArr)
			{
				if (Fields == null || FieldsArr.Contains(obj[1]))
				{
					ReportOptionsField field = new ReportOptionsField((string)obj[0], (string)obj[1], (string)obj[2], (bool)obj[3], (bool)obj[4], (bool)obj[5], (bool)obj[6], (bool)obj[8]);
					if ((bool)obj[7])
					{
						field.Sort = "asc";
						reportOptions.SetSortOrder(field, true);
					}
					reportOptions.Fields.Add(field);
				}
			}
			return reportOptions;
		}

		public static DbSummary GetDbSummary(Guid CurrentUserId, out ErrorObj Error)
		{
			DbSummary RetVal = new DbSummary();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_DealsBreakdownSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						if (rdr.HasRows)
						{
							rdr.Read();
							RetVal.TotalAffiliates = int.Parse(rdr["total_affiliates"].ToString());
							RetVal.ActiveAffiliates = int.Parse(rdr["active_affiliates"].ToString());
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							RetVal.DealsBreakdown.Add(new object[] { rdr["deal_status_name"].ToString(), int.Parse(rdr["count"].ToString()) });
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return RetVal;
		}

		public static ReportOptions GetAffiliateReportOptions(bool IncAffiliateTypes, bool IncDealTypes, bool IncMarketingTypes, bool IncCountries, bool IncOperators, out ErrorObj Error)
		{
			Error = null;
			ReportOptions reportOptions = new ReportOptions();

			reportOptions.AddField("ID", "affiliate_id", "int");

			ReportOptionsField sortedField = new ReportOptionsField("Affiliate", "affiliate_name", "string", false, true, false, false, false);
			sortedField.Sort = "asc";
			reportOptions.SetSortOrder(sortedField, true);
			reportOptions.Fields.Add(sortedField);

			reportOptions.AddField("First Name", "contact_name", "string");
			if (Roles.IsUserInRole("UnrestrictedReports") || Roles.IsUserInRole("SuperUser"))
			{
				reportOptions.AddField("Acc. Manager", "AffManagerUserName", "string");
			}
			//contact_type_id
			//contact_type_name
			reportOptions.AddField("Primary Contact", "contact_detail", "string");
			reportOptions.AddField("Primary URL", "url_text", "string");
			reportOptions.AddField("Size", "affiliate_size", "short");
			reportOptions.AddField("Contactable", "allow_contact", "bool");
			reportOptions.AddField("MaxMail", "allow_contact", "bool");
			if (IncAffiliateTypes)
			{
				reportOptions.AddField("Affiliate Types", "affiliate_types", "string");
			}
			if (IncDealTypes)
			{
				reportOptions.AddField("Deal Types", "deal_types", "string");
			}
			if (IncMarketingTypes)
			{
				reportOptions.AddField("Marketing Types", "marketing_types", "string");
			}
			if (IncCountries)
			{
				reportOptions.AddField("Countries", "countries", "string");
			}
			if (IncOperators)
			{
				reportOptions.AddField("Operators", "clients", "string");
			}
			return reportOptions;
		}
		public static ReportData GetAffiliateReport(Guid IhreUserId, string SearchStr, string AffManagerIDs, bool IncludeUnassignedAffiliates, Int16 AffiliateSizeType, Int16 Contactable, Int16 MaxMailType, Int16 AffStatusType, Int16 OperatorOption, string OperatorIDs, string CountryOption, string CountryIDs, string AffiliateTypeOption, string AffiliateTypeIDs, string DealTypeOption, string DealTypeIDs, string MarketingTypeOption, string MarketingTypeIDs, DateTime? CreatedFrom, DateTime? CreatedTo, bool IncAffiliateTypes, bool IncDealTypes, bool IncMarketingTypes, bool IncCountries, bool IncOperators, bool CheckQuerySpeed, out int ResultCount, out ErrorObj Error)
		{
			ResultCount = 0;
			Error = null;
			ReportData reportData = new ReportData();
			ReportOptions reportOptions = GetAffiliateReportOptions(IncAffiliateTypes, IncDealTypes, IncMarketingTypes, IncCountries, IncOperators, out Error);
			reportData.Headers = reportOptions.Fields;
			reportData.SortInfo = reportOptions.SortInfo;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateSearch", true))
				{
					cmd.AddParamWithValue("search_str", SearchStr);
					cmd.AddParamWithValue("affmanager_userid_list", AffManagerIDs);
					cmd.AddParamWithValue("include_unassigned", IncludeUnassignedAffiliates);
					cmd.AddParamWithValue("affiliate_size", AffiliateSizeType);//smallint
					cmd.AddParamWithValue("contactable", Contactable);
					cmd.AddParamWithValue("maxmail", MaxMailType);//smallint
					cmd.AddParamWithValue("active", AffStatusType);

					cmd.AddParamWithValue("created_from", CreatedFrom);
					cmd.AddParamWithValue("created_to", CreatedTo);

					if (CountryIDs != null && CountryIDs != "")
					{
						cmd.AddParamWithValue("countrysearch", CountryOption);
						cmd.AddParamWithValue("country_id_list", CountryIDs);
					}
					if (OperatorIDs != null && OperatorIDs != "")
					{
						cmd.AddParamWithValue("clientsearch", OperatorOption);
						cmd.AddParamWithValue("client_id_list", OperatorIDs);
					}
					if (AffiliateTypeIDs != null && AffiliateTypeIDs != "")
					{
						cmd.AddParamWithValue("affiliate_typesearch", AffiliateTypeOption);
						cmd.AddParamWithValue("affiliate_type_id_list", AffiliateTypeIDs);
					}
					if (DealTypeIDs != null && DealTypeIDs != "")
					{
						cmd.AddParamWithValue("deal_typesearch", DealTypeOption);
						cmd.AddParamWithValue("deal_type_id_list", DealTypeIDs);
					}
					if (MarketingTypeIDs != null && MarketingTypeIDs != "")
					{
						cmd.AddParamWithValue("marketing_typesearch", MarketingTypeOption);
						cmd.AddParamWithValue("marketing_type_id_list", MarketingTypeIDs);
					}

					cmd.AddParamWithValue("inc_affiliate_types", IncAffiliateTypes);
					cmd.AddParamWithValue("inc_deal_types", IncDealTypes);
					cmd.AddParamWithValue("inc_marketing_types", IncMarketingTypes);
					cmd.AddParamWithValue("inc_countries", IncCountries);
					cmd.AddParamWithValue("inc_clients", IncOperators);

					cmd.AddParamWithValue("check_query_speed", CheckQuerySpeed);
					cmd.AddParamWithValue("offset", 0);
					cmd.AddParamWithValue("count", 9999999);
					cmd.OpenConnection();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							List<string> dataRow = new List<string>();
							foreach (ReportOptionsField field in reportData.Headers)
							{
								dataRow.Add(rdr[field.ID].ToString());
							}
							reportData.Rows.Add(dataRow);
						}
					}
					Error = cmd.CheckErrors();
					ResultCount = cmd.ResultCount;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return reportData;
		}

		public static ReportOptions GetOperatorReportOptions(bool IncBrands, bool IncSignupUrls, bool IncCountries, bool IncLanguages, bool IncAffiliateTypes, bool IncDealTypes, bool IncSoftwareTypes, bool IncLicenceTypes, bool IncPaymentTypes, bool IncSecondaryContactDetails, out ErrorObj Error)
		{
			Error = null;
			ReportOptions reportOptions = new ReportOptions();

			reportOptions.AddField("ID", "client_id", "int");

			ReportOptionsField sortedField = new ReportOptionsField("Operator", "client_name", "string", false, true, false, false, false);
			sortedField.Sort = "asc";
			reportOptions.SetSortOrder(sortedField, true);
			reportOptions.Fields.Add(sortedField);
			reportOptions.AddField("Primary Contact", "contact_detail", "string");

			if (IncBrands)
			{
				reportOptions.AddField("Brands", "brands_str", "string");
			}
			if (IncSignupUrls)
			{
				reportOptions.AddField("Signup URL", "affiliate_signup_url", "string");
			}
			if (IncCountries)
			{
				reportOptions.AddField("Countries", "countries", "string");
			}
			if (IncLanguages)
			{
				reportOptions.AddField("Languages", "languages", "string");
			}
			if (IncAffiliateTypes)
			{
				reportOptions.AddField("Affiliate Types", "affiliate_types", "string");
			}
			if (IncDealTypes)
			{
				reportOptions.AddField("Deal Types", "deal_types", "string");
			}
			if (IncSoftwareTypes)
			{
				reportOptions.AddField("Software Types", "software_types", "string");
			}
			if (IncSoftwareTypes)
			{
				reportOptions.AddField("Software Types", "software_types", "string");
			}
			if (IncLicenceTypes)
			{
				reportOptions.AddField("Licence Types", "licence_types", "string");
			}
			if (IncPaymentTypes)
			{
				reportOptions.AddField("Payment Types", "payment_types", "string");
			}
			if (IncSecondaryContactDetails)
			{
				reportOptions.AddField("Secondary Contact Details", "secondary_contact_details", "string");
			}
			return reportOptions;
		}
		public static ReportData GetOperatorReport(Guid IhreUserId, Int16 StatusType, DateTime? CreatedFrom, DateTime? CreatedTo, string CountryOption, string CountryIDs, string LanguageOption, string LanguageIDs, string AffiliateTypeOption, string AffiliateTypeIDs, string DealTypeOption, string DealTypeIDs, string SoftwareTypeOption, string SoftwareTypeIDs, string LicenceTypeOption, string LicenceTypeIDs, string PaymentTypeOption, string PaymentTypeIDs, bool IncBrands, bool IncSignupUrls, bool IncCountries, bool IncLanguages, bool IncAffiliateTypes, bool IncDealTypes, bool IncSoftwareTypes, bool IncLicenceTypes, bool IncPaymentTypes, bool IncSecondaryContactDetails, bool CheckQuerySpeed, out int ResultCount, out ErrorObj Error)
		{
			ResultCount = 0;
			Error = null;
			ReportData reportData = new ReportData();
			ReportOptions reportOptions = GetOperatorReportOptions(IncBrands, IncSignupUrls, IncCountries, IncLanguages, IncAffiliateTypes, IncDealTypes, IncSoftwareTypes, IncLicenceTypes, IncPaymentTypes, IncSecondaryContactDetails, out Error);
			reportData.Headers = reportOptions.Fields;
			reportData.SortInfo = reportOptions.SortInfo;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_OperatorSearch", true))
				{
					cmd.AddParamWithValue("active", StatusType);
					cmd.AddParamWithValue("created_from", CreatedFrom);
					cmd.AddParamWithValue("created_to", CreatedTo);

					if (CountryIDs != null && CountryIDs != "")
					{
						cmd.AddParamWithValue("countrysearch", CountryOption);
						cmd.AddParamWithValue("country_id_list", CountryIDs);
					}
					if (LanguageIDs != null && LanguageIDs != "")
					{
						cmd.AddParamWithValue("languagesearch", LanguageOption);
						cmd.AddParamWithValue("language_id_list", LanguageIDs);
					}
					if (AffiliateTypeIDs != null && AffiliateTypeIDs != "")
					{
						cmd.AddParamWithValue("affiliate_typesearch", AffiliateTypeOption);
						cmd.AddParamWithValue("affiliate_type_id_list", AffiliateTypeIDs);
					}
					if (DealTypeIDs != null && DealTypeIDs != "")
					{
						cmd.AddParamWithValue("deal_typesearch", DealTypeOption);
						cmd.AddParamWithValue("deal_type_id_list", DealTypeIDs);
					}

					if (SoftwareTypeIDs != null && SoftwareTypeIDs != "")
					{
						cmd.AddParamWithValue("software_typesearch", SoftwareTypeOption);
						cmd.AddParamWithValue("software_type_id_list", SoftwareTypeIDs);
					}
					if (LicenceTypeIDs != null && LicenceTypeIDs != "")
					{
						cmd.AddParamWithValue("licence_typesearch", LicenceTypeOption);
						cmd.AddParamWithValue("licence_type_id_list", LicenceTypeIDs);
					}
					if (PaymentTypeIDs != null && PaymentTypeIDs != "")
					{
						cmd.AddParamWithValue("payment_typesearch", PaymentTypeOption);
						cmd.AddParamWithValue("payment_type_id_list", PaymentTypeIDs);
					}

					cmd.AddParamWithValue("inc_countries", IncCountries);
					cmd.AddParamWithValue("inc_languages", IncLanguages);
					cmd.AddParamWithValue("inc_affiliate_types", IncAffiliateTypes);
					cmd.AddParamWithValue("inc_deal_types", IncDealTypes);
					cmd.AddParamWithValue("inc_software_types", IncSoftwareTypes);
					cmd.AddParamWithValue("inc_licence_types", IncLicenceTypes);
					cmd.AddParamWithValue("inc_payment_types", IncPaymentTypes);
					cmd.AddParamWithValue("inc_secondary_contact_details", IncSecondaryContactDetails);

					cmd.AddParamWithValue("check_query_speed", CheckQuerySpeed);
					cmd.AddParamWithValue("offset", 0);
					cmd.AddParamWithValue("count", 9999999);
					cmd.OpenConnection();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							List<string> dataRow = new List<string>();
							foreach (ReportOptionsField field in reportData.Headers)
							{
								dataRow.Add(rdr[field.ID].ToString());
							}
							reportData.Rows.Add(dataRow);
						}
					}
					Error = cmd.CheckErrors();
					ResultCount = cmd.ResultCount;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return reportData;
		}




		public static ReportOptions GetDealReportOptions(out ErrorObj Error)
		{
			Error = null;
			ReportOptions reportOptions = new ReportOptions();
			reportOptions.AddField("ID", "deal_id", "int");



			reportOptions.AddField("Affiliate ID", "affiliate_id", "int");
			reportOptions.AddField("Affiliate", "affiliate_name", "string");
			//reportOptions.AddField("Affiliate First Name", "contact_name", "string");

			reportOptions.AddField("Operator ID", "client_id", "int");
			reportOptions.AddField("Operator", "client_name", "string");

			//reportOptions.AddField("Active", "active", "bool");

			//reportOptions.AddField("Deal Info ID", "deal_info_id", "int");
			reportOptions.AddField("Deal Info", "deal_info_name", "string");
			//reportOptions.AddField("Deal Info Rank", "deal_info_value_rank", "int");

			//reportOptions.AddField("Deal Status ID", "deal_status_id", "int");
			reportOptions.AddField("Deal Status", "deal_status_name", "string");

			reportOptions.AddField("Notes", "deal_text", "string");

			ReportOptionsField sortedField = new ReportOptionsField("Deal Set", "date_set", "datetime", false, true, false, false, false);
			sortedField.Sort = "desc";
			reportOptions.SetSortOrder(sortedField, true);
			reportOptions.Fields.Add(sortedField);

			if (Roles.IsUserInRole("UnrestrictedReports") || Roles.IsUserInRole("SuperUser"))
			{
				reportOptions.AddField("Assigned To", "AssignedUserName", "string");
			}
			//reportOptions.AddField("Assigned User ID", "AssignedUserId", "string");

			reportOptions.AddField("Created", "created", "datetime");
			if (Roles.IsUserInRole("UnrestrictedReports") || Roles.IsUserInRole("SuperUser"))
			{
				reportOptions.AddField("Created By", "CreatedUserName", "string");
			}
			//reportOptions.AddField("Created By User ID", "CreatedUserId", "string");


			return reportOptions;
		}
		public static ReportData GetDealReport(Guid IhreUserId, string AffManagerOption, string AffManagerIDs, bool IncludeUnassignedAffiliates, Int16 DealActiveStatusType, DateTime? CreatedFrom, DateTime? CreatedTo, DateTime? DateSetFrom, DateTime? DateSetTo, string DealStatusIDs, string DealInfoIDs, bool CheckQuerySpeed, out int ResultCount, out ErrorObj Error)
		{
			ResultCount = 0;
			Error = null;
			ReportData reportData = new ReportData();
			ReportOptions reportOptions = GetDealReportOptions(out Error);
			reportData.Headers = reportOptions.Fields;
			reportData.SortInfo = reportOptions.SortInfo;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_DealSearch", true))
				{
					if (AffManagerIDs != null && AffManagerIDs != "")
					{
						cmd.AddParamWithValue("affmanager_option", AffManagerOption);
						cmd.AddParamWithValue("affmanager_userid_list", AffManagerIDs);
					}
					cmd.AddParamWithValue("include_unassigned", IncludeUnassignedAffiliates);
					cmd.AddParamWithValue("active", DealActiveStatusType);
					cmd.AddParamWithValue("created_from", CreatedFrom);
					cmd.AddParamWithValue("created_to", CreatedTo);
					cmd.AddParamWithValue("date_set_from", DateSetFrom);
					cmd.AddParamWithValue("date_set_to", DateSetTo);
					cmd.AddParamWithValue("deal_status_id_list", DealStatusIDs);
					cmd.AddParamWithValue("deal_info_id_list", DealInfoIDs);
					cmd.AddParamWithValue("check_query_speed", CheckQuerySpeed);
					cmd.AddParamWithValue("offset", 0);
					cmd.AddParamWithValue("count", 9999999);
					cmd.OpenConnection();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							List<string> dataRow = new List<string>();
							foreach (ReportOptionsField field in reportData.Headers)
							{
								dataRow.Add(rdr[field.ID].ToString());
							}
							reportData.Rows.Add(dataRow);
						}
					}
					Error = cmd.CheckErrors();
					ResultCount = cmd.ResultCount;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return reportData;
		}





		public static List<Dictionary<string, object>> GetDealsReport(Guid CurrentUserId, Guid? FilterUserId, int? FilterClientId, int? FilterAffiliateId, bool? FilterActive, string FilterDealStatusIdsStr, string FilterDealInfoIdsStr, DateTime? FilterFrom, DateTime? FilterTo, int Offset, int Count, out int ResultCount, out ErrorObj Error)
		{
			List<Dictionary<string, object>> report = new List<Dictionary<string, object>>();
			ResultCount = 0;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_DealSearch", true))
				{
					if (FilterUserId != null)
					{
						cmd.AddParamWithValue("affmanager_option", "ASSIGNED");
						cmd.AddParamWithValue("affmanager_userid_list", FilterUserId.ToString());
					}
					cmd.AddParamWithValue("include_unassigned", 1);
					cmd.AddParamWithValue("active", FilterActive == true ? 1 : 0);
					cmd.AddParamWithValue("created_from", null);
					cmd.AddParamWithValue("created_to", null);
					cmd.AddParamWithValue("date_set_from", FilterFrom);
					cmd.AddParamWithValue("date_set_to", FilterTo);
					cmd.AddParamWithValue("deal_status_id_list", FilterDealStatusIdsStr);
					cmd.AddParamWithValue("deal_info_id_list", FilterDealInfoIdsStr);
					cmd.AddParamWithValue("check_query_speed", false);
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Dictionary<string, object> row = new Dictionary<string, object>();
							for (int c = 0; c < rdr.FieldCount; c++)
							{
								row[rdr.GetName(c)] = rdr[c];
							}
							report.Add(row);
						}
					}
					Error = cmd.CheckErrors();
					ResultCount = cmd.ResultCount;
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return report;
		}
		public static List<Dictionary<string, object>> GetAffiliateDealsReport(Guid CurrentUserId, Guid? FilterUserId, int? FilterClientId, int? FilterAffiliateId, bool? FilterActive, string FilterDealStatusIdsStr, string FilterDealInfoIdsStr, DateTime? FilterFrom, DateTime? FilterTo, int Offset, int Count, out int ResultCount, out ErrorObj Error)
		{
			List<Dictionary<string, object>> report = new List<Dictionary<string, object>>();
			ResultCount = 0;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_ReportAffiliateDeals", true))
				{
					cmd.AddParamWithValue("filter_UserId", FilterUserId);
					cmd.AddParamWithValue("filter_client_id", FilterClientId);
					cmd.AddParamWithValue("filter_affiliate_id", FilterAffiliateId);
					cmd.AddParamWithValue("active", FilterActive);
					cmd.AddParamWithValue("deal_status_id_list", FilterDealStatusIdsStr);
					cmd.AddParamWithValue("deal_info_id_list", FilterDealInfoIdsStr);
					cmd.AddParamWithValue("filter_from", FilterFrom);
					cmd.AddParamWithValue("filter_to", FilterTo);
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Dictionary<string, object> row = new Dictionary<string, object>();
							for (int c = 0; c < rdr.FieldCount; c++)
							{
								row[rdr.GetName(c)] = rdr[c];
							}
							report.Add(row);
						}
					}
					ResultCount = cmd.ResultCount;
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return report;
		}
		public static List<Dictionary<string, object>> GetOperatorDealsReport(Guid CurrentUserId, Guid? FilterUserId, int? FilterClientId, int? FilterAffiliateId, bool? FilterActive, string FilterDealStatusIdsStr, string FilterDealInfoIdsStr, DateTime? FilterFrom, DateTime? FilterTo, int Offset, int Count, out int ResultCount, out ErrorObj Error)
		{
			List<Dictionary<string, object>> report = new List<Dictionary<string, object>>();
			ResultCount = 0;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_ReportOperatorDeals", true))
				{
					cmd.AddParamWithValue("filter_UserId", FilterUserId);
					cmd.AddParamWithValue("filter_client_id", FilterClientId);
					cmd.AddParamWithValue("filter_affiliate_id", FilterAffiliateId);
					cmd.AddParamWithValue("active", FilterActive);
					cmd.AddParamWithValue("deal_status_id_list", FilterDealStatusIdsStr);
					cmd.AddParamWithValue("deal_info_id_list", FilterDealInfoIdsStr);
					cmd.AddParamWithValue("filter_from", FilterFrom);
					cmd.AddParamWithValue("filter_to", FilterTo);
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Dictionary<string, object> row = new Dictionary<string, object>();
							for (int c = 0; c < rdr.FieldCount; c++)
							{
								row[rdr.GetName(c)] = rdr[c];
							}
							report.Add(row);
						}
					}
					ResultCount = cmd.ResultCount;
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return report;
		}

/*
		public static List<Dictionary<string, object>> GetOperatorDealsSummaryReport(Guid CurrentUserId, int Offset, int Count, out int ResultCount, out ErrorObj Error)
		{
			List<Dictionary<string, object>> report = new List<Dictionary<string, object>>();
					
			ResultCount = 0;
			Error = null;
			try
			{
				Dictionary<string, Dictionary<string, string>> Results = new Dictionary<string, Dictionary<string, string>>();
				List<DealStatus> DealStatuses = DealHelper.GetDealStatuses(CurrentUserId, out Error);

				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_ReportOperatorDealsSummary", true))
				{
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							string OperatorName = rdr["client_name"].ToString();
							string DealStatusId = rdr["status"].ToString();
							string DealStatusCount = rdr["count"].ToString();

							if (!Results.ContainsKey(OperatorName))
							{
								Results.Add(OperatorName, new Dictionary<string, string>());
							}
							Results[OperatorName].Add(DealStatusId, DealStatusCount);
						}
					}
					Error = cmd.CheckErrors();
					ResultCount = cmd.ResultCount;
				}

				foreach (KeyValuePair<string,  Dictionary<string, string>> operatorItem in Results)
				{
					Dictionary<string, object> reportRow = new Dictionary<string, object>();
					reportRow.Add("Operator", operatorItem.Key);
					foreach (DealStatus dealStatusItem in DealStatuses)
					{
						reportRow.Add(dealStatusItem.DealStatusName, operatorItem.Value[dealStatusItem.DealStatusID.ToString()]);
					}

				}


			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return report;
		}*/



		public static DealSummary GetOperatorDealsSummaryReport(Guid CurrentUserId, Int16 FilterDealStatusType, DateTime? FilterFrom, DateTime? FilterTo, out ErrorObj Error)
		{
			DealSummary dealSummary = new DealSummary();
			dealSummary.SummaryType = "OperatorDealsSummary";
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_OperatorDealsSummaryReport"))
				{
					cmd.AddParamWithValue("active", FilterDealStatusType);
					cmd.AddParamWithValue("filter_from", FilterFrom);
					cmd.AddParamWithValue("filter_to", FilterTo);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							DealSummaryRow row = new DealSummaryRow();
							row.ID = rdr["client_id"].ToString();
							row.Label = rdr["client_name"].ToString();
							row.Cols.Add(2, int.Parse(rdr["deal_status_2"].ToString()));
							row.Cols.Add(3, int.Parse(rdr["deal_status_3"].ToString()));
							row.Cols.Add(4, int.Parse(rdr["deal_status_4"].ToString()));
							row.Cols.Add(5, int.Parse(rdr["deal_status_5"].ToString()));
							row.Cols.Add(6, int.Parse(rdr["deal_status_6"].ToString()));
							row.Cols.Add(7, int.Parse(rdr["deal_status_7"].ToString()));
							row.Cols.Add(8, int.Parse(rdr["deal_status_8"].ToString()));
							row.Cols.Add(10, int.Parse(rdr["deal_status_10"].ToString()));
							row.Cols.Add(11, int.Parse(rdr["deal_status_11"].ToString()));
							row.Cols.Add(12, int.Parse(rdr["deal_status_12"].ToString()));
							dealSummary.Rows.Add(row);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealSummary;
		}
		public static DealSummary GetUserDealsSummaryReport(Guid CurrentUserId, Int16 FilterDealStatusType, DateTime? FilterFrom, DateTime? FilterTo, out ErrorObj Error)
		{
			DealSummary dealSummary = new DealSummary();
			dealSummary.SummaryType = "UserDealsSummary";
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(CurrentUserId, "db2_UserDealsSummaryReport"))
				{
					cmd.AddParamWithValue("active", FilterDealStatusType);
					cmd.AddParamWithValue("filter_from", FilterFrom);
					cmd.AddParamWithValue("filter_to", FilterTo);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							DealSummaryRow row = new DealSummaryRow();
							row.ID = rdr["UserId"].ToString();
							row.Label = rdr["UserName"].ToString();
							row.Cols.Add(2, int.Parse(rdr["deal_status_2"].ToString()));
							row.Cols.Add(3, int.Parse(rdr["deal_status_3"].ToString()));
							row.Cols.Add(4, int.Parse(rdr["deal_status_4"].ToString()));
							row.Cols.Add(5, int.Parse(rdr["deal_status_5"].ToString()));
							row.Cols.Add(6, int.Parse(rdr["deal_status_6"].ToString()));
							row.Cols.Add(7, int.Parse(rdr["deal_status_7"].ToString()));
							row.Cols.Add(8, int.Parse(rdr["deal_status_8"].ToString()));
							row.Cols.Add(10, int.Parse(rdr["deal_status_10"].ToString()));
							row.Cols.Add(11, int.Parse(rdr["deal_status_11"].ToString()));
							row.Cols.Add(12, int.Parse(rdr["deal_status_12"].ToString()));
							dealSummary.Rows.Add(row);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return dealSummary;
		}

	}

}