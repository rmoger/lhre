﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class Affiliate
	{
		public int AffiliateID { get; set; }
		public string AffiliateName { get; set; }
		public string ContactName { get; set; }
		public IhreUser AffManagerUser { get; set; }
		public bool AllowContact {get;set;}
		public bool IsSignedUp {get;set;}
		public bool IsActive {get;set;}
		public bool MaxMail { get; set; }
		public Int16 AffiliateSize { get; set; }
		public string Comment { get; set; }
		public ContactDetail PrimaryContactDetail { get; set; }
		public AffiliateURL PrimaryURL { get; set; }

		public List<AffiliateType> AffiliateTypes { get; set; }
		public List<DealType> DealTypes { get; set; }
		public List<MarketingType> MarketingTypes { get; set; }
		public List<ContactDetail> ContactDetails { get; set; }
		public List<AffiliateURL> URLs { get; set; }
		public List<Country> Countries { get; set; }

		public int ConfirmedActiveDealsCount { get; set; }
		public int ConfirmedInactiveDealsCount { get; set; }

		public bool IsSelected { get; set; }
		public Affiliate()
		{
			this.IsSelected = false;
		}
		public Affiliate(int AffiliateID, string AffiliateName)
		{
			this.AffiliateID = AffiliateID;
			this.AffiliateName = AffiliateName;
			this.IsSelected = false;
		}
	}
	public class AffiliateType
	{
		public int AffiliateTypeID { get; set; }
		public string AffiliateTypeName { get; set; }
		public bool IsSelected { get; set; }
		public AffiliateType(int AffiliateTypeID, string AffiliateTypeName)
		{
			this.AffiliateTypeID = AffiliateTypeID;
			this.AffiliateTypeName = AffiliateTypeName;
			this.IsSelected = false;
		}
	}
	public class MarketingType
	{
		public int MarketingTypeID { get; set; }
		public string MarketingTypeName { get; set; }
		public bool IsSelected { get; set; }
		public MarketingType(int MarketingTypeID, string MarketingTypeName)
		{
			this.MarketingTypeID = MarketingTypeID;
			this.MarketingTypeName = MarketingTypeName;
			this.IsSelected = false;
		}
	}
	public class AffiliateURL
	{
		public int UrlID { get; set; }
		public string Url { get; set; }
		public int AffiliateID { get; set; }
		public bool IsActive { get; set; }
		public Int16? PageRank { get; set; }
		public bool IsPrimary { get; set; }
		public AffiliateURL() { }
	}
	public class AffiliateNote
	{
		public int NoteID { get; set; }
		public string NoteText { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public string AccManagerUserName { get; set; }
		public bool IsSelected { get; set; }
		public AffiliateNote() {
			this.IsSelected = false;
		}
	}
			



	public static class AffiliateHelper
	{
		public static List<Affiliate> GetAffiliates(Guid IhreUserId,
			string SearchStr, string AffManagerIDs, bool IncludeUnassignedAffiliates, Int16 AffiliateSizeType, Int16 Contactable, Int16 MaxMailType, Int16 AffStatusType, Int16 OperatorOption, string OperatorIDs, string CountryOption, string CountryIDs, string AffiliateTypeOption, string AffiliateTypeIDs, string DealTypeOption, string DealTypeIDs, string MarketingTypeOption, string MarketingTypeIDs,
			DateTime? CreatedFrom, DateTime? CreatedTo,

			bool IncAffiliateTypes, bool IncDealTypes, bool IncMarketingTypes, bool IncCountries, bool IncOperators,

			bool CheckQuerySpeed, int Offset, int Count, out int ResultCount, out ErrorObj Error
		)
		{
			List<Affiliate> affiliates = new List<Affiliate>();
			ResultCount = 0;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateSearch", true))
				{
					cmd.AddParamWithValue("search_str", SearchStr);
					cmd.AddParamWithValue("affmanager_userid_list", AffManagerIDs);
					cmd.AddParamWithValue("include_unassigned", IncludeUnassignedAffiliates);
					cmd.AddParamWithValue("affiliate_size", AffiliateSizeType);//smallint
					cmd.AddParamWithValue("contactable", Contactable);
					cmd.AddParamWithValue("maxmail", MaxMailType);//smallint
					cmd.AddParamWithValue("active", AffStatusType);
					if (CountryIDs != null && CountryIDs != "")
					{
						cmd.AddParamWithValue("countrysearch", CountryOption);
						cmd.AddParamWithValue("country_id_list", CountryIDs);
					}
					if (OperatorIDs != null && OperatorIDs != "")
					{
						cmd.AddParamWithValue("clientsearch", OperatorOption);
						cmd.AddParamWithValue("client_id_list", OperatorIDs);
					}
					if (AffiliateTypeIDs != null && AffiliateTypeIDs != "")
					{
						cmd.AddParamWithValue("affiliate_typesearch", AffiliateTypeOption);
						cmd.AddParamWithValue("affiliate_type_id_list", AffiliateTypeIDs);
					}
					if (DealTypeIDs != null && DealTypeIDs != "")
					{
						cmd.AddParamWithValue("deal_typesearch", DealTypeOption);
						cmd.AddParamWithValue("deal_type_id_list", DealTypeIDs);
					}
					if (MarketingTypeIDs != null && MarketingTypeIDs != "")
					{
						cmd.AddParamWithValue("marketing_typesearch", MarketingTypeOption);
						cmd.AddParamWithValue("marketing_type_id_list", MarketingTypeIDs);
					}

					cmd.AddParamWithValue("inc_affiliate_types", IncAffiliateTypes);
					cmd.AddParamWithValue("inc_deal_types", IncDealTypes);
					cmd.AddParamWithValue("inc_marketing_types", IncMarketingTypes);
					cmd.AddParamWithValue("inc_countries", IncCountries);
					cmd.AddParamWithValue("inc_clients", IncOperators);

					cmd.AddParamWithValue("check_query_speed", CheckQuerySpeed);
					cmd.AddParamWithValue("offset", Offset);
					cmd.AddParamWithValue("count", Count);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Affiliate aff = new Affiliate();
							aff.AffiliateID = int.Parse(rdr["affiliate_id"].ToString());
							aff.AffiliateName = rdr["affiliate_name"].ToString();
							aff.ContactName = rdr["contact_name"].ToString();
							aff.AffManagerUser = new IhreUser();
							aff.AffManagerUser.UserName = rdr["AffManagerUserName"].ToString();
							aff.PrimaryContactDetail = new ContactDetail();
							if (rdr["contact_type_id"].ToString() != "")
							{
								aff.PrimaryContactDetail.ContactDetailText = rdr["contact_detail"].ToString();
								aff.PrimaryContactDetail.ContactType = new ContactDetailType();
								aff.PrimaryContactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
								aff.PrimaryContactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							}
							aff.PrimaryURL = new AffiliateURL();
							aff.PrimaryURL.Url = rdr["url_text"].ToString();
							affiliates.Add(aff);
						}
					}
					ResultCount = cmd.ResultCount;
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliates;
		}
		
		public static List<AffiliateType> GetAffiliateTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<AffiliateType> affiliateTypes = new List<AffiliateType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateTypesSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							AffiliateType affiliateType = new AffiliateType(
								int.Parse(rdr["affiliate_type_id"].ToString()),
								rdr["affiliate_type_name"].ToString()
							);
							affiliateTypes.Add(affiliateType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliateTypes;
		}
		public static void UpdateAffiliateType(Guid IhreUserId, int AffiliateTypeID, string AffiliateTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateTypeUpdate"))
				{
					cmd.AddParamWithValue("affiliate_type_id", AffiliateTypeID);
					cmd.AddParamWithValue("affiliate_type_name", AffiliateTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static AffiliateType AddAffiliateType(Guid IhreUserId, string AffiliateTypeName, out ErrorObj Error)
		{
			AffiliateType affiliateType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateTypeInsert", false, true))
				{
					cmd.AddParamWithValue("affiliate_type_name", AffiliateTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					affiliateType = new AffiliateType(cmd.NewId, AffiliateTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliateType;
		}

		public static List<MarketingType> GetMarketingTypes(Guid IhreUserId, out ErrorObj Error)
		{
			List<MarketingType> marketingTypes = new List<MarketingType>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_MarketingTypesSelect", false))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							MarketingType marketingType = new MarketingType(
								int.Parse(rdr["marketing_type_id"].ToString()),
								rdr["marketing_type_name"].ToString()
							);
							marketingTypes.Add(marketingType);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return marketingTypes;
		}
		public static void UpdateMarketingType(Guid IhreUserId, int MarketingTypeID, string MarketingTypeName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_MarketingTypeUpdate"))
				{
					cmd.AddParamWithValue("marketing_type_id", MarketingTypeID);
					cmd.AddParamWithValue("marketing_type_name", MarketingTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static MarketingType AddMarketingType(Guid IhreUserId, string MarketingTypeName, out ErrorObj Error)
		{
			MarketingType marketingType = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_MarketingTypeInsert", false, true))
				{
					cmd.AddParamWithValue("marketing_type_name", MarketingTypeName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					marketingType = new MarketingType(cmd.NewId, MarketingTypeName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return marketingType;
		}

	


		public static Affiliate GetAffiliate(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			Affiliate affiliate = new Affiliate();
			affiliate.ContactDetails = new List<ContactDetail>();
			affiliate.URLs = new List<AffiliateURL>();
			affiliate.Countries = new List<Country>();
			affiliate.AffiliateTypes = new List<AffiliateType>();
			affiliate.DealTypes = new List<DealType>();
			affiliate.MarketingTypes = new List<MarketingType>();

			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateSelect", false))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							affiliate.AffiliateID = int.Parse(rdr["affiliate_id"].ToString());
							affiliate.AffiliateName = rdr["affiliate_name"].ToString();
							affiliate.ContactName = rdr["contact_name"].ToString();
							affiliate.AffManagerUser = new IhreUser();
							if (rdr["AffManagerUserId"].ToString() != "")
							{
								affiliate.AffManagerUser.UserId = Guid.Parse(rdr["AffManagerUserId"].ToString());
								affiliate.AffManagerUser.UserName = rdr["AffManagerUserName"].ToString();
							}
							affiliate.AllowContact = bool.Parse(rdr["allow_contact"].ToString());
							affiliate.IsSignedUp = bool.Parse(rdr["signed_up"].ToString());
							affiliate.IsActive = bool.Parse(rdr["active"].ToString());
							affiliate.MaxMail = bool.Parse(rdr["maxmail"].ToString());
							affiliate.AffiliateSize = Int16.Parse(rdr["affiliate_size"].ToString());
							affiliate.Comment = rdr["comment"].ToString();
							affiliate.ConfirmedActiveDealsCount = int.Parse(rdr["confirmed_active_deals_count"].ToString());
							affiliate.ConfirmedInactiveDealsCount = int.Parse(rdr["confirmed_inactive_deals_count"].ToString());
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							ContactDetail contactDetail = new ContactDetail();
							contactDetail.ContactDetailID = int.Parse(rdr["contact_detail_id"].ToString());
							contactDetail.AffiliateID = AffiliateID;
							contactDetail.ContactType = new ContactDetailType();
							contactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
							contactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							contactDetail.ContactDetailText = rdr["contact_detail"].ToString();
							contactDetail.IsActive = bool.Parse(rdr["active"].ToString());
							contactDetail.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());
							affiliate.ContactDetails.Add(contactDetail);
							if (contactDetail.IsPrimary)
							{
								affiliate.PrimaryContactDetail = contactDetail;
							}
						}

						rdr.NextResult();
						while (rdr.Read())
						{
							AffiliateURL affiliateUrl = new AffiliateURL();
							affiliateUrl.UrlID = int.Parse(rdr["url_id"].ToString());
							affiliateUrl.Url = rdr["url_text"].ToString();
							affiliateUrl.AffiliateID = AffiliateID;
							affiliateUrl.IsActive = bool.Parse(rdr["active"].ToString());
							if (rdr["pagerank"].ToString() != "")
							{
								affiliateUrl.PageRank = Int16.Parse(rdr["pagerank"].ToString());
							}
							affiliateUrl.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());

							affiliate.URLs.Add(affiliateUrl);
							if (affiliateUrl.IsPrimary)
							{
								affiliate.PrimaryURL = affiliateUrl;
							}
						}

						rdr.NextResult();
						while (rdr.Read())
						{
							Country country = new Country(int.Parse(rdr["country_id"].ToString()), rdr["country_name"].ToString());
							country.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							affiliate.Countries.Add(country);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							AffiliateType affiliateType = new AffiliateType(int.Parse(rdr["affiliate_type_id"].ToString()), rdr["affiliate_type_name"].ToString());
							affiliateType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							affiliate.AffiliateTypes.Add(affiliateType);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							DealType dealType = new DealType(int.Parse(rdr["deal_type_id"].ToString()), rdr["deal_type_name"].ToString());
							dealType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							affiliate.DealTypes.Add(dealType);
						}
						rdr.NextResult();
						while (rdr.Read())
						{
							MarketingType marketingType = new MarketingType(int.Parse(rdr["marketing_type_id"].ToString()), rdr["marketing_type_name"].ToString());
							marketingType.IsSelected = bool.Parse(rdr["is_selected"].ToString());
							affiliate.MarketingTypes.Add(marketingType);
						}

					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliate;
		}

		public static int AddAffiliate(Guid IhreUserId, string AffiliateName, string ContactName, Guid? UserId, Int16 AffiliateSizeType, bool Contactable, bool MaxMailType, string Comment, int PrimaryContactDetailType, string PrimaryContactDetailText, string PrimaryURL, string CountryIDs, string AffiliateTypeIDs, string DealTypeIDs, out ErrorObj Error)
		{
			int NewAffiliateID = -1;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateInsert", false, true))
				{
					cmd.AddParamWithValue("affiliate_name", AffiliateName);
					cmd.AddParamWithValue("contact_name", ContactName);
					cmd.AddParamWithValue("UserId", UserId);
					cmd.AddParamWithValue("allow_contact", Contactable);
					cmd.AddParamWithValue("affiliate_size", AffiliateSizeType);
					cmd.AddParamWithValue("maxmail", MaxMailType);
					cmd.AddParamWithValue("comment", Comment); 
					cmd.AddParamWithValue("contact_type_id", PrimaryContactDetailType);
					cmd.AddParamWithValue("contact_detail", PrimaryContactDetailText);
					cmd.AddParamWithValue("url_text", PrimaryURL);
					cmd.AddParamWithValue("country_ids_str", CountryIDs);
					cmd.AddParamWithValue("affiliate_type_ids_str", AffiliateTypeIDs);
					cmd.AddParamWithValue("deal_type_ids_str", DealTypeIDs);

					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					NewAffiliateID = cmd.NewId; 
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return NewAffiliateID;
		}

		public static bool RemoveAffiliate(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateRemove"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return true;
		}

		public static List<Operator> GetAffiliateOperatorSuggestions(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			List<Operator> operatorSuggestions = new List<Operator>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateOperatorSuggestions", false))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Operator operatorSuggestion = new Operator(int.Parse(rdr["client_id"].ToString()), rdr["client_name"].ToString());
							operatorSuggestions.Add(operatorSuggestion);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return operatorSuggestions;
		}

		public static void ReassignAffiliates(Guid IhreUserId, Guid AffManagerUserId, string AffiliateIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliatesReassign"))
				{
					cmd.AddParamWithValue("UserId", AffManagerUserId);
					cmd.AddParamWithValue("affiliate_ids_str", AffiliateIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}



		public static void UpdateAffiliateProfile(Guid IhreUserId, int AffiliateID, string AffiliateName, string ContactName, Guid? AffManagerUserId, Int16 AffSize, bool AllowContact, bool MaxMail, bool IsActive, string Comment, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateProfileUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("affiliate_name", AffiliateName);
					cmd.AddParamWithValue("contact_name", ContactName);
					cmd.AddParamWithValue("UserId", AffManagerUserId);
					cmd.AddParamWithValue("affiliate_size", AffSize);
					cmd.AddParamWithValue("allow_contact", AllowContact);
					cmd.AddParamWithValue("maxmail", MaxMail);
					cmd.AddParamWithValue("active", IsActive);
					cmd.AddParamWithValue("comment", Comment);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
	
		public static List<ContactDetail> GetAffiliateContactDetails(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			List<ContactDetail> contactDetails = new List<ContactDetail>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateContactDetailsSelect", false))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							ContactDetail contactDetail = new ContactDetail();
							contactDetail.ContactDetailID = int.Parse(rdr["contact_detail_id"].ToString());
							contactDetail.AffiliateID = AffiliateID;
							contactDetail.ContactType = new ContactDetailType();
							contactDetail.ContactType.ContactDetailTypeID = int.Parse(rdr["contact_type_id"].ToString());
							contactDetail.ContactType.ContactDetailTypeName = rdr["contact_type_name"].ToString();
							contactDetail.ContactDetailText = rdr["contact_detail"].ToString();
							contactDetail.IsActive = bool.Parse(rdr["active"].ToString());
							contactDetail.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());
							contactDetails.Add(contactDetail);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return contactDetails;
		}
		public static void UpdateAffiliateContactDetails(Guid IhreUserId, int AffiliateID, string ContactDetailInfo, out ErrorObj Error)
		{
			Error = null;
			try
			{
				//	contact_detail_id,contact_type_id,contact_detail,active,is_primary
				DataTable updatesTable = new DataTable();
				updatesTable.Columns.Add("contact_detail_id", typeof(int));
				updatesTable.Columns.Add("contact_type_id", typeof(int));
				updatesTable.Columns.Add("contact_detail", typeof(string));
				updatesTable.Columns.Add("active", typeof(bool));
				updatesTable.Columns.Add("is_primary", typeof(bool));
				foreach (string contactDetailToUpdate in ContactDetailInfo.Split('*'))
				{
					string[] contactDetailDataCols = contactDetailToUpdate.Split(',');
					int? contactDetailId = contactDetailDataCols[0] != "null" ? int.Parse(contactDetailDataCols[0]) : (int?)null;
					int? contactDetailTypeId = contactDetailDataCols[1] != "null" ? int.Parse(contactDetailDataCols[1]) : (int?)null;
					string contactDetailText = contactDetailDataCols[2];
					bool contactDetailIsActive = bool.Parse(contactDetailDataCols[3]);
					bool contactDetailIsPrimary = bool.Parse(contactDetailDataCols[4]);
					updatesTable.Rows.Add(contactDetailId, contactDetailTypeId,contactDetailText, contactDetailIsActive, contactDetailIsPrimary);
				}
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateContactDetailsUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("contact_details_table", updatesTable);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
				updatesTable.Dispose();
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void RemoveAffiliateContactDetail(Guid IhreUserId, int ContactDetailID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateContactDetailRemove"))
				{
					cmd.AddParamWithValue("contact_detail_id", ContactDetailID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		public static List<AffiliateURL> GetAffiliateURLs(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			List<AffiliateURL> affiliateUrls = new List<AffiliateURL>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateUrlsSelect", false))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							AffiliateURL affiliateUrl = new AffiliateURL();
							affiliateUrl.UrlID = int.Parse(rdr["url_id"].ToString());
							affiliateUrl.Url = rdr["url_text"].ToString();
							affiliateUrl.AffiliateID = AffiliateID;
							affiliateUrl.IsActive = bool.Parse(rdr["active"].ToString());
							if (rdr["pagerank"].ToString() != "")
							{
								affiliateUrl.PageRank = Int16.Parse(rdr["pagerank"].ToString());
							}
							affiliateUrl.IsPrimary = rdr["is_primary"].ToString() != "" && bool.Parse(rdr["is_primary"].ToString());

							affiliateUrls.Add(affiliateUrl);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliateUrls;
		}
		public static void UpdateAffiliateURLs(Guid IhreUserId, int AffiliateID, string UrlInfo, out ErrorObj Error)
		{
			Error = null;
			try
			{
				//	UrlID,IsActive,IsPrimary,PageRank,Url*...
				DataTable updatesTable = new DataTable();
				updatesTable.Columns.Add("url_id", typeof(int));
				updatesTable.Columns.Add("url_text", typeof(string));
				updatesTable.Columns.Add("active", typeof(bool));
				updatesTable.Columns.Add("pagerank", typeof(short));
				updatesTable.Columns.Add("is_primary", typeof(bool));
				foreach (string urlToUpdate in UrlInfo.Split('*'))
				{
					string[] urlDataCols = urlToUpdate.Split(',');
					
					int? urlId = urlDataCols[0] != "null" ? int.Parse(urlDataCols[0]) : (int?)null;
					string urlText = urlDataCols[4];
					bool urlIsActive = bool.Parse(urlDataCols[1]);
					Int16? urlPageRank = urlDataCols[3] != "null" ? Int16.Parse(urlDataCols[3]) : (Int16?)null;
					bool urlIsPrimary = bool.Parse(urlDataCols[2]);
					updatesTable.Rows.Add(urlId, urlText, urlIsActive, urlPageRank, urlIsPrimary);
				}
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateUrlsUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("urls_table", updatesTable);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
				updatesTable.Dispose();
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void RemoveAffiliateURL(Guid IhreUserId, int UrlID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateUrlRemove"))
				{
					cmd.AddParamWithValue("url_id", UrlID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		

		public static void UpdateAffiliateCountries(Guid IhreUserId, int AffiliateID, string CountryIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateCountriesUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("country_ids_str", CountryIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateAffiliateAffiliateTypes(Guid IhreUserId, int AffiliateID, string AffiliateTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateAffiliateTypesUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("affiliate_type_ids_str", AffiliateTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateAffiliateDealTypes(Guid IhreUserId, int AffiliateID, string DealTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateDealTypesUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("deal_type_ids_str", DealTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static void UpdateAffiliateMarketingTypes(Guid IhreUserId, int AffiliateID, string MarketingTypeIDs, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateMarketingTypesUpdate"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.AddParamWithValue("marketing_type_ids_str", MarketingTypeIDs);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}


		public static List<AffiliateNote> GetAffiliateNotes(Guid IhreUserId, int AffiliateID, out ErrorObj Error)
		{
			List<AffiliateNote> affiliateNotes = new List<AffiliateNote>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateNotesSelect"))
				{
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							AffiliateNote affiliateNote = new AffiliateNote();
							affiliateNote.NoteID = int.Parse(rdr["note_id"].ToString());
							affiliateNote.NoteText = rdr["note_text"].ToString();
							affiliateNote.Created= DateTime.Parse( rdr["created"].ToString());
							affiliateNote.Modified = DateTime.Parse(rdr["modified"].ToString());
							affiliateNote.AccManagerUserName = rdr["AffManagerUserName"].ToString();
							affiliateNotes.Add(affiliateNote);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliateNotes;
		}
		public static void UpdateAffiliateNote(Guid IhreUserId, int NoteID, string NoteText, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateNoteUpdate"))
				{
					cmd.AddParamWithValue("note_id", NoteID);
					cmd.AddParamWithValue("note_text", NoteText);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
		}
		public static AffiliateNote AddAffiliateNote(Guid IhreUserId, int AffiliateID, string NoteText, out ErrorObj Error)
		{
			AffiliateNote affiliateNote = new AffiliateNote();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateNoteInsert", false, true))
				{
					cmd.AddParamWithValue("note_text", NoteText);
					cmd.AddParamWithValue("affiliate_id", AffiliateID);
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							affiliateNote.NoteID = int.Parse(rdr["note_id"].ToString());
							affiliateNote.NoteText = rdr["note_text"].ToString();
							affiliateNote.Created = DateTime.Parse(rdr["created"].ToString());
							affiliateNote.Modified = DateTime.Parse(rdr["modified"].ToString());
							affiliateNote.AccManagerUserName = rdr["AffManagerUserName"].ToString();
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return affiliateNote;
		}
		public static void RemoveAffiliateNote(Guid IhreUserId, int NoteID, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_AffiliateNoteRemove"))
				{
					cmd.AddParamWithValue("note_id", NoteID);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}


	}

}