﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IhreDB2.App_Code
{
	public class Language
	{
		public int LanguageID { get; set; }
		public string LanguageName { get; set; }
		public bool IsSelected { get; set; }
		public Language(int LanguageID, string LanguageName)
		{
			this.LanguageID = LanguageID;
			this.LanguageName = LanguageName;
			this.IsSelected = false;
		}
	}
	public class LanguageHelper
	{
		public static List<Language> GetLanguages(Guid IhreUserId, out ErrorObj Error)
		{
			List<Language> languages = new List<Language>();
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LanguagesSelect"))
				{
					cmd.OpenConnection();
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							Language language = new Language(int.Parse(rdr["language_id"].ToString()), rdr["language_name"].ToString());
							languages.Add(language);
						}
					}
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return languages;
		}
		public static void UpdateLanguage(Guid IhreUserId, int LanguageID, string LanguageName, out ErrorObj Error)
		{
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LanguageUpdate"))
				{
					cmd.AddParamWithValue("language_id", LanguageID);
					cmd.AddParamWithValue("language_name", LanguageName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return;
		}
		public static Language AddLanguage(Guid IhreUserId, string LanguageName, out ErrorObj Error)
		{
			Language language = null;
			Error = null;
			try
			{
				using (DbCommand cmd = new DbCommand(IhreUserId, "db2_LanguageInsert", false, true))
				{
					cmd.AddParamWithValue("language_name", LanguageName);
					cmd.OpenConnection();
					cmd.ExecuteNonQuery();
					language = new Language(cmd.NewId, LanguageName);
					Error = cmd.CheckErrors();
				}
			}
			catch (Exception ex)
			{
				Error = new ErrorObj(ex.ToString());
			}
			return language;
		}
	}
}