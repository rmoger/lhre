﻿function GC_CreateLineChart(target, data, cols, options) {
	return new google.visualization.ChartWrapper({
		chartType: 'LineChart',
		containerId: target,
		dataTable: data,
		options: {
			width: document.getElementById(target).clientWidth,
			height: options.height !== undefined ? options.height : 250,
			chartArea: { width: document.getElementById(target).clientWidth - 100, height: 170, left: 50, top: 40 },
			legend: { position: 'top', alignment: 'center' },
			vAxis: { minValue: 0, viewWindow: { min: 0 } },
		},
		view: { 'columns': cols }
	});
}
function GC_CreateStackedColumnChart(target, data, cols, options) {
	return new google.visualization.ChartWrapper({
		chartType: 'ColumnChart',
		containerId: target,
		dataTable: data,
		options: {
			width: document.getElementById(target).clientWidth,
			height: options.height !== undefined ? options.height : 250,
			chartArea: { width: document.getElementById(target).clientWidth - 100, height: 170, left: 50, top: 40 },
			legend: { position: 'top', alignment: 'center' },
			vAxis: { minValue: 0, viewWindow: { min: 0 } },
			isStacked: true,
		},
		view: { 'columns': cols }
	});
};
function GC_CreateGeoChart(target, data, cols, options) {
	var theChart = new google.visualization.ChartWrapper({
		chartType: 'GeoChart',
		containerId: target,
		dataTable: data,
		options: {
			width: document.getElementById(target).clientWidth,
			magnifyingGlass: { enable: true, zoomFactor: 7.5 }
		},
		view: { 'columns': cols }
	});
	if (options.selectionCallback !== undefined) {
		google.visualization.events.addListener(theChart, 'ready', function () {
			google.visualization.events.addListener(theChart.getChart(), 'select', function () {
				var selectionObj = theChart.getChart().getSelection();
				options.selectionCallback(selectionObj[0].row);
			});
		});
	}
	return theChart;
}

function GC_CreatePagedTable(target, data, cols, options) {
	var theTable = new google.visualization.ChartWrapper({
		chartType: 'Table',
		containerId: target,
		dataTable: data,
		options: {
			width: document.getElementById(target).clientWidth,
			page: 'enable',
			pageSize: options.pageSize !== undefined ? options.pageSize : 7
		},
		view: { 'columns': cols }
	});

	if (options.selectionCallback !== undefined || options.totalsData !== undefined) {
		google.visualization.events.addListener(theTable, 'ready', function () {
			if (options.selectionCallback !== undefined) {
				google.visualization.events.addListener(theTable.getChart(), 'select', function () {
					var selectionObj = theTable.getChart().getSelection();
					options.selectionCallback(selectionObj[0].row);
				});
			}
			if (options.totalsData !== undefined) {
				var totalsArr = [];
				for (c = 0; c < cols.length; c++) {
					totalsArr.push(options.totalsData[cols[c]]);
				}
				GC_AddTableFooterfunction(target, totalsArr);
				google.visualization.events.addListener(theTable.getChart(), 'page', function () { GC_AddTableFooterfunction(target, totalsArr); });
				google.visualization.events.addListener(theTable.getChart(), 'sort', function () { GC_AddTableFooterfunction(target, totalsArr); });
			}
		});
	}
	return theTable;
}
function GC_AddTableFooterfunction(footerId, data) {
	if (document.getElementById('google-visualization-table-footer-' + footerId)) {
		document.getElementById('google-visualization-table-footer-' + footerId).parentElement.removeChild(document.getElementById('google-visualization-table-footer-' + footerId));
	}//return;
	var tables = document.getElementById(footerId).getElementsByTagName('TABLE');
	for (i = 0; i < tables.length; i++) {
		if (tables[i].className == 'google-visualization-table-table') {
			var r = tables[i].insertRow(tables[i].rows.length);
			r.id = 'google-visualization-table-footer-' + footerId;
			var c;
			r.className = 'google-visualization-table-tr-head';
			for (j = 0; j < data.length; j++) {
				c = r.insertCell(j);
				c.className = 'google-visualization-table-th';
				c.innerHTML = data[j];
				c.colSpan = 1;
			}
		}
	}
}





var GC_FormatterCurrency = new google.visualization.NumberFormat({
	negativeColor: 'red',
	negativeParens: true,
	pattern: '###,##0.00',
});
