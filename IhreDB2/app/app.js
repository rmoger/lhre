﻿var app = angular.module('ihreapp', ['ngAnimate', 'ngCookies', 'ngDragDrop', 'ngTouch']);

app.config(["$httpProvider", function ($httpProvider) {
	$httpProvider.defaults.transformResponse.push(function (responseData) {
		convertDateStringsToDatesAndUrlDecode(responseData);
		return responseData;
	});
}]);
app.directive('a', function () {
	return {
		restrict: 'E',
		link: function (scope, elem, attrs) {
			elem.on('click', function (e) {
				if (attrs['ngClick'] != null) {
					e.preventDefault();
				}
			});
		}
	};
});
function convertDateStringsToDatesAndUrlDecode(input) {
	try {
		if (typeof input != "object") return input;
		for (var key in input) {
			if (!input.hasOwnProperty(key)) continue;
			var value = input[key];
			var match;
			if (typeof value == "string" && (match = value.match(/^(\d{4})-(\d{2})-(\d{2})$/))) {
				var dateParts = value.split('-');
				input[key] = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
			} else if (typeof value == "object") {
				convertDateStringsToDatesAndUrlDecode(value);
			}
		}
	} catch (ex) {
		alert(ex);
	}
}
/*app.filter('reverse', function () {
	return function (items) {
		if (items) {
			return items.slice().reverse();
		}
	};

});
app.directive('uiColorpicker', function () {
	return {
		restrict: 'E',
		require: 'ngModel',
		scope: false,
		replace: true,
		template: "<span><input class='input-small' /></span>",
		link: function (scope, element, attrs, ngModel) {
			var input = element.find('input');
			var options = angular.extend(
				{
					color: ngModel.$viewValue,
					change: function (color) {
						scope.$apply(function () {
							ngModel.$setViewValue(color.toHexString());
						});
					}
				},
				scope.$eval(attrs.options)
			);
			ngModel.$render = function () {
				input.spectrum('set', ngModel.$viewValue || '');
			};
			input.spectrum(options);
		}
	};
});
app.directive('monthPicker', function () {
	return {
		restrict: "E",
		scope: {
			monthpickermodel: '=monthpickermodel',
			monthpickersource: '=monthpickersource'
		},
		replace: true,
		template: '<select ng-model="monthpickermodel" ng-options="item.Label for item in monthpickersource"></select>'
	};
});
app.directive('countryFlag', function () {
	return {
		restrict: "E",
		scope: {
			code: '=code'
		},
		replace: true,
		template: '<span class="flag flag-{{code.toLowerCase()}}"></span>'
	};
});
app.directive('datePicker', function ($timeout) {
	return {
		restrict: "E",
		scope: {
			datepickerid: '@datepickerid',
			datepickermodel: '=datepickermodel',
			datepickerchanged: '&datepickerchanged'
		},
		replace: true,
		link: function (scope, element, attr, ngModel) {
			$timeout(function () {
				$('#' + attr.datepickerid).datepicker({
					dateFormat: 'yy-mm-dd', changeMonth: true
				});
			}, 0, false);
		},
		template: '<div class="datepicker">'
			+ '<span><input type="text" id="{{datepickerid}}" ng-model="datepickermodel" ng-change="datepickerchanged()" /></span>'
		+ '</div>',
	};
});
*/
app.directive('dateRangePicker', function ($timeout) {
	return {
		restrict: "E",
		scope: {
			dpid: '@dpid',
			dpincludecheckbox: '=dpincludecheckbox',
			dpfromon: '=dpfromon',
			dptoon: '=dptoon',
			dpfrom: '=dpfrom',
			dpto: '=dpto',
			dpdisabled: '=dpdisabled',
			dponchange: '&dponchange',
			dpformerrorfrom: '=dpformerrorfrom',
			dpformerrorto: '=dpformerrorto',
		},
		replace: true,
		link: function (scope, element, attr, ngModel) {
			$timeout(function () {
				$('#' + attr.dpid + '_from').datepicker({
					dateFormat: 'yy-mm-dd', changeMonth: true, maxDate: $('#' + attr.dpid + '_to').val(),
					onClose: function (selectedDate) { $('#' + attr.dpid + '_to').datepicker('option', 'minDate', selectedDate); },
					onSelect: function (selectedDate) {
						scope.dpfrom = selectedDate;
						scope.$apply();
						scope.dponchange();
					}
				});
				$('#' + attr.dpid + '_to').datepicker({
					dateFormat: 'yy-mm-dd', changeMonth: true, minDate: $('#' + attr.dpid + '_from').val(),
					onClose: function (selectedDate) { $('#' + attr.dpid + '_from').datepicker('option', 'maxDate', selectedDate); },
					onSelect: function (selectedDate) {
						scope.dpto = selectedDate;
						scope.$apply();
						scope.dponchange();
					}
				});
			}, 0, false);
		},
		template: '<div class="datepicker">'
			+ '<span>'
			+ '<input type="checkbox" ng-show="dpincludecheckbox" ng-model="dpfromon" ng-checked="dpfromon" ng-disabled="dpdisabled" />'
			+ '<input type="text" id="{{dpid}}_from" ng-model="dpfrom" ng-disabled="dpincludecheckbox && (!dpfromon || dpdisabled)" form-error="dpformerrorfrom" />'
			+ '</span><span>'
			+ '<input type="checkbox" ng-show="dpincludecheckbox" ng-model="dptoon" ng-checked="dptoon" ng-disabled="dpdisabled" />'
			+ '<input type="text" id="{{dpid}}_to" ng-model="dpto" ng-disabled="dpincludecheckbox && (!dptoon || dpdisabled)" form-error="dpformerrorto" />'
			+ '</span>'
		+ '</div>'
	};
});

app.directive('pagerHeader', function () {
	return {
		restrict: "E",
		scope: {
			pagerobj: '=pagerobj',
		},
		replace: true,
		template: '<h1 class="pager-header">'
			+ '<span>Show <select ng-model="pagerobj.Info.Count" ng-options="o for o in pagerobj.SearchDisplayCounts" ng-change="pagerobj.UpdateDisplayCount();"></select> results</span>'
			+ '<span ng-bind="pagerobj.Info.Message"></span>'
			//+ '<span>Show: <span class="link" ng-repeat="x in pagerobj.SearchDisplayCounts" ng-click="pagerobj.UpdateDisplayCount(x);">[{{x}}]</span></span>'
			+ '</h1>'
	};
});
app.directive('pagerFooter', function () {
	return {
		restrict: "E",
		scope: {
			pagerobj: '=pagerobj',
		},
		template: '<div class="pager">'
			+ '<span class="first" ng-class="pagerobj.Info.ShowPrev ? \'\' : \'disabled\'" ng-click="pagerobj.Info.ShowPrev ? pagerobj.GotoFirst() : null;" title="First"><span></span></span>'
			+ '<span class="prev" ng-class="pagerobj.Info.ShowPrev ? \'\' : \'disabled\'" ng-click="pagerobj.Info.ShowPrev ? pagerobj.GotoPrev() : null;" title="Previous"><span></span></span>'
			+ '<span class="next" ng-class="pagerobj.Info.ShowNext ? \'\' : \'disabled\'" ng-click="pagerobj.Info.ShowNext ? pagerobj.GotoNext() : null;" title="Next"><span></span></span>'
			+ '<span class="last" ng-class="pagerobj.Info.ShowNext ? \'\' : \'disabled\'" ng-click="pagerobj.Info.ShowNext ? pagerobj.GotoLast() : null;" title="Last"><span></span></span>'
			+ '<span class="pageno" ng-repeat="pageNo in pagerobj.Info.PageNos" ng-click="pagerobj.Info.CurrentPageNo == pageNo ? null : pagerobj.GotoPage(pageNo);" ng-class="pagerobj.Info.CurrentPageNo == pageNo ? \'disabled\' : \'\'"><span ng-bind="pageNo"></span></span>'
		+ '</div>'

	};
});
/*
Number.prototype.toMoney = function () {
	var c = 2;
	var d = '.';
	var t = ',';

	var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function cloneObject(obj) {
	if (obj == null || typeof (obj) != 'object')
		return obj;
	var temp = obj.constructor(); // changed
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			temp[key] = cloneObject(obj[key]);
		}
	}
	return temp;
}


*/
$(function () {
	/*window.addEventListener("resize", function () {
		var scope = angular.element('#DocBody').scope();
		scope.$apply(function () {
			scope.RenderCharts();
		});
	});*/
	$(window).bind("popstate", function (data) {
		//if (!!event.state)
		{
			if (data.target.history.state != null) {
				if (data.target.history.state.iswahg) {
					var scope = angular.element('#DocBody').scope();
					scope.$apply(function () {
						scope.RenderSection(data.target.history.state.section, data.target.history.state.param);
					});
				}
			}
		}
	});
});

function ErrorDictionary() {
	this.Keys = [];
	this.Values = [];
}
if (!ErrorDictionary.prototype.HasErrors) {
	ErrorDictionary.prototype.HasErrors = function () {
		return this.Keys.length > 0;
	}
}
if (!ErrorDictionary.prototype.Get) {
	ErrorDictionary.prototype.Get = function (key) {
		if (key == null) {
			return null;
		}
		for (var i = 0; i < this.Keys.length; i++) {
			if (this.Keys[i] == key) {
				return this.Values[i];
			}
		}
		return null;
	}
}
if (!ErrorDictionary.prototype.Update) {
	ErrorDictionary.prototype.Update = function (key, val) {
		var keyUpdated = false;
		for (var i = 0; i < this.Keys.length; i++) {
			if (this.Keys[i] == key) {
				if (val == null) {
					this.Keys.shift(key);
					this.Values.shift(this.Values[i]);
				} else {
					this.Values[i] = val;
					keyUpdated = true;
				}
			}
		}
		if (!keyUpdated && val != null) {
			this.Keys.push(key);
			this.Values.push(val);
		}
	}
}

function datenum(v, date1904) {
	if (date1904) v += 1462;
	var epoch = Date.parse(v);
	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}
function sheet_from_array_of_arrays(data, opts) {
	var ws = {};
	var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
	for (var R = 0; R != data.length; ++R) {
		for (var C = 0; C != data[R].length; ++C) {
			if (range.s.r > R) range.s.r = R;
			if (range.s.c > C) range.s.c = C;
			if (range.e.r < R) range.e.r = R;
			if (range.e.c < C) range.e.c = C;
			var cell = { v: data[R][C] };
			if (cell.v == null) continue;
			var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

			if (typeof cell.v === 'number') cell.t = 'n';
			else if (typeof cell.v === 'boolean') cell.t = 'b';
			else if (cell.v instanceof Date) {
				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
				cell.v = datenum(cell.v);
			}
			else cell.t = 's';

			ws[cell_ref] = cell;
		}
	}
	if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

function Workbook() {
	if (!(this instanceof Workbook)) return new Workbook();
	this.SheetNames = [];
	this.Sheets = {};
}
function s2ab(s) {
	var buf = new ArrayBuffer(s.length);
	var view = new Uint8Array(buf);
	for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
	return buf;
}
/*




app.directive('reportFields', function () {
	return {
		restrict: "E",
		replace: true,
		scope: {
			reportoptions: '=reportoptions',
		},
		template: '<div class="report-fields">'
			+ '<fieldset>'
			+ '<legend>Show Report Fields</legend>'
			+ '<div ng-repeat="field in reportoptions.Fields">'
			+ '<label><input type="checkbox" ng-model="field.IsChecked" ng-disabled="!field.IsOptional" /><span ng-bind="field.Label"></span></label>'
			+ '</div>'
			+ '</fieldset>'
			+ '<fieldset>'
			+ '<legend>Sort By</legend>'
			+ '<select ng-model="reportoptions.SortInfo.Field" ng-options="field.Label for field in (reportoptions.Fields | filter:{IsChecked:true}) track by field.ID "></select>'
			+ '<select ng-model="reportoptions.SortInfo.Dir" ng-options="o for o in [\'asc\', \'desc\']"></select>'
			+ '</fieldset>'
			+ '</div>'
	};
});
*/
app.directive('reportTable', function () {
	return {
		restrict: "E",
		replace: true,
		scope: {
			pagerobj: '=pagerobj',
			reporthelper: '=reporthelper',
			showtotals: '@showtotals'
		},
		template: '<div ng-show="reporthelper.ReportLoaded">'
			+ '<p ng-show="reporthelper.ReportData.Rows.length == 0">There are no results matching your search criteria.</p>'
			+ '<div ng-show="reporthelper.ReportData.Rows.length > 0">'
				+ '<pager-header pagerobj="pagerobj" extendedheader="false"></pager-header>'
				+ '<div>'
					+ '<fieldset class="filterbar">'
						+ '<legend></legend>'
						+ '<dl><dd>'
			//			+ '<span class="link reporticon reporticon-html" ng-click="reporthelper.JSONToHTMLConvertor();">Download HTML</span>'
						+ '<span class="link reporticon reporticon-csv" ng-click="reporthelper.JSONToCSVConvertor();">Download CSV</span>'
						+ '<span class="link reporticon reporticon-xlsx" ng-click="reporthelper.JSONToXLSXConvertor();">Download Excel</span>'
			//			+ '<span class="link reporticon reporticon-pdf" ng-click="reporthelper.JSONToPDFConvertor();">Download PDF</span>'
						+ '</dd></dl>'
					+ '</fieldset>'
				+ '</div>'
				+ '<div class="tablecontainer paged">'
					+ '<table>'
						+ '<tr>'
							+ '<th ng-repeat="reportfield in reporthelper.ReportData.Headers" class="th-sortable" ng-click="reporthelper.Sort(reportfield);" ng-class="reporthelper.ReportData.SortInfo.Field.ID == reportfield.ID ? (reporthelper.ReportData.SortInfo.Dir == \'asc\' ? \'th-sortable-asc\' : \'th-sortable-desc\') : \'\'" title="{{reporthelper.ReportData.SortInfo.Field.ID == reportfield.ID  && reporthelper.ReportData.SortInfo.Dir == \'asc\' ? \'Sort Descending\' : \'Sort Ascending\'}}">'
								+ '<span ng-bind="reportfield.Label"></span><span></span>'
							+ '</th>'
						+ '</tr>'
						+ '<tr ng-repeat="reportrow in reporthelper.ReportData.Rows track by $index" ng-class="$index % 2 == 0 ? \'alt\' : \'\'" ng-show="$index >= pagerobj.Info.Offset && $index < pagerobj.Info.Offset + pagerobj.Info.Count">'
							+ '<td ng-repeat="reportcol in reportrow track by $index">'
								+ '<span ng-bind="reporthelper.FormatReportValue(reportcol,$index)"></span>'
							+ '</td>'
						+ '</tr>'
						+ '<tr ng-show="showtotals">'
							+ '<th ng-repeat="reportfield in reporthelper.ReportData.Headers">'
								+ '<span ng-bind="reporthelper.GetReportTotalForField($index)"></span>'
							+ '</th>'
						+ '</tr>'
					+ '</table>'
				+ '</div>'
				+ '<pager-footer pagerobj="pagerobj"></pager-footer>'
			+ '</div>'
		+ '</div>'
	};
});





app.directive('formError', function ($compile) {
	return {
		restrict: 'A',
		scope: {
			error: '=formError'
		},
		link: function (scope, element, attrs) {
			element.on("focus", function () {
				var eleParent = element[0];
				for (var c = 0; c < 5; c++) {
					eleParent = eleParent.parentElement;
					if (eleParent.nodeName.toUpperCase() == 'DD') {
						eleParent.className = 'activefield';
					}
				}
			});
			element.on('blur', function () {
				var eleParent = element[0];
				for (var c = 0; c < 5; c++) {
					eleParent = eleParent.parentElement;
					if (eleParent.nodeName.toUpperCase() == 'DD') {
						eleParent.className = '';
					}
				}
			});

			if (element.next().length) {
				element.next().after(element);
			}
			var errortooltip = angular.element("<span class='error' ng-show='error != null && error != \"\"'><span ng-bind=\"error\"></span></span>");
			element.after(errortooltip);
			$compile(errortooltip)(scope);
		}

	};
});



/*


app.directive('infoTooltip', function ($compile) {
	return {
		restrict: 'A',
		scope: {
			infotooltip: '@infoTooltip'
		},
		link: function (scope, element, attrs) {
			var infotooltip = angular.element("<span class='tooltip' ng-bind=\"infotooltip\"></span>");
			$(element).append(infotooltip);
			$compile(infotooltip)(scope);
		}

	};
});



app.directive('fileUploader', function (httpPostFactory) {
	return {
		//scope.progress = Math.round(evt.loaded * 100 / evt.total)
		restrict: 'A',
		scope: {
			fileUploaderScope: '=fileUploaderScope',
		},
		link: function (scope, element, attr) {
			element.bind('change', { scope: scope }, function () {
				var formData = new FormData();
				formData.append('file', element[0].files[0]);
				httpPostFactory('/api/uploadfile', formData, arguments[0].data.scope, function (scope, result) {
					// recieve image name to use in a ng-src 
					scope.fileUploaderScope.Callback(result);

				});
			});
		}
	};
});
app.factory('httpPostFactory', function ($http) {
	return function (uploadUrl, data, scope, callback) {
		$http.post(uploadUrl, data, {
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined }
		}).success(function (result) {
			if (!debugmode) {
				result = JSON.parse(Base64.decode(result));
			}
			callback(scope, result);
		}).error(function (response) {
			callback(scope, {
				Success: false,
				Error: status,
				ErrorCode: 500,
				ResponseData: null
			});
		});
	};
});
*/


function CreatePagerObj() {
	var _pager = {
		SearchDisplayCounts: [5, 10, 20, 50, 100, 250, 500, 1000],
		Info: {
			SearchFunction: null,
			From: 0,
			To: 0,
			Offset: 0,
			Count: 20,//SearchDisplayCounts[1],
			Total: 0,
			Message: 'Displaying 0 to 0 of 0',
			ShowNext: false,
			ShowPrev: false,
			PageNos: [],
			CurrentPageNo: 0
		},
		Reset: function () {
			_pager.Info.Offset = 0;
			if (arguments.length > 0) {
				_pager.Info.Count = arguments[0];
			} else {
				_pager.Info.Count = _pager.SearchDisplayCounts[2];
			}
		},
		SetPagerInfo: function (total, searchFunction) {
			_pager.Info.Total = total;
			_pager.Info.SearchFunction = searchFunction;
			_pager.Info.From = _pager.Info.Offset + 1;
			_pager.Info.To = _pager.Info.Offset + _pager.Info.Count;
			if (_pager.Info.To > total) {
				_pager.Info.To = total;
			}
			_pager.Info.PageNos = [];
			var c = 0;
			for (t = total; t > 0; t -= _pager.Info.Count) {
				c++; _pager.Info.PageNos.push(c);
			}
			if (_pager.Info.PageNos.length > 5) {
				var startingPageNoIndex = (_pager.Info.Offset / _pager.Info.Count) - 2;
				if (startingPageNoIndex < 0) {
					startingPageNoIndex = 0;
				}
				if (startingPageNoIndex > _pager.Info.PageNos.length - 5) {
					startingPageNoIndex = _pager.Info.PageNos.length - 5;
				}
				var newPageNos = [];
				for (s = startingPageNoIndex; s < startingPageNoIndex + 5; s++) {
					newPageNos.push(_pager.Info.PageNos[s]);
				}
				_pager.Info.PageNos = newPageNos;
			}
			_pager.Info.Message = _pager.Info.Total == 0 ? 'Displaying 0 to 0 of 0' : 'Displaying ' + _pager.Info.From + ' to ' + _pager.Info.To + ' of ' + _pager.Info.Total;
			_pager.Info.ShowNext = _pager.Info.To < _pager.Info.Total;
			_pager.Info.ShowPrev = _pager.Info.From > 1;
			_pager.Info.CurrentPageNo = (_pager.Info.Offset / _pager.Info.Count) + 1;
		},
		Update: function () {
			_pager.SetPagerInfo(_pager.Info.Total, _pager.Info.SearchFunction);
			if (_pager.Info.SearchFunction != null) {
				_pager.Info.SearchFunction(_pager.Info.Offset);
			}
		},
		UpdateDisplayCount: function (arg) {
			_pager.Info.Offset = 0;
			_pager.Update();
		},
		GotoFirst: function () {
			_pager.Info.Offset = 0;
			_pager.Update();
		},
		GotoPrev: function () {
			_pager.Info.Offset -= _pager.Info.Count;
			_pager.Update();
		},
		GotoNext: function () {
			_pager.Info.Offset += _pager.Info.Count;
			_pager.Update();
		},
		GotoLast: function () {
			_pager.Info.Offset = (Math.ceil(_pager.Info.Total / _pager.Info.Count) * _pager.Info.Count) - _pager.Info.Count;
			_pager.Update();
		},
		GotoPage: function (page) {
			_pager.Info.Offset = (page - 1) * _pager.Info.Count;
			_pager.Update();
		}
	};
	return _pager;
}

function CreateReportHelperObj() {
	var _ReportHelper = {
		ReportLoaded: false,
		ReportTitle: 'Unnamed Report',
		ReportData: {},
		Reset: function () {
			_ReportHelper.ReportData = {};
			_ReportHelper.ReportLoaded = false;
			if (arguments.length > 0) {
				_ReportHelper.ReportTitle = arguments[0];
			} else {
				_ReportHelper.ReportTitle = 'Unnamed Report';
			}
		},
		RenderReport: function (reportData) {
			_ReportHelper.ReportData = reportData;
			_ReportHelper.Sort(_ReportHelper.ReportData.SortInfo.Field, _ReportHelper.ReportData.SortInfo.Dir);
			_ReportHelper.ReportLoaded = true;
			var filterBlocks = document.getElementsByClassName('report-filter');
			for (c = 0; c < filterBlocks.length; c++) {
				filterBlocks[c].classList.add('toggleoff');
			}
		},
		Sort: function (fieldArg) {
			var sortdir = 'asc';
			if (arguments.length > 1) {
				sortdir = arguments[1];
			} else {
				if (fieldArg.ID == _ReportHelper.ReportData.SortInfo.Field.ID) {
					sortdir = _ReportHelper.ReportData.SortInfo.Dir == 'asc' ? 'desc' : 'asc';
				}
			}
			_ReportHelper.ReportData.SortInfo = {
				Field: fieldArg,
				Dir: sortdir
			};
			var reportDataSortIndex = _ReportHelper.GetReportColumnForField(fieldArg.ID);

			switch (fieldArg.Type) {
				case 'int':
					_ReportHelper.ReportData.Rows.sort(function (a, b) {
						var newA = a[reportDataSortIndex] == '' ? 0 : parseInt(a[reportDataSortIndex]);
						var newB = b[reportDataSortIndex] == '' ? 0 : parseInt(b[reportDataSortIndex]);
						if (newA < newB) {
							return sortdir == 'desc' ? 1 : -1;
						} else if (newA > newB) {
							return sortdir == 'desc' ? -1 : 1;
						} else {
							return 0;
						}
					});
					break;
				case 'decimal':
				case 'float':
					_ReportHelper.ReportData.Rows.sort(function (a, b) {
						var newA = a[reportDataSortIndex] == '' ? 0 : parseFloat(a[reportDataSortIndex]);
						var newB = b[reportDataSortIndex] == '' ? 0 : parseFloat(b[reportDataSortIndex]);
						if (newA < newB) {
							return sortdir == 'desc' ? 1 : -1;
						} else if (newA > newB) {
							return sortdir == 'desc' ? -1 : 1;
						} else {
							return 0;
						}
					});
					break;
				case 'date':
					_ReportHelper.ReportData.Rows.sort(function (a, b) {

						//var date_a = Date.parse(a[reportDataSortIndex]);
						//var date_b = Date.parse(b[reportDataSortIndex]);

						var arr_a = a[reportDataSortIndex].split(' ')[0].split('/');
						var date_a = new Date(parseInt(arr_a[2]), parseInt(arr_a[1]), parseInt(arr_a[0]));

						var arr_b = b[reportDataSortIndex].split(' ')[0].split('/');
						var date_b = new Date(parseInt(arr_b[2]), parseInt(arr_b[1]), parseInt(arr_b[0]));


						if (date_a < date_b) {
							return sortdir == 'desc' ? 1 : -1;
						} else if (date_a > date_b) {
							return sortdir == 'desc' ? -1 : 1;
						} else {
							return 0;
						}
					});
					break;
				default:
					_ReportHelper.ReportData.Rows.sort(function (a, b) {
						if (a[reportDataSortIndex] < b[reportDataSortIndex]) {
							return sortdir == 'desc' ? 1 : -1;
						} else if (a[reportDataSortIndex] > b[reportDataSortIndex]) {
							return sortdir == 'desc' ? -1 : 1;
						} else {
							return 0;
						}
					});
					break;
			}
		},
		GetReportColumnForField: function (fieldId) {
			var report_c = 0;
			for (c = 0; c < _ReportHelper.ReportData.Headers.length; c++) {
				if (_ReportHelper.ReportData.Headers[c].ID == fieldId) {
					return report_c;
				}
				report_c++;
			}
			return 0;
		},
		GetReportTotalForField: function (colIndex) {
			if (_ReportHelper.ReportData.Headers[colIndex].CanTotal) {
				var colType = _ReportHelper.ReportData.Headers[colIndex].Type;
				switch (colType) {
					case 'decimal':
					case 'int':
						var total = 0;
						for (c = 0; c < _ReportHelper.ReportData.Rows.length; c++) {
							if (_ReportHelper.ReportData.Rows[c][colIndex] != '') {
								total += parseFloat(_ReportHelper.ReportData.Rows[c][colIndex]);
							}
						}
						if (colType == 'decimal') {
							return total.toFixed(2);
						} else {
							return total;
						}
				}
			}
			return '';
		},
		FormatReportValue: function (value, colIndex) {
			switch (_ReportHelper.ReportData.Headers[colIndex].Type) {
				case 'date':
					return FormatReportDate(value, 'd M, y');
				case 'int':
					return value == '' ? '0' : parseInt(value);
				case 'decimal':
					return value == '' ? '0.00' : parseFloat(value).toFixed(2);
				default:
					return value;
			}
		},
		JSONToXLSXConvertor: function () {
			var data = [];
			var data_headers = [];
			for (var header_c = 0; header_c < _ReportHelper.ReportData.Headers.length; header_c++) {
				data_headers.push(_ReportHelper.ReportData.Headers[header_c].Label);
			}
			data.push(data_headers);
			for (var row_c = 0; row_c < _ReportHelper.ReportData.Rows.length; row_c++) {
				var data_row = [];
				for (var col_c = 0; col_c < data_headers.length; col_c++) {
					switch (_ReportHelper.ReportData.Headers[col_c].Type) {
						case "decimal":
							data_row.push(parseFloat(_ReportHelper.ReportData.Rows[row_c][col_c]));
							break;
						case "int":
							data_row.push(parseInt(_ReportHelper.ReportData.Rows[row_c][col_c]));
							break;
						default:
							data_row.push(_ReportHelper.ReportData.Rows[row_c][col_c]);
							break;
					}

				}
				data.push(data_row);
			}
			var wb = new Workbook();
			var ws = sheet_from_array_of_arrays(data);
			wb.SheetNames.push(_ReportHelper.ReportTitle);
			wb.Sheets[_ReportHelper.ReportTitle] = ws;
			var fileName = "Report_" + _ReportHelper.ReportTitle.replace(/ /g, "_");
			var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
			saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), fileName + ".xlsx")
		},
		JSONToCSVConvertor: function () {
			var JSONData = _ReportHelper.ReportData.Rows;
			var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
			var CSV = _ReportHelper.ReportTitle + '\r\n\n';
			var row = "";
			for (var header_c = 0; header_c < _ReportHelper.ReportData.Headers.length; header_c++) {
				row += _ReportHelper.ReportData.Headers[header_c].Label + ',';
			}
			row = row.slice(0, -1);
			CSV += row + '\r\n';
			for (var i = 0; i < arrData.length; i++) {
				var row = "";
				for (var index in arrData[i]) {
					row += '"' + arrData[i][index] + '",';
				}
				row.slice(0, row.length - 1);
				CSV += row + '\r\n';
			}
			if (CSV == '') {
				alert("Invalid data");
				return;
			}
			var fileName = "Report_" + _ReportHelper.ReportTitle.replace(/ /g, "_");
			var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
			var link = document.createElement("a");
			link.href = uri;
			link.style = "visibility:hidden";
			link.download = fileName + ".csv";
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	};
	return _ReportHelper;
}
function FormatReportDate(arg, format) {
	//18/02/2015 00:00:00
	if (arg != null) {
		var datetimeSplit = arg.split(' ');
		if (datetimeSplit.length == 2) {
			var dateSplit = datetimeSplit[0].split('/');
			if (dateSplit.length == 3) {
				var timeSplit = datetimeSplit[1].split(':');
				if (timeSplit.length == 3) {
					return FormatJsDate(
						new Date(dateSplit[2], parseInt(dateSplit[1]) - 1, dateSplit[0], timeSplit[0], timeSplit[1], timeSplit[2]),
						format
					);
				}
			}
		}
	}
	return null;
};

function FormatJsDate(dateObj, format) {
	if (dateObj == null) {
		return '';
	}
	var retval = '';
	var d = dateObj.getDate().toString();
	var dd = dateObj.getDate() < 10 ? '0' + dateObj.getDate().toString() : dateObj.getDate().toString();
	var mm = (dateObj.getMonth() + 1) < 10 ? '0' + (dateObj.getMonth() + 1).toString() : (dateObj.getMonth() + 1).toString();
	var mmm = '';
	switch (dateObj.getMonth()) {
		case 0: mmm = 'Jan'; break;
		case 1: mmm = 'Feb'; break;
		case 2: mmm = 'Mar'; break;
		case 3: mmm = 'Apr'; break;
		case 4: mmm = 'May'; break;
		case 5: mmm = 'Jun'; break;
		case 6: mmm = 'Jul'; break;
		case 7: mmm = 'Aug'; break;
		case 8: mmm = 'Sep'; break;
		case 9: mmm = 'Oct'; break;
		case 10: mmm = 'Nov'; break;
		case 11: mmm = 'Dec'; break;
	}
	var yyyy = dateObj.getFullYear().toString();
	var HH = dateObj.getHours() < 10 ? '0' + dateObj.getHours().toString() : dateObj.getHours().toString();
	var MM = dateObj.getMinutes() < 10 ? '0' + dateObj.getMinutes().toString() : dateObj.getMinutes().toString();
	var SS = dateObj.getSeconds() < 10 ? '0' + dateObj.getSeconds().toString() : dateObj.getSeconds().toString();

	switch (format) {
		case 'yyyy-mm-dd':
		case 'YYYY-MM-DD':
			return yyyy + '-' + mm + '-' + dd;
		case 'dd/mm/yyyy':
			return dd + '/' + mm + '/' + yyyy;
		case 'dd/mm/yyyy hh:mm:ss':
			return dd + '/' + mm + '/' + yyyy + ' ' + HH + ':' + MM + ':' + SS;
		case 'dd/mm/yyyy hh:mm':
			return dd + '/' + mm + '/' + yyyy + ' ' + HH + ':' + MM;
		case 'd M, y':
			return d + ' ' + mmm + ', ' + yyyy;
		case 'M, y':
			return mmm + ', ' + yyyy;
		case 'd M, y hh:mm':
			return d + ' ' + mmm + ', ' + yyyy + ' ' + HH + ':' + MM;
	}
	return '';
};












app.directive(
	'ngShowMessage',
	function () {
		function link($scope, element, attributes) {
			var expression = attributes.ngShowMessage;
			if (!$scope.$eval(expression)) {
				element.hide();
			}
			$scope.$watch(
				expression,
				function (newValue, oldValue) {
					if (newValue === oldValue) {
						return;
					}
					if (newValue) {
						element.show();
						var newTop = $('#message').height() + 60 + 20;
						$('div.layoutcols.layoutcol_fixedleft_400 div.layoutcol.layoutcol_1_2').css({ 'top': newTop + 'px' });
					} else {
						element.hide();
						var newTop = 60;
						$('div.layoutcols.layoutcol_fixedleft_400 div.layoutcol.layoutcol_1_2').css({ 'top': newTop + 'px' });
					}
				}
			);
		}
		return ({
			link: link,
			restrict: "A"
		});
	}
);


Array.prototype.remove = function (from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};