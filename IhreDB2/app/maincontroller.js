﻿app.controller("MainController", function ($scope, $http, $sce, $store, $timeout, $interval, $window) {
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


	/*BEGIN VARS*/
	$scope.CurrentRoles = null;
	$scope.IsUserInRole = function (roleArg) {
		if ($scope.CurrentRoles == null) return false;
		return $scope.CurrentRoles[roleArg] == true || $scope.CurrentRoles['SuperUser'] == true;
	};

	$scope.Message = null;
	$scope.PopupMessage = null;

	$scope.isInitialising = true;
	$scope.isProcessing = false;
	$scope.CurrentSection = 'init';
	$scope.activeCharts = [];

	$scope.FontSizeOptions = [
		{ Label: 'Text Size 10', Class: 'font-size-10' },
		{ Label: 'Text Size 11', Class: 'font-size-11' },
		{ Label: 'Text Size 12', Class: 'font-size-12' },
		{ Label: 'Text Size 13', Class: 'font-size-13' },
		{ Label: 'Text Size 14', Class: 'font-size-14' },
		{ Label: 'Text Size 15', Class: 'font-size-15' },
		{ Label: 'Text Size 16', Class: 'font-size-16' }
	];
	$scope.FontSize = $scope.FontSizeOptions[1];

	$scope.ChartColorsArr = ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00', '#66aa00', '#cc6633'];
	$scope.DateFilters = [
		{ ID: 0, Label: 'Custom' },
		{ ID: 1, Label: 'This Month' },
		{ ID: 9, Label: 'Today' },
		{ ID: 2, Label: 'Yesterday' },
		{ ID: 3, Label: 'Last 7 Days' },
		{ ID: 4, Label: 'Last 2 Weeks' },
		{ ID: 5, Label: 'Last Month' },
		{ ID: 6, Label: 'This Year' },
		{ ID: 7, Label: 'Last Year' },
		{ ID: 8, Label: 'All Time' },
	];
	$scope.GetFilterDate = function (DateTypeLabel, CustomFrom, CustomTo) {
		var retVal = { From: null, To: null };
		switch (DateTypeLabel) {
			case 'Custom': retVal = { From: CustomFrom, To: CustomTo }; break;
			case 'This Month': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Today': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Yesterday': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Last 7 Days': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 7, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), -1, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Last 2 Weeks': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 14, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Last Month': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, 1, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), 0, 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'This Year': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear(), 0, 1, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'Last Year': retVal = { From: $scope.FormatJsDate(new Date((new Date()).getFullYear() - 1, 0, 1, 0, 0, 0), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date((new Date()).getFullYear(), 0, 0, 0, 0, 0), 'YYYY-MM-DD') }; break;
			case 'All Time': retVal = { From: $scope.FormatJsDate(new Date(2001, 1, 1), 'YYYY-MM-DD'), To: $scope.FormatJsDate(new Date(), 'YYYY-MM-DD') }; break;
		}
		return retVal;
	};
	$scope.AffiliateSizes = [
		{ Value: 0, Label: 'Not Set' },
		{ Value: 1, Label: 'Small' },
		{ Value: 2, Label: 'Medium' },
		{ Value: 3, Label: 'Large' }
	];
	$scope.CountryStatuses = [
		{ Value: -1, Label: 'Not Set' },
		{ Value: 0, Label: 'Banned' },
		{ Value: 1, Label: 'Targets' },
	];
	$scope.ValidifyUrl = function (url) {
		if (url == null) {
			return '';
		}
		if (url.indexOf('//') == -1) {
			return '//' + url;
		}
		return url;
	}

	$scope.GroupByOptions = [
		{ Value: 'affiliate', Label: 'Group by Affiliate' },
		{ Value: 'operator', Label: 'Group by Operator' },
		{ Value: 'none', Label: 'No Grouping' },
	];
	$scope.UnassignedUserObj = {
		Comment: null,
		CreateDate: '0001-01-01T00:00:00',
		Email: null,
		IsApproved: false,
		IsLockedOut: false,
		IsOnline: false,
		IsSelected: false,
		LastLoginDate: '0001-01-01T00:00:00',
		Password: null,
		Status: null,
		UserId: '00000000-0000-0000-0000-000000000000',
		UserName: "Unassigned"
	}
	$scope.GetDropDownSelectedIndex = function (listArr, selectedObj, idVal) {
		for (var c = 0; c < listArr.length; c++) {
			if (listArr[c][idVal] == selectedObj[idVal]) {
				return c;
			}
		}
		return -1;
	};
	/*END VARS*/


	/*BEGIN GENERIC*/
	$scope.ApiCall = function (params, callback /*,omitloading*/) {
		var omitLoading = arguments.length > 2 && arguments[2] == true;
		if (!omitLoading) {
			$scope.isProcessing = true;
			$scope.ShowMessage(null);
		}
		var postData = '';
		for (c = 0; c < params.length; c++) {
			if (postData != '') {
				postData += '***';
			}
			postData += params[c][0] + '|' + encodeURIComponent(params[c][1]);
		}
		if (!debugmode) {
			postData = Base64.encode(postData);
		}
		postData = 'data=' + postData;
		$http.post('/api', postData)
			.success(function (result, status, headers, config) {
				if (!debugmode) {
					result = JSON.parse(Base64.decode(result));
				}
				$scope.isProcessing = false;
				if (result.Success) {
					callback(result);
				} else {
					if (result.ErrorCode == 1) {
						document.location.href = '/login?sessiontimeout=1';
					} else {
						callback(result);
					}
				}
			})
			.error(function (data, status, headers, config) {
				$scope.isProcessing = false;
				callback({
					Success: false,
					Error: status,
					ErrorCode: 500,
					ResponseData: null
				});
			})
		;
	};
	$scope.RenderSection = function (NewSection/*[,param][,ignoreparminurl]*/) {
		var param = arguments.length > 1 ? arguments[1] : null;
		if (arguments.length > 2 && arguments[2] == true) {
			$scope.UpdateBrowserState(NewSection);
		} else {
			$scope.UpdateBrowserState(NewSection, param);
		}
		$scope.ShowMessage(null);
		$scope.activeCharts = [];
		switch (NewSection) {
			case 'dashboard': $scope.DashboardInit(param); break;
			case 'affiliates': $scope.AffiliatesInit(param); break;
			case 'addaffiliate': $scope.AddAffiliateInit(param); break;
			case 'affiliate': $scope.AffiliateViewInit(param); break;
			case 'operators': $scope.OperatorsInit(param); break;
			case 'addoperator': $scope.AddOperatorInit(param); break;
			case 'operator': $scope.OperatorViewInit(param); break;
			case 'operatorsignuplinks': $scope.OperatorSignupLinksInit(param); break;
			case 'users': $scope.UsersInit(param); break;
			case 'adduser': $scope.AddUserInit(param); break;
			case 'settings': $scope.SettingsInit(param); break;
			case 'report-custom': $scope.ReportCustomInit(param); break;
			case 'report-affiliates': $scope.ReportAffiliatesInit(param); break;
			case 'report-operators': $scope.ReportOperatorsInit(param); break;
			case 'report-deals': $scope.ReportDealsInit(param); break;
			case 'report-user-deals-summary': $scope.ReportUserDealsSummaryInit(param); break;
			case 'report-operator-deals-summary': $scope.ReportOperatorDealsSummaryInit(param); break;
		}
	};
	$scope.ShowMessage = function (message /*, type*/) {
		var messageType = arguments.length > 1 ? arguments[1] : 'regular';
		if (messageType == 'popup') {
			$scope.RenderPopupMessage('messages', message, arguments.length > 2 ? arguments[2] : null);
		} else if (messageType == 'both') {
			$scope.RenderBothMessage('messages', message, arguments.length > 2 ? arguments[2] : null);
		} else {
			$scope.RenderMessage('messages', message);
		}
	};
	$scope.ShowError = function (error /*, type*/) {
		var messageType = arguments.length > 1 ? arguments[1] : 'regular';
		if (messageType == 'popup') {
			$scope.RenderPopupMessage('errors', error);
		} else if (messageType == 'both') {
			$scope.RenderBothMessage('errors', error);
		} else {
			$scope.RenderMessage('errors', error);
		}
	};
	$scope.RenderMessage = function (cssclass, message) {
		if (message == null) {
			$scope.Message = null;
			$('div.layoutcols.layoutcol_fixedleft_400 div.layoutcol.layoutcol_1_2').css({ 'top': '60px' });
		} else {
			document.getElementById('main-scroller').scrollTop = 0;
			$scope.Message = { cssclass: cssclass, text: message };
		}
	};
	$scope.RenderPopupMessage = function (cssclass, message) {
		if (message == null) {
			$scope.PopupMessage = null;
		} else {
			$scope.PopupMessage = { cssclass: cssclass, text: message, callbackfunc: arguments.length > 2 ? arguments[2] : null };
		}
	};
	$scope.RenderBothMessage = function (cssclass, message) {
		if (message == null) {
			$scope.Message = null;
			$scope.PopupMessage = null;
			$('div.layoutcols.layoutcol_fixedleft_400 div.layoutcol.layoutcol_1_2').css({ 'top': '60px' });
		} else {
			document.getElementById('main-scroller').scrollTop = 0;
			$scope.Message = { cssclass: cssclass, text: message };
			$scope.PopupMessage = { cssclass: cssclass, text: message, callbackfunc: arguments.length > 2 ? arguments[2] : null };
		}
	};
	$scope.ClosePopupMessage = function () {
		if ($scope.PopupMessage.callbackfunc != undefined) {
			var popupCallbackFunc = $scope.PopupMessage.callbackfunc;
			$timeout(popupCallbackFunc, 1);
		}
		$scope.PopupMessage = null;
		$('div.layoutcols.layoutcol_fixedleft_400 div.layoutcol.layoutcol_1_2').css({ 'top': '60px' });
	};

	$scope.GetBestDealInfo = function (deals) {
		if (deals.length == 0) {
			return 'No Deals';
		}
		var bestDeal = deals[0].DealInfo;
		$scope.Operator.Deals.forEach(function (opDealObj, index, array) {
			if (opDealObj.DealInfo.DealInfoValueRank != -1) {
				if (bestDeal.DealInfoValueRank == -1 || opDealObj.DealInfo.DealInfoValueRank < bestDeal.DealInfoValueRank) {
					bestDeal = opDealObj.DealInfo;
				}
			}
		});
		return bestDeal.DealInfoName;
	}

	$scope.GetLink = function (arg) {
		var link = '/area/' + arg + '/';
		if (arguments.length > 1) {
			link += arguments[1] + '/';
		}
		return link;
	};
	$scope.UpdateBrowserState = function (section, param /*[,message]*/) {
		$scope.CurrentSection = section;
		$scope.ShowMessage(arguments.length > 2 ? arguments[2] : null);
		if (history.state == null || !(history.state.section == section && history.state.param == param)) {
			history.pushState({ isihre: true, section: section, param: param }, 'IHRE', '/area/' + section + '/' + (param != null ? param + '/' : ''));
		}
	}
	/*END GENERIC*/

	/*BEGIN DATE FORMATTING*/
	$scope.TimestampToJsDate = function (arg) {
		if (arg != null) {
			var datetimeSplit = arg.split('T');
			if (datetimeSplit.length == 2) {
				var dateSplit = datetimeSplit[0].split('-');
				if (dateSplit.length == 3) {
					var timeSplit = datetimeSplit[1].split(':');
					if (timeSplit.length == 3) {
						return new Date(dateSplit[0], parseInt(dateSplit[1]) - 1, dateSplit[2], timeSplit[0], timeSplit[1], timeSplit[2]);
					}
				}
			}
		}
		return null;
	};
	$scope.FormatTimestampString = function (timestampStr, format) {
		return $scope.FormatJsDate($scope.TimestampToJsDate(timestampStr), format);
	};
	$scope.FormatJsDate = function (dateObj, format) {
		return FormatJsDate(dateObj, format);
	};
	/*END DATE FORMATTING*/



	/*BEGIN VALIDATION*/
	$scope.ValidateRequired = function (value, errordict, errorkey) {
		if (value != null && value != '') {
			errordict.Update(errorkey, null);
			return true;
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!');
			return false;
		}
	};
	$scope.ValidateCommaNos = function (value, errordict, errorkey) {
		if (value && value != '') {
			var valsArr = value.trim().replace(/(\r\n|\n|\r)/gm, "").split(',');
			for (c = 0; c < valsArr.length; c++) {
				if (isNaN(valsArr[c].trim())) {
					errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid Value');
					return;
				}
			}
		}
		errordict.Update(errorkey, null);
	};
	$scope.ValidateUsername = function (value, errordict, errorkey) {
		var patt = new RegExp(/^([a-zA-Z]{1})([a-zA-Z0-9]{3,15})$/);
		if (patt.test(value)) {
			errordict.Update(errorkey, null);
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid username - Must start with a letter and be between 6-16 alphanumeric characters!');
		}
	};
	$scope.ValidatePassword = function (value, errordict, errorkey) {
		var patt = new RegExp(/^([a-zA-Z]{1})([a-zA-Z0-9!]{5,15})$/);
		if (patt.test(value)) {
			errordict.Update(errorkey, null);
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid password - Must start with a letter and be between 6-16 alphanumeric characters!');
		}
	};
	$scope.ValidateCompare = function (value1, value2, errordict, errorkey) {
		if (value1 != null && value2 != null && value1 == value2) {
			errordict.Update(errorkey, null);
		} else {
			errordict.Update(errorkey, arguments.length > 4 ? $scope.GetTranslation(arguments[4]) : 'Fields do not match!');
		}
	};
	$scope.ValidateEmail = function (value, errordict, errorkey) {
		var patt = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/);
		if (patt.test(value)) {
			errordict.Update(errorkey, null);
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid email!');
		}
	};
	$scope.ValidateCheckbox = function (value, errordict, errorkey) {
		if (value && value == true) {
			errordict.Update(errorkey, null);
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!');
		}
	};
	$scope.ValidateNumber = function (value, errordict, errorkey) {
		if (value != null && value != '') {
			if (isNaN(value)) {
				errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid number!');
				return;
			}
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!');
			return;
		}
		errordict.Update(errorkey, null);
	};
	$scope.ValidateNumberGt = function (value, value2, errordict, errorkey) {
		if (value != null && value != '') {
			if (isNaN(value)) {
				errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid number!');
				return;
			} else {
				if (parseFloat(value) <= parseFloat(value2)) {
					errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Value too low!');
					return;
				}
			}
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!');
			return;
		}
		errordict.Update(errorkey, null);
	};
	$scope.ValidatePercentage = function (value, errordict, errorkey) {
		if (value != null && value != '') {
			if (isNaN(value)) {
				errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid percentage!');
				return;
			} else if (value > 1 || value < 0) {
				errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid percentage!');
				return;
			}
		} else {
			errordict.Update(errorkey, arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!');
			return;
		}
		errordict.Update(errorkey, null);
	};
	/*END VALIDATION*/


	/*BEGIN DASHBOARD*/
	$scope.DashboardInit = function () {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);
		$scope.DashboardFilter = {
			UserStats: {
				DateType: $scope.DateFilters[1],
				From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
				To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
			},
			PendingDeals: {
				AllUsers: false,
				DateType: $scope.DateFilters[1],
				From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
				To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
				GroupByOption: $scope.GroupByOptions[0],
				Pager: CreatePagerObj(),
			},
			ConfirmedActiveDeals: {
				AllUsers: false,
				DateType: $scope.DateFilters[1],
				From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
				To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
				GroupByOption: $scope.GroupByOptions[0],
				Pager: CreatePagerObj(),
			},
			ConfirmedInactiveDeals: {
				AllUsers: false,
				DateType: $scope.DateFilters[1],
				From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
				To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
				GroupByOption: $scope.GroupByOptions[0],
				Pager: CreatePagerObj(),
			},
			AwaitingResponseDeals: {
				AllUsers: false,
				DateType: $scope.DateFilters[1],
				From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
				To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
				GroupByOption: $scope.GroupByOptions[0],
				Pager: CreatePagerObj(),
			},

		};

		var userStatsDates = $scope.GetFilterDate($scope.DashboardFilter.UserStats.DateType.Label, $scope.DashboardFilter.UserStats.From, $scope.DashboardFilter.UserStats.To);
		var pendingDealsDates = $scope.GetFilterDate($scope.DashboardFilter.PendingDeals.DateType.Label, $scope.DashboardFilter.PendingDeals.From, $scope.DashboardFilter.PendingDeals.To);
		var confirmedActiveDealsDates = $scope.GetFilterDate($scope.DashboardFilter.ConfirmedActiveDeals.DateType.Label, $scope.DashboardFilter.ConfirmedActiveDeals.From, $scope.DashboardFilter.ConfirmedActiveDeals.To);
		var confirmedInactiveDealsDates = $scope.GetFilterDate($scope.DashboardFilter.ConfirmedInactiveDeals.DateType.Label, $scope.DashboardFilter.ConfirmedInactiveDeals.From, $scope.DashboardFilter.ConfirmedInactiveDeals.To);
		var awaitingResponseDealsDates = $scope.GetFilterDate($scope.DashboardFilter.AwaitingResponseDeals.DateType.Label, $scope.DashboardFilter.AwaitingResponseDeals.From, $scope.DashboardFilter.AwaitingResponseDeals.To);
		$scope.ApiCall(
			[
				['action', 'getdbsummary_getfavouriteoperators_getlatestoperators_getuserstats_getdashboardpendingdeals_getdashboardconfirmedactivedeals_getdashboardconfirmedinactivedeals_getdashboardawaitingresponsedeals'],
				['dashboarduserstatsfrom', userStatsDates.From],
				['dashboarduserstatsto', userStatsDates.To],

				['dashboardpendingdealsallusers', $scope.DashboardFilter.PendingDeals.AllUsers],
				['dashboardpendingdealsfrom', pendingDealsDates.From],
				['dashboardpendingdealsto', pendingDealsDates.To],
				['dashboardpendingdealsgroupby', $scope.DashboardFilter.PendingDeals.GroupByOption.Value],
				['dashboardpendingdealsoffset', $scope.DashboardFilter.PendingDeals.Pager.Info.Offset],
				['dashboardpendingdealscount', $scope.DashboardFilter.PendingDeals.Pager.Info.Count],

				['dashboardconfirmedactivedealsallusers', $scope.DashboardFilter.ConfirmedActiveDeals.AllUsers],
				['dashboardconfirmedactivedealsfrom', confirmedActiveDealsDates.From],
				['dashboardconfirmedactivedealsto', confirmedActiveDealsDates.To],
				['dashboardconfirmedactivedealsgroupby', $scope.DashboardFilter.ConfirmedActiveDeals.GroupByOption.Value],
				['dashboardconfirmedactivedealsoffset', $scope.DashboardFilter.ConfirmedActiveDeals.Pager.Info.Offset],
				['dashboardconfirmedactivedealscount', $scope.DashboardFilter.ConfirmedActiveDeals.Pager.Info.Count],

				['dashboardconfirmedinactivedealsallusers', $scope.DashboardFilter.ConfirmedInactiveDeals.AllUsers],
				['dashboardconfirmedinactivedealsfrom', confirmedInactiveDealsDates.From],
				['dashboardconfirmedinactivedealsto', confirmedInactiveDealsDates.To],
				['dashboardconfirmedinactivedealsgroupby', $scope.DashboardFilter.ConfirmedInactiveDeals.GroupByOption.Value],
				['dashboardconfirmedinactivedealsoffset', $scope.DashboardFilter.ConfirmedInactiveDeals.Pager.Info.Offset],
				['dashboardconfirmedinactivedealscount', $scope.DashboardFilter.ConfirmedInactiveDeals.Pager.Info.Count],

				['dashboardawaitingresponsedealsallusers', $scope.DashboardFilter.AwaitingResponseDeals.AllUsers],
				['dashboardawaitingresponsedealsfrom', awaitingResponseDealsDates.From],
				['dashboardawaitingresponsedealsto', awaitingResponseDealsDates.To],
				['dashboardawaitingresponsedealsgroupby', $scope.DashboardFilter.AwaitingResponseDeals.GroupByOption.Value],
				['dashboardawaitingresponsedealsoffset', $scope.DashboardFilter.AwaitingResponseDeals.Pager.Info.Offset],
				['dashboardawaitingresponsedealscount', $scope.DashboardFilter.AwaitingResponseDeals.Pager.Info.Count],

			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard = {
						DbSummary: result.ResponseData.DbSummary,
						FavouriteOperators: result.ResponseData.FavouriteOperators,
						LatestOperators: result.ResponseData.LatestOperators,
						UserStats: result.ResponseData.UserStats,
						PendingDeals: result.ResponseData.PendingDeals,
						ConfirmedActiveDeals: result.ResponseData.ConfirmedActiveDeals,
						ConfirmedInactiveDeals: result.ResponseData.ConfirmedInactiveDeals,
						AwaitingResponseDeals: result.ResponseData.AwaitingResponseDeals,
					}

					$scope.DashboardFilter.PendingDeals.Pager.SetPagerInfo(result.ResponseData.PendingDealsCount, $scope.DashboardUpdatePendingDeals);
					$scope.DashboardFilter.ConfirmedActiveDeals.Pager.SetPagerInfo(result.ResponseData.ConfirmedActiveDealsCount, $scope.DashboardUpdateConfirmedActiveDeals);
					$scope.DashboardFilter.ConfirmedInactiveDeals.Pager.SetPagerInfo(result.ResponseData.ConfirmedInactiveDealsCount, $scope.DashboardUpdateConfirmedInactiveDeals);
					$scope.DashboardFilter.AwaitingResponseDeals.Pager.SetPagerInfo(result.ResponseData.AwaitingResponseDealsCount, $scope.DashboardUpdateAwaitingResponseDeals);


					$scope.DashboardRefreshUserStatsChart();
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};

	$scope.DashboardUpdatePendingDeals = function (/*[offset]*/) {
		$scope.DashboardFilter.PendingDeals.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var pendingDealsDates = $scope.GetFilterDate($scope.DashboardFilter.PendingDeals.DateType.Label, $scope.DashboardFilter.PendingDeals.From, $scope.DashboardFilter.PendingDeals.To);
		$scope.ApiCall(
			[
				['action', 'getdashboardpendingdeals'],
				['dashboardpendingdealsallusers', $scope.DashboardFilter.PendingDeals.AllUsers],
				['dashboardpendingdealsfrom', pendingDealsDates.From],
				['dashboardpendingdealsto', pendingDealsDates.To],
				['dashboardpendingdealsgroupby', $scope.DashboardFilter.PendingDeals.GroupByOption.Value],
				['dashboardpendingdealsoffset', $scope.DashboardFilter.PendingDeals.Pager.Info.Offset],
				['dashboardpendingdealscount', $scope.DashboardFilter.PendingDeals.Pager.Info.Count],
			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard.PendingDeals = result.ResponseData.PendingDeals;
					$scope.DashboardFilter.PendingDeals.Pager.SetPagerInfo(result.ResponseData.PendingDealsCount, $scope.DashboardUpdatePendingDeals);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.DashboardUpdateConfirmedActiveDeals = function (/*[offset]*/) {
		$scope.DashboardFilter.ConfirmedActiveDeals.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var confirmedActiveDealsDates = $scope.GetFilterDate($scope.DashboardFilter.ConfirmedActiveDeals.DateType.Label, $scope.DashboardFilter.ConfirmedActiveDeals.From, $scope.DashboardFilter.ConfirmedActiveDeals.To);
		$scope.ApiCall(
			[
				['action', 'getdashboardconfirmedactivedeals'],
				['dashboardconfirmedactivedealsallusers', $scope.DashboardFilter.ConfirmedActiveDeals.AllUsers],
				['dashboardconfirmedactivedealsfrom', confirmedActiveDealsDates.From],
				['dashboardconfirmedactivedealsto', confirmedActiveDealsDates.To],
				['dashboardconfirmedactivedealsgroupby', $scope.DashboardFilter.ConfirmedActiveDeals.GroupByOption.Value],
				['dashboardconfirmedactivedealsoffset', $scope.DashboardFilter.ConfirmedActiveDeals.Pager.Info.Offset],
				['dashboardconfirmedactivedealscount', $scope.DashboardFilter.ConfirmedActiveDeals.Pager.Info.Count],
			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard.ConfirmedActiveDeals = result.ResponseData.ConfirmedActiveDeals;
					$scope.DashboardFilter.ConfirmedActiveDeals.Pager.SetPagerInfo(result.ResponseData.ConfirmedActiveDealsCount, $scope.DashboardUpdateConfirmedActiveDeals);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.DashboardUpdateConfirmedInactiveDeals = function (/*[offset]*/) {
		$scope.DashboardFilter.ConfirmedInactiveDeals.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var confirmedInactiveDealsDates = $scope.GetFilterDate($scope.DashboardFilter.ConfirmedInactiveDeals.DateType.Label, $scope.DashboardFilter.ConfirmedInactiveDeals.From, $scope.DashboardFilter.ConfirmedInactiveDeals.To);
		$scope.ApiCall(
			[
				['action', 'getdashboardconfirmedinactivedeals'],
				['dashboardconfirmedinactivedealsallusers', $scope.DashboardFilter.ConfirmedInactiveDeals.AllUsers],
				['dashboardconfirmedinactivedealsfrom', confirmedInactiveDealsDates.From],
				['dashboardconfirmedinactivedealsto', confirmedInactiveDealsDates.To],
				['dashboardconfirmedinactivedealsgroupby', $scope.DashboardFilter.ConfirmedInactiveDeals.GroupByOption.Value],
				['dashboardconfirmedinactivedealsoffset', $scope.DashboardFilter.ConfirmedInactiveDeals.Pager.Info.Offset],
				['dashboardconfirmedinactivedealscount', $scope.DashboardFilter.ConfirmedInactiveDeals.Pager.Info.Count],
			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard.ConfirmedInactiveDeals = result.ResponseData.ConfirmedInactiveDeals;
					$scope.DashboardFilter.ConfirmedInactiveDeals.Pager.SetPagerInfo(result.ResponseData.ConfirmedInactiveDealsCount, $scope.DashboardUpdateConfirmedInactiveDeals);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.DashboardUpdateAwaitingResponseDeals = function (/*[offset]*/) {
		$scope.DashboardFilter.AwaitingResponseDeals.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var awaitingResponseDealsDates = $scope.GetFilterDate($scope.DashboardFilter.AwaitingResponseDeals.DateType.Label, $scope.DashboardFilter.AwaitingResponseDeals.From, $scope.DashboardFilter.AwaitingResponseDeals.To);
		$scope.ApiCall(
			[
				['action', 'getdashboardawaitingresponsedeals'],
				['dashboardawaitingresponsedealsallusers', $scope.DashboardFilter.AwaitingResponseDeals.AllUsers],
				['dashboardawaitingresponsedealsfrom', awaitingResponseDealsDates.From],
				['dashboardawaitingresponsedealsto', awaitingResponseDealsDates.To],
				['dashboardawaitingresponsedealsgroupby', $scope.DashboardFilter.AwaitingResponseDeals.GroupByOption.Value],
				['dashboardawaitingresponsedealsoffset', $scope.DashboardFilter.AwaitingResponseDeals.Pager.Info.Offset],
				['dashboardawaitingresponsedealscount', $scope.DashboardFilter.AwaitingResponseDeals.Pager.Info.Count],
			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard.AwaitingResponseDeals = result.ResponseData.AwaitingResponseDeals;
					$scope.DashboardFilter.AwaitingResponseDeals.Pager.SetPagerInfo(result.ResponseData.AwaitingResponseDealsCount, $scope.DashboardUpdateAwaitingResponseDeals);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	
	$scope.DashboardUpdateUserStats = function () {
		var userStatsDates = $scope.GetFilterDate($scope.DashboardFilter.UserStats.DateType.Label, $scope.DashboardFilter.UserStats.From, $scope.DashboardFilter.UserStats.To);
		$scope.ApiCall(
			[
				['action', 'getuserstats'],
				['dashboarduserstatsfrom', userStatsDates.From],
				['dashboarduserstatsto', userStatsDates.To],
			],
			function (result) {
				if (result.Success) {
					$scope.Dashboard.UserStats = result.ResponseData.UserStats;
					$scope.DashboardRefreshUserStatsChart();
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};

	$scope.DashboardRefreshUserStatsChart = function () {
		$timeout(function () {
			if ($scope.Dashboard != null) {
				if ($scope.Dashboard.UserStats != null) {

	
					$('#acc-manager-stats-chart-container').highcharts({
						chart: {
							type: 'bar',
							height: 250,
							//spacing: [0, 0, 0, 0]
						},
						title: null,
						subtitle: null,
						xAxis: { type: 'category', },
						yAxis: { min: 0, title: null },
						legend: { enabled: true },
						plotOptions: {
							series: {
								//stacking: 'normal'
							}
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.y} ({point.percentage:.1f}%)</b>'
						},
						series: [
							{
								name: 'Total Deals',
								data: $scope.Dashboard.UserStats.SeriesData[2].SeriesValues,
								colorByPoint: false,
								visible: true,
							},
							{
								name: 'User Deals',
								data: $scope.Dashboard.UserStats.SeriesData[0].SeriesValues,
								colorByPoint: false,
							}
						]
					});
				}
			}
		}, 1);
	};

	/*END DASHBOARD*/


	/*BEGIN AFFILIATES*/
	$scope.AffiliatesInit = function (/*searchparam*/) {
		var quickSearchStr = arguments.length > 0 ? arguments[0] : null;
		$scope.ApiCall(
			[
				['action', 'getcountries_getuserslite_getoperatorslite_getdealstatuses_getdealtypes_getaffiliatetypes_getmarketingtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.FilterAffiliates = {
						AffiliateID: '',
						QuickSearch: null,
						SearchStr: quickSearchStr,
						AffiliateManagerFilterOn: false, AffiliateManagerList: result.ResponseData.Users,
						SizeList: [{ ID: 0, Label: 'Any' }, { ID: 1, Label: 'Small' }, { ID: 2, Label: 'Medium' }, { ID: 3, Label: 'Large' }], SizeSelected: null,
						ContactableList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Yes' }, { ID: 0, Label: 'No' }], ContactableSelected: null,
						MaxMailList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Yes' }, { ID: 0, Label: 'No' }], MaxMailSelected: null,
						AffStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], AffStatusSelected: null,
						IncludeUnassigned: true,
						OperatorFilterOn: false, OperatorList: result.ResponseData.Operators, OperatorOptions: result.ResponseData.DealStatuses, OperatorOptionSelected: null,
						CountryFilterOn: false, CountryList: result.ResponseData.Countries, CountryOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], CountryOptionSelected: null,
						AffiliateTypeFilterOn: false, AffiliateTypeList: result.ResponseData.AffiliateTypes, AffiliateTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], AffiliateTypeOptionSelected: null,
						DealTypeFilterOn: false, DealTypeList: result.ResponseData.DealTypes, DealTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], DealTypeOptionSelected: null,
						MarketingTypeFilterOn: false, MarketingTypeList: result.ResponseData.MarketingTypes, MarketingTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], MarketingTypeOptionSelected: null,

						ShowBatchOperations: false,
						BatchOperationSelectedAffiliateManager: null,
						Pager: CreatePagerObj(),
						Errors: new ErrorDictionary()
					};

					$scope.FilterAffiliates.SizeSelected = $scope.FilterAffiliates.SizeList[0];
					$scope.FilterAffiliates.ContactableSelected = $scope.FilterAffiliates.ContactableList[0];
					$scope.FilterAffiliates.MaxMailSelected = $scope.FilterAffiliates.MaxMailList[0];
					$scope.FilterAffiliates.AffStatusSelected = $scope.FilterAffiliates.AffStatusList[0];

					$scope.FilterAffiliates.OperatorOptionSelected = $scope.FilterAffiliates.OperatorOptions[0];
					$scope.FilterAffiliates.CountryOptionSelected = $scope.FilterAffiliates.CountryOptions[0];
					$scope.FilterAffiliates.AffiliateTypeOptionSelected = $scope.FilterAffiliates.AffiliateTypeOptions[0];
					$scope.FilterAffiliates.DealTypeOptionSelected = $scope.FilterAffiliates.DealTypeOptions[0];
					$scope.FilterAffiliates.MarketingTypeOptionSelected = $scope.FilterAffiliates.MarketingTypeOptions[0];

					$scope.FilterAffiliates.BatchOperationSelectedAffiliateManager = $scope.FilterAffiliates.AffiliateManagerList[0];
					$scope.AffiliatesSearchSubmit($scope.FilterAffiliates.Pager.Info.Offset);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};

	$scope.AffiliateQuickSearchEnter = function (keyEvent, param) {
		if (keyEvent.which === 13) {
			$scope.AffiliateQuickSearch(param);
		}
	};
	$scope.AffiliateQuickSearch = function (param) {
		if (isNaN(param)) {
			$scope.RenderSection('affiliates', param);
		} else {
			$scope.RenderSection('affiliate', param);
		}
	};

	$scope.AffiliatesFilterSearchEnter = function (keyEvent) {
		if (keyEvent.which === 13) {
			$scope.AffiliatesSearchSubmit();
		}
	};
	$scope.AffiliatesSearchSubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterAffiliates.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var affmanager_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.AffiliateManagerList.length; c++) {
			if ($scope.FilterAffiliates.AffiliateManagerList[c].IsSelected) {
				affmanager_ids += (affmanager_ids != '' ? ',' : '') + $scope.FilterAffiliates.AffiliateManagerList[c].UserId;
			}
		}
		var operator_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.OperatorList.length; c++) {
			if ($scope.FilterAffiliates.OperatorList[c].IsSelected) {
				operator_ids += (operator_ids != '' ? ',' : '') + $scope.FilterAffiliates.OperatorList[c].OperatorID;
			}
		}
		var country_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.CountryList.length; c++) {
			if ($scope.FilterAffiliates.CountryList[c].IsSelected) {
				country_ids += (country_ids != '' ? ',' : '') + $scope.FilterAffiliates.CountryList[c].CountryID;
			}
		}
		var affiliatetype_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.AffiliateTypeList.length; c++) {
			if ($scope.FilterAffiliates.AffiliateTypeList[c].IsSelected) {
				affiliatetype_ids += (affiliatetype_ids != '' ? ',' : '') + $scope.FilterAffiliates.AffiliateTypeList[c].AffiliateTypeID;
			}
		}
		var dealtype_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.DealTypeList.length; c++) {
			if ($scope.FilterAffiliates.DealTypeList[c].IsSelected) {
				dealtype_ids += (dealtype_ids != '' ? ',' : '') + $scope.FilterAffiliates.DealTypeList[c].DealTypeID;
			}
		}
		var marketingtype_ids = '';
		for (var c = 0; c < $scope.FilterAffiliates.MarketingTypeList.length; c++) {
			if ($scope.FilterAffiliates.MarketingTypeList[c].IsSelected) {
				marketingtype_ids += (marketingtype_ids != '' ? ',' : '') + $scope.FilterAffiliates.MarketingTypeList[c].MarketingTypeID;
			}
		}

		$scope.ApiCall(
			[
				['action', 'getaffiliates'],
				['affiliateid', $scope.FilterAffiliates.AffiliateID],
				['searchstr', $scope.FilterAffiliates.SearchStr],
				['affmanagerids', affmanager_ids],
				['affsizetype', $scope.FilterAffiliates.SizeSelected.ID],
				['contactabletype', $scope.FilterAffiliates.ContactableSelected.ID],
				['maxmailtype', $scope.FilterAffiliates.MaxMailSelected.ID],
				['affstatustype', $scope.FilterAffiliates.AffStatusSelected.ID],
				['includeunassigned', $scope.FilterAffiliates.IncludeUnassigned],
				['operatortypeoption', $scope.FilterAffiliates.OperatorOptionSelected.DealStatusID],
				['operatorids', operator_ids],
				['countrytypeoption', $scope.FilterAffiliates.CountryOptionSelected.ID],
				['countryids', country_ids],
				['affiliatetypeoption', $scope.FilterAffiliates.AffiliateTypeOptionSelected.ID],
				['affiliatetypeids', affiliatetype_ids],
				['dealtypeoption', $scope.FilterAffiliates.DealTypeOptionSelected.ID],
				['dealtypeids', dealtype_ids],
				['marketingtypeoption', $scope.FilterAffiliates.MarketingTypeOptionSelected.ID],
				['marketingtypeids', marketingtype_ids],
				['offset', $scope.FilterAffiliates.Pager.Info.Offset],
				['count', $scope.FilterAffiliates.Pager.Info.Count]],
			function (result) {
				if (result.Success) {
					$scope.FilterAffiliates.Pager.SetPagerInfo(result.ResponseData.AffiliatesCount, $scope.AffiliatesSearchSubmit);
					$scope.Affiliates = result.ResponseData.Affiliates;
					$scope.isProcessing = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};


	$scope.AffiliatesSearchReport = function()
	{
		$scope.CustomReportParamsShow(function () {
			var customReportParams = '';
			
			customReportParams += 'SearchStr___' + $scope.FilterAffiliates.SearchStr;
			customReportParams += '^^^AllUsers___1';
			customReportParams += '^^^SizeSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.SizeList, $scope.FilterAffiliates.SizeSelected, 'ID'))
			customReportParams += '^^^ContactableSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.ContactableList, $scope.FilterAffiliates.ContactableSelected, 'ID'))
			customReportParams += '^^^MaxMailSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.MaxMailList, $scope.FilterAffiliates.MaxMailSelected, 'ID'));
			customReportParams += '^^^AffStatusSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.AffStatusList, $scope.FilterAffiliates.AffStatusSelected, 'ID'));
			customReportParams += '^^^IncludeUnassigned___' + ($scope.FilterAffiliates.IncludeUnassigned ? '1' : '0');
			customReportParams += '^^^CreatedDateRange___9_2000-01-01_2100-01-01';

			var affmanager_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.AffiliateManagerList.length; c++) {
				if ($scope.FilterAffiliates.AffiliateManagerList[c].IsSelected) {
					affmanager_ids += (affmanager_ids != '' ? '_' : '') + $scope.FilterAffiliates.AffiliateManagerList[c].UserId;
				}
			}
			customReportParams += '^^^AffiliateManagers___' + affmanager_ids;

			customReportParams += '^^^OperatorOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.OperatorOptions, $scope.FilterAffiliates.OperatorOptionSelected, 'DealStatusID'));
			var operator_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.OperatorList.length; c++) {
				if ($scope.FilterAffiliates.OperatorList[c].IsSelected) {
					operator_ids += (operator_ids != '' ? '_' : '') + $scope.FilterAffiliates.OperatorList[c].OperatorID;
				}
			}
			customReportParams += '^^^Operators___' + operator_ids;
			
			customReportParams += '^^^CountryOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.CountryOptions, $scope.FilterAffiliates.CountryOptionSelected, 'ID'));

			var country_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.CountryList.length; c++) {
				if ($scope.FilterAffiliates.CountryList[c].IsSelected) {
					country_ids += (country_ids != '' ? '_' : '') + $scope.FilterAffiliates.CountryList[c].CountryID;
				}
			}
			customReportParams += '^^^Countries___' + country_ids;

			customReportParams += '^^^AffiliateTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.AffiliateTypeOptions, $scope.FilterAffiliates.AffiliateTypeOptionSelected, 'ID'));
			var affiliatetype_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.AffiliateTypeList.length; c++) {
				if ($scope.FilterAffiliates.AffiliateTypeList[c].IsSelected) {
					affiliatetype_ids += (affiliatetype_ids != '' ? '_' : '') + $scope.FilterAffiliates.AffiliateTypeList[c].AffiliateTypeID;
				}
			}
			customReportParams += '^^^AffiliateTypes___' + affiliatetype_ids;

			customReportParams += '^^^DealTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.DealTypeOptions, $scope.FilterAffiliates.DealTypeOptionSelected, 'ID'));
			var dealtype_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.DealTypeList.length; c++) {
				if ($scope.FilterAffiliates.DealTypeList[c].IsSelected) {
					dealtype_ids += (dealtype_ids != '' ? '_' : '') + $scope.FilterAffiliates.DealTypeList[c].DealTypeID;
				}
			}
			customReportParams += '^^^DealTypes___' + dealtype_ids;

			customReportParams += '^^^MarketingTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterAffiliates.MarketingTypeOptions, $scope.FilterAffiliates.MarketingTypeOptionSelected, 'ID'));
			var marketingtype_ids = '';
			for (var c = 0; c < $scope.FilterAffiliates.MarketingTypeList.length; c++) {
				if ($scope.FilterAffiliates.MarketingTypeList[c].IsSelected) {
					marketingtype_ids += (marketingtype_ids != '' ? '_' : '') + $scope.FilterAffiliates.MarketingTypeList[c].MarketingTypeID;
				}
			}
			customReportParams += '^^^MarketingTypes___' + marketingtype_ids;

			customReportParams += '^^^IncAffiliateTypes___0';
			customReportParams += '^^^IncDealTypes___0';
			customReportParams += '^^^IncMarketingTypes___0';
			customReportParams += '^^^IncCountries___0';
			customReportParams += '^^^IncOperators___0';

			$scope.ApiCall(
				[
					['action', 'newcustomreport'],

					['customreportname', $scope.CustomReport.CustomReportName],
					['customreporttype', 'report-affiliates'],
					['customreportparams', customReportParams],
					['currentuseronly', $scope.CustomReport.CurrentUserOnly]
				],
				function (result) {
					if (result.Success) {
						$scope.RenderSection('report-affiliates', result.ResponseData.NewCustomReportId);
					} else {
						$scope.ShowError('Error: ' + result.Error);
					}
				}
			);

		});
	}


	$scope.AffiliatesBatchCheckAllSelected = function () {
		if ($scope.Affiliates != null) {
			for (var c = 0; c < $scope.Affiliates.length; c++) {
				if (!$scope.Affiliates[c].IsSelected) {
					return false;
				}
			}
		}
		return true;
	};
	$scope.AffiliatesBatchSelectAll = function () {
		var allAreSelected = $scope.AffiliatesBatchCheckAllSelected();
		for (var c = 0; c < $scope.Affiliates.length; c++) {
			$scope.Affiliates[c].IsSelected = allAreSelected ? false : true;
		}
	};
	$scope.AffiliatesReassign = function () {
		var affiliateIds = '';
		if ($scope.Affiliates != null) {
			for (var c = 0; c < $scope.Affiliates.length; c++) {
				if ($scope.Affiliates[c].IsSelected) {
					affiliateIds += (affiliateIds != '' ? ',' : '') + $scope.Affiliates[c].AffiliateID;
				}
			}
		}
		$scope.ApiCall(
			[
				['action', 'reassignaffiliates'],
				['affmanageruserid', $scope.FilterAffiliates.BatchOperationSelectedAffiliateManager.UserId],
				['affiliateids', affiliateIds],
			],
			function (result) {
				if (result.Success) {
					for (var c = 0; c < $scope.Affiliates.length; c++) {
						if ($scope.Affiliates[c].IsSelected) {
							$scope.Affiliates[c].AffManagerUser = $scope.FilterAffiliates.BatchOperationSelectedAffiliateManager;
							$scope.Affiliates[c].IsSelected = false;
						}
					}
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);

	};

	$scope.AddAffiliateInit = function () {
		$scope.ApiCall(
			[
				['action', 'getcontactdetailtypes_getcountries_getaffiliatetypes_getusersliteselcur_getdealtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.NewAffiliate = {
						AffiliateName: '',
						ContactName: '',
						AllowContact: true,
						AffManagerUser: null,
						AffiliateManagerList: result.ResponseData.Users,
						AffiliateSize: $scope.AffiliateSizes[0],
						MaxMail: true,
						Comment: '',
						ContactDetailTypes: result.ResponseData.ContactDetailTypes,
						PrimaryContactDetailType: null,
						PrimaryContactDetailText: '',
						PrimaryUrlText: '',
						Countries: result.ResponseData.Countries,
						AffiliateTypes: result.ResponseData.AffiliateTypes,
						DealTypes: result.ResponseData.DealTypes,
						Errors: new ErrorDictionary()
					};

					$scope.NewAffiliate.AffManagerUser = $scope.NewAffiliate.AffiliateManagerList[0];
					$scope.NewAffiliate.AffiliateManagerList.forEach(function (affManObj, index, array) {
						if (affManObj.IsSelected) {
							$scope.NewAffiliate.AffManagerUser = affManObj;
						}
					});
					

					$scope.NewAffiliate.PrimaryContactDetailType = $scope.NewAffiliate.ContactDetailTypes[0];
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);				
	};
	$scope.AddAffiliateSubmit = function () {
		$scope.ShowMessage(null);
		$scope.NewAffiliate.Errors = new ErrorDictionary();
		$scope.ValidateRequired($scope.NewAffiliate.AffiliateName, $scope.NewAffiliate.Errors, 'AffiliateName');
		$scope.ValidateRequired($scope.NewAffiliate.ContactName, $scope.NewAffiliate.Errors, 'ContactName');
		$scope.ValidateRequired($scope.NewAffiliate.PrimaryContactDetailText, $scope.NewAffiliate.Errors, 'PrimaryContactDetailText');
		$scope.ValidateRequired($scope.NewAffiliate.PrimaryUrlText, $scope.NewAffiliate.Errors, 'PrimaryUrlText');


		var countryids = '';
		for (var c = 0; c < $scope.NewAffiliate.Countries.length; c++) {
			if ($scope.NewAffiliate.Countries[c].IsSelected) {
				countryids += (countryids != '' ? ',' : '') + $scope.NewAffiliate.Countries[c].CountryID;
			}
		}
		if (countryids == '') {
			$scope.NewAffiliate.Errors.Update('Countries', 'At least one country must be selected.');
		}
		var affiliatetypeids = '';
		for (var c = 0; c < $scope.NewAffiliate.AffiliateTypes.length; c++) {
			if ($scope.NewAffiliate.AffiliateTypes[c].IsSelected) {
				affiliatetypeids += (affiliatetypeids != '' ? ',' : '') + $scope.NewAffiliate.AffiliateTypes[c].AffiliateTypeID;
			}
		}
		if (affiliatetypeids == '') {
			$scope.NewAffiliate.Errors.Update('AffiliateTypes', 'At least one affiliate type must be selected.');
		}

		var dealtypeids = '';
		for (var c = 0; c < $scope.NewAffiliate.DealTypes.length; c++) {
			if ($scope.NewAffiliate.DealTypes[c].IsSelected) {
				dealtypeids += (dealtypeids != '' ? ',' : '') + $scope.NewAffiliate.DealTypes[c].DealTypeID;
			}
		}
		/*if (dealtypeids == '') {
			$scope.NewAffiliate.Errors.Update('DealTypes', 'At least one deal type must be selected.');
		}*/


		if (!$scope.NewAffiliate.Errors.HasErrors()) {
			$scope.ApiCall(
				[
					['action', 'newaffiliate'],
					['affiliatename', $scope.NewAffiliate.AffiliateName],
					['contactname', $scope.NewAffiliate.AffiliateName],
					['userid', $scope.NewAffiliate.AffManagerUser.UserId],
					['affiliatesize', $scope.NewAffiliate.AffiliateSize.Value],
					['contactable', $scope.NewAffiliate.AllowContact],
					['maxmail', $scope.NewAffiliate.MaxMail],
					['comment', $scope.NewAffiliate.Comment],
					['primarycontactdetailtype', $scope.NewAffiliate.PrimaryContactDetailType.ContactDetailTypeID],
					['primarycontactdetailtext', $scope.NewAffiliate.PrimaryContactDetailText],
					['primaryurltext', $scope.NewAffiliate.PrimaryUrlText],
					['countryids', countryids],
					['affiliatetypeids', affiliatetypeids],
					['dealtypeids', dealtypeids],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('New affiliate created');
						$scope.NewAffiliate = null;
						$scope.RenderSection('affiliate', result.ResponseData.NewAffiliateID);
					} else {
						$scope.NewAffiliate.Errors.Update('Main', result.Error);
					}
				}
			);
		}
	};

	$scope.RemoveAffiliateSubmit = function () {
		$scope.ShowMessage(null);
		if (confirm('Remove Affiliate?')) {
			$scope.ApiCall(
				[
					['action', 'removeaffiliate'],
					['affiliateid', $scope.Affiliate.AffiliateID],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate Removed');
						$scope.RenderSection('affiliates');
					} else {
						$scope.Affiliate.Errors.Update('Main', result.Error);
					}
				}
			);
		}
	};

	$scope.AffiliateViewInit = function (affiliateid) {
		$scope.AffiliateFilter = {
			ViewContactDetailsModeOn: true,
			ViewUrlsModeOn: true,
			EditMode: 'none',
			ActiveTab: 'overview',
			ContactDetailNewText: null,
			ContactDetailNewType: null,
			UrlNewText: null,

			DealsAssignedUserList: [],
			DealsDealStatusList: [],
			DealsDealInfoList : [],
			DealsOperatorList: [],
			DealsShowFilter: false,
			DealsIncludeInactive: true,
			DealsAssignedUserSelected: null,
			DealsDealInfoSelected: null,
			DealsDealStatusSelected: null,

			DealsNewDealModeOn: false,
			DealsNewDealStatus: null,
			DealsNewDealText: '',
			DealsNewDealInfo: null,
			DealsNewOperator: null,

			NotesShowOlderNotes: false,
			NotesNewNoteModeOn: false,
			NotesNewNoteText: '',
		};

		$scope.ApiCall(
			[
				['action', 'getaffiliate_getcontactdetailtypes_getuserslite_getaffiliatenotes_getaffiliatedeals_getoperatorslite_getdealstatuses_getdealinfos_getaffiliateoperatorsuggestions'],
				['affiliateid', affiliateid],
				['dealsincludeinactive', $scope.AffiliateFilter.DealsIncludeInactive],
				['dealsassigneduser', $scope.AffiliateFilter.DealsAssignedUserSelected != null ? $scope.AffiliateFilter.DealsAssignedUserSelected.UserId : null],
				['dealsdealinfo', $scope.AffiliateFilter.DealsDealInfoSelected != null ? $scope.AffiliateFilter.DealsDealInfoSelected.DealInfoID : null],
				['dealsdealstatus', $scope.AffiliateFilter.DealsDealStatusSelected != null ? $scope.AffiliateFilter.DealsDealStatusSelected.DealStatusID : null],
			],
			function (result) {
				if (result.Success) {
					$scope.Affiliate = result.ResponseData.Affiliate;
					$scope.Affiliate.AffiliateManagerList = result.ResponseData.Users;
					$scope.Affiliate.AffiliateManagerList[$scope.Affiliate.AffiliateManagerList.length] = $scope.UnassignedUserObj;

					$scope.Affiliate.Notes = result.ResponseData.AffiliateNotes;
					$scope.Affiliate.Deals = result.ResponseData.AffiliateDeals;
					//$scope.Affiliate.BestDealInfoName = $scope.GetBestDealInfo($scope.Affiliate.Deals);

					$scope.Affiliate.OperatorSuggestions = result.ResponseData.AffiliateOperatorSuggestions;

					$scope.AffiliateFilter.ContactDetailTypes = result.ResponseData.ContactDetailTypes;
					$scope.AffiliateFilter.DealsAssignedUserList = result.ResponseData.Users;
					$scope.AffiliateFilter.DealsDealStatusList = result.ResponseData.DealStatuses;
					$scope.AffiliateFilter.DealsDealInfoList = result.ResponseData.DealInfos;
					$scope.AffiliateFilter.DealsOperatorList = result.ResponseData.Operators;


					$scope.AffiliateViewRefreshCharts();
				} else {
					$scope.Affiliate = null;
					$scope.ShowError(result.Error, 'popup');
				}
			}
		);
	};
	$scope.AffiliateSelectTab = function (tab) {
		$scope.AffiliateFilter.ActiveTab = tab;
		if (tab == 'overview') {
			$scope.AffiliateViewRefreshCharts();
		}
	};

	$scope.AffiliateOperatorSuggestionsUpdate = function () {
		$scope.ApiCall(
			[
				['action', 'getaffiliateoperatorsuggestions'],
				['affiliateid', $scope.Affiliate.AffiliateID],
			],
			function (result) {
				if (result.Success) {
					$scope.Affiliate.OperatorSuggestions = result.ResponseData.AffiliateOperatorSuggestions;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateViewRefreshCharts = function () {
		$timeout(function () {
			if ($scope.Affiliate != null) {
				var confirmedActiveDealCount = $scope.Affiliate.ConfirmedActiveDealsCount;
				var confirmedInactiveDealCount = $scope.Affiliate.ConfirmedInactiveDealsCount;

				$('#affiliate-confirmed-active-vs-confirmed-inactive-chart-container').highcharts({
					chart: {
						type: 'pie',
						margin: [0, 0, -100, 0],
					},
					title: null,
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					legend: {
						layout: 'vertical',
						floating: false,
						enabled: true,
						align: 'left',
						verticalAlign: 'middle',
						labelFormat: '<b>{name}</b><br />{y} ({percentage:.1f}) %',

						
					},
					plotOptions: {
						pie: {
							dataLabels: {
								enabled: false,
								distance: 0,
								format: '<b>{point.name}</b><br />{point.y} ({point.percentage:.1f}) %',
								style: { fontWeight: 'bold', }
							},
							showInLegend: true,
							startAngle: -90,
							endAngle: 90,
							point: {
								events: {
									legendItemClick: function (e) {
										e.target.options.events.click();
										return false;
									}
								}
							}
						},
					},
					series: [{
						name: 'Deals',
						colorByPoint: true,
						data: [
							{
								name: 'Confirmed - Active',
								y: confirmedActiveDealCount,
								color: '#090',
								events: {
									click: function (event) {
										$scope.AffiliateFilter.DealsDealStatusList.forEach(function (DealStatusObj, index, array) {
											if (DealStatusObj.DealStatusName == 'Confirmed - Active') {
												$scope.AffiliateFilter.DealsDealStatusSelected = DealStatusObj;
											}
										});
										$scope.AffiliateRefreshDeals();
										$scope.AffiliateSelectTab('deals');
									},
								},
							},
							{
								name: 'Confirmed - Inactive',
								y: confirmedInactiveDealCount,
								color: '#900',
								events: {
									click: function (event) {
										$scope.AffiliateFilter.DealsDealStatusList.forEach(function (DealStatusObj, index, array) {
											if (DealStatusObj.DealStatusName == 'Confirmed - Inactive') {
												$scope.AffiliateFilter.DealsDealStatusSelected = DealStatusObj;
											}
										});
										$scope.AffiliateRefreshDeals();
										$scope.AffiliateSelectTab('deals');
									},
								},
							},
							//{ name: 'Other', y: otherDealCount, color: '#999' }
						],
						cursor: 'pointer',
					}]
				});

			}
		}, 1);
	};
	$scope.AffiliateRefreshDeals = function () {
		$scope.ApiCall(
			[
				['action', 'getaffiliatedeals'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['dealsincludeinactive', $scope.AffiliateFilter.DealsIncludeInactive],
				['dealsassigneduser', $scope.AffiliateFilter.DealsAssignedUserSelected != null ? $scope.AffiliateFilter.DealsAssignedUserSelected.UserId : null],
				['dealsdealinfo', $scope.AffiliateFilter.DealsDealInfoSelected != null ? $scope.AffiliateFilter.DealsDealInfoSelected.DealInfoID : null],
				['dealsdealstatus', $scope.AffiliateFilter.DealsDealStatusSelected != null ? $scope.AffiliateFilter.DealsDealStatusSelected.DealStatusID : null],
			],
			function (result) {
				if (result.Success) {
					$scope.Affiliate.Deals = result.ResponseData.AffiliateDeals;
					//$scope.Affiliate.BestDealInfoName = $scope.GetBestDealInfo($scope.Affiliate.Deals);
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.AffiliateEdit = function (editmode) {
		$scope.AffiliateOriginal = angular.copy($scope.Affiliate);
		if (editmode != null) {
			$scope.AffiliateFilter.EditMode = 'edit';
		}
	};
	$scope.AffiliateEditCancel = function () {
		$scope.isProcessing = true;
		$timeout(function () {
			$scope.Affiliate = angular.copy($scope.AffiliateOriginal);
			$scope.AffiliateFilter.EditMode = 'none';
			$scope.isProcessing = false;
		}, 10);
	};



	$scope.AffiliateEditUpdate = function () {
		var contactDetailInfo = '';
		for (var c = 0; c < $scope.Affiliate.ContactDetails.length; c++) {
			var thisContactDetail = $scope.Affiliate.ContactDetails[c];
			contactDetailInfo += (contactDetailInfo != '' ? '*' : '') + thisContactDetail.ContactDetailID + ',' + thisContactDetail.ContactType.ContactDetailTypeID + ',' + thisContactDetail.ContactDetailText + ',' + thisContactDetail.IsActive + ',' + thisContactDetail.IsPrimary;
		}
		var urlInfo = '';
		for (var c = 0; c < $scope.Affiliate.URLs.length; c++) {
			var thisUrl = $scope.Affiliate.URLs[c];
			urlInfo += (urlInfo != '' ? '*' : '') + thisUrl.UrlID + ',' + thisUrl.IsActive + ',' + thisUrl.IsPrimary + ',' + thisUrl.PageRank + ',' + thisUrl.Url;
		}
		var countryids = '';
		for (var c = 0; c < $scope.Affiliate.Countries.length; c++) {
			if ($scope.Affiliate.Countries[c].IsSelected) {
				countryids += (countryids != '' ? ',' : '') + $scope.Affiliate.Countries[c].CountryID;
			}
		}
		var affiliatetypeids = '';
		for (var c = 0; c < $scope.Affiliate.AffiliateTypes.length; c++) {
			if ($scope.Affiliate.AffiliateTypes[c].IsSelected) {
				affiliatetypeids += (affiliatetypeids != '' ? ',' : '') + $scope.Affiliate.AffiliateTypes[c].AffiliateTypeID;
			}
		}
		var dealtypeids = '';
		for (var c = 0; c < $scope.Affiliate.DealTypes.length; c++) {
			if ($scope.Affiliate.DealTypes[c].IsSelected) {
				dealtypeids += (dealtypeids != '' ? ',' : '') + $scope.Affiliate.DealTypes[c].DealTypeID;
			}
		}
		var marketingtypeids = '';
		for (var c = 0; c < $scope.Affiliate.MarketingTypes.length; c++) {
			if ($scope.Affiliate.MarketingTypes[c].IsSelected) {
				marketingtypeids += (marketingtypeids != '' ? ',' : '') + $scope.Affiliate.MarketingTypes[c].MarketingTypeID;
			}
		}

		if (countryids == '') {
			$scope.ShowError('You must select at least one country');
		} else if (affiliatetypeids == '') {
			$scope.ShowError('You must select at least one affiliate type');
		} else {
			$scope.ApiCall(
				[
					['action', 'updateaffiliateprofile_updateaffiliatecontactdetails_updateaffiliateurls_updateaffiliatecountries_updateaffiliateaffiliatetypes_updateaffiliatedealtypes_updateaffiliatemarketingtypes'],
					['affiliateid', $scope.Affiliate.AffiliateID],

					['affiliatename', $scope.Affiliate.AffiliateName],
					['contactname', $scope.Affiliate.ContactName],
					['affmanageruserid', $scope.Affiliate.AffManagerUser.UserId],
					['affsize', $scope.Affiliate.AffiliateSize],
					['allowcontact', $scope.Affiliate.AllowContact],
					['maxmail', $scope.Affiliate.MaxMail],
					['isactive', $scope.Affiliate.IsActive],
					['comment', $scope.Affiliate.Comment],

					['contactdetailinfo', contactDetailInfo],
					['urlinfo', urlInfo],
					['countryids', countryids],
					['affiliatetypeids', affiliatetypeids],
					['dealtypeids', dealtypeids],
					['marketingtypeids', marketingtypeids],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate updated');
						//$scope.AffiliateFilter.EditMode = 'none';
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};




	$scope.AffiliateEditUpdateProfile = function () {
		$scope.ApiCall(
			[
				['action', 'updateaffiliateprofile'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['affiliatename', $scope.Affiliate.AffiliateName],
				['contactname', $scope.Affiliate.ContactName],
				['affmanageruserid', $scope.Affiliate.AffManagerUser.UserId],
				['affsize', $scope.Affiliate.AffiliateSize],
				['allowcontact', $scope.Affiliate.AllowContact],
				['maxmail', $scope.Affiliate.MaxMail],
				['isactive', $scope.Affiliate.IsActive],
				['comment', $scope.Affiliate.Comment],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate profile updated');
					//$scope.AffiliateFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.AffiliateEditSetPrimaryContactDetail = function (contactDetail) {
		angular.forEach($scope.Affiliate.ContactDetails, function (contactDetail) {
			contactDetail.IsPrimary = false;
		});
		contactDetail.IsPrimary = true;
		$scope.Affiliate.PrimaryContactDetail = contactDetail;
	};
	$scope.AffiliateEditUpdateContactDetails = function () {
		var contactDetailInfo = '';
		for (var c = 0; c < $scope.Affiliate.ContactDetails.length; c++) {
			var thisContactDetail = $scope.Affiliate.ContactDetails[c];
			contactDetailInfo += (contactDetailInfo != '' ? '*' : '') + thisContactDetail.ContactDetailID + ',' + thisContactDetail.ContactType.ContactDetailTypeID + ',' + thisContactDetail.ContactDetailText + ',' + thisContactDetail.IsActive + ',' + thisContactDetail.IsPrimary;
		}
		$scope.ApiCall(
			[
				['action', 'updateaffiliatecontactdetails'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['contactdetailinfo', contactDetailInfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate Contact Details Updated');
					//$scope.AffiliateFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateEditAddContactDetail = function () {
		if ($scope.AffiliateFilter.ContactDetailNewText == undefined || $scope.AffiliateFilter.ContactDetailNewText == '' || $scope.AffiliateFilter.ContactDetailNewType == undefined || $scope.AffiliateFilter.ContactDetailNewType == null) {
			$scope.ShowError('No contact detail defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addaffiliatecontactdetail'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['contactdetailinfo', 'null,' + $scope.AffiliateFilter.ContactDetailNewType.ContactDetailTypeID + ',' + $scope.AffiliateFilter.ContactDetailNewText + ',true,false'],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate contact detail added');
						$scope.AffiliateFilter.ContactDetailNewText = undefined;
						$scope.AffiliateFilter.ContactDetailNewType = undefined;
						$scope.Affiliate.ContactDetails = result.ResponseData.AffiliateContactDetails;
						$scope.AffiliateEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	$scope.AffiliateEditRemoveContactDetail = function (contactDetail) {
		if (contactDetail.IsPrimary) {
			$scope.ShowError('Cannot remove primary contact detail');
		} else {
			$scope.ApiCall(
				[
					['action', 'removeaffiliatecontactdetail'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['contactdetailid', contactDetail.ContactDetailID],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate contact detail removed');
						$scope.Affiliate.ContactDetails = result.ResponseData.AffiliateContactDetails;
						$scope.AffiliateEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}

	$scope.AffiliateEditSetPrimaryUrl = function (url) {
		angular.forEach($scope.Affiliate.URLs, function (url) {
			url.IsPrimary = false;
		});
		url.IsPrimary = true;
		$scope.Affiliate.PrimaryURL = url;
	};
	$scope.AffiliateEditUpdateUrls = function () {
		var urlInfo = '';
		for (var c = 0; c < $scope.Affiliate.URLs.length; c++) {
			var thisUrl = $scope.Affiliate.URLs[c];
			urlInfo += (urlInfo != '' ? '*' : '') + thisUrl.UrlID + ',' + thisUrl.IsActive + ',' + thisUrl.IsPrimary + ',' + thisUrl.PageRank + ',' + thisUrl.Url;
		}
		$scope.ApiCall(
			[
				['action', 'updateaffiliateurls'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['urlinfo', urlInfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate URLs updated');
					//$scope.AffiliateFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateEditAddUrl = function () {
		if ($scope.AffiliateFilter.UrlNewText == undefined || $scope.AffiliateFilter.UrlNewText == '') {
			$scope.ShowError('No URL defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addaffiliateurl'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['urlinfo', 'null,true,false,null,' + $scope.AffiliateFilter.UrlNewText],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate URL Added');
						$scope.AffiliateFilter.UrlNewText = undefined;
						$scope.Affiliate.URLs = result.ResponseData.AffiliateUrls;
						$scope.AffiliateEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	$scope.AffiliateEditRemoveUrl = function (url) {
		if (url.IsPrimary) {
			$scope.ShowError('Cannot remove primary URL');
		} else {
			$scope.ApiCall(
				[
					['action', 'removeaffiliateurl'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['urlid', url.UrlID],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate URL Removed');
						$scope.Affiliate.URLs = result.ResponseData.AffiliateUrls;
						$scope.AffiliateEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.AffiliateEditUpdateCountries = function () {
		var countryids = '';
		for (var c = 0; c < $scope.Affiliate.Countries.length; c++) {
			if ($scope.Affiliate.Countries[c].IsSelected) {
				countryids += (countryids != '' ? ',' : '') + $scope.Affiliate.Countries[c].CountryID;
			}
		}
		if (countryids == '') {
			$scope.ShowError('You must select at least one country');
		} else {
			$scope.ApiCall(
				[
					['action', 'updateaffiliatecountries'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['countryids', countryids],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate Countries Updated');
						//$scope.AffiliateFilter.EditMode = 'none';
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};
	$scope.AffiliateEditUpdateAffiliateTypes = function () {
		var affiliatetypeids = '';
		for (var c = 0; c < $scope.Affiliate.AffiliateTypes.length; c++) {
			if ($scope.Affiliate.AffiliateTypes[c].IsSelected) {
				affiliatetypeids += (affiliatetypeids != '' ? ',' : '') + $scope.Affiliate.AffiliateTypes[c].AffiliateTypeID;
			}
		}
		if (affiliatetypeids == '') {
			$scope.ShowError('You must select at least one affiliate type');
		} else {
			$scope.ApiCall(
				[
					['action', 'updateaffiliateaffiliatetypes'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['affiliatetypeids', affiliatetypeids],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate Types Updated');
						//$scope.AffiliateFilter.EditMode = 'none';
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};
	$scope.AffiliateEditUpdateDealTypes = function () {
		var dealtypeids = '';
		for (var c = 0; c < $scope.Affiliate.DealTypes.length; c++) {
			if ($scope.Affiliate.DealTypes[c].IsSelected) {
				dealtypeids += (dealtypeids != '' ? ',' : '') + $scope.Affiliate.DealTypes[c].DealTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateaffiliatedealtypes'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['dealtypeids', dealtypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate Deal Types Updated');
					//$scope.AffiliateFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateEditUpdateMarketingTypes = function () {
		var marketingtypeids = '';
		for (var c = 0; c < $scope.Affiliate.MarketingTypes.length; c++) {
			if ($scope.Affiliate.MarketingTypes[c].IsSelected) {
				marketingtypeids += (marketingtypeids != '' ? ',' : '') + $scope.Affiliate.MarketingTypes[c].MarketingTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateaffiliatemarketingtypes'],
				['affiliateid', $scope.Affiliate.AffiliateID],
				['marketingtypeids', marketingtypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate Marketing Types Updated');
					//$scope.AffiliateFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.AffiliateDealEditToggle = function (affDeal) {
		if (affDeal.EditModeOn) {
			affDeal.DealText = affDeal.BackupDealText;
			affDeal.AssignedUser = affDeal.BackupAssignedUser;
			affDeal.DealType = affDeal.BackupDealType;
			affDeal.DealStatus = affDeal.BackupDealStatus;
			affDeal.IsActive = affDeal.BackupIsActive;
		} else {
			affDeal.BackupDealText = affDeal.DealText;
			affDeal.BackupAssignedUser = affDeal.AssignedUser;
			affDeal.BackupDealType = affDeal.DealType;
			affDeal.BackupDealStatus = affDeal.DealStatus;
			affDeal.BackupIsActive = affDeal.IsActive;
		}
		affDeal.EditModeOn = !affDeal.EditModeOn;
	};
	$scope.AffiliateDealEditUpdate = function (affDeal, index) {
		$scope.ApiCall(
			[
				['action', 'updatedeal'],
				['dealid', affDeal.DealID],
				['assigneduserid', affDeal.AssignedUser.UserId],
				['dealstatusid', affDeal.DealStatus.DealStatusID],
				['dealtext', affDeal.DealText],
				['dealinfoid', affDeal.DealInfo.DealInfoID],
				['isactive', affDeal.IsActive],
				['operatorid', affDeal.Operator.OperatorID],
				['affiliateid', $scope.Affiliate.AffiliateID],
			],
			function (result) {
				if (result.Success) {
					if (affDeal.DealStatus.DealStatusName == 'Confirmed - Active') {
						$scope.Affiliate.ConfirmedActiveDealsCount++;
					}
					if (affDeal.BackupDealStatus.DealStatusName == 'Confirmed - Active') {
						$scope.Affiliate.ConfirmedActiveDealsCount--;
					}
					if (affDeal.DealStatus.DealStatusName == 'Confirmed - Inactive') {
						$scope.Affiliate.ConfirmedInactiveDealsCount++;
					}
					if (affDeal.BackupDealStatus.DealStatusName == 'Confirmed - Inactive') {
						$scope.Affiliate.ConfirmedInactiveDealsCount--;
					}

					$scope.Affiliate.Deals[index] = result.ResponseData.UpdatedDeal;
					$scope.Affiliate.Deals[index].IsSelected = true;
					$scope.Affiliate.Deals[index].EditModeOn = false;

					$scope.AffiliateViewRefreshCharts();
					$scope.ShowMessage('Deal updated');
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateDealToggleNew = function () {
		$scope.AffiliateFilter.DealsNewDealStatus = null;
		$scope.AffiliateFilter.DealsNewDealText = '';
		$scope.AffiliateFilter.DealsNewDealInfo = null;
		$scope.AffiliateFilter.DealsNewOperator = null;
		$scope.AffiliateFilter.DealsNewDealModeOn = !$scope.AffiliateFilter.DealsNewDealModeOn;
	};
	$scope.AffiliateDealNew = function () {
		if ($scope.AffiliateFilter.DealsNewOperator == null) {
			$scope.ShowError('No Operator selected', 'popup');
		} else if ($scope.AffiliateFilter.DealsNewDealInfo == null) {
			$scope.ShowError('No deal info selected', 'popup');
		} else if ($scope.AffiliateFilter.DealsNewDealStatus == null) {
			$scope.ShowError('No deal status selected', 'popup');
		}
		else {
			$scope.ApiCall(
				[
					['action', 'adddeal'],
					['dealstatusid', $scope.AffiliateFilter.DealsNewDealStatus.DealStatusID],
					['dealtext', $scope.AffiliateFilter.DealsNewDealText],
					['dealinfoid', $scope.AffiliateFilter.DealsNewDealInfo.DealInfoID],
					['operatorid', $scope.AffiliateFilter.DealsNewOperator.OperatorID],
					['affiliateid', $scope.Affiliate.AffiliateID],
				],
				function (result) {
					if (result.Success) {
						$scope.Affiliate.Deals[$scope.Affiliate.Deals.length] = result.ResponseData.NewDeal;

						if ($scope.AffiliateFilter.DealsNewDealStatus.DealStatusName == 'Confirmed - Active') {
							$scope.Affiliate.ConfirmedActiveDealsCount++;
						}
						if ($scope.AffiliateFilter.DealsNewDealStatus.DealStatusName == 'Confirmed - Inactive') {
							$scope.Affiliate.ConfirmedInactiveDealsCount++;
						}

						$scope.AffiliateDealToggleNew();

						$scope.AffiliateViewRefreshCharts();
						$scope.AffiliateOperatorSuggestionsUpdate();

						//$scope.Affiliate.BestDealInfoName = $scope.GetBestDealInfo($scope.Affiliate.Deals);
						$scope.ShowMessage('Deal added');
					} else {
						$scope.ShowError(result.Error, 'popup');
					}
				}
			);
		}
	};
	
	$scope.AffiliateDoShowNote = function (affNote) {
		if ($scope.AffiliateFilter.NotesShowOlderNotes) {
			return true;
		}
		return $scope.TimestampToJsDate(affNote.Modified) > new Date(Date.now() - (1000 * 60 * 60 * 24 * 365 * 2));
	};
	$scope.AffiliateNoteEditToggle = function (affNote) {
		if (affNote.IsSelected) {
			affNote.NoteText = affNote.Backup;
		} else {
			affNote.Backup = affNote.NoteText;
		}
		affNote.IsSelected = !affNote.IsSelected;
	};
	$scope.AffiliateNoteEditUpdate = function (affNote) {
		$scope.AffiliateFilter.NotesNewNoteText
		if (affNote.NoteText == '') {
			$scope.ShowError('Note text is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'updateaffiliatenote'],
					['noteid', affNote.NoteID],
					['notetext', affNote.NoteText],
				],
				function (result) {
					if (result.Success) {
						affNote.IsSelected = false;
						$scope.ShowMessage('Note updated');
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};
	$scope.AffiliateNoteRemove = function (affNote) {
		$scope.ApiCall(
					[
				['action', 'removeaffiliatenote'],
				['noteid', affNote.NoteID],
					],
			function (result) {
				if (result.Success) {
					$scope.Affiliate.Notes.forEach(function (affNoteObj, index, array) {
						if (affNoteObj.NoteID === affNote.NoteID) {
							$scope.Affiliate.Notes.remove(index);
						}
					});
					$scope.ShowMessage('Note removed');
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.AffiliateNoteToggleNew = function () {
		$scope.AffiliateFilter.NotesNewNoteText = '';
		$scope.AffiliateFilter.NotesNewNoteModeOn = !$scope.AffiliateFilter.NotesNewNoteModeOn;
	};
	$scope.AffiliateNoteNew = function () {
		if ($scope.AffiliateFilter.NotesNewNoteText == '') {
			$scope.ShowError('No note defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addaffiliatenote'],
					['affiliateid', $scope.Affiliate.AffiliateID],
					['notetext', $scope.AffiliateFilter.NotesNewNoteText],
				],
				function (result) {
					if (result.Success) {

						$scope.Affiliate.Notes.splice(0, 0, result.ResponseData.NewAffiliateNote);

						$scope.AffiliateFilter.NotesNewNoteText = '';
						$scope.ShowMessage('Note added');
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	/*END AFFILIATES*/


	/*BEGIN OPERATORS*/
	$scope.OperatorsInit = function (/*[viewsignupurls]*/) {
		var viewSignupUrls = arguments.length > 0 && arguments[0] == 'signuplinks';
		var quickSearchStr = arguments.length > 0 && arguments[0] != 'signuplinks' ? arguments[0] : null;
		$scope.ApiCall(
			[
				['action', 'getcountries_getlanguages_getaffiliatetypes_getdealtypes_getsoftwaretypes_getlicencetypes_getpaymenttypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.FilterOperators = {
						ViewAffiliateSignupUrls: false,
						OperatorID: '',
						QuickSearch: null,
						SearchStr: quickSearchStr,
						OperatorStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], OperatorStatusSelected: null,
						CountryFilterOn: false, CountryList: result.ResponseData.Countries, CountryOptions: [{ ID: 'INCLUDE', Label: 'Targets' }, { ID: 'EXCLUDE', Label: 'Banned from' }], CountryOptionSelected: null,
						LanguageFilterOn: false, LanguageList: result.ResponseData.Languages, LanguageOptions: [{ ID: 'INCLUDE', Label: 'Supports' }, { ID: 'EXCLUDE', Label: 'Does not support' }], LanguageOptionSelected: null,
						AffiliateTypeFilterOn: false, AffiliateTypeList: result.ResponseData.AffiliateTypes, AffiliateTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], AffiliateTypeOptionSelected: null,
						DealTypeFilterOn: false, DealTypeList: result.ResponseData.DealTypes, DealTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], DealTypeOptionSelected: null,
						SoftwareTypeFilterOn: false, SoftwareTypeList: result.ResponseData.SoftwareTypes, SoftwareTypeOptions: [{ ID: 'INCLUDE', Label: 'Uses' }, { ID: 'EXCLUDE', Label: 'Does not use' }], SoftwareTypeOptionSelected: null,
						LicenceTypeFilterOn: false, LicenceTypeList: result.ResponseData.LicenceTypes, LicenceTypeOptions: [{ ID: 'INCLUDE', Label: 'Uses' }, { ID: 'EXCLUDE', Label: 'Does not use' }], LicenceTypeOptionSelected: null,
						PaymentTypeFilterOn: false, PaymentTypeList: result.ResponseData.PaymentTypes, PaymentTypeOptions: [{ ID: 'INCLUDE', Label: 'Uses' }, { ID: 'EXCLUDE', Label: 'Does not use' }], PaymentTypeOptionSelected: null,
						Pager: CreatePagerObj(),
						Errors: new ErrorDictionary()
					};
					$scope.FilterOperators.OperatorStatusSelected = $scope.FilterOperators.OperatorStatusList[0];
					$scope.FilterOperators.CountryOptionSelected = $scope.FilterOperators.CountryOptions[0];
					$scope.FilterOperators.LanguageOptionSelected = $scope.FilterOperators.LanguageOptions[0];
					$scope.FilterOperators.AffiliateTypeOptionSelected = $scope.FilterOperators.AffiliateTypeOptions[0];
					$scope.FilterOperators.DealTypeOptionSelected = $scope.FilterOperators.DealTypeOptions[0];
					$scope.FilterOperators.SoftwareTypeOptionSelected = $scope.FilterOperators.SoftwareTypeOptions[0];
					$scope.FilterOperators.LicenceTypeOptionSelected = $scope.FilterOperators.LicenceTypeOptions[0];
					$scope.FilterOperators.PaymentTypeOptionSelected = $scope.FilterOperators.PaymentTypeOptions[0];
					$scope.FilterOperators.ViewAffiliateSignupUrls = viewSignupUrls;

					$scope.OperatorsSearchSubmit($scope.FilterOperators.Pager.Info.Offset);
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};

	$scope.OperatorQuickSearchEnter = function (keyEvent, param) {
		if (keyEvent.which === 13) {
			$scope.OperatorQuickSearch(param);
		}
	};
	$scope.OperatorQuickSearch = function (param) {
		if (isNaN(param)) {
			$scope.RenderSection('operators', param);
		} else {
			$scope.RenderSection('operator', param);
		}
	};

	$scope.OperatorsFilterSearchEnter = function (keyEvent) {
		if (keyEvent.which === 13)
		{
			$scope.OperatorsSearchSubmit();
		}
	};

	$scope.OperatorsSearchSubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterOperators.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;

		var country_ids = '';
		for (var c = 0; c < $scope.FilterOperators.CountryList.length; c++) {
			if ($scope.FilterOperators.CountryList[c].IsSelected) {
				country_ids += (country_ids != '' ? ',' : '') + $scope.FilterOperators.CountryList[c].CountryID;
			}
		}
		var language_ids = '';
		for (var c = 0; c < $scope.FilterOperators.LanguageList.length; c++) {
			if ($scope.FilterOperators.LanguageList[c].IsSelected) {
				language_ids += (language_ids != '' ? ',' : '') + $scope.FilterOperators.LanguageList[c].LanguageID;
			}
		}
		var affiliatetype_ids = '';
		for (var c = 0; c < $scope.FilterOperators.AffiliateTypeList.length; c++) {
			if ($scope.FilterOperators.AffiliateTypeList[c].IsSelected) {
				affiliatetype_ids += (affiliatetype_ids != '' ? ',' : '') + $scope.FilterOperators.AffiliateTypeList[c].AffiliateTypeID;
			}
		}
		var dealtype_ids = '';
		for (var c = 0; c < $scope.FilterOperators.DealTypeList.length; c++) {
			if ($scope.FilterOperators.DealTypeList[c].IsSelected) {
				dealtype_ids += (dealtype_ids != '' ? ',' : '') + $scope.FilterOperators.DealTypeList[c].DealTypeID;
			}
		}
		var softwaretype_ids = '';
		for (var c = 0; c < $scope.FilterOperators.SoftwareTypeList.length; c++) {
			if ($scope.FilterOperators.SoftwareTypeList[c].IsSelected) {
				softwaretype_ids += (softwaretype_ids != '' ? ',' : '') + $scope.FilterOperators.SoftwareTypeList[c].SoftwareTypeID;
			}
		}
		var licencetype_ids = '';
		for (var c = 0; c < $scope.FilterOperators.LicenceTypeList.length; c++) {
			if ($scope.FilterOperators.LicenceTypeList[c].IsSelected) {
				licencetype_ids += (licencetype_ids != '' ? ',' : '') + $scope.FilterOperators.LicenceTypeList[c].LicenceTypeID;
			}
		}
		var paymenttype_ids = '';
		for (var c = 0; c < $scope.FilterOperators.PaymentTypeList.length; c++) {
			if ($scope.FilterOperators.PaymentTypeList[c].IsSelected) {
				paymenttype_ids += (paymenttype_ids != '' ? ',' : '') + $scope.FilterOperators.PaymentTypeList[c].PaymentTypeID;
			}
		}

		$scope.ApiCall(
			[
				['action', 'getoperators'],
				['operatorid', $scope.FilterOperators.OperatorID],
				['searchstr', $scope.FilterOperators.SearchStr],
				['operatorstatustype', $scope.FilterOperators.OperatorStatusSelected.ID],
				['countrytypeoption', $scope.FilterOperators.CountryOptionSelected.ID],
				['countryids', country_ids],
				['languagetypeoption', $scope.FilterOperators.LanguageOptionSelected.ID],
				['languageids', language_ids],
				['affiliatetypeoption', $scope.FilterOperators.AffiliateTypeOptionSelected.ID],
				['affiliatetypeids', affiliatetype_ids],
				['dealtypeoption', $scope.FilterOperators.DealTypeOptionSelected.ID],
				['dealtypeids', dealtype_ids],
				['softwaretypeoption', $scope.FilterOperators.SoftwareTypeOptionSelected.ID],
				['softwaretypeids', softwaretype_ids],
				['licencetypeoption', $scope.FilterOperators.LicenceTypeOptionSelected.ID],
				['licencetypeids', licencetype_ids],
				['paymenttypeoption', $scope.FilterOperators.PaymentTypeOptionSelected.ID],
				['paymenttypeids', paymenttype_ids],
				['offset', $scope.FilterOperators.Pager.Info.Offset],
				['count', $scope.FilterOperators.Pager.Info.Count]],
			function (result) {
				if (result.Success) {
					$scope.FilterOperators.Pager.SetPagerInfo(result.ResponseData.OperatorsCount, $scope.OperatorsSearchSubmit);
					$scope.Operators = result.ResponseData.Operators;
					$scope.isProcessing = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};

	$scope.AddOperatorInit = function () {

		$scope.ApiCall(
			[
				['action', 'getcontactdetailtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.NewOperator = {
						OperatorName: '',
						AffiliateSignupUrl: '',
						WebsiteReviewUrl: '',
						ContactDetailTypes: result.ResponseData.ContactDetailTypes,
						PrimaryContactDetailType: null,
						PrimaryContactDetailText: '',
						Errors: new ErrorDictionary()
					};
					$scope.NewOperator.PrimaryContactDetailType = $scope.NewOperator.ContactDetailTypes[0];
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);				
	};
	$scope.AddOperatorSubmit = function () {
		$scope.ShowMessage(null);
		$scope.NewOperator.Errors = new ErrorDictionary();
		$scope.ValidateRequired($scope.NewOperator.OperatorName, $scope.NewOperator.Errors, 'OperatorName');
		//$scope.ValidateRequired($scope.NewOperator.AffiliateSignupUrl, $scope.NewOperator.Errors, 'AffiliateSignupUrl');
		$scope.ValidateRequired($scope.NewOperator.PrimaryContactDetailText, $scope.NewOperator.Errors, 'PrimaryContactDetailText');
		if (!$scope.NewOperator.Errors.HasErrors()) {
			$scope.ApiCall(
				[
					['action', 'newoperator'],
					['operatorname', $scope.NewOperator.OperatorName],
					['affiliatesignupurl', $scope.NewOperator.AffiliateSignupUrl],
					['websitereviewurl', $scope.NewOperator.WebsiteReviewUrl],
					['primarycontactdetailtype', $scope.NewOperator.PrimaryContactDetailType.ContactDetailTypeID],
					['primarycontactdetailtext', $scope.NewOperator.PrimaryContactDetailText]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('New operator created');
						$scope.NewOperator = null;
						$scope.RenderSection('operator', result.ResponseData.NewOperatorID);
					} else {
						$scope.NewOperator.Errors.Update('Main', result.Error);
					}
				}
			);
		}
	};

	$scope.OperatorViewInit = function (operatorid) {
		$scope.OperatorFilter = {
			ViewContactDetailsModeOn: true,
			EditMode: 'none',
			ActiveTab: 'overview',
			ContactDetailNewText: null,
			ContactDetailNewType: null,
			BrandNewText: null,

			DealsAssignedUserList: [],
			DealsDealStatusList: [],
			DealsDealInfoList: [],
			DealsShowFilter: false,
			DealsIncludeInactive: true,
			DealsAssignedUserSelected: null,
			DealsDealInfoSelected: null,
			DealsDealStatusSelected: null,
			
			NotesShowOlderNotes: false,
			NotesNewNoteModeOn: false,
			NotesNewNoteText: '',
		};

		$scope.ApiCall(
			[
				['action', 'getoperator_getcontactdetailtypes_getuserslite_getoperatornotes_getoperatordeals_getdealstatuses_getdealinfos'],
				['operatorid', operatorid],
				['dealsincludeinactive', $scope.OperatorFilter.DealsIncludeInactive],
				['dealsassigneduser', $scope.OperatorFilter.DealsAssignedUserSelected != null ? $scope.OperatorFilter.DealsAssignedUserSelected.UserId : null],
				['dealsdealinfo', $scope.OperatorFilter.DealsDealInfoSelected != null ? $scope.OperatorFilter.DealsDealInfoSelected.DealInfoID : null],
				['dealsdealstatus', $scope.OperatorFilter.DealsDealStatusSelected != null ? $scope.OperatorFilter.DealsDealStatusSelected.DealStatusID : null],
			],
			function (result) {
				if (result.Success) {
					$scope.Operator = result.ResponseData.Operator;
					$scope.Operator.Notes = result.ResponseData.OperatorNotes;
					$scope.Operator.Deals = result.ResponseData.OperatorDeals;
					for (var c = 0; c < $scope.Operator.Deals.length; c++) {
						$scope.Operator.Deals[c].IsSelected = true;
					}
					$scope.Operator.BestDealInfoName = $scope.GetBestDealInfo($scope.Operator.Deals);

					$scope.OperatorFilter.ContactDetailTypes = result.ResponseData.ContactDetailTypes;
					$scope.OperatorFilter.DealsAssignedUserList = result.ResponseData.Users;
					$scope.OperatorFilter.DealsDealStatusList = result.ResponseData.DealStatuses;
					$scope.OperatorFilter.DealsDealInfoList = result.ResponseData.DealInfos;

					$scope.OperatorViewRefreshCharts();
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}

	$scope.OperatorSelectTab = function (tab) {
		$scope.OperatorFilter.ActiveTab = tab;
		if (tab == 'overview') {
			$scope.OperatorViewRefreshCharts();
		}
	};
	$scope.OperatorViewRefreshCharts = function () {
		$timeout(function () {
			if ($scope.Operator != null) {
				if ($scope.Operator.Deals != null) {

					var dealStatusesArr = [];
					var dealInfosArr = [];

					$scope.Operator.Deals.forEach(function (opDealObj, index, array) {

						$scope.OperatorFilter.DealsDealStatusList.forEach(function (dealStatusObj, index, array) {
							if (opDealObj.DealStatus.DealStatusID == dealStatusObj.DealStatusID) {
								var found;
								dealStatusesArr.some(function (obj) {
									if (obj[0] === dealStatusObj.DealStatusName) {
										found = obj;
									}
								});
								if (found) {
									found[1]++;
								} else {
									dealStatusesArr.push([dealStatusObj.DealStatusName, 1]);
								}
							}
						});
						$scope.OperatorFilter.DealsDealInfoList.forEach(function (dealInfoObj, index, array) {
							if (opDealObj.DealInfo.DealInfoID == dealInfoObj.DealInfoID) {
								var found;
								dealInfosArr.some(function (obj) {
									if (obj[0] === dealInfoObj.DealInfoName) {
										found = obj;
									}
								});
								if (found) {
									found[1]++;
								} else {
									dealInfosArr.push([dealInfoObj.DealInfoName, 1]);
								}
							}
						});

					});
					$('#operator-deal-statuses-chart-container').highcharts({
						chart: {
							type: 'bar',
							height: 200,
							spacing: [0, 0, 0, 0]
						},
						title: null,
						subtitle: null,
						xAxis: { type: 'category', },
						yAxis: { min: 0, title: null },
						legend: { enabled: false },
						series: [{
							name: 'Deals',
							data: dealStatusesArr,
							colorByPoint: true,

						}]
					});
					$('#operator-deal-infos-chart-container').highcharts({
						chart: {
							type: 'bar',
							height: 200,
							spacing: [0, 0, 0, 0]
						},
						title: null,
						subtitle: null,
						xAxis: { type: 'category', },
						yAxis: { min: 0, title: null },
						legend: { enabled: false },

						series: [{
							name: 'Deals',
							data: dealInfosArr,
							colorByPoint: true,
						}]
					});

					$scope.test = {
						name: 'Deals',
						data: dealInfosArr,
						colorByPoint: true,
					};

				}
			}
		}, 1);
	};


	$scope.OperatorRefreshDeals = function () {
		$scope.ApiCall(
			[
				['action', 'getoperatordeals'],
				['operatorid', $scope.Operator.OperatorID],
				['dealsincludeinactive', $scope.OperatorFilter.DealsIncludeInactive],
				['dealsassigneduser', $scope.OperatorFilter.DealsAssignedUserSelected != null ? $scope.OperatorFilter.DealsAssignedUserSelected.UserId : null],
				['dealsdealinfo', $scope.OperatorFilter.DealsDealInfoSelected != null ? $scope.OperatorFilter.DealsDealInfoSelected.DealInfoID : null],
				['dealsdealstatus', $scope.OperatorFilter.DealsDealStatusSelected != null ? $scope.OperatorFilter.DealsDealStatusSelected.DealStatusID : null],
			],
			function (result) {
				if (result.Success) {
					$scope.Operator.Deals = result.ResponseData.OperatorDeals;
					$scope.Operator.BestDealInfoName = $scope.GetBestDealInfo($scope.Operator.Deals);
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.OperatorEdit = function (editmode) {
		$scope.OperatorOriginal = angular.copy($scope.Operator);
		if (editmode != null) {
			$scope.OperatorFilter.EditMode = editmode;
		}
	};
	$scope.OperatorEditCancel = function () {
		$scope.isProcessing = true;
		$timeout(function () {
			$scope.Operator = angular.copy($scope.OperatorOriginal);
			$scope.OperatorFilter.EditMode = 'none';
			$scope.isProcessing = false;
		}, 10);
	};


	$scope.OperatorEditUpdate = function () {
		var contactDetailInfo = '';
		for (var c = 0; c < $scope.Operator.ContactDetails.length; c++) {
			var thisContactDetail = $scope.Operator.ContactDetails[c];
			contactDetailInfo += (contactDetailInfo != '' ? '*' : '') + thisContactDetail.ContactDetailID + ',' + thisContactDetail.ContactType.ContactDetailTypeID + ',' + thisContactDetail.ContactDetailText + ',' + thisContactDetail.IsActive + ',' + thisContactDetail.IsPrimary;
		}
		var countryinfo = '';
		for (var c = 0; c < $scope.Operator.Countries.length; c++) {
			if ($scope.Operator.Countries[c].Status == -1) {
				$scope.Operator.Countries[c].IsSelected = false;
			} else {
				$scope.Operator.Countries[c].IsSelected = true;
				countryinfo += (countryinfo != '' ? '*' : '') + $scope.Operator.Countries[c].CountryID + ',' + $scope.Operator.Countries[c].Status;
			}
		}
		var languageids = '';
		for (var c = 0; c < $scope.Operator.Languages.length; c++) {
			if ($scope.Operator.Languages[c].IsSelected) {
				languageids += (languageids != '' ? ',' : '') + $scope.Operator.Languages[c].LanguageID;
			}
		}
		var affiliatetypeids = '';
		for (var c = 0; c < $scope.Operator.AffiliateTypes.length; c++) {
			if ($scope.Operator.AffiliateTypes[c].IsSelected) {
				affiliatetypeids += (affiliatetypeids != '' ? ',' : '') + $scope.Operator.AffiliateTypes[c].AffiliateTypeID;
			}
		}
		var dealtypeids = '';
		for (var c = 0; c < $scope.Operator.DealTypes.length; c++) {
			if ($scope.Operator.DealTypes[c].IsSelected) {
				dealtypeids += (dealtypeids != '' ? ',' : '') + $scope.Operator.DealTypes[c].DealTypeID;
			}
		}
		var softwaretypeids = '';
		for (var c = 0; c < $scope.Operator.SoftwareTypes.length; c++) {
			if ($scope.Operator.SoftwareTypes[c].IsSelected) {
				softwaretypeids += (softwaretypeids != '' ? ',' : '') + $scope.Operator.SoftwareTypes[c].SoftwareTypeID;
			}
		}
		var licencetypeids = '';
		for (var c = 0; c < $scope.Operator.LicenceTypes.length; c++) {
			if ($scope.Operator.LicenceTypes[c].IsSelected) {
				licencetypeids += (licencetypeids != '' ? ',' : '') + $scope.Operator.LicenceTypes[c].LicenceTypeID;
			}
		}
		var paymenttypeids = '';
		for (var c = 0; c < $scope.Operator.PaymentTypes.length; c++) {
			if ($scope.Operator.PaymentTypes[c].IsSelected) {
				paymenttypeids += (paymenttypeids != '' ? ',' : '') + $scope.Operator.PaymentTypes[c].PaymentTypeID;
			}
		}
		var brandInfo = '';
		for (var c = 0; c < $scope.Operator.Brands.length; c++) {
			var thisBrand = $scope.Operator.Brands[c];
			brandInfo += (brandInfo != '' ? '*' : '') + thisBrand.BrandID + ',' + thisBrand.BrandName;
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorprofile_updateoperatorcontactdetails_updateoperatorcountries_updateoperatorlanguages_updateoperatoraffiliatetypes_updateoperatordealtypes_updateoperatorsoftwaretypes_updateoperatorlicencetypes_updateoperatorpaymenttypes_updateoperatorbrands'],
				['operatorid', $scope.Operator.OperatorID],
				['operatorname', $scope.Operator.OperatorName],
				['affiliatesignupurl', $scope.Operator.AffiliateSignupUrl],
				['websitereviewurl', $scope.Operator.WebsiteReviewUrl],
				['isactive', $scope.Operator.IsActive],
				['contactdetailinfo', contactDetailInfo],
				['countryinfo', countryinfo],
				['languageids', languageids],
				['affiliatetypeids', affiliatetypeids],
				['dealtypeids', dealtypeids],
				['softwaretypeids', softwaretypeids],
				['licencetypeids', licencetypeids],
				['paymenttypeids', paymenttypeids],
				['brandinfo', brandInfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);

	};

	$scope.OperatorEditUpdateProfile = function () {
		$scope.ApiCall(
			[
				['action', 'updateoperatorprofile'],
				['operatorid', $scope.Operator.OperatorID],
				['operatorname', $scope.Operator.OperatorName],
				['affiliatesignupurl', $scope.Operator.AffiliateSignupUrl],
				['websitereviewurl', $scope.Operator.WebsiteReviewUrl],
				['isactive', $scope.Operator.IsActive],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator profile updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.OperatorEditSetPrimaryContactDetail = function (contactDetail) {
		angular.forEach($scope.Operator.ContactDetails, function (contactDetail) {
			contactDetail.IsPrimary = false;
		});
		contactDetail.IsPrimary = true;
		$scope.Operator.PrimaryContactDetail = contactDetail;
	};
	$scope.OperatorEditUpdateContactDetails = function () {
		var contactDetailInfo = '';
		for (var c = 0; c < $scope.Operator.ContactDetails.length; c++) {
			var thisContactDetail = $scope.Operator.ContactDetails[c];
			contactDetailInfo += (contactDetailInfo != '' ? '*' : '') + thisContactDetail.ContactDetailID + ',' + thisContactDetail.ContactType.ContactDetailTypeID + ',' + thisContactDetail.ContactDetailText + ',' + thisContactDetail.IsActive + ',' + thisContactDetail.IsPrimary;
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorcontactdetails'],
				['operatorid', $scope.Operator.OperatorID],
				['contactdetailinfo', contactDetailInfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator contact details updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditAddContactDetail = function () {
		if ($scope.OperatorFilter.ContactDetailNewText == undefined || $scope.OperatorFilter.ContactDetailNewText == '' || $scope.OperatorFilter.ContactDetailNewType == undefined || $scope.OperatorFilter.ContactDetailNewType == null) {
			$scope.ShowError('No contact detail defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addoperatorcontactdetail'],
					['operatorid', $scope.Operator.OperatorID],
					['contactdetailinfo', 'null,' + $scope.OperatorFilter.ContactDetailNewType.ContactDetailTypeID + ',' + $scope.OperatorFilter.ContactDetailNewText + ',true,false'],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Operator contact detail added');
						$scope.OperatorFilter.ContactDetailNewText = undefined;
						$scope.OperatorFilter.ContactDetailNewType = undefined;
						$scope.Operator.ContactDetails = result.ResponseData.OperatorContactDetails;
						$scope.OperatorEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	$scope.OperatorEditRemoveContactDetail = function (contactDetail) {
		if (contactDetail.IsPrimary) {
			$scope.ShowError('Cannot remove primary contact detail');
		} else {
			$scope.ApiCall(
				[
					['action', 'removeoperatorcontactdetail'],
					['operatorid', $scope.Operator.OperatorID],
					['contactdetailid', contactDetail.ContactDetailID],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Operator contact detail removed');
						$scope.Operator.ContactDetails = result.ResponseData.OperatorContactDetails;
						$scope.OperatorEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	$scope.OperatorToggleFavourite = function () {
		$scope.Operator.IsFavourite = !$scope.Operator.IsFavourite;
		$scope.ApiCall(
			[
				['action', 'updateoperatorfavourite'],
				['operatorid', $scope.Operator.OperatorID],
				['isfavourite', $scope.Operator.IsFavourite],
			],
			function (result) {
				if (result.Success) {
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.OperatorEditUpdateCountries = function () {
		var countryinfo = '';
		for (var c = 0; c < $scope.Operator.Countries.length; c++) {
			if ($scope.Operator.Countries[c].Status == -1) {
				$scope.Operator.Countries[c].IsSelected = false;
			} else {
				$scope.Operator.Countries[c].IsSelected = true;
				countryinfo += (countryinfo != '' ? '*' : '') + $scope.Operator.Countries[c].CountryID + ',' + $scope.Operator.Countries[c].Status;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorcountries'],
				['operatorid', $scope.Operator.OperatorID],
				['countryinfo', countryinfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator Countries Updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateLanguages = function () {
		var languageids = '';
		for (var c = 0; c < $scope.Operator.Languages.length; c++) {
			if ($scope.Operator.Languages[c].IsSelected) {
				languageids += (languageids != '' ? ',' : '') + $scope.Operator.Languages[c].LanguageID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorlanguages'],
				['operatorid', $scope.Operator.OperatorID],
				['languageids', languageids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator languages updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateAffiliateTypes = function () {
		var affiliatetypeids = '';
		for (var c = 0; c < $scope.Operator.AffiliateTypes.length; c++) {
			if ($scope.Operator.AffiliateTypes[c].IsSelected) {
				affiliatetypeids += (affiliatetypeids != '' ? ',' : '') + $scope.Operator.AffiliateTypes[c].AffiliateTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatoraffiliatetypes'],
				['operatorid', $scope.Operator.OperatorID],
				['affiliatetypeids', affiliatetypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator affiliate types updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateDealTypes = function () {
		var dealtypeids = '';
		for (var c = 0; c < $scope.Operator.DealTypes.length; c++) {
			if ($scope.Operator.DealTypes[c].IsSelected) {
				dealtypeids += (dealtypeids != '' ? ',' : '') + $scope.Operator.DealTypes[c].DealTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatordealtypes'],
				['operatorid', $scope.Operator.OperatorID],
				['dealtypeids', dealtypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator deal types updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateSoftwareTypes = function () {
		var softwaretypeids = '';
		for (var c = 0; c < $scope.Operator.SoftwareTypes.length; c++) {
			if ($scope.Operator.SoftwareTypes[c].IsSelected) {
				softwaretypeids += (softwaretypeids != '' ? ',' : '') + $scope.Operator.SoftwareTypes[c].SoftwareTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorsoftwaretypes'],
				['operatorid', $scope.Operator.OperatorID],
				['softwaretypeids', softwaretypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator software types updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateLicenceTypes = function () {
		var licencetypeids = '';
		for (var c = 0; c < $scope.Operator.LicenceTypes.length; c++) {
			if ($scope.Operator.LicenceTypes[c].IsSelected) {
				licencetypeids += (licencetypeids != '' ? ',' : '') + $scope.Operator.LicenceTypes[c].LicenceTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorlicencetypes'],
				['operatorid', $scope.Operator.OperatorID],
				['licencetypeids', licencetypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator licence types updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdatePaymentTypes = function () {
		var paymenttypeids = '';
		for (var c = 0; c < $scope.Operator.PaymentTypes.length; c++) {
			if ($scope.Operator.PaymentTypes[c].IsSelected) {
				paymenttypeids += (paymenttypeids != '' ? ',' : '') + $scope.Operator.PaymentTypes[c].PaymentTypeID;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorpaymenttypes'],
				['operatorid', $scope.Operator.OperatorID],
				['paymenttypeids', paymenttypeids],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator payment types updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditUpdateBrands = function () {
		var brandInfo = '';
		for (var c = 0; c < $scope.Operator.Brands.length; c++) {
			var thisBrand = $scope.Operator.Brands[c];
			brandInfo += (brandInfo != '' ? '*' : '') + thisBrand.BrandID + ',' + thisBrand.BrandName;
		}
		$scope.ApiCall(
			[
				['action', 'updateoperatorbrands'],
				['operatorid', $scope.Operator.OperatorID],
				['brandinfo', brandInfo],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Operator brands updated');
					//$scope.OperatorFilter.EditMode = 'none';
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorEditAddBrand = function () {
		if ($scope.OperatorFilter.BrandNewText == undefined || $scope.OperatorFilter.BrandNewText == '') {
			$scope.ShowError('No brand defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addoperatorbrand'],
					['operatorid', $scope.Operator.OperatorID],
					['brandinfo', 'null,' + $scope.OperatorFilter.BrandNewText],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Operator brand added');
						$scope.OperatorFilter.BrandNewText = undefined;

						$scope.Operator.Brands = result.ResponseData.OperatorBrands;
						
						$scope.OperatorEdit();
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}

	$scope.OperatorDealEditToggle = function (opDeal) {
		if (opDeal.EditModeOn) {
			opDeal.DealText = opDeal.BackupDealText;
			opDeal.AssignedUser = opDeal.BackupAssignedUser;
			opDeal.DealType = opDeal.BackupDealType;
			opDeal.DealStatus = opDeal.BackupDealStatus;
			opDeal.IsActive = opDeal.BackupIsActive;
		} else {
			opDeal.BackupDealText = opDeal.DealText;
			opDeal.BackupAssignedUser = opDeal.AssignedUser;
			opDeal.BackupDealType = opDeal.DealType;
			opDeal.BackupDealStatus = opDeal.DealStatus;
			opDeal.BackupIsActive = opDeal.IsActive;
		}
		opDeal.EditModeOn = !opDeal.EditModeOn;
	};
	$scope.OperatorDealEditUpdate = function (opDeal, index) {
		$scope.ApiCall(
			[
				['action', 'updatedeal'],
				['dealid', opDeal.DealID],
				['assigneduserid', opDeal.AssignedUser.UserId],
				['dealstatusid', opDeal.DealStatus.DealStatusID],
				['dealtext', opDeal.DealText],
				['dealinfoid', opDeal.DealInfo.DealInfoID],
				['isactive', opDeal.IsActive],
				['operatorid', $scope.Operator.OperatorID],
				['affiliateid', opDeal.Affiliate.AffiliateID],
			],
			function (result) {
				if (result.Success) {

					$scope.Operator.Deals[index] = results.ResponseData.UpdatedDeal;
					$scope.Operator.Deals[index].IsSelected = true;
					$scope.Operator.Deals[index].EditModeOn = false;

					$scope.Operator.BestDealInfoName = $scope.GetBestDealInfo($scope.Operator.Deals);
					$scope.ShowMessage('Deal updated');
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};

	$scope.OperatorDoShowNote = function (opNote) {
		if ($scope.OperatorFilter.NotesShowOlderNotes) {
			return true;
		}
		return $scope.TimestampToJsDate(opNote.Modified) > new Date(Date.now() - (1000 * 60 * 60 * 24 * 365 * 2));
	};
	$scope.OperatorNoteEditToggle = function (opNote) {
		if (opNote.IsSelected) {
			opNote.NoteText = opNote.Backup;
		} else {
			opNote.Backup = opNote.NoteText;
		}
		opNote.IsSelected = !opNote.IsSelected;
	};
	$scope.OperatorNoteEditUpdate = function (opNote) {
		$scope.OperatorFilter.NotesNewNoteText
		if (opNote.NoteText == '') {
			$scope.ShowError('Note text is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'updateoperatornote'],
					['noteid', opNote.NoteID],
					['notetext', opNote.NoteText],
				],
				function (result) {
					if (result.Success) {
						opNote.IsSelected = false;
						$scope.ShowMessage('Note updated');
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};
	$scope.OperatorNoteRemove = function (opNote) {
		$scope.ApiCall(
			[
				['action', 'removeoperatornote'],
				['noteid', opNote.NoteID],
			],
			function (result) {
				if (result.Success) {
					$scope.Operator.Notes.forEach(function (opNoteObj, index, array) {
						if (opNoteObj.NoteID === opNote.NoteID) {
							$scope.Operator.Notes.remove(index);
						}
					});
					$scope.ShowMessage('Note removed');
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.OperatorNoteToggleNew = function () {
		$scope.OperatorFilter.NotesNewNoteText = '';
		$scope.OperatorFilter.NotesNewNoteModeOn = !$scope.OperatorFilter.NotesNewNoteModeOn;
	};
	$scope.OperatorNoteNew = function () {
		if ($scope.OperatorFilter.NotesNewNoteText == '') {
			$scope.ShowError('No note defined');
		} else {
			$scope.ApiCall(
				[
					['action', 'addoperatornote'],
					['operatorid', $scope.Operator.OperatorID],
					['notetext', $scope.OperatorFilter.NotesNewNoteText],
				],
				function (result) {
					if (result.Success) {
						$scope.Operator.Notes.splice(0, 0, result.ResponseData.NewOperatorNote);
						$scope.OperatorFilter.NotesNewNoteText = '';
						$scope.ShowMessage('Note added');
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}


	$scope.OperatorSignupLinksInit = function () {

	};

	/*END OPERATORS*/


	/*BEGIN SETTINGS INIT*/
	$scope.SettingsInit = function () {
		var activeTabParam = 'affiliatetypes';
		if (arguments.length > 0 && arguments[0] != null) {
			activeTabParam = arguments[0];
		}
		$scope.Settings = {
			ActiveTab: activeTabParam,
			AffiliateTypes: null,
			NewAffiliateType: '',
			ContactDetailTypes: null,
			NewContactDetailType: '',
			Countries: null,
			NewCountry: '',
			DealTypes: null,
			NewDealType: '' ,
			DealInfos: null,
			NewDealInfo: '',
			MarketingTypes: null,
			NewMarketingType: '',
			SoftwareType: null,
			NewSoftwareType: '',
			LicenceTypes: null,
			NewLicenceType: '',
			Languages: null,
			NewLanguage: '',
			PaymentTypes: null,
			NewPaymentType: '',
		};
		switch ($scope.Settings.ActiveTab) {
			case 'affiliatetypes': $scope.SettingsAffiliateTypesInit(); break;
			case 'contactdetailtypes': $scope.SettingsContactDetailTypesInit(); break;
			case 'countries': $scope.SettingsCountriesInit(); break;
			case 'dealtypes': $scope.SettingsDealTypesInit(); break;
			case 'dealinfos': $scope.SettingsDealInfosInit(); break;
			case 'marketingtypes': $scope.SettingsMarketingTypesInit(); break;
			case 'softwaretypes': $scope.SettingsSoftwareTypesInit(); break;
			case 'licencetypes': $scope.SettingsLicenceTypesInit(); break;
			case 'languages': $scope.SettingsLanguagesInit(); break;
			case 'paymenttypes': $scope.SettingsPaymentTypesInit(); break;
		}
	};

	$scope.SettingsAffiliateTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getaffiliatetypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.AffiliateTypes = result.ResponseData.AffiliateTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsAffiliateTypeEdit = function (arg) {
		arg.BackupValue = arg.AffiliateTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsAffiliateTypeEditCancel = function (arg) {
		arg.AffiliateTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsAffiliateTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updateaffiliatetype'],
				['affiliatetypeid', arg.AffiliateTypeID],
				['affiliatetypename', arg.AffiliateTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Affiliate type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsAffiliateTypeAdd = function () {
		if ($scope.Settings.NewAffiliateType == '') {
			$scope.ShowError('New affiliate type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addaffiliatetype'],
					['affiliatetypename', $scope.Settings.NewAffiliateType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Affiliate type added')
						$scope.Settings.AffiliateTypes[$scope.Settings.AffiliateTypes.length] = result.ResponseData.NewAffiliateType;
						$scope.Settings.NewAffiliateType = '';
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsContactDetailTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getcontactdetailtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.ContactDetailTypes = result.ResponseData.ContactDetailTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsContactDetailTypeEdit = function (arg) {
		arg.BackupValue = arg.ContactDetailTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsContactDetailTypeEditCancel = function (arg) {
		arg.ContactDetailTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsContactDetailTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatecontactdetailtype'],
				['contactdetailtypeid', arg.ContactDetailTypeID],
				['contactdetailtypename', arg.ContactDetailTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Contact detail type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsContactDetailTypeAdd = function () {
		if ($scope.Settings.NewContactDetailType == '') {
			$scope.ShowError('New contact detail type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addcontactdetailtype'],
					['contactdetailtypename', $scope.Settings.NewContactDetailType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Contact detail type added')
						$scope.Settings.ContactDetailTypes[$scope.Settings.ContactDetailTypes.length] = result.ResponseData.NewContactDetailType;
						$scope.Settings.NewContactDetailType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsCountriesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getcountries'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.Countries = result.ResponseData.Countries;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsCountryEdit = function (arg) {
		arg.BackupValue = arg.CountryName;
		arg.IsSelected = true;
	};
	$scope.SettingsCountryEditCancel = function (arg) {
		arg.CountryName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsCountryEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatecountry'],
				['countryid', arg.CountryID],
				['countryname', arg.CountryName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Country Updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsCountryAdd = function () {
		if ($scope.Settings.NewCountry == '') {
			$scope.ShowError('New country is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addcountry'],
					['countryname', $scope.Settings.NewCountry],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Country Added')
						$scope.Settings.Countries[$scope.Settings.Countries.length] = result.ResponseData.NewCountry;
						$scope.Settings.NewCountry = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsDealTypesInit = function(){
		$scope.ApiCall(
			[
				['action', 'getdealtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.DealTypes = result.ResponseData.DealTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsDealTypeEdit = function (arg) {
		arg.BackupValue = arg.DealTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsDealTypeEditCancel = function (arg) {
		arg.DealTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsDealTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatedealtype'],
				['dealtypeid', arg.DealTypeID],
				['dealtypename', arg.DealTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Deal type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsDealTypeAdd = function () {
		if ($scope.Settings.NewDealType == '') {
			$scope.ShowError('New deal type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'adddealtype'],
					['dealtypename', $scope.Settings.NewDealType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Deal type added')
						$scope.Settings.DealTypes[$scope.Settings.DealTypes.length] = result.ResponseData.NewDealType;
						$scope.Settings.NewDealType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsDealInfosInit = function () {
		$scope.ApiCall(
			[
				['action', 'getdealinfos'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.DealInfos = result.ResponseData.DealInfos;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsDealInfoEdit = function (arg) {
		arg.BackupValue = arg.DealInfoName;
		arg.BackupRankValue = arg.DealInfoValueRank;
		arg.IsSelected = true;
	};
	$scope.SettingsDealInfoEditCancel = function (arg) {
		arg.DealInfoName = arg.BackupValue;
		arg.DealInfoValueRank = arg.BackupRankValue;
		arg.IsSelected = false;
	};
	$scope.SettingsDealInfoEditUpdate = function (arg) {
		if (arg.DealInfoName.trim() == '') {
			$scope.ShowError('Label cannot be empty');
		}else if (isNaN(arg.DealInfoValueRank)) {
			$scope.ShowError('Rank must be a numeric value greater than zero');
		} else if (arg.DealInfoValueRank < 1) {
			$scope.ShowError('Rank must be a numeric value greater than zero');
		} else {
			$scope.ApiCall(
				[
					['action', 'updatedealinfo'],
					['dealinfoid', arg.DealInfoID],
					['dealinfoname', arg.DealInfoName],
					['dealinfovaluerank', arg.DealInfoValueRank],
				],
				function (result) {
					if (result.Success) {
						$scope.Settings.DealInfos = result.ResponseData.DealInfos;
						$scope.ShowMessage('Deal info updated')
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	}
	$scope.SettingsDealInfoAdd = function () {
		if ($scope.Settings.NewDealInfo == '') {
			$scope.ShowError('New deal info is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'adddealinfo'],
					['dealinfoname', $scope.Settings.NewDealInfo],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Deal info added')
						$scope.Settings.DealInfos = result.ResponseData.DealInfos;
						$scope.Settings.NewDealInfo = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsMarketingTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getmarketingtypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.MarketingTypes = result.ResponseData.MarketingTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsMarketingTypeEdit = function (arg) {
		arg.BackupValue = arg.MarketingTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsMarketingTypeEditCancel = function (arg) {
		arg.MarketingTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsMarketingTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatemarketingtype'],
				['marketingtypeid', arg.MarketingTypeID],
				['marketingtypename', arg.MarketingTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Marketing type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsMarketingTypeAdd = function () {
		if ($scope.Settings.NewMarketingType == '') {
			$scope.ShowError('New marketing type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addmarketingtype'],
					['marketingtypename', $scope.Settings.NewMarketingType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Marketing type added')
						$scope.Settings.MarketingTypes[$scope.Settings.MarketingTypes.length] = result.ResponseData.NewMarketingType;
						$scope.Settings.NewMarketingType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsSoftwareTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getsoftwaretypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.SoftwareTypes = result.ResponseData.SoftwareTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsSoftwareTypeEdit = function (arg) {
		arg.BackupValue = arg.SoftwareTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsSoftwareTypeEditCancel = function (arg) {
		arg.SoftwareTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsSoftwareTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatesoftwaretype'],
				['softwaretypeid', arg.SoftwareTypeID],
				['softwaretypename', arg.SoftwareTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Software type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsSoftwareTypeAdd = function () {
		if ($scope.Settings.NewSoftwareType == '') {
			$scope.ShowError('New software type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addsoftwaretype'],
					['softwaretypename', $scope.Settings.NewSoftwareType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Software type added')
						$scope.Settings.SoftwareTypes[$scope.Settings.SoftwareTypes.length] = result.ResponseData.NewSoftwareType;
						$scope.Settings.NewSoftwareType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsLicenceTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getlicencetypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.LicenceTypes = result.ResponseData.LicenceTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsLicenceTypeEdit = function (arg) {
		arg.BackupValue = arg.LicenceTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsLicenceTypeEditCancel = function (arg) {
		arg.LicenceTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsLicenceTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatelicencetype'],
				['licencetypeid', arg.LicenceTypeID],
				['licencetypename', arg.LicenceTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Licence type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsLicenceTypeAdd = function () {
		if ($scope.Settings.NewLicenceType == '') {
			$scope.ShowError('New licence type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addlicencetype'],
					['licencetypename', $scope.Settings.NewLicenceType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Licence type added')
						$scope.Settings.LicenceTypes[$scope.Settings.LicenceTypes.length] = result.ResponseData.NewLicenceType;
						$scope.Settings.NewLicenceType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsLanguagesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getlanguages'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.Languages = result.ResponseData.Languages;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsLanguageEdit = function (arg) {
		arg.BackupValue = arg.LanguageName;
		arg.IsSelected = true;
	};
	$scope.SettingsLanguageEditCancel = function (arg) {
		arg.LanguageName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsLanguageEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatelanguage'],
				['languageid', arg.LanguageID],
				['languagename', arg.LanguageName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Language updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsLanguageAdd = function () {
		if ($scope.Settings.NewLanguage == '') {
			$scope.ShowError('New language is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addlanguage'],
					['languagename', $scope.Settings.NewLanguage],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Language added')
						$scope.Settings.Languages[$scope.Settings.Languages.length] = result.ResponseData.NewLanguage;
						$scope.Settings.NewLanguage = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.SettingsPaymentTypesInit = function () {
		$scope.ApiCall(
			[
				['action', 'getpaymenttypes'],
			],
			function (result) {
				if (result.Success) {
					$scope.Settings.PaymentTypes = result.ResponseData.PaymentTypes;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	};
	$scope.SettingsPaymentTypeEdit = function (arg) {
		arg.BackupValue = arg.PaymentTypeName;
		arg.IsSelected = true;
	};
	$scope.SettingsPaymentTypeEditCancel = function (arg) {
		arg.PaymentTypeName = arg.BackupValue;
		arg.IsSelected = false;
	};
	$scope.SettingsPaymentTypeEditUpdate = function (arg) {
		$scope.ApiCall(
			[
				['action', 'updatepaymenttype'],
				['paymenttypeid', arg.PaymentTypeID],
				['paymenttypename', arg.PaymentTypeName],
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('Payment type updated')
					arg.IsSelected = false;
				} else {
					$scope.ShowError(result.Error);
				}
			}
		);
	}
	$scope.SettingsPaymentTypeAdd = function () {
		if ($scope.Settings.NewPaymentType == '') {
			$scope.ShowError('New payment type is empty');
		} else {
			$scope.ApiCall(
				[
					['action', 'addpaymenttype'],
					['paymenttypename', $scope.Settings.NewPaymentType],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Payment type added')
						$scope.Settings.PaymentTypes[$scope.Settings.PaymentTypes.length] = result.ResponseData.NewPaymentType;
						$scope.Settings.NewPaymentType = '';
						arg.IsSelected = false;
					} else {
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	/*END SETTINGS INIT*/


	/*BEGIN USERS*/
	$scope.ChangePassword = function () {
		$scope.ChangePassword = {
			Password: '',
			Password2: '',
			Errors: new ErrorDictionary()
		};
		$scope.ChangePasswordModeOn = true;
	};
	$scope.ChangePasswordCancel = function () {
		$scope.ChangePassword = null;
		$scope.ChangePasswordModeOn = false;
	};
	$scope.ChangePasswordUpdate = function () {
		$scope.ChangePassword.Errors = new ErrorDictionary();
		$scope.ValidatePassword($scope.ChangePassword.Password, $scope.ChangePassword.Errors, 'Password');
		$scope.ValidateCompare($scope.ChangePassword.Password2, $scope.ChangePassword.Password, $scope.ChangePassword.Errors, 'Password2');
		if (!$scope.ChangePassword.Errors.HasErrors()) {
			$scope.ApiCall(
				[
					['action', 'changepassword'],
					['password', $scope.ChangePassword.Password]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Password Changed');
						$scope.ChangePassword = null;
						$scope.ChangePasswordModeOn = false;
					} else {
						$scope.ChangePassword.Errors.Update('Main', result.Error);
					}
				}
			);
		}
	}
	$scope.AddUserInit = function () {
		$scope.ApiCall(
			[
				['action', 'getroles'],
			],
			function (result) {
				if (result.Success) {
					$scope.NewUser = {
						UserName: '',
						Email: '',
						Password: '',
						Password2: '',
						Errors: new ErrorDictionary()
					};
					$scope.NewUserRoles = result.ResponseData.NewUserRoles;
					//$scope.isProcessing = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.AddUserSubmit = function () {
		$scope.ShowMessage(null);
		$scope.NewUser.Errors = new ErrorDictionary();
		$scope.ValidateUsername($scope.NewUser.UserName, $scope.NewUser.Errors, 'UserName');
		$scope.ValidatePassword($scope.NewUser.Password, $scope.NewUser.Errors, 'Password');
		$scope.ValidateCompare($scope.NewUser.Password2, $scope.NewUser.Password, $scope.NewUser.Errors, 'Password2');
		$scope.ValidateEmail($scope.NewUser.Email, $scope.NewUser.Errors, 'Email');
		if (!$scope.NewUser.Errors.HasErrors()) {
			var roles = '';
			for (c = 0; c < $scope.NewUserRoles.length; c++) {
				if ($scope.NewUserRoles[c].UserHasRole) {
					roles += (roles == '' ? '' : '_') + $scope.NewUserRoles[c].RoleName;
				}
			}
			$scope.ApiCall(
				[
					['action', 'newuser'],
					['username', $scope.NewUser.UserName],
					['password', $scope.NewUser.Password],
					['email', $scope.NewUser.Email],
					['roles', roles]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('New User created');
						$scope.NewUser = {
							UserName: '',
							Email: '',
							Password: '',
							Password2: '',
							Errors: new ErrorDictionary()
						};
						//$scope.NewUserRoles = result.ResponseData.NewUserRoles;
					} else {
						$scope.NewUser.Errors.Update('Main', result.Error);
					}
				}
			);
		}
	};
	$scope.UsersInit = function () {
		$scope.UserRolesModeOn = false;
		$scope.UserRoles = null;
		$scope.ApiCall(
			[
				['action', 'getusers'],
			],
			function (result) {
				if (result.Success) {
					$scope.Users = result.ResponseData.Users;
					//result.ResponseData.UsersCount;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.UserActivate = function (userid) {
		$scope.ApiCall(
			[
				['action', 'activateuser'],
				['userid', userid]
			],
			function (result) {
				if (result.Success) {
					$scope.Users = result.ResponseData.Users;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.UserDeactivate = function (userid) {
		$scope.ApiCall(
			[
				['action', 'deactivateuser'],
				['userid', userid]
			],
			function (result) {
				if (result.Success) {
					$scope.Users = result.ResponseData.Users;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.UserUnlock = function (userid) {
		$scope.ApiCall(
			[
				['action', 'unlockuser'],
				['userid', userid]
			],
			function (result) {
				if (result.Success) {
					$scope.Users = result.ResponseData.Users;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.UserShowRoles = function (userid) {
		$scope.ApiCall(
			[
				['action', 'getuserroles'],
				['userid', userid]
			],
			function (result) {
				if (result.Success) {
					$scope.UserRoles = result.ResponseData.UserRoles;
					$scope.UserRolesModeOn = true;
				} else {
					$scope.ShowError('Error: ' + result.Error);
					$scope.UserRolesModeOn = false;
				}
			}
		);
	};
	$scope.UserShowRolesCancel = function () {
		$scope.UserRoles = null;
		$scope.UserRolesModeOn = false;
	};
	$scope.UserShowRolesUpdate = function () {
		var roles = '';
		for (c = 0; c < $scope.UserRoles.Roles.length; c++) {
			if ($scope.UserRoles.Roles[c].UserHasRole) {
				roles += (roles == '' ? '' : '_') + $scope.UserRoles.Roles[c].RoleName;
			}
		}
		$scope.ApiCall(
			[
				['action', 'updateuserroles'],
				['userid', $scope.UserRoles.UserId],
				['roles', roles]
			],
			function (result) {
				if (result.Success) {
					$scope.ShowMessage('User roles updated');
					$scope.UserRoles = null;
					$scope.UserRolesModeOn = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);

	};
	/*END USERS*/


	/*BEGIN REPORT*/
	$scope.ReportCustomInit = function (param) {
		$scope.ApiCall(
			[
				['action', 'getcustomreports'],
			],
			function (result) {
				if (result.Success) {
					$scope.CustomReports = result.ResponseData.CustomReports;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportCustomRemove = function (customreportinfo) {
		if (confirm('Delete this report?')) {
			$scope.ApiCall(
				[
					['action', 'removecustomreport'],
					['customreportid', customreportinfo.CustomReportID],
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage('Custom report deleted');
						$scope.CustomReports = result.ResponseData.CustomReports;
					} else {
						$scope.ShowError('Error: ' + result.Error);
					}
				}
			);
		}
	};
	$scope.CustomReportParamsShow = function (callbackFunc) {
		$scope.CustomReport = {
			CustomReportName: '',
			CurrentUserOnly: false,
			CallbackFunc: callbackFunc,
			Errors: new ErrorDictionary()
		};
		$scope.CustomReportParamsModeOn = true;
	};
	$scope.CustomReportParamsSave = function () {
		$scope.CustomReport.Errors = new ErrorDictionary();
		$scope.ValidateRequired($scope.CustomReport.CustomReportName, $scope.CustomReport.Errors, 'CustomReportName');
		if (!$scope.CustomReport.Errors.HasErrors()) {
			$scope.CustomReportParamsModeOn = false;
			$timeout($scope.CustomReport.CallbackFunc, 1);
		}
	};

	$scope.ReportAffiliatesInit = function (/*[customparams]*/) {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);

		var isCustomReport = arguments.length > 0 && arguments[0] != null;
		var actions = [['action', 'getcountries_getuserslite_getoperatorslite_getdealstatuses_getdealtypes_getaffiliatetypes_getmarketingtypes' + (isCustomReport ? '_getcustomreportinfo' : '')]];
		if (isCustomReport) {
			actions[actions.length] = ['customreportid', arguments[0]];
		}

		$scope.ApiCall(
			actions,
			function (result) {
				if (result.Success) {
					$scope.FilterReportAffiliates = {
						ExpandFilter: true,
						CheckQuerySpeed: true,
						AffiliateID: '',
						SearchStr: '',
						AffiliateManagerFilterOn: false, AffiliateManagerList: result.ResponseData.Users,
						AllUsers: true,
						SizeList: [{ ID: 0, Label: 'Any' }, { ID: 1, Label: 'Small' }, { ID: 2, Label: 'Medium' }, { ID: 3, Label: 'Large' }], SizeSelected: null,
						ContactableList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Yes' }, { ID: 0, Label: 'No' }], ContactableSelected: null,
						MaxMailList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Yes' }, { ID: 0, Label: 'No' }], MaxMailSelected: null,
						AffStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], AffStatusSelected: null,
						IncludeUnassigned: true,
						CreatedDateRange: {
							DateType: $scope.DateFilters[9],
							From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
							To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
						},
						OperatorFilterOn: false, OperatorList: result.ResponseData.Operators, OperatorOptions: result.ResponseData.DealStatuses, OperatorOptionSelected: null,
						CountryFilterOn: false, CountryList: result.ResponseData.Countries, CountryOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], CountryOptionSelected: null,
						AffiliateTypeFilterOn: false, AffiliateTypeList: result.ResponseData.AffiliateTypes, AffiliateTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], AffiliateTypeOptionSelected: null,
						DealTypeFilterOn: false, DealTypeList: result.ResponseData.DealTypes, DealTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], DealTypeOptionSelected: null,
						MarketingTypeFilterOn: false, MarketingTypeList: result.ResponseData.MarketingTypes, MarketingTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], MarketingTypeOptionSelected: null,

						IncAffiliateTypes: false,
						IncDealTypes: false,
						IncMarketingTypes: false,
						IncCountries: false,
						IncOperators: false,

						Pager: CreatePagerObj(),
						ReportHelper: CreateReportHelperObj(),

						Errors: new ErrorDictionary()
					};

					
					var customParams = {};
					if (isCustomReport) {
						var customParamsSplit = result.ResponseData.CustomReportInfo.CustomReportParamsStr.split('^^^');
						for (var c = 0; c < customParamsSplit.length; c++) {
							var customParam = customParamsSplit[c].split('___');
							customParams[customParam[0]] = customParam[1];
						}

						$scope.FilterReportAffiliates.SearchStr = customParams['SearchStr'];
						$scope.FilterReportAffiliates.AllUsers = customParams['AllUsers'] == '1';
						$scope.FilterReportAffiliates.SizeSelected = $scope.FilterReportAffiliates.SizeList[customParams['SizeSelected']];
						$scope.FilterReportAffiliates.ContactableSelected = $scope.FilterReportAffiliates.ContactableList[customParams['ContactableSelected']];
						$scope.FilterReportAffiliates.MaxMailSelected = $scope.FilterReportAffiliates.MaxMailList[customParams['MaxMailSelected']];
						$scope.FilterReportAffiliates.AffStatusSelected = $scope.FilterReportAffiliates.AffStatusList[customParams['AffStatusSelected']];
						$scope.FilterReportAffiliates.IncludeUnassigned = customParams['IncludeUnassigned'] == '1';
						var customParamCreated = customParams['CreatedDateRange'].split('_');
						$scope.FilterReportAffiliates.CreatedDateRange.DateType = $scope.DateFilters[customParamCreated[0]];
						if (customParamCreated.length > 1) {
							$scope.FilterReportAffiliates.CreatedDateRange.From = customParamCreated[1];
							$scope.FilterReportAffiliates.CreatedDateRange.To = customParamCreated[2];
						}

						$scope.FilterReportAffiliates.AffiliateManagerList.forEach(function (affmanObj) {
							if (('_' + customParams['AffiliateManagers'] + '_').indexOf(affmanObj.UserId) != -1) {
								affmanObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.OperatorOptionSelected = $scope.FilterReportAffiliates.OperatorOptions[customParams['OperatorOption']];
						$scope.FilterReportAffiliates.OperatorList.forEach(function (operatorObj) {
							if (('_' + customParams['Operators'] + '_').indexOf(operatorObj.OperatorID) != -1) {
								operatorObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.CountryOptionSelected = $scope.FilterReportAffiliates.CountryOptions[customParams['CountryOption']];
						$scope.FilterReportAffiliates.CountryList.forEach(function (countryObj) {
							if (('_' + customParams['Countries'] + '_').indexOf(countryObj.CountryID) != -1) {
								countryObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.AffiliateTypeOptionSelected = $scope.FilterReportAffiliates.AffiliateTypeOptions[customParams['AffiliateTypeOption']];
						$scope.FilterReportAffiliates.AffiliateTypeList.forEach(function (afftypeObj) {
							if (('_' + customParams['AffiliateTypes'] + '_').indexOf(afftypeObj.AffiliateTypeID) != -1) {
								afftypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.DealTypeOptionSelected = $scope.FilterReportAffiliates.DealTypeOptions[customParams['DealTypeOption']];
						$scope.FilterReportAffiliates.DealTypeList.forEach(function (dealtypeObj) {
							if (('_' + customParams['DealTypes'] + '_').indexOf(dealtypeObj.DealTypeID) != -1) {
								dealtypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.MarketingTypeOptionSelected = $scope.FilterReportAffiliates.MarketingTypeOptions[customParams['MarketingTypeOption']];
						$scope.FilterReportAffiliates.MarketingTypeList.forEach(function (marketingtypeObj) {
							if (('_' + customParams['MarketingTypes'] + '_').indexOf(marketingtypeObj.MarketingTypeID) != -1) {
								marketingtypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportAffiliates.IncAffiliateTypes = customParams['IncAffiliateTypes'] == '1';
						$scope.FilterReportAffiliates.IncDealTypes = customParams['IncDealTypes'] == '1';
						$scope.FilterReportAffiliates.IncMarketingTypes = customParams['IncMarketingTypes'] == '1';
						$scope.FilterReportAffiliates.IncCountries = customParams['IncCountries'] == '1';
						$scope.FilterReportAffiliates.IncOperators = customParams['IncOperators'] == '1';

					} else {
						$scope.FilterReportAffiliates.SizeSelected = $scope.FilterReportAffiliates.SizeList[0];
						$scope.FilterReportAffiliates.ContactableSelected = $scope.FilterReportAffiliates.ContactableList[0];
						$scope.FilterReportAffiliates.MaxMailSelected = $scope.FilterReportAffiliates.MaxMailList[0];
						$scope.FilterReportAffiliates.AffStatusSelected = $scope.FilterReportAffiliates.AffStatusList[1];
						$scope.FilterReportAffiliates.OperatorOptionSelected = $scope.FilterReportAffiliates.OperatorOptions[0];
						$scope.FilterReportAffiliates.CountryOptionSelected = $scope.FilterReportAffiliates.CountryOptions[0];
						$scope.FilterReportAffiliates.AffiliateTypeOptionSelected = $scope.FilterReportAffiliates.AffiliateTypeOptions[0];
						$scope.FilterReportAffiliates.DealTypeOptionSelected = $scope.FilterReportAffiliates.DealTypeOptions[0];
						$scope.FilterReportAffiliates.MarketingTypeOptionSelected = $scope.FilterReportAffiliates.MarketingTypeOptions[0];
					}

					$scope.FilterReportAffiliates.Pager.Info.Count = 100;

					$scope.FilterReportAffiliates.ReportHelper.Reset();
					$scope.FilterReportAffiliates.ReportHelper.ReportTitle = 'Affiliates Report';
					if (isCustomReport) {
						$scope.ReportAffiliatesSubmit($scope.FilterReportAffiliates.Pager.Info.Offset);
					}

				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportAffiliatesSave = function () {
		$scope.CustomReportParamsShow(function () {
			var customReportParams = '';
			
			customReportParams += 'SearchStr___' + $scope.FilterReportAffiliates.SearchStr;
			customReportParams += '^^^AllUsers___' + ($scope.FilterReportAffiliates.AllUsers ? '1' : '0');
			customReportParams += '^^^SizeSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.SizeList, $scope.FilterReportAffiliates.SizeSelected, 'ID'));
			customReportParams += '^^^ContactableSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.ContactableList, $scope.FilterReportAffiliates.ContactableSelected, 'ID'));
			customReportParams += '^^^MaxMailSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.MaxMailList, $scope.FilterReportAffiliates.MaxMailSelected, 'ID'));
			customReportParams += '^^^AffStatusSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.AffStatusList, $scope.FilterReportAffiliates.AffStatusSelected, 'ID'));
			customReportParams += '^^^IncludeUnassigned___' + ($scope.FilterReportAffiliates.IncludeUnassigned ? '1' : '0');
			customReportParams += '^^^CreatedDateRange___' + ($scope.GetDropDownSelectedIndex($scope.DateFilters, $scope.FilterReportAffiliates.CreatedDateRange.DateType, 'ID') + '_' + $scope.FilterReportAffiliates.CreatedDateRange.From + '_' + $scope.FilterReportAffiliates.CreatedDateRange.To)

			var affmans = '';
			$scope.FilterReportAffiliates.AffiliateManagerList.forEach(function (affmanObj) {
				if (affmanObj.IsSelected) {
					affmans += (affmans != '' ? '_' : '') + affmanObj.UserId;
				}
			});
			customReportParams += '^^^AffiliateManagers___' + affmans;
				
			customReportParams += '^^^OperatorOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.OperatorOptions, $scope.FilterReportAffiliates.OperatorOptionSelected, 'DealStatusID'));
			var operators = '';
			$scope.FilterReportAffiliates.OperatorList.forEach(function (operatorObj) {
				if (operatorObj.IsSelected) {
					operators += (operators != '' ? '_' : '') + operatorObj.OperatorID;
				}
			});
			customReportParams += '^^^Operators___' + operators;

			customReportParams += '^^^CountryOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.CountryOptions, $scope.FilterReportAffiliates.CountryOptionSelected, 'ID'));
			var countries = '';
			$scope.FilterReportAffiliates.CountryList.forEach(function (countryObj) {
				if (countryObj.IsSelected) {
					countries += (countries != '' ? '_' : '') + countryObj.CountryID;
				}
			});
			customReportParams += '^^^Countries___' + countries;

			customReportParams += '^^^AffiliateTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.AffiliateTypeOptions, $scope.FilterReportAffiliates.AffiliateTypeOptionSelected, 'ID'));
			var afftypes = '';
			$scope.FilterReportAffiliates.AffiliateTypeList.forEach(function (afftypeObj) {
				if (afftypeObj.IsSelected) {
					afftypes += (afftypes != '' ? '_' : '') + afftypeObj.AffiliateTypeID;
				}
			});
			customReportParams += '^^^AffiliateTypes___' + afftypes;

			customReportParams += '^^^DealTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.DealTypeOptions, $scope.FilterReportAffiliates.DealTypeOptionSelected, 'ID'));
			var dealtypes = '';
			$scope.FilterReportAffiliates.DealTypeList.forEach(function (dealtypeObj) {
				if (dealtypeObj.IsSelected) {
					dealtypes += (dealtypes != '' ? '_' : '') + dealtypeObj.DealTypeID;
				}
			});
			customReportParams += '^^^DealTypes___' + dealtypes;

			customReportParams += '^^^MarketingTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportAffiliates.MarketingTypeOptions, $scope.FilterReportAffiliates.MarketingTypeOptionSelected, 'ID'));
			var marketingtypes = '';
			$scope.FilterReportAffiliates.MarketingTypeList.forEach(function (marketingtypeObj) {
				if (marketingtypeObj.IsSelected) {
					marketingtypes += (marketingtypes != '' ? '_' : '') + marketingtypeObj.MarketingTypeID;
				}
			});
			customReportParams += '^^^MarketingTypes___' + marketingtypes;


			customReportParams += '^^^IncAffiliateTypes___' + ($scope.FilterReportAffiliates.IncAffiliateTypes ? '1' : '0');
			customReportParams += '^^^IncDealTypes___' + ($scope.FilterReportAffiliates.IncDealTypes ? '1' : '0');
			customReportParams += '^^^IncMarketingTypes___' + ($scope.FilterReportAffiliates.IncMarketingTypes ? '1' : '0');
			customReportParams += '^^^IncCountries___' + ($scope.FilterReportAffiliates.IncCountries ? '1' : '0');
			customReportParams += '^^^IncOperators___' + ($scope.FilterReportAffiliates.IncOperators ? '1' : '0');


			$scope.ApiCall(
				[
					['action', 'newcustomreport'],
					['customreportname', $scope.CustomReport.CustomReportName],
					['customreporttype', 'report-affiliates'],
					['customreportparams', customReportParams],
					['currentuseronly', $scope.CustomReport.CurrentUserOnly]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage(
							'Custom Report Saved',
							'popup',
							function () {
								$scope.ReportAffiliatesInit(result.ResponseData.NewCustomReportId);
							}
						);
					
					} else {
						$scope.ShowError('Error: ' + result.Error);
					}
				}
			);

		});

	};
	$scope.ReportAffiliatesSubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterReportAffiliates.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;

		var affmanager_ids = '';
		if (!$scope.IsUserInRole('RestrictedReports')) {
			affmanager_ids = '';
		} else {
			for (var c = 0; c < $scope.FilterReportAffiliates.AffiliateManagerList.length; c++) {
				if ($scope.FilterReportAffiliates.AffiliateManagerList[c].IsSelected) {
					affmanager_ids += (affmanager_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.AffiliateManagerList[c].UserId;
				}
			}
		}

		var operator_ids = '';
		for (var c = 0; c < $scope.FilterReportAffiliates.OperatorList.length; c++) {
			if ($scope.FilterReportAffiliates.OperatorList[c].IsSelected) {
				operator_ids += (operator_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.OperatorList[c].OperatorID;
			}
		}

		var country_ids = '';
		for (var c = 0; c < $scope.FilterReportAffiliates.CountryList.length; c++) {
			if ($scope.FilterReportAffiliates.CountryList[c].IsSelected) {
				country_ids += (country_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.CountryList[c].CountryID;
			}
		}
		var affiliatetype_ids = '';
		for (var c = 0; c < $scope.FilterReportAffiliates.AffiliateTypeList.length; c++) {
			if ($scope.FilterReportAffiliates.AffiliateTypeList[c].IsSelected) {
				affiliatetype_ids += (affiliatetype_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.AffiliateTypeList[c].AffiliateTypeID;
			}
		}
		var dealtype_ids = '';
		for (var c = 0; c < $scope.FilterReportAffiliates.DealTypeList.length; c++) {
			if ($scope.FilterReportAffiliates.DealTypeList[c].IsSelected) {
				dealtype_ids += (dealtype_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.DealTypeList[c].DealTypeID;
			}
		}
		var marketingtype_ids = '';
		for (var c = 0; c < $scope.FilterReportAffiliates.MarketingTypeList.length; c++) {
			if ($scope.FilterReportAffiliates.MarketingTypeList[c].IsSelected) {
				marketingtype_ids += (marketingtype_ids != '' ? ',' : '') + $scope.FilterReportAffiliates.MarketingTypeList[c].MarketingTypeID;
			}
		}

		var createdDates = $scope.GetFilterDate($scope.FilterReportAffiliates.CreatedDateRange.DateType.Label, $scope.FilterReportAffiliates.CreatedDateRange.From, $scope.FilterReportAffiliates.CreatedDateRange.To);

		$scope.ApiCall(
			[
				['action', 'getreportaffiliates'],
				['affiliateid', $scope.FilterReportAffiliates.AffiliateID],
				['searchstr', $scope.FilterReportAffiliates.SearchStr],
				['affmanagerids', affmanager_ids],
				['allusers', $scope.FilterReportAffiliates.AllUsers],
				['affsizetype', $scope.FilterReportAffiliates.SizeSelected.ID],
				['contactabletype', $scope.FilterReportAffiliates.ContactableSelected.ID],
				['maxmailtype', $scope.FilterReportAffiliates.MaxMailSelected.ID],
				['affstatustype', $scope.FilterReportAffiliates.AffStatusSelected.ID],
				['includeunassigned', $scope.FilterReportAffiliates.IncludeUnassigned],
				['operatortypeoption', $scope.FilterReportAffiliates.OperatorOptionSelected.DealStatusID],
				['operatorids', operator_ids],
				['countrytypeoption', $scope.FilterReportAffiliates.CountryOptionSelected.ID],
				['countryids', country_ids],
				['affiliatetypeoption', $scope.FilterReportAffiliates.AffiliateTypeOptionSelected.ID],
				['affiliatetypeids', affiliatetype_ids],
				['dealtypeoption', $scope.FilterReportAffiliates.DealTypeOptionSelected.ID],
				['dealtypeids', dealtype_ids],
				['marketingtypeoption', $scope.FilterReportAffiliates.MarketingTypeOptionSelected.ID],
				['marketingtypeids', marketingtype_ids],

				['createdfrom', createdDates.From],
				['createdto', createdDates.To],

				['incaffiliatetypes', $scope.FilterReportAffiliates.IncAffiliateTypes],
				['incdealtypes', $scope.FilterReportAffiliates.IncDealTypes],
				['incmarketingtypes', $scope.FilterReportAffiliates.IncMarketingTypes],
				['inccountries', $scope.FilterReportAffiliates.IncCountries],
				['incoperators', $scope.FilterReportAffiliates.IncOperators],

				['checkqueryspeed', $scope.FilterReportAffiliates.CheckQuerySpeed],
				//['offset', $scope.FilterReportAffiliates.Pager.Info.Offset],
				//['count', $scope.FilterReportAffiliates.Pager.Info.Count]
			],
			function (result) {
				$scope.FilterReportAffiliates.CheckQuerySpeed = true;
				if (result.Success) {

					$scope.FilterReportAffiliates.Pager.SetPagerInfo(result.ResponseData.AffiliatesReportData.Rows.length, null);
					$scope.FilterReportAffiliates.ReportHelper.RenderReport(result.ResponseData.AffiliatesReportData);
					$scope.FilterReportAffiliates.ExpandFilter = false;
					$scope.isProcessing = false;

				} else if (result.ErrorCode == 9999999) {
					if (confirm(result.Error + '\r\n\r\nDo you wish to proceed?')) {
						$scope.FilterReportAffiliates.CheckQuerySpeed = false;
						$scope.ReportAffiliatesSubmit();
					}
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);

	};

	$scope.ReportOperatorsInit = function () {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);

		var isCustomReport = arguments.length > 0 && arguments[0] != null;
		var actions = [['action', 'getcountries_getlanguages_getdealtypes_getaffiliatetypes_getsoftwaretypes_getlicencetypes_getpaymenttypes' + (isCustomReport ? '_getcustomreportinfo' : '')]];
		if (isCustomReport) {
			actions[actions.length] = ['customreportid', arguments[0]];
		}

		$scope.ApiCall(
			actions,
			function (result) {
				if (result.Success) {
					$scope.FilterReportOperators = {
						ExpandFilter: true,
						CheckQuerySpeed: true,
						StatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], StatusSelected: null,

						CountryFilterOn: false, CountryList: result.ResponseData.Countries, CountryOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], CountryOptionSelected: null,
						LanguageFilterOn: false, LanguageList: result.ResponseData.Languages, LanguageOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], LanguageOptionSelected: null,
						AffiliateTypeFilterOn: false, AffiliateTypeList: result.ResponseData.AffiliateTypes, AffiliateTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], AffiliateTypeOptionSelected: null,
						DealTypeFilterOn: false, DealTypeList: result.ResponseData.DealTypes, DealTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], DealTypeOptionSelected: null,
						SoftwareTypeFilterOn: false, SoftwareTypeList: result.ResponseData.SoftwareTypes, SoftwareTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], SoftwareTypeOptionSelected: null,
						LicenceTypeFilterOn: false, LicenceTypeList: result.ResponseData.LicenceTypes, LicenceTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], LicenceTypeOptionSelected: null,
						PaymentTypeFilterOn: false, PaymentTypeList: result.ResponseData.PaymentTypes, PaymentTypeOptions: [{ ID: 'INCLUDE', Label: 'Promotes' }, { ID: 'EXCLUDE', Label: 'Does not promote' }], PaymentTypeOptionSelected: null,

						CreatedDateRange: {
							DateType: $scope.DateFilters[9],
							From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
							To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
						},

						IncBrands: false,
						IncSignupUrls: false,

						IncCountries: false,
						IncLanguages: false,
						IncAffiliateTypes: false,
						IncDealTypes: false,
						IncSoftwareTypes: false,
						IncLicenceTypes: false,
						IncPaymentTypes: false,
						IncSecondaryContactDetails: false,

						Pager: CreatePagerObj(),
						ReportHelper: CreateReportHelperObj(),

						Errors: new ErrorDictionary()
					};

					var customParams = {};
					if (isCustomReport) {
						var customParamsSplit = result.ResponseData.CustomReportInfo.CustomReportParamsStr.split('^^^');
						for (var c = 0; c < customParamsSplit.length; c++) {
							var customParam = customParamsSplit[c].split('___');
							customParams[customParam[0]] = customParam[1];
						}

						$scope.FilterReportOperators.StatusSelected = $scope.FilterReportOperators.StatusList[customParams['StatusSelected']];
						var customParamCreated = customParams['CreatedDateRange'].split('_');
						$scope.FilterReportOperators.CreatedDateRange.DateType = $scope.DateFilters[customParamCreated[0]];
						if (customParamCreated.length > 1) {
							$scope.FilterReportOperators.CreatedDateRange.From = customParamCreated[1];
							$scope.FilterReportOperators.CreatedDateRange.To = customParamCreated[2];
						}

						$scope.FilterReportOperators.CountryOptionSelected = $scope.FilterReportOperators.CountryOptions[customParams['CountryOption']];
						$scope.FilterReportOperators.CountryList.forEach(function (countryObj) {
							if (('_' + customParams['Countries'] + '_').indexOf(countryObj.CountryID) != -1) {
								countryObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.LanguageOptionSelected = $scope.FilterReportOperators.LanguageOptions[customParams['LanguageOption']];
						$scope.FilterReportOperators.LanguageList.forEach(function (languageObj) {
							if (('_' + customParams['Languages'] + '_').indexOf(languageObj.LanguageID) != -1) {
								languageObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.AffiliateTypeOptionSelected = $scope.FilterReportOperators.AffiliateTypeOptions[customParams['AffiliateTypeOption']];
						$scope.FilterReportOperators.AffiliateTypeList.forEach(function (afftypeObj) {
							if (('_' + customParams['AffiliateTypes'] + '_').indexOf(afftypeObj.AffiliateTypeID) != -1) {
								afftypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.DealTypeOptionSelected = $scope.FilterReportOperators.DealTypeOptions[customParams['DealTypeOption']];
						$scope.FilterReportOperators.DealTypeList.forEach(function (dealtypeObj) {
							if (('_' + customParams['DealTypes'] + '_').indexOf(dealtypeObj.DealTypeID) != -1) {
								dealtypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.SoftwareTypeOptionSelected = $scope.FilterReportOperators.SoftwareTypeOptions[customParams['SoftwareTypeOption']];
						$scope.FilterReportOperators.SoftwareTypeList.forEach(function (softwaretypeObj) {
							if (('_' + customParams['SoftwareTypes'] + '_').indexOf(softwaretypeObj.SoftwareTypeID) != -1) {
								softwaretypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.LicenceTypeOptionSelected = $scope.FilterReportOperators.LicenceTypeOptions[customParams['LicenceTypeOption']];
						$scope.FilterReportOperators.LicenceTypeList.forEach(function (licencetypeObj) {
							if (('_' + customParams['LicenceTypes'] + '_').indexOf(licencetypeObj.LicenceTypeID) != -1) {
								licencetypeObj.IsSelected = true;
							}
						});

						$scope.FilterReportOperators.PaymentTypeOptionSelected = $scope.FilterReportOperators.PaymentTypeOptions[customParams['PaymentTypeOption']];
						$scope.FilterReportOperators.PaymentTypeList.forEach(function (paymenttypeObj) {
							if (('_' + customParams['PaymentTypes'] + '_').indexOf(paymenttypeObj.PaymentTypeID) != -1) {
								paymenttypeObj.IsSelected = true;
							}
						});



						$scope.FilterReportOperators.IncBrands = customParams['IncBrands'] == '1';
						$scope.FilterReportOperators.IncSignupUrls = customParams['IncSignupUrls'] == '1';
						$scope.FilterReportOperators.IncCountries = customParams['IncCountries'] == '1';
						$scope.FilterReportOperators.IncLanguages = customParams['IncLanguages'] == '1';
						$scope.FilterReportOperators.IncAffiliateTypes = customParams['IncAffiliateTypes'] == '1';
						$scope.FilterReportOperators.IncDealTypes = customParams['IncDealTypes'] == '1';
						$scope.FilterReportOperators.IncSoftwareTypes = customParams['IncSoftwareTypes'] == '1';
						$scope.FilterReportOperators.IncLicenceTypes = customParams['IncLicenceTypes'] == '1';
						$scope.FilterReportOperators.IncPaymentTypes = customParams['IncPaymentTypes'] == '1';
						$scope.FilterReportOperators.IncSecondaryContactDetails = customParams['IncSecondaryContactDetails'] == '1';
						
					}
					else {
						$scope.FilterReportOperators.StatusSelected = $scope.FilterReportOperators.StatusList[1];

						$scope.FilterReportOperators.CountryOptionSelected = $scope.FilterReportOperators.CountryOptions[0];
						$scope.FilterReportOperators.LanguageOptionSelected = $scope.FilterReportOperators.LanguageOptions[0];
						$scope.FilterReportOperators.AffiliateTypeOptionSelected = $scope.FilterReportOperators.AffiliateTypeOptions[0];
						$scope.FilterReportOperators.DealTypeOptionSelected = $scope.FilterReportOperators.DealTypeOptions[0];

						$scope.FilterReportOperators.SoftwareTypeOptionSelected = $scope.FilterReportOperators.SoftwareTypeOptions[0];
						$scope.FilterReportOperators.LicenceTypeOptionSelected = $scope.FilterReportOperators.LicenceTypeOptions[0];
						$scope.FilterReportOperators.PaymentTypeOptionSelected = $scope.FilterReportOperators.PaymentTypeOptions[0];
					}

					$scope.FilterReportOperators.Pager.Info.Count = 100;

					$scope.FilterReportOperators.ReportHelper.Reset();
					$scope.FilterReportOperators.ReportHelper.ReportTitle = 'Operators Report';
					if (isCustomReport) {
						$scope.ReportOperatorsSubmit($scope.FilterReportOperators.Pager.Info.Offset);
					}

				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportOperatorsSave = function () {
		$scope.CustomReportParamsShow(function () {
			var customReportParams = '';

			customReportParams += 'StatusSelected___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.StatusList, $scope.FilterReportOperators.StatusSelected, 'ID'));
			customReportParams += '^^^CreatedDateRange___' + ($scope.GetDropDownSelectedIndex($scope.DateFilters, $scope.FilterReportOperators.CreatedDateRange.DateType, 'ID') + '_' + $scope.FilterReportOperators.CreatedDateRange.From + '_' + $scope.FilterReportOperators.CreatedDateRange.To)

			customReportParams += '^^^CountryOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.CountryOptions, $scope.FilterReportOperators.CountryOptionSelected, 'ID'));
			var countries = '';
			$scope.FilterReportOperators.CountryList.forEach(function (countryObj) {
				if (countryObj.IsSelected) {
					countries += (countries != '' ? '_' : '') + countryObj.CountryID;
				}
			});
			customReportParams += '^^^Countries___' + countries;

			customReportParams += '^^^LanguageOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.LanguageOptions, $scope.FilterReportOperators.LanguageOptionSelected, 'ID'));
			var languages = '';
			$scope.FilterReportOperators.LanguageList.forEach(function (languageObj) {
				if (languageObj.IsSelected) {
					languages += (languages != '' ? '_' : '') + languageObj.LanguageID;
				}
			});
			customReportParams += '^^^Languages___' + languages;

			customReportParams += '^^^AffiliateTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.AffiliateTypeOptions, $scope.FilterReportOperators.AffiliateTypeOptionSelected, 'ID'));
			var afftypes = '';
			$scope.FilterReportOperators.AffiliateTypeList.forEach(function (afftypeObj) {
				if (afftypeObj.IsSelected) {
					afftypes += (afftypes != '' ? '_' : '') + afftypeObj.AffiliateTypeID;
				}
			});
			customReportParams += '^^^AffiliateTypes___' + afftypes;

			customReportParams += '^^^DealTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.DealTypeOptions, $scope.FilterReportOperators.DealTypeOptionSelected, 'ID'));
			var dealtypes = '';
			$scope.FilterReportOperators.DealTypeList.forEach(function (dealtypeObj) {
				if (dealtypeObj.IsSelected) {
					dealtypes += (dealtypes != '' ? '_' : '') + dealtypeObj.DealTypeID;
				}
			});
			customReportParams += '^^^DealTypes___' + dealtypes;

			customReportParams += '^^^SoftwareTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.SoftwareTypeOptions, $scope.FilterReportOperators.SoftwareTypeOptionSelected, 'ID'));
			var softwaretypes = '';
			$scope.FilterReportOperators.SoftwareTypeList.forEach(function (softwaretypeObj) {
				if (softwaretypeObj.IsSelected) {
					softwaretypes += (softwaretypes != '' ? '_' : '') + softwaretypeObj.SoftwareTypeID;
				}
			});
			customReportParams += '^^^SoftwareTypes___' + softwaretypes;

			customReportParams += '^^^LicenceTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.LicenceTypeOptions, $scope.FilterReportOperators.LicenceTypeOptionSelected, 'ID'));
			var licencetypes = '';
			$scope.FilterReportOperators.LicenceTypeList.forEach(function (licencetypeObj) {
				if (licencetypeObj.IsSelected) {
					licencetypes += (licencetypes != '' ? '_' : '') + licencetypeObj.LicenceTypeID;
				}
			});
			customReportParams += '^^^LicenceTypes___' + licencetypes;

			customReportParams += '^^^PaymentTypeOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportOperators.PaymentTypeOptions, $scope.FilterReportOperators.PaymentTypeOptionSelected, 'ID'));
			var paymenttypes = '';
			$scope.FilterReportOperators.PaymentTypeList.forEach(function (paymenttypeObj) {
				if (paymenttypeObj.IsSelected) {
					paymenttypes += (paymenttypes != '' ? '_' : '') + paymenttypeObj.PaymentTypeID;
				}
			});
			customReportParams += '^^^PaymentTypes___' + paymenttypes;


			customReportParams += '^^^IncBrands___' + ($scope.FilterReportOperators.IncBrands ? '1' : '0');
			customReportParams += '^^^IncSignupUrls___' + ($scope.FilterReportOperators.IncSignupUrls ? '1' : '0');
			customReportParams += '^^^IncCountries___' + ($scope.FilterReportOperators.IncCountries ? '1' : '0');
			customReportParams += '^^^IncLanguages___' + ($scope.FilterReportOperators.IncLanguages ? '1' : '0');
			customReportParams += '^^^IncAffiliateTypes___' + ($scope.FilterReportOperators.IncAffiliateTypes ? '1' : '0');
			customReportParams += '^^^IncDealTypes___' + ($scope.FilterReportOperators.IncDealTypes ? '1' : '0');
			customReportParams += '^^^IncSoftwareTypes___' + ($scope.FilterReportOperators.IncSoftwareTypes ? '1' : '0');
			customReportParams += '^^^IncLicenceTypes___' + ($scope.FilterReportOperators.IncLicenceTypes ? '1' : '0');
			customReportParams += '^^^IncPaymentTypes___' + ($scope.FilterReportOperators.IncPaymentTypes ? '1' : '0');
			customReportParams += '^^^IncSecondaryContactDetails___' + ($scope.FilterReportOperators.IncSecondaryContactDetails  ? '1' : '0');

			$scope.ApiCall(
				[
					['action', 'newcustomreport'],
					['customreportname', $scope.CustomReport.CustomReportName],
					['customreporttype', 'report-operators'],
					['customreportparams', customReportParams],
					['currentuseronly', $scope.CustomReport.CurrentUserOnly]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage(
							'Custom Report Saved',
							'popup',
							function () {
								$scope.ReportOperatorsInit(result.ResponseData.NewCustomReportId);
							}
						);

					} else {
						$scope.ShowError('Error: ' + result.Error);
					}
				}
			);

		});
	};
	$scope.ReportOperatorsSubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterReportOperators.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;

		var country_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.CountryList.length; c++) {
			if ($scope.FilterReportOperators.CountryList[c].IsSelected) {
				country_ids += (country_ids != '' ? ',' : '') + $scope.FilterReportOperators.CountryList[c].CountryID;
			}
		}
		var language_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.LanguageList.length; c++) {
			if ($scope.FilterReportOperators.LanguageList[c].IsSelected) {
				language_ids += (language_ids != '' ? ',' : '') + $scope.FilterReportOperators.LanguageList[c].LanguageID;
			}
		}
		var affiliatetype_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.AffiliateTypeList.length; c++) {
			if ($scope.FilterReportOperators.AffiliateTypeList[c].IsSelected) {
				affiliatetype_ids += (affiliatetype_ids != '' ? ',' : '') + $scope.FilterReportOperators.AffiliateTypeList[c].AffiliateTypeID;
			}
		}
		var dealtype_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.DealTypeList.length; c++) {
			if ($scope.FilterReportOperators.DealTypeList[c].IsSelected) {
				dealtype_ids += (dealtype_ids != '' ? ',' : '') + $scope.FilterReportOperators.DealTypeList[c].DealTypeID;
			}
		}
		var softwaretype_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.SoftwareTypeList.length; c++) {
			if ($scope.FilterReportOperators.SoftwareTypeList[c].IsSelected) {
				softwaretype_ids += (softwaretype_ids != '' ? ',' : '') + $scope.FilterReportOperators.SoftwareTypeList[c].SoftwareTypeID;
			}
		}
		var licencetype_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.LicenceTypeList.length; c++) {
			if ($scope.FilterReportOperators.LicenceTypeList[c].IsSelected) {
				licencetype_ids += (licencetype_ids != '' ? ',' : '') + $scope.FilterReportOperators.LicenceTypeList[c].LicenceTypeID;
			}
		}
		var paymenttype_ids = '';
		for (var c = 0; c < $scope.FilterReportOperators.PaymentTypeList.length; c++) {
			if ($scope.FilterReportOperators.PaymentTypeList[c].IsSelected) {
				paymenttype_ids += (paymenttype_ids != '' ? ',' : '') + $scope.FilterReportOperators.PaymentTypeList[c].PaymentTypeID;
			}
		}
		var createdDates = $scope.GetFilterDate($scope.FilterReportOperators.CreatedDateRange.DateType.Label, $scope.FilterReportOperators.CreatedDateRange.From, $scope.FilterReportOperators.CreatedDateRange.To);

		$scope.ApiCall(
			[
				['action', 'getreportoperators'],
				['statustype', $scope.FilterReportOperators.StatusSelected.ID],
				['createdfrom', createdDates.From],
				['createdto', createdDates.To],
				['countrytypeoption', $scope.FilterReportOperators.CountryOptionSelected.ID],
				['countryids', country_ids],
				['affiliatetypeoption', $scope.FilterReportOperators.AffiliateTypeOptionSelected.ID],
				['affiliatetypeids', affiliatetype_ids],
				['dealtypeoption', $scope.FilterReportOperators.DealTypeOptionSelected.ID],
				['dealtypeids', dealtype_ids],
				['softwaretypeoption', $scope.FilterReportOperators.SoftwareTypeOptionSelected.ID],
				['softwaretypeids', softwaretype_ids],
				['licencetypeoption', $scope.FilterReportOperators.LicenceTypeOptionSelected.ID],
				['licencetypeids', licencetype_ids],
				['paymenttypeoption', $scope.FilterReportOperators.PaymentTypeOptionSelected.ID],
				['paymenttypeids', paymenttype_ids],
				['incbrands', $scope.FilterReportOperators.IncBrands],
				['incsignupurls', $scope.FilterReportOperators.IncSignupUrls],
				['inccountries', $scope.FilterReportOperators.IncCountries],
				['inclanguages', $scope.FilterReportOperators.IncLanguages],
				['incaffiliatetypes', $scope.FilterReportOperators.IncAffiliateTypes],
				['incdealtypes', $scope.FilterReportOperators.IncDealTypes],
				['incsoftwaretypes', $scope.FilterReportOperators.IncSoftwareTypes],
				['inclicencetypes', $scope.FilterReportOperators.IncLicenceTypes],
				['incpaymenttypes', $scope.FilterReportOperators.IncPaymentTypes],
				['incsecondarycontactdetails', $scope.FilterReportOperators.IncSecondaryContactDetails],

				['checkqueryspeed', $scope.FilterReportOperators.CheckQuerySpeed],
			],
			function (result) {
				$scope.FilterReportOperators.CheckQuerySpeed = true;
				if (result.Success) {

					$scope.FilterReportOperators.Pager.SetPagerInfo(result.ResponseData.OperatorsReportData.Rows.length, null);
					$scope.FilterReportOperators.ReportHelper.RenderReport(result.ResponseData.OperatorsReportData);
					$scope.FilterReportOperators.ExpandFilter = false;
					$scope.isProcessing = false;

				} else if (result.ErrorCode == 9999999) {
					if (confirm(result.Error + '\r\n\r\nDo you wish to proceed?')) {
						$scope.FilterReportOperators.CheckQuerySpeed = false;
						$scope.ReportOperatorsSubmit();
					}
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);

	};

	$scope.ReportDealsInit = function (/*[customreportid (or params)]*/) {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);

		var isCustomReport = arguments.length > 0 && arguments[0] != null && !isNaN(arguments[0]);
		var drillDownReportParams = !isCustomReport && arguments.length > 0 && arguments[0] != null ? arguments[0] : null;


		var actions = [['action', 'getuserslite_getdealstatuses_getdealinfos' + (isCustomReport ? '_getcustomreportinfo' : '')]];
		if (isCustomReport) {
			actions[actions.length] = ['customreportid', arguments[0]];
		}
		$scope.ApiCall(
			actions,
			function (result) {
				if (result.Success) {
					$scope.FilterReportDeals = {
						ExpandFilter: true,
						CheckQuerySpeed: true,

						DealActiveStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], DealActiveStatusSelected: null,
						IncludeUnassigned: true,
						CreatedDateRange: {
							DateType: $scope.DateFilters[9],
							From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
							To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
						},
						DateSetDateRange: {
							DateType: $scope.DateFilters[8],
							From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
							To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
						},

						AffiliateManagerFilterOn: false, AffiliateManagerList: result.ResponseData.Users, AffiliateManagerOptions: [{ ID: 'ASSIGNED', Label: 'Assigned To' }, { ID: 'CREATED', Label: 'Created By' }], AffiliateManagerOptionSelected: null,
						AllUsers: true,
						DealStatusFilterOn: false, DealStatusList: result.ResponseData.DealStatuses,
						DealInfoFilterOn: false, DealInfoList: result.ResponseData.DealInfos,

						Pager: CreatePagerObj(),
						ReportHelper: CreateReportHelperObj(),

						Errors: new ErrorDictionary()
					};

					var customParams = {};
					if (isCustomReport || drillDownReportParams != null) {
						var customParamsSplit = isCustomReport ? result.ResponseData.CustomReportInfo.CustomReportParamsStr.split('^^^') : drillDownReportParams.split('^^^');
						for (var c = 0; c < customParamsSplit.length; c++) {
							var customParam = customParamsSplit[c].split('___');
							customParams[customParam[0]] = customParam[1];
						}

						$scope.FilterReportDeals.AllUsers = customParams['AllUsers'] == '1';
						$scope.FilterReportDeals.DealActiveStatusSelected = $scope.FilterReportDeals.DealActiveStatusList[customParams['DealActiveStatus']];
						$scope.FilterReportDeals.IncludeUnassigned = customParams['IncludeUnassigned'] == '1';

						var customParamCreated = customParams['CreatedDateRange'].split('_');
						$scope.FilterReportDeals.CreatedDateRange.DateType = $scope.DateFilters[customParamCreated[0]];
						if (customParamCreated.length > 1) {
							$scope.FilterReportDeals.CreatedDateRange.From = customParamCreated[1];
							$scope.FilterReportDeals.CreatedDateRange.To = customParamCreated[2];
						}
						var customParamDateSet = customParams['DateSetDateRange'].split('_');
						$scope.FilterReportDeals.DateSetDateRange.DateType = $scope.DateFilters[customParamDateSet[0]];
						if (customParamDateSet.length > 1) {
							$scope.FilterReportDeals.DateSetDateRange.From = customParamDateSet[1];
							$scope.FilterReportDeals.DateSetDateRange.To = customParamDateSet[2];
						}

						$scope.FilterReportDeals.AffiliateManagerOptionSelected = $scope.FilterReportDeals.AffiliateManagerOptions[customParams['AffiliateManagerOption']];


						$scope.FilterReportDeals.AffiliateManagerList.forEach(function (affmanObj) {
							if (('_' + customParams['AffiliateManagers'] + '_').indexOf(affmanObj.UserId) != -1) {
								affmanObj.IsSelected = true;
							}
						});

						$scope.FilterReportDeals.DealStatusList.forEach(function (dealstatusObj) {
							if (('_' + customParams['DealStatuses'] + '_').indexOf(dealstatusObj.DealStatusID) != -1) {
								dealstatusObj.IsSelected = true;
							}
						});

						$scope.FilterReportDeals.DealInfoList.forEach(function (dealinfoObj) {
							if (('_' + customParams['DealInfos'] + '_').indexOf(dealinfoObj.DealInfoID) != -1) {
								dealinfoObj.IsSelected = true;
							}
						});

					}
					else {
						$scope.FilterReportDeals.DealActiveStatusSelected = $scope.FilterReportDeals.DealActiveStatusList[1];
						$scope.FilterReportDeals.AffiliateManagerOptionSelected = $scope.FilterReportDeals.AffiliateManagerOptions[0];
					}

					$scope.FilterReportDeals.Pager.Info.Count = 100;

					$scope.FilterReportDeals.ReportHelper.Reset();
					$scope.FilterReportDeals.ReportHelper.ReportTitle = 'Deals Report';
					if (isCustomReport || drillDownReportParams != null) {
						$scope.ReportDealsSubmit($scope.FilterReportDeals.Pager.Info.Offset);
					}
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportDealsSave = function () {
		$scope.CustomReportParamsShow(function () {
			var customReportParams = '';

			customReportParams += 'AllUsers___' + ($scope.FilterReportDeals.AllUsers ? '1' : '0');
			customReportParams += '^^^DealActiveStatus___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportDeals.DealActiveStatusList, $scope.FilterReportDeals.DealActiveStatusSelected, 'ID'));
			customReportParams += '^^^IncludeUnassigned___' + ($scope.FilterReportDeals.IncludeUnassigned ? '1' : '0');
			customReportParams += '^^^CreatedDateRange___' + ($scope.GetDropDownSelectedIndex($scope.DateFilters, $scope.FilterReportDeals.CreatedDateRange.DateType, 'ID') + '_' + $scope.FilterReportDeals.CreatedDateRange.From + '_' + $scope.FilterReportDeals.CreatedDateRange.To)
			customReportParams += '^^^DateSetDateRange___' + ($scope.GetDropDownSelectedIndex($scope.DateFilters, $scope.FilterReportDeals.DateSetDateRange.DateType, 'ID') + '_' + $scope.FilterReportDeals.DateSetDateRange.From + '_' + $scope.FilterReportDeals.DateSetDateRange.To)

			customReportParams += '^^^AffiliateManagerOption___' + ($scope.GetDropDownSelectedIndex($scope.FilterReportDeals.AffiliateManagerOptions, $scope.FilterReportDeals.AffiliateManagerOptionSelected, 'UserId'));
			var affmans = '';
			$scope.FilterReportDeals.AffiliateManagerList.forEach(function (affmanObj) {
				if (affmanObj.IsSelected) {
					affmans += (affmans != '' ? '_' : '') + affmanObj.UserId;
				}
			});
			customReportParams += '^^^AffiliateManagers___' + affmans;

			var dealstatuses = '';
			$scope.FilterReportDeals.DealStatusList.forEach(function (dealstatusObj) {
				if (dealstatusObj.IsSelected) {
					dealstatuses += (dealstatuses != '' ? '_' : '') + dealstatusObj.DealStatusID;
				}
			});
			customReportParams += '^^^DealStatuses___' + dealstatuses;

			var dealinfos = '';
			$scope.FilterReportDeals.DealInfoList.forEach(function (dealinfoObj) {
				if (dealinfoObj.IsSelected) {
					dealinfos += (dealinfos != '' ? '_' : '') + dealinfoObj.DealInfoID;
				}
			});
			customReportParams += '^^^DealInfos___' + dealinfos;
		
			$scope.ApiCall(
				[
					['action', 'newcustomreport'],
					['customreportname', $scope.CustomReport.CustomReportName],
					['customreporttype', 'report-deals'],
					['customreportparams', customReportParams],
					['currentuseronly', $scope.CustomReport.CurrentUserOnly]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage(
							'Custom Report Saved',
							'popup',
							function () {
								$scope.ReportDealsInit(result.ResponseData.NewCustomReportId);
							}
						);

					} else {
						$scope.ShowError('Error: ' + result.Error);
					}
				}
			);

		});
	};
	$scope.ReportDealsSubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterReportDeals.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;

		var affmanager_ids = '';
		for (var c = 0; c < $scope.FilterReportDeals.AffiliateManagerList.length; c++) {
			if ($scope.FilterReportDeals.AffiliateManagerList[c].IsSelected) {
				affmanager_ids += (affmanager_ids != '' ? ',' : '') + $scope.FilterReportDeals.AffiliateManagerList[c].UserId;
			}
		}
		var dealinfo_ids = '';
		for (var c = 0; c < $scope.FilterReportDeals.DealInfoList.length; c++) {
			if ($scope.FilterReportDeals.DealInfoList[c].IsSelected) {
				dealinfo_ids += (dealinfo_ids != '' ? ',' : '') + $scope.FilterReportDeals.DealInfoList[c].DealInfoID;
			}
		}
		var dealstatus_ids = '';
		for (var c = 0; c < $scope.FilterReportDeals.DealStatusList.length; c++) {
			if ($scope.FilterReportDeals.DealStatusList[c].IsSelected) {
				dealstatus_ids += (dealstatus_ids != '' ? ',' : '') + $scope.FilterReportDeals.DealStatusList[c].DealStatusID;
			}
		}

		var createdDates = $scope.GetFilterDate($scope.FilterReportDeals.CreatedDateRange.DateType.Label, $scope.FilterReportDeals.CreatedDateRange.From, $scope.FilterReportDeals.CreatedDateRange.To);
		var datesetDates = $scope.GetFilterDate($scope.FilterReportDeals.DateSetDateRange.DateType.Label, $scope.FilterReportDeals.DateSetDateRange.From, $scope.FilterReportDeals.DateSetDateRange.To);

		$scope.ApiCall(
			[
				['action', 'getreportdeals'],
				
				['allusers', $scope.FilterReportDeals.AllUsers],
				['dealactivestatustype', $scope.FilterReportDeals.DealActiveStatusSelected.ID],
				['includeunassigned', $scope.FilterReportDeals.IncludeUnassigned],

				['createdfrom', createdDates.From],
				['createdto', createdDates.To],
				['datesetfrom', datesetDates.From],
				['datesetto', datesetDates.To],

				['affmanageroption', $scope.FilterReportDeals.AffiliateManagerOptionSelected.ID],
				['affmanagerids', affmanager_ids],

				['dealstatusids', dealstatus_ids],
				['dealinfoids', dealinfo_ids],

				['checkqueryspeed', $scope.FilterReportDeals.CheckQuerySpeed],
				//['offset', $scope.FilterReportAffiliates.Pager.Info.Offset],
				//['count', $scope.FilterReportAffiliates.Pager.Info.Count]
			],
			function (result) {
				$scope.FilterReportDeals.CheckQuerySpeed = true;
				if (result.Success) {

					$scope.FilterReportDeals.Pager.SetPagerInfo(result.ResponseData.DealsReportData.Rows.length, null);
					$scope.FilterReportDeals.ReportHelper.RenderReport(result.ResponseData.DealsReportData);
					$scope.FilterReportDeals.ExpandFilter = false;
					$scope.isProcessing = false;

				} else if (result.ErrorCode == 9999999) {
					if (confirm(result.Error + '\r\n\r\nDo you wish to proceed?')) {
						$scope.FilterReportDeals.CheckQuerySpeed = false;
						$scope.ReportDealsSubmit();
					}
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);

	};




	$scope.ReportUserDealsSummaryInit = function () {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);
		$scope.ApiCall(
			[['action', 'getdealstatuses']],
			function (result) {
				if (result.Success) {
					if ($scope.FilterReportUserDealsSummary == undefined) {
						$scope.FilterReportUserDealsSummary = {
							ExpandFilter: true,
							IsLoaded: false,
							DealStatusList: result.ResponseData.DealStatuses,
							DealActiveStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], DealActiveStatusSelected: null,
							DateSetDateRange: {
								DateType: $scope.DateFilters[9],
								From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
								To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
							},
							Pager: CreatePagerObj(),
							Errors: new ErrorDictionary()
						};
						$scope.FilterReportUserDealsSummary.DealActiveStatusSelected = $scope.FilterReportUserDealsSummary.DealActiveStatusList[1];
						var preSelectedStatuses = [3, 6];
						for (var c = 0; c < $scope.FilterReportUserDealsSummary.DealStatusList.length; c++) {
							$scope.FilterReportUserDealsSummary.DealStatusList[c].IsSelected = preSelectedStatuses.indexOf($scope.FilterReportUserDealsSummary.DealStatusList[c].DealStatusID) != -1;
						}
					}
					$scope.FilterReportUserDealsSummary.Pager.Info.Count = 100;
					$scope.ReportUserDealsSummarySubmit();
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportUserDealsSummarySubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterReportUserDealsSummary.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var datesetDates = $scope.GetFilterDate($scope.FilterReportUserDealsSummary.DateSetDateRange.DateType.Label, $scope.FilterReportUserDealsSummary.DateSetDateRange.From, $scope.FilterReportUserDealsSummary.DateSetDateRange.To);
		$scope.ApiCall(
			[
				['action', 'getuserdealssummary'],
				['dealactivestatustype', $scope.FilterReportUserDealsSummary.DealActiveStatusSelected.ID],
				['datesetfrom', datesetDates.From],
				['datesetto', datesetDates.To],
			],
			function (result) {
				if (result.Success) {
					$scope.ReportUserDealsSummary = result.ResponseData.UserDealsSummary;
					$scope.FilterReportUserDealsSummary.IsLoaded = true;
					$scope.FilterReportUserDealsSummary.Pager.SetPagerInfo(result.ResponseData.UserDealsSummary.Rows.length, null);
					$scope.FilterReportUserDealsSummary.ExpandFilter = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportUserDealsDrilldown = function (userid, dealstatusid) {

		var dealstatusIds = dealstatusid;
		if (dealstatusIds == null) {
			dealstatusIds = '';
			for (var c = 0; c < $scope.FilterReportUserDealsSummary.DealStatusList.length; c++) {
				if ($scope.FilterReportUserDealsSummary.DealStatusList[c].IsSelected) {
					dealstatusIds += (dealstatusIds != '' ? '_' : '') + $scope.FilterReportUserDealsSummary.DealStatusList[c].DealStatusID;
				}
			}
		}


		var drilldownDealReportParams =
		'AllUsers___0^^^DealActiveStatus___' + $scope.FilterReportUserDealsSummary.DealActiveStatusSelected.ID + '^^^IncludeUnassigned___1^^^CreatedDateRange___8_2015-09-01_2016-09-30^^^'
		+ 'DateSetDateRange___' + $scope.FilterReportUserDealsSummary.DateSetDateRange.DateType.ID + '_'
		+ $scope.FilterReportUserDealsSummary.DateSetDateRange.From
		+ '_'
		+ $scope.FilterReportUserDealsSummary.DateSetDateRange.To	+ '^^^AffiliateManagerOption___0^^^AffiliateManagers___' + userid + '^^^DealStatuses___' +		dealstatusIds + '^^^DealInfos___';

		$scope.RenderSection('report-deals', drilldownDealReportParams, true);
	};


	$scope.ReportOperatorDealsSummaryInit = function () {
		var now = new Date();
		var to = new Date(now.getFullYear(), now.getMonth() + 1, 0);
		var from = new Date(to.getFullYear() - 1, to.getMonth(), 1);
		$scope.ApiCall(
			[['action', 'getdealstatuses']],
			function (result) {
				if (result.Success) {
					if ($scope.FilterReportOperatorDealsSummary == undefined) {
						$scope.FilterReportOperatorDealsSummary = {
							ExpandFilter: true,
							IsLoaded:false,
							DealStatusList: result.ResponseData.DealStatuses,
							DealActiveStatusList: [{ ID: 2, Label: 'Any' }, { ID: 1, Label: 'Active' }, { ID: 0, Label: 'Inactive' }], DealActiveStatusSelected: null,
							DateSetDateRange: {
								DateType: $scope.DateFilters[9],
								From: $scope.FormatJsDate(from, 'YYYY-MM-DD'),
								To: $scope.FormatJsDate(to, 'YYYY-MM-DD'),
							},
							Pager: CreatePagerObj(),
							Errors: new ErrorDictionary()
						};
						$scope.FilterReportOperatorDealsSummary.DealActiveStatusSelected = $scope.FilterReportOperatorDealsSummary.DealActiveStatusList[1];
						var preSelectedStatuses = [3, 6];
						for (var c = 0; c < $scope.FilterReportOperatorDealsSummary.DealStatusList.length; c++) {
							$scope.FilterReportOperatorDealsSummary.DealStatusList[c].IsSelected = preSelectedStatuses.indexOf($scope.FilterReportOperatorDealsSummary.DealStatusList[c].DealStatusID) != -1;
						}
					}
					$scope.FilterReportOperatorDealsSummary.Pager.Info.Count = 100;
					$scope.ReportOperatorDealsSummarySubmit();
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.ReportOperatorDealsSummarySubmit = function (/*[offset]*/) {
		$scope.ShowMessage(null);
		$scope.FilterReportOperatorDealsSummary.Pager.Info.Offset = arguments.length > 0 ? arguments[0] : 0;
		var datesetDates = $scope.GetFilterDate($scope.FilterReportOperatorDealsSummary.DateSetDateRange.DateType.Label, $scope.FilterReportOperatorDealsSummary.DateSetDateRange.From, $scope.FilterReportOperatorDealsSummary.DateSetDateRange.To);
		$scope.ApiCall(
			[
				['action', 'getoperatordealssummary'],
				['dealactivestatustype', $scope.FilterReportOperatorDealsSummary.DealActiveStatusSelected.ID],
				['datesetfrom', datesetDates.From],
				['datesetto', datesetDates.To],
			],
			function (result) {
				if (result.Success) {
					$scope.ReportOperatorDealsSummary = result.ResponseData.OperatorDealsSummary;
					$scope.FilterReportOperatorDealsSummary.IsLoaded = true;
					$scope.FilterReportOperatorDealsSummary.Pager.SetPagerInfo(result.ResponseData.OperatorDealsSummary.Rows.length, null);
					$scope.FilterReportOperatorDealsSummary.ExpandFilter = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);

	};


	/*END REPORT*/


	/*BEGIN INIT*/
	$scope.init = function () {
		$scope.ApiCall(
			[
				['action', 'getcurrentuserroles'],
			],
			function (result) {
				if (result.Success) {
					$scope.CurrentRoles = result.ResponseData.UserRoles;
					$scope.RenderSection(initialsection, initialparam);
					$scope.isInitialising = false;
				} else {
					$scope.ShowError('Error: ' + result.Error);
				}
			}
		);
	};
	$scope.init();
	/*END INIT*/






});
