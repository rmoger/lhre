﻿var app = angular.module('wahganon', [])
.config(["$httpProvider", function ($httpProvider) {
	$httpProvider.defaults.transformResponse.push(function (responseData) {
		convertDateStringsToDates(responseData);
		return responseData;
	});
}])
.directive("formError", function () {
	return {
		restrict: "E",
		scope: {
			error: "=error"
		},
		replace: true,
		template: "<span class='error' ng-show='error != null && error != \"\"'><span ng-bind=\"error\"></span></span>"
	}
})
.controller("AnonController", function ($scope, $http, $sce) {
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

	$scope.CurrentSection = 'login';
	$scope.Lang = 'en';
	$scope.isProcessing = false;

	$scope.ApiCall = function (params, callback) {
		$scope.isProcessing = true;
		var postData = '';
		for (c = 0; c < params.length; c++) {
			if (postData != '') {
				postData += '***';
			}
			postData += params[c][0] + '|' + encodeURIComponent(params[c][1]);
		}
		if (!debugmode) {
			postData = Base64.encode(postData);
		}
		postData = 'data=' + postData;
		$http.post('/auth', postData)
			.success(function (result, status, headers, config) {
				$scope.isProcessing = false;
				if (!debugmode) {
					result = JSON.parse(Base64.decode(result));
				}
				if (result.Success) {
					callback(result);
				} else {
					if (result.ErrorCode == 1) {
						document.location.href = '/login?sessiontimeout=1';
					} else {
						callback(result);
					}
				}
			})
			.error(function (data, status, headers, config) {
				$scope.isProcessing = false;
				callback({
					Success: false,
					Error: status,
					ErrorCode: 500,
					ResponseData: null
				});
			})
		;
	};
	$scope.ShowMessage = function (message) {
		$scope.RenderMessage('messages', message);
	};
	$scope.ShowError = function (error) {
		$scope.RenderMessage('errors', error);
	};
	$scope.ShowWarning = function(warning) {
		$scope.RenderMessage('warnings', warning);
	};
	$scope.RenderMessage = function (cssclass, message) {
		if (message == null) {
			$scope.Message = null;
		} else {
			$scope.Message = { cssclass: cssclass, text: message };
		}
	};
	$scope.GetTranslation = function (arg) {
		return $('#t_' + arg).val();
	};



	/*BEGIN VALIDATION*/
	$scope.ValidateRequired = function (value, errorobj, errorkey) {
		if (value && value != '') {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!';
		}
	};
	$scope.ValidateCommaNos = function (value, errorobj, errorkey) {
		delete errorobj[errorkey];
		if (value && value != '') {
			var valsArr = value.trim().replace(/(\r\n|\n|\r)/gm, "").split(',');
			for (c = 0; c < valsArr.length; c++) {
				if (isNaN(valsArr[c].trim())) {
					errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid Value';
				}
			}
		}
	};
	$scope.ValidateUsername = function (value, errorobj, errorkey) {
		var patt = new RegExp(/^([a-zA-Z]{1})([a-zA-Z0-9]{5,15})$/);
		if (patt.test(value)) {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid username!';
		}
	};
	$scope.ValidatePassword = function (value, errorobj, errorkey) {
		var patt = new RegExp(/^([a-zA-Z]{1})([a-zA-Z0-9!]{5,15})$/);
		if (patt.test(value)) {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid password!';
		}
	};
	$scope.ValidateCompare = function (value1, value2, errorobj, errorkey) {
		if (value1 && value2 && value1 == value2) {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 4 ? $scope.GetTranslation(arguments[4]) : 'Fields do not match!';
		}
	};
	$scope.ValidateEmail = function (value, errorobj, errorkey) {
		var patt = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/);
		if (patt.test(value)) {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid email!';
		}
	};
	$scope.ValidateCheckbox = function (value, errorobj, errorkey) {
		if (value && value == true) {
			delete errorobj[errorkey];
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!';
		}
	};
	$scope.ValidateNumber = function (value, errorobj, errorkey) {
		delete errorobj[errorkey];
		if (value && value != '') {
			if (isNaN(value)) {
				errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid number!';
			}
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!';
		}
	};
	$scope.ValidateNumberGt = function (value, value2, errorobj, errorkey) {
		delete errorobj[errorkey];
		if (value && value != '') {
			if (isNaN(value)) {
				errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Invalid number!';
			} else {
				if (parseFloat(value) <= parseFloat(value2)) {
					errorobj[errorkey] = 'Value is too low!';
				}
			}
		} else {
			errorobj[errorkey] = arguments.length > 3 ? $scope.GetTranslation(arguments[3]) : 'Required!';
		}
	};
	/*END VALIDATION*/


	$scope.RenderSection = function (section) {
		$scope.CurrentSection = section;
		if (history.state == null || !(history.state.section == section)) {
			history.pushState({ section: section}, 'WAHG', '/' + section + '/' + $scope.Lang);
		}
		switch (section) {
			case 'login':
				$scope.ResetLoginObj();
				if (sessiontimedout) {
					$scope.ShowWarning($scope.GetTranslation('ERROR_SessionTimeout'));
					sessiontimedout = false;
				}
				break;
			case 'forgottenpw':
				$scope.ResetResetPasswordObj();
				break;
			case 'register':
				if ($scope.Countries.length == 0 || $scope.Languages.length == 0) {
					$scope.ApiCall(
						[
							['action', 'getcountries_getlanguages']
						],
						function (result) {
							if (result.Success) {
								$scope.Countries = result.ResponseData.Countries;
								$scope.Languages = result.ResponseData.Languages;
								$scope.ResetRegisterObj();
							} else {
								$scope.ShowError(result.Error);
							}
						}
					);
				} else {
					$scope.ResetRegisterObj();
				}
				break;
		}
	};

	$scope.Countries = [];
	$scope.Languages = [];

	$scope.LoginObj = {};
	$scope.ResetLoginObj = function(){
		$scope.LoginObj = {
			Username: '',
			Password: '',
			Errors: {}
		};
	}
	$scope.loginKeyPress = function (e) {
		if (e.keyCode == 13) {
			$scope.Login();
		}
	}
	$scope.Login = function () {
		$scope.LoginObj.Errors = {};
		$scope.ValidateUsername($scope.LoginObj.Username, $scope.LoginObj.Errors, 'Username', 'ERROR_InvalidUserName');
		$scope.ValidatePassword($scope.LoginObj.Password, $scope.LoginObj.Errors, 'Password', 'ERROR_InvalidPassword');
		if (Object.keys($scope.LoginObj.Errors).length == 0) {
			$scope.ApiCall(
				[
					['action', 'login'],
					['username', $scope.LoginObj.Username],
					['password', $scope.LoginObj.Password],
					['lang', $scope.Lang]
				],
				function (result) {
					if (result.Success) {
						document.location.href = '/' + $scope.Lang + '/home/';
					} else {
						$scope.LoginObj.Errors.Main = result.Error;
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.ResetPasswordObj = {};
	$scope.ResetResetPasswordObj = function () {
		$scope.ResetPasswordObj = {
			Email: '',
			Errors: {}
		};
	}
	$scope.ResetPassword = function () {
		$scope.ResetPasswordObj.Errors = {};
		$scope.ValidateEmail($scope.ResetPasswordObj.Email, $scope.ResetPasswordObj.Errors, 'Email', 'ERROR_InvalidEmail');
		if (Object.keys($scope.ResetPasswordObj.Errors).length == 0) {
			$scope.ApiCall(
				[
					['action', 'resetpw'],
					['email', $scope.ResetPasswordObj.Email]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage($scope.GetTranslation('MESSAGE_NewPasswordSent'));
					} else {
						$scope.ResetPasswordObj.Errors.Main = result.Error;
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.RegisterObj = {};
	$scope.ResetRegisterObj = function(){
		$scope.RegisterObj = {
			Username: '',
			Password: '',
			Password2: '',
			Email: '',
			Country: $scope.Countries[0],
			Language: $scope.Languages[0],
			Forename: '',
			Surname: '',
			Urls: '',
			Company: '',
			Address: '',
			City: '',
			County: '',
			Postcode: '',
			PhoneHome: '',
			PhoneMobile: '',
			PhoneOffice: '',
			Fax: '',
			Skype: '',
			Msn: '',
			AcceptTsAndCs: '',
			Errors: {}
		};
	}
	$scope.Register = function () {
		$scope.RegisterObj.Errors = {};
		$scope.ValidateUsername($scope.RegisterObj.Username, $scope.RegisterObj.Errors, 'Username', 'ERROR_InvalidUserName');
		$scope.ValidatePassword($scope.RegisterObj.Password, $scope.RegisterObj.Errors, 'Password', 'ERROR_InvalidPassword');
		$scope.ValidateCompare($scope.RegisterObj.Password, $scope.RegisterObj.Errors, 'Password2', 'ERROR_PasswordsMatch');
		$scope.ValidateEmail($scope.RegisterObj.Email, $scope.RegisterObj.Errors, 'Email', 'ERROR_InvalidEmail');
		$scope.ValidateRequired($scope.RegisterObj.Forename, $scope.RegisterObj.Errors, 'Forename', 'ERROR_Required');
		$scope.ValidateRequired($scope.RegisterObj.Surname, $scope.RegisterObj.Errors, 'Surname', 'ERROR_Required');
		$scope.ValidateCheckbox($scope.RegisterObj.AcceptTsAndCs, $scope.RegisterObj.Errors, 'AcceptTsAndCs', 'ERROR_Required');
		if (Object.keys($scope.RegisterObj.Errors).length == 0) {
			$scope.ApiCall(
				[
					['action', 'register'],
					['username', $scope.RegisterObj.Username],
					['password', $scope.RegisterObj.Password],
					['email', $scope.RegisterObj.Email],
					['countryid', $scope.RegisterObj.Country.ID],
					['languageid', $scope.RegisterObj.Language.ID],
					['forename', $scope.RegisterObj.Forename],
					['surname', $scope.RegisterObj.Surname],
					['urls', $scope.RegisterObj.Urls],
					['company', $scope.RegisterObj.Company],
					['address', $scope.RegisterObj.Address],
					['city', $scope.RegisterObj.City],
					['county', $scope.RegisterObj.County],
					['postcode', $scope.RegisterObj.Postcode],
					['phonehome', $scope.RegisterObj.PhoneHome],
					['phonemobile', $scope.RegisterObj.PhoneMobile],
					['phoneoffice', $scope.RegisterObj.PhoneOffice],
					['fax', $scope.RegisterObj.Fax],
					['skype', $scope.RegisterObj.Skype],
					['msn', $scope.RegisterObj.Msn]
				],
				function (result) {
					if (result.Success) {
						$scope.ShowMessage($scope.GetTranslation('MESSAGE_RegisterSuccess'));
						$scope.ResetRegisterObj();
					} else {
						$scope.RegisterObj.Errors.Main = result.Error;
						$scope.ShowError(result.Error);
					}
				}
			);
		}
	};

	$scope.Lang = lang;
	$scope.RenderSection(initialsection);
})
;


$(function () {
	$(window).bind("popstate", function (data) {
		if (data.target.history.state != null) {
			if (data.target.history.state.section != null) {
				var scope = angular.element('#DocBody').scope();
				scope.$apply(function () {
					scope.RenderSection(data.target.history.state.section);
				});
			}
		}
	});
});
function convertDateStringsToDates(input) {
	if (typeof input != "object") return input;
	for (var key in input) {
		if (!input.hasOwnProperty(key)) continue;
		var value = input[key];
		var match;
		if (typeof value == "string" && (match = value.match(/^(\d{4})-(\d{2})-(\d{2})$/))) {
			var dateParts = value.split('-');
			input[key] = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
		} else if (typeof value == "object") {
			convertDateStringsToDates(value);
		}
	}
}
