﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="IhreDB2.Anon" Codebehind="Anon.aspx.cs" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Ihre Consulting Database</title>
	<link type="text/css" rel="stylesheet" href="/styles/main.css" />
</head>
<body>
	<form id="form1" runat="server">
		<div class="header">
			<div class="logo"></div>
		</div>
		<div class="main">



			<asp:Login ID="Login1" runat="server" RenderOuterTable="false" OnLoggedIn="Login1_LoggedIn">
				<LayoutTemplate>
					<div class="component login-control" id="panel_login">
						<h3>Log In</h3>
						<div>
							<dl>
								<dt>
									<label for="UserName">User Name:</label>
								</dt>
								<dd>
									<asp:TextBox ID="UserName" runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
								</dd>
							</dl>
							<dl>
								<dt>
									<label for="Password">Password:</label>
								</dt>
								<dd>
									<asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
									<asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
								</dd>
							</dl>
							<dl>
								<dt></dt>
								<dd>
									<asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
								</dd>
							</dl>
							<div style="color:Red;text-align:center;">
								<asp:Literal ID="FailureText" runat="server" EnableViewState="False" />
							</div>
							<dl>
								<dt></dt>
								<dd>
									<asp:Button ID="LoginButton" runat="server" CssClass="btn" CommandName="Login" Text="Log In" ValidationGroup="Login1" />
								</dd>
							</dl>
							<p style="text-align:center;">
								<asp:LinkButton ID="ForgottenPasswordButton" runat="server" Text="Forgotten Password?" CssClass="text-btn" onclick="ForgottenPasswordButton_Click" />
							</p>
						</div>
					</div>
				</LayoutTemplate>
			</asp:Login>

			
			<asp:PasswordRecovery ID="PasswordRecovery1" runat="server" RenderOuterTable="false" OnSendingMail="PasswordRecovery1_SendingMail" Visible="false">
				<UserNameTemplate>
					<div class="component login-control">
						<h3>Forgot your Password?</h3>
						<div>
							<p>Enter your User Name to receive your password.</p>
							<dl>
								<dt>
									<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
								</dt>
								<dd>
									<asp:TextBox ID="UserName" runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="PasswordRecovery1">*</asp:RequiredFieldValidator>
								</dd>
							</dl>
							<div style="color:Red;text-align:center;">
								<asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
							</div>
							<dl>
								<dt></dt>
								<dd>
									<asp:Button ID="SubmitButton" runat="server" CssClass="btn" CommandName="Submit" Text="Submit" ValidationGroup="PasswordRecovery1" />
									<asp:Button ID="CancelForgottenPasswordButton" runat="server" CssClass="btn" Text="Cancel" onclick="CancelForgottenPasswordButton_Click" />
								</dd>
							</dl>
						</div>
					</div>
				</UserNameTemplate>
				<SuccessTemplate>
					<div class="component login-control">
						<h3>Success</h3>
						<div>
							<p>Your password has been sent to you.</p>
							<dl>
								<dt></dt>
								<dd>
									<asp:Button ID="ForgottenPasswordContinueButton" runat="server" CssClass="btn" Text="Login" onclick="ForgottenPasswordContinueButton_Click" />
								</dd>
							</dl>
						</div>
					</div>
				</SuccessTemplate>

			</asp:PasswordRecovery>


		</div>
		<div class="footer"><p>&copy; <%= DateTime.Now.Year.ToString() %> - Ihre Consulting AB</p></div>
	</form>
</body>
</html>
